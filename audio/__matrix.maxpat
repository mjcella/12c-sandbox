{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 35.0, 79.0, 805.0, 683.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_slidermatrix.maxpat",
					"numinlets" : 0,
					"numoutlets" : 32,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 399.415619, 1384.799927, 1339.917725, 467.200073 ],
					"presentation" : 1,
					"presentation_rect" : [ 78.666664, 107.666733, 1339.917725, 481.86673 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1850.400024, 1616.799927, 75.0, 22.0 ],
					"style" : "",
					"text" : "random 255"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1793.600098, 1676.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "pack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1845.600098, 1516.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "- 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "bang" ],
					"patching_rect" : [ 1845.600098, 1553.600098, 40.0, 22.0 ],
					"style" : "",
					"text" : "t b i b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1793.600098, 1616.799927, 29.5, 22.0 ],
					"style" : "",
					"text" : "i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 1804.733154, 1540.0, 30.0, 22.0 ],
					"style" : "",
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1800.0, 1420.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "-1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "int" ],
					"patching_rect" : [ 1821.600098, 1468.800049, 43.0, 22.0 ],
					"style" : "",
					"text" : "uzi 32"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "int" ],
					"patching_rect" : [ 1776.0, 1384.799927, 43.0, 22.0 ],
					"style" : "",
					"text" : "uzi 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1776.0, 1350.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 264.0, 1657.666626, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1648.0, 128.333313, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[20]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-174",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 194.666672, 1657.666626, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1578.666626, 128.333313, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[21]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 121.333344, 1657.666626, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1505.333374, 128.333313, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[19]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-171",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 52.0, 1657.666626, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1436.0, 128.333313, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 76.0, 1832.0, 45.0, 45.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 9,
					"outlettype" : [ "int", "int", "float", "float", "float", "", "int", "float", "" ],
					"patching_rect" : [ -151.0, 1034.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "transport"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ -784.333313, 1448.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ -654.333313, 1444.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ -498.333313, 1444.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-164",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ -298.000031, 1444.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ -119.333374, 1444.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1473.0, 686.666687, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1268.0, 686.666687, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1063.0, 686.666687, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1519.0, 1072.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1427.0, 1072.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1337.333252, 1072.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1246.666626, 1072.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1153.666626, 1072.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1068.0, 1072.000122, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 932.0, 881.333374, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-148",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 791.5, 881.333374, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 621.149902, 881.333374, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 420.75, 881.333374, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 216.833313, 881.333374, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 13.0, 881.333374, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1756.266724, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1717.666748, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1677.866821, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1639.266846, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1601.066772, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1562.466797, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1522.666748, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1484.066772, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1444.266846, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1405.666992, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-284",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1364.266724, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-285",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1325.666748, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-282",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1285.866821, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-283",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1247.266846, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-280",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1209.066772, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-281",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1170.466797, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-278",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1130.666748, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-279",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1092.066772, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-276",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1052.266846, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-277",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1013.666931, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-274",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 975.466797, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-275",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 936.866821, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-272",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 897.066772, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-273",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 858.466797, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-270",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 819.466797, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-271",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 780.866882, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-268",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 741.066833, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 702.466919, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 664.266846, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 625.666931, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 585.866821, 2011.133301, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-261",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 963.666687, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-262",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 832.095276, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-263",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 696.714294, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-264",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 561.333374, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 422.142883, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 286.761902, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-258",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 151.380966, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-257",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 15.999994, 25.0, 63.0, 22.0 ],
					"style" : "",
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-252",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 546.266907, 1899.799927, 69.0, 22.0 ],
					"style" : "",
					"text" : "matrix_row"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 332.666687, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 247.333344, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 165.333344, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 80.000008, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -7.333338, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-232",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -92.666664, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -174.666672, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-229",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -260.0, 824.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "send~ delta1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-224",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1788.333374, 942.666687, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ reverbR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-223",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1063.0, 946.666687, 88.0, 22.0 ],
					"style" : "",
					"text" : "send~ reverbL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1625.966675, 1317.0, 89.0, 22.0 ],
					"style" : "",
					"text" : "send~ stutterR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1083.666626, 1317.0, 87.0, 22.0 ],
					"style" : "",
					"text" : "send~ stutterL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -206.333298, 1714.666626, 98.0, 22.0 ],
					"style" : "",
					"text" : "send~ vid_audR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -781.0, 1714.666626, 96.0, 22.0 ],
					"style" : "",
					"text" : "send~ vid_audL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 945.0, 1317.0, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythm6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 841.714172, 1317.0, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythm5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 733.261841, 1317.0, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythm4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 588.904724, 1317.0, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythm3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 446.357086, 1317.0, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythm2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 302.904724, 1317.0, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythm1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-196",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 159.452362, 1317.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythmR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 16.0, 1317.0, 90.0, 22.0 ],
					"style" : "",
					"text" : "send~ rhythmL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ -119.333374, 1405.266724, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -119.333374, 1488.933472, 105.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0. 240."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ -298.000031, 1405.266724, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -298.000031, 1488.933472, 123.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -127. 127."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ -498.333313, 1410.600098, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -498.333313, 1494.266846, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0.01 10."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ -654.333313, 1410.600098, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -654.333313, 1494.266846, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0.01 10."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -781.0, 1494.266846, 105.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0. 127."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ -781.0, 1410.600098, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1519.0, 1036.999878, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1427.0, 1036.999878, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1337.333252, 1036.999878, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1246.666626, 1036.999878, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1153.666626, 1036.999878, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1061.666626, 1036.999878, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1045.333374, 300.0, 45.0, 45.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"linecount" : 15,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1155.0, 410.333313, 150.0, 208.0 ],
					"style" : "",
					"text" : "signal outs:\nrhythm [8]\nvid_aud [2]\nstutter [2]\nreverb [2]\n+ 8 from delta\n22\n\nnumber of inputs\ndelta [8]\nreverb [5]\nstutter[8] \nrhythm[6]\nvid_aud[5]\n32"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-102",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_vid_aud.maxpat",
					"numinlets" : 5,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ -781.0, 1535.600098, 702.666687, 152.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 409.666595, 1289.666748, 702.666687, 152.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 932.0, 846.333313, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 791.5, 846.333313, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 621.149902, 846.333313, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 420.75, 846.333313, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 216.833313, 846.333313, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 932.0, 914.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 791.5, 914.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 621.149902, 914.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 420.75, 914.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 216.833313, 914.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 13.0, 914.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 13.0, 846.333313, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1473.0, 646.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1268.0, 646.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1063.0, 646.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1473.0, 725.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1268.0, 725.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1063.0, 725.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -70. 6."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-81",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_reverb.maxpat",
					"numinlets" : 7,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1063.0, 757.0, 839.0, 174.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 981.333374, 591.533447, 839.0, 174.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-80",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_stutter.maxpat",
					"numinlets" : 10,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1099.666626, 1114.333374, 601.0, 181.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 993.0, 767.533447, 601.0, 181.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-79",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_rhythm.maxpat",
					"numinlets" : 6,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 16.0, 953.0, 1023.166565, 342.333344 ],
					"presentation" : 1,
					"presentation_rect" : [ 993.0, 950.533447, 1023.166565, 342.333344 ],
					"varname" : "_rhythm",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 498.999969, 765.5, 150.0, 74.0 ],
					"style" : "",
					"text" : "needs to be a mapping between each cell and a *~. i can organize these into rows, but yeah it's a 1-to-1 mapping"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 963.666687, -5.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 828.285767, -5.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 692.904785, -5.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 557.523804, -5.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 422.142883, -5.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 286.761902, -5.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 151.380966, -5.0, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 15.999994, -5.666668, 83.0, 22.0 ],
					"style" : "",
					"text" : "snapshot~ 33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 8,
					"numoutlets" : 8,
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 188.0, 97.0, 878.0, 555.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-92",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 285.799988, 180.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-91",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 244.799988, 154.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-90",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 207.399994, 125.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 169.399994, 91.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 151.399994, 180.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 115.399994, 154.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 79.699982, 125.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 96.0, 105.0, 22.0 ],
									"style" : "",
									"text" : "scale 0. 127. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-24",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 288.799988, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 250.600006, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-22",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 218.600006, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-21",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 184.399994, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-4",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 151.399994, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 117.199982, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 84.200012, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 285.799988, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 251.599991, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 218.600006, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 184.399994, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 151.399994, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 117.199982, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 84.200012, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 50.0, 220.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-37",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-38",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 84.200012, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-39",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 117.199982, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-40",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 151.399994, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-41",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 184.399994, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-42",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 218.600006, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-43",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 251.599991, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-44",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 285.799988, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-92", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-92", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 16.000006, 777.266663, 156.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p delta_signals"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "float", "float", "float", "float", "float", "float", "float", "float" ],
					"patching_rect" : [ 16.000006, 740.0, 156.0, 22.0 ],
					"style" : "",
					"text" : "unpack 0. 0. 0. 0. 0. 0. 0. 0."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-3",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_delta.maxpat",
					"numinlets" : 8,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float", "float", "float", "float", "float", "float", "float", "float" ],
					"patching_rect" : [ 15.999994, 60.666698, 966.666687, 667.666687 ],
					"presentation" : 1,
					"presentation_rect" : [ 78.666664, 591.533447, 966.666687, 815.666687 ],
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 31 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 30 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 29 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 28 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 27 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 26 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 25 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 24 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 23 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 22 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-265", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-269", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-270", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-276", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-277", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-279", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-281", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-282", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-284", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-285", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-133", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-159", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-171", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-171", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-172", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-173", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-174", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-257", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-258", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-262", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-263", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-264", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-265", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-268", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-270", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-272", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-273", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-275", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 9 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-276", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 8 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-277", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-279", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-280", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-281", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-282", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-283", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-284", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-285", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-232", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-233", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-234", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-258", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-262", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-195", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-198", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-202", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-81", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-81", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-3::obj-49::obj-7" : [ "LFO Freq Menu[2]", "LFO Freq Menu", 0 ],
			"obj-3::obj-25::obj-35" : [ "LFO Modulation Amount[1]", "Modulation", 0 ],
			"obj-79::obj-271::obj-44" : [ "live.gain~[16]", "live.gain~[5]", 0 ],
			"obj-79::obj-201" : [ "live.text[10]", "live.text", 0 ],
			"obj-80::obj-99::obj-78::obj-31" : [ "live.text[15]", "live.text", 0 ],
			"obj-80::obj-20" : [ "Bypass % 1", "Bypass", 0 ],
			"obj-102::obj-68" : [ "Curve Coefficient 2", "Curve 2", 0 ],
			"obj-3::obj-33::obj-14::obj-155" : [ "Receive Delta Min Value[14]", "Minimum", 0 ],
			"obj-80::obj-107::obj-78::obj-31" : [ "live.text[39]", "live.text", 0 ],
			"obj-80::obj-10" : [ "Sample Playback Speed 3", "Speed", 0 ],
			"obj-3::obj-25::obj-7" : [ "LFO Freq Menu[1]", "LFO Freq Menu", 0 ],
			"obj-3::obj-11::obj-15" : [ "live.toggle", "live.toggle", 0 ],
			"obj-79::obj-276::obj-27" : [ "live.toggle[1]", "live.toggle", 0 ],
			"obj-80::obj-48.3::obj-51::obj-43" : [ "Number of Bars[3]", "Bars", 0 ],
			"obj-102::obj-42" : [ "Curve Coefficient", "Curve 1", 0 ],
			"obj-3::obj-33::obj-19::obj-156" : [ "Receive Delta Max Value[12]", "Maximum", 0 ],
			"obj-3::obj-12::obj-22" : [ "Slider 6 On", "Slider 6 On", 0 ],
			"obj-3::obj-38::obj-113" : [ "live.dial[24]", "live.dial", 0 ],
			"obj-80::obj-85::obj-79::obj-56" : [ "Dropdown Control[7]", "Select", 0 ],
			"obj-80::obj-88::obj-79::obj-13" : [ "Quantize Metro Rate[2]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-47::obj-1" : [ "live.toggle[14]", "live.toggle", 0 ],
			"obj-3::obj-2::obj-36::obj-123" : [ "Shuffle Mux", "Shuffle Mux", 0 ],
			"obj-3::obj-38::obj-122" : [ "live.dial[22]", "live.dial", 0 ],
			"obj-3::obj-38::obj-105" : [ "live.dial[30]", "live.dial", 0 ],
			"obj-79::obj-272::obj-44" : [ "live.gain~[13]", "live.gain~[5]", 0 ],
			"obj-80::obj-64::obj-34" : [ "live.text[31]", "live.text", 0 ],
			"obj-80::obj-24" : [ "Channel2 On", "Channel2 On", 0 ],
			"obj-102::obj-96" : [ "Curve Breakpoint", "Curve Breakpoint", 0 ],
			"obj-3::obj-33::obj-17::obj-2" : [ "receive_delta_on[9]", "receive_delta_on", 0 ],
			"obj-3::obj-6::obj-16" : [ "Note Map Max", "Maximum", 0 ],
			"obj-79::obj-192" : [ "live.text[1]", "live.text", 0 ],
			"obj-80::obj-79::obj-79::obj-18" : [ "umenu[3]", "umenu", 0 ],
			"obj-102::obj-8" : [ "Harmonic Logic On", "Harmonic On", 0 ],
			"obj-3::obj-47::obj-56" : [ "Ramp Speed[2]", "Ramp Speed", 0 ],
			"obj-3::obj-33::obj-16::obj-2" : [ "receive_delta_on", "receive_delta_on", 0 ],
			"obj-3::obj-12::obj-12" : [ "Slider +/- Range", "Slider +/- Range", 0 ],
			"obj-79::obj-273::obj-1" : [ "live.gain~[12]", "live.gain~", 0 ],
			"obj-80::obj-86::obj-34" : [ "live.text[42]", "live.text", 0 ],
			"obj-102::obj-16" : [ "Feedback[1]", "Feed\b", 0 ],
			"obj-3::obj-38::obj-27" : [ "live.dial[36]", "live.dial", 0 ],
			"obj-79::obj-275::obj-44" : [ "live.gain~[4]", "live.gain~[5]", 0 ],
			"obj-80::obj-79::obj-79::obj-13" : [ "Quantize Metro Rate[5]", "Quantize Metro Rate", 0 ],
			"obj-80::obj-99::obj-78::obj-12" : [ "Metro On/Off[4]", "Metro On/Off", 0 ],
			"obj-102::obj-117" : [ "Transpose Root", "Transpose", 0 ],
			"obj-3::obj-33::obj-12::obj-156" : [ "Receive Delta Max Value[16]", "Maximum", 0 ],
			"obj-3::obj-38::obj-70" : [ "live.toggle[16]", "live.toggle", 0 ],
			"obj-79::obj-87" : [ "number[24]", "number[1]", 0 ],
			"obj-80::obj-89::obj-12" : [ "Metro On/Off[8]", "Metro On/Off", 0 ],
			"obj-81::obj-40" : [ "live.text[50]", "live.text", 0 ],
			"obj-3::obj-38::obj-88" : [ "live.toggle[15]", "live.toggle", 0 ],
			"obj-80::obj-98::obj-78::obj-31" : [ "live.text[17]", "live.text", 0 ],
			"obj-80::obj-99::obj-78::obj-10" : [ "Manual Metro Rate[4]", "Rate", 0 ],
			"obj-80::obj-57" : [ "# Bars in Stutter structure", "Bars", 0 ],
			"obj-80::obj-45" : [ "Volume[1]", "Volume", 0 ],
			"obj-80::obj-117" : [ "live.text[35]", "live.text", 0 ],
			"obj-81::obj-24" : [ "live.menu", "live.menu", 0 ],
			"obj-102::obj-83" : [ "Metro Speed", "Speed", 0 ],
			"obj-3::obj-33::obj-13::obj-155" : [ "Receive Delta Min Value[10]", "Minimum", 0 ],
			"obj-3::obj-11::obj-43" : [ "Number of Bars", "Bars", 0 ],
			"obj-3::obj-38::obj-44" : [ "live.dial[5]", "live.dial", 0 ],
			"obj-80::obj-89::obj-13" : [ "Quantize Metro Rate[8]", "Quantize Metro Rate", 0 ],
			"obj-80::obj-99::obj-78::obj-56" : [ "Dropdown Control[4]", "Select", 0 ],
			"obj-102::obj-23" : [ "Movie Volume", "Movie Volume", 0 ],
			"obj-3::obj-49::obj-133" : [ "LFO Y-Offset[2]", "Y-Offset", 0 ],
			"obj-3::obj-12::obj-17" : [ "Slider All On", "Slider All On", 0 ],
			"obj-79::obj-74" : [ "live.dial[7]", "live.dial", 0 ],
			"obj-80::obj-48.1::obj-51::obj-15" : [ "live.toggle[11]", "live.toggle", 0 ],
			"obj-80::obj-98::obj-78::obj-34" : [ "live.text[16]", "live.text", 0 ],
			"obj-80::obj-93" : [ "live.numbox[1]", "live.numbox", 0 ],
			"obj-80::obj-94" : [ "live.text[43]", "live.text", 0 ],
			"obj-81::obj-10" : [ "Volume2 Cutoff", "Volume2 Cutoff", 0 ],
			"obj-3::obj-48::obj-56" : [ "Ramp Speed[1]", "Ramp Speed", 0 ],
			"obj-3::obj-33::obj-14::obj-2" : [ "receive_delta_on[15]", "receive_delta_on", 0 ],
			"obj-3::obj-38::obj-117" : [ "live.dial[19]", "live.dial", 0 ],
			"obj-80::obj-107::obj-78::obj-18" : [ "umenu[6]", "umenu", 0 ],
			"obj-80::obj-42" : [ "Volume", "Volume", 0 ],
			"obj-81::obj-16" : [ "multislider[2]", "multislider[2]", 0 ],
			"obj-3::obj-49::obj-132" : [ "LFO Freq[2]", "Freq", 0 ],
			"obj-3::obj-48::obj-35" : [ "Ramp Go-To-Point[1]", "Ramp Go-To-Point", 0 ],
			"obj-79::obj-13" : [ "live.text", "live.text", 0 ],
			"obj-80::obj-95" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-80::obj-90" : [ "live.text[38]", "live.text", 0 ],
			"obj-102::obj-243::obj-162" : [ "Root Frequency", "Root", 0 ],
			"obj-3::obj-33::obj-19::obj-2" : [ "receive_delta_on[14]", "receive_delta_on", 0 ],
			"obj-80::obj-85::obj-79::obj-34" : [ "live.text[22]", "live.text", 0 ],
			"obj-3::obj-48::obj-43" : [ "function[4]", "function", 0 ],
			"obj-3::obj-7::obj-15" : [ "DB slide down rate", "DB slide down rate", 0 ],
			"obj-3::obj-38::obj-52" : [ "gain~[1]", "gain~[1]", 0 ],
			"obj-79::obj-272::obj-6" : [ "live.text[48]", "live.text[1]", 0 ],
			"obj-80::obj-64::obj-18" : [ "umenu[9]", "umenu", 0 ],
			"obj-3::obj-33::obj-18::obj-156" : [ "Receive Delta Max Value[13]", "Maximum", 0 ],
			"obj-79::obj-274::obj-1" : [ "live.gain~[9]", "live.gain~", 0 ],
			"obj-80::obj-85::obj-79::obj-13" : [ "Quantize Metro Rate[6]", "Quantize Metro Rate", 0 ],
			"obj-80::obj-4" : [ "Bypass % 2", "Bypass", 0 ],
			"obj-80::obj-8" : [ "Repeat Rate Adjust", "Rate", 0 ],
			"obj-80::obj-66" : [ "live.text[36]", "live.text", 0 ],
			"obj-102::obj-104" : [ "Q", "Q", 0 ],
			"obj-3::obj-47::obj-7" : [ "Ramp Duration[2]", "Ramp Duration", 0 ],
			"obj-3::obj-25::obj-191" : [ "LFO Freq Number[1]", "LFO Freq Number", 0 ],
			"obj-80::obj-86::obj-31" : [ "live.text[45]", "live.text", 0 ],
			"obj-81::obj-87" : [ "Module2 Volume", "Volume2 [Feed]", 0 ],
			"obj-3::obj-33::obj-17::obj-155" : [ "Receive Delta Min Value[9]", "Minimum", 0 ],
			"obj-3::obj-12::obj-18" : [ "Slider 4 On", "Slider 4 On", 0 ],
			"obj-80::obj-79::obj-79::obj-12" : [ "Metro On/Off[5]", "Metro On/Off", 0 ],
			"obj-3::obj-25::obj-22" : [ "LFO On[1]", "On", 0 ],
			"obj-3::obj-38::obj-102" : [ "live.dial[32]", "live.dial", 0 ],
			"obj-3::obj-38::obj-119" : [ "live.dial[20]", "live.dial", 0 ],
			"obj-79::obj-273::obj-44" : [ "live.gain~[10]", "live.gain~[5]", 0 ],
			"obj-80::obj-86::obj-18" : [ "umenu[8]", "umenu", 0 ],
			"obj-102::obj-64" : [ "Video Theta", "Theta", 0 ],
			"obj-3::obj-48::obj-50" : [ "number[23]", "number[2]", 0 ],
			"obj-3::obj-33::obj-6" : [ "number[15]", "number[15]", 0 ],
			"obj-80::obj-88::obj-79::obj-18" : [ "umenu[2]", "umenu", 0 ],
			"obj-80::obj-31" : [ "Gain", "Gain", 0 ],
			"obj-3::obj-33::obj-13::obj-2" : [ "receive_delta_on[10]", "receive_delta_on", 0 ],
			"obj-3::obj-7::obj-56" : [ "DB loop speed (notes)[1]", "DB loop speed (notes)", 0 ],
			"obj-3::obj-38::obj-21" : [ "live.toggle[254]", "live.toggle", 0 ],
			"obj-79::obj-274::obj-3" : [ "live.gain~[8]", "live.gain~[5]", 0 ],
			"obj-79::obj-276::obj-1" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-80::obj-89::obj-56" : [ "Dropdown Control[9]", "Select", 0 ],
			"obj-80::obj-98::obj-78::obj-18" : [ "umenu[1]", "umenu", 0 ],
			"obj-81::obj-41" : [ "Range", "Range", 0 ],
			"obj-102::obj-63" : [ "Video Zoom Y", "Zoom Y", 0 ],
			"obj-3::obj-48::obj-7" : [ "Ramp Duration[1]", "Ramp Duration", 0 ],
			"obj-80::obj-98::obj-78::obj-12" : [ "Metro On/Off[1]", "Metro On/Off", 0 ],
			"obj-80::obj-17" : [ "Buffer Size", "Size", 0 ],
			"obj-80::obj-108" : [ "live.numbox[8]", "live.numbox", 0 ],
			"obj-3::obj-33::obj-15::obj-155" : [ "Receive Delta Min Value[15]", "Minimum", 0 ],
			"obj-79::obj-276::obj-44" : [ "live.gain~[1]", "live.gain~[5]", 0 ],
			"obj-79::obj-89" : [ "live.dial[11]", "live.dial", 0 ],
			"obj-80::obj-107::obj-78::obj-13" : [ "Quantize Metro Rate[7]", "Quantize Metro Rate", 0 ],
			"obj-102::obj-4" : [ "Metro Manual Speed", "Manual Speed", 0 ],
			"obj-102::obj-62" : [ "Video Zoom X", "Zoom X", 0 ],
			"obj-3::obj-49::obj-55" : [ "LFO Low Ramp[2]", "Low Ramp", 0 ],
			"obj-3::obj-25::obj-122" : [ "LFO Intensity[1]", "Intensity", 0 ],
			"obj-3::obj-7::obj-39" : [ "DB minimum output value", "DB minimum output value", 0 ],
			"obj-79::obj-271::obj-1" : [ "live.gain~[18]", "live.gain~", 0 ],
			"obj-79::obj-275::obj-27" : [ "live.toggle[5]", "live.toggle", 0 ],
			"obj-79::obj-276::obj-6" : [ "live.text[3]", "live.text[1]", 0 ],
			"obj-80::obj-48.2::obj-51::obj-43" : [ "Number of Bars[2]", "Bars", 0 ],
			"obj-80::obj-106" : [ "live.numbox[7]", "live.numbox", 0 ],
			"obj-3::obj-33::obj-14::obj-156" : [ "Receive Delta Max Value[14]", "Maximum", 0 ],
			"obj-3::obj-12::obj-23" : [ "Slider 7 On", "Slider 7 On", 0 ],
			"obj-3::obj-12::obj-4" : [ "Slider Rate", "Slider Rate", 0 ],
			"obj-79::obj-8" : [ "live.toggle[10]", "live.toggle", 0 ],
			"obj-80::obj-107::obj-78::obj-10" : [ "Manual Metro Rate[7]", "Rate", 0 ],
			"obj-3::obj-48::obj-1" : [ "live.toggle[4]", "live.toggle", 0 ],
			"obj-3::obj-2::obj-36::obj-4" : [ "Random Mux", "Random Mux", 0 ],
			"obj-3::obj-7::obj-22" : [ "live.toggle[83]", "live.toggle", 0 ],
			"obj-3::obj-38::obj-104" : [ "live.dial[29]", "live.dial", 0 ],
			"obj-80::obj-64::obj-13" : [ "Quantize Metro Rate[10]", "Quantize Metro Rate", 0 ],
			"obj-80::obj-25" : [ "Channel3 On", "Channel3 On", 0 ],
			"obj-80::obj-105" : [ "live.numbox[6]", "live.numbox", 0 ],
			"obj-81::obj-3" : [ "multislider", "multislider", 0 ],
			"obj-3::obj-33::obj-18::obj-2" : [ "receive_delta_on[13]", "receive_delta_on", 0 ],
			"obj-3::obj-11::obj-57" : [ "Smooth Output", "Smooth Output", 0 ],
			"obj-3::obj-38::obj-39" : [ "live.dial[4]", "live.dial", 0 ],
			"obj-80::obj-85::obj-79::obj-12" : [ "Metro On/Off[6]", "Metro On/Off", 0 ],
			"obj-80::obj-126" : [ "Channel1 Delay Offset", "Offset", 0 ],
			"obj-3::obj-47::obj-43" : [ "function[1]", "function", 0 ],
			"obj-3::obj-25::obj-55" : [ "LFO Low Ramp[1]", "Low Ramp", 0 ],
			"obj-3::obj-4::obj-8" : [ "live.toggle[3]", "live.toggle", 0 ],
			"obj-79::obj-53" : [ "live.dial", "live.dial", 0 ],
			"obj-80::obj-64::obj-10" : [ "Manual Metro Rate[10]", "Rate", 0 ],
			"obj-3::obj-33::obj-17::obj-156" : [ "Receive Delta Max Value[9]", "Maximum", 0 ],
			"obj-3::obj-38::obj-30" : [ "live.dial[35]", "live.dial", 0 ],
			"obj-79::obj-273::obj-2" : [ "live.dial[14]", "live.dial[4]", 0 ],
			"obj-79::obj-275::obj-1" : [ "live.gain~[6]", "live.gain~", 0 ],
			"obj-79::obj-43" : [ "number", "number", 0 ],
			"obj-80::obj-79::obj-79::obj-34" : [ "live.text[20]", "live.text", 0 ],
			"obj-80::obj-88::obj-79::obj-12" : [ "Metro On/Off[2]", "Metro On/Off", 0 ],
			"obj-80::obj-114" : [ "Channel2 Delay Offset", "Offset", 0 ],
			"obj-3::obj-47::obj-50" : [ "number[26]", "number[2]", 0 ],
			"obj-79::obj-273::obj-3" : [ "live.gain~[11]", "live.gain~[5]", 0 ],
			"obj-80::obj-86::obj-13" : [ "Quantize Metro Rate[9]", "Quantize Metro Rate", 0 ],
			"obj-80::obj-56" : [ "Quantize Buffer Size[3]", "Quantize Buffer Size", 0 ],
			"obj-102::obj-70" : [ "live.text[52]", "live.text", 0 ],
			"obj-79::obj-272::obj-2" : [ "live.dial[15]", "live.dial[4]", 0 ],
			"obj-80::obj-88::obj-79::obj-56" : [ "Dropdown Control[5]", "Select", 0 ],
			"obj-80::obj-113" : [ "Channel3 Delay Offset", "Offset", 0 ],
			"obj-80::obj-49" : [ "Volume[2]", "Volume", 0 ],
			"obj-80::obj-6" : [ "Repeat Rate Adjust[1]", "Rate", 0 ],
			"obj-3::obj-33::obj-12::obj-155" : [ "Receive Delta Min Value[16]", "Minimum", 0 ],
			"obj-3::obj-33::obj-10" : [ "number[21]", "number[21]", 0 ],
			"obj-80::obj-89::obj-34" : [ "live.text[41]", "live.text", 0 ],
			"obj-80::obj-51" : [ "Quantize Buffer Size[2]", "Quantize Buffer Size", 0 ],
			"obj-102::obj-21" : [ "Movie Playback Rate", "Movie Playback Rate", 0 ],
			"obj-3::obj-12::obj-5" : [ "Slider 2 On", "Slider 2 On", 0 ],
			"obj-79::obj-275::obj-2" : [ "live.dial[12]", "live.dial[4]", 0 ],
			"obj-80::obj-98::obj-78::obj-13" : [ "Quantize Metro Rate[1]", "Quantize Metro Rate", 0 ],
			"obj-81::obj-9" : [ "multislider[1]", "multislider", 0 ],
			"obj-3::obj-33::obj-15::obj-11" : [ "receive_delta_out_channel_num[15]", "Output Channel", 0 ],
			"obj-3::obj-33::obj-16::obj-155" : [ "Receive Delta Min Value", "Minimum", 0 ],
			"obj-3::obj-33::obj-11" : [ "number[20]", "number[20]", 0 ],
			"obj-3::obj-7::obj-49" : [ "DB frame rate", "rate (ms)", 0 ],
			"obj-3::obj-38::obj-116" : [ "live.dial[18]", "live.dial", 0 ],
			"obj-79::obj-197" : [ "live.text[9]", "live.text", 0 ],
			"obj-80::obj-89::obj-18" : [ "umenu[7]", "umenu", 0 ],
			"obj-80::obj-99::obj-78::obj-18" : [ "umenu[5]", "umenu", 0 ],
			"obj-174" : [ "live.gain~[21]", "live.gain~", 0 ],
			"obj-3::obj-49::obj-191" : [ "LFO Freq Number[2]", "LFO Freq Number", 0 ],
			"obj-3::obj-38::obj-13" : [ "live.numbox[12]", "live.numbox", 0 ],
			"obj-79::obj-271::obj-27" : [ "live.toggle[9]", "live.toggle", 0 ],
			"obj-79::obj-274::obj-2" : [ "live.dial[13]", "live.dial[4]", 0 ],
			"obj-80::obj-48.1::obj-51::obj-43" : [ "Number of Bars[1]", "Bars", 0 ],
			"obj-80::obj-99::obj-78::obj-34" : [ "live.text[14]", "live.text", 0 ],
			"obj-80::obj-100" : [ "live.text[29]", "live.text", 0 ],
			"obj-3::obj-33::obj-14::obj-11" : [ "receive_delta_out_channel_num[14]", "Output Channel", 0 ],
			"obj-3::obj-33::obj-16::obj-11" : [ "receive_delta_out_channel_num", "Output Channel", 0 ],
			"obj-3::obj-33::obj-21" : [ "number[19]", "number[19]", 0 ],
			"obj-80::obj-107::obj-78::obj-56" : [ "Dropdown Control[8]", "Select", 0 ],
			"obj-81::obj-33" : [ "live.text[51]", "live.text", 0 ],
			"obj-102::obj-45" : [ "Number of Harmonics", "Harmonics", 0 ],
			"obj-172" : [ "live.gain~[19]", "live.gain~", 0 ],
			"obj-3::obj-49::obj-22" : [ "LFO On[2]", "On", 0 ],
			"obj-3::obj-38::obj-25" : [ "live.toggle[236]", "live.toggle", 0 ],
			"obj-80::obj-48.3::obj-51::obj-15" : [ "live.toggle[13]", "live.toggle", 0 ],
			"obj-80::obj-73" : [ "Sample Playback Speed 1", "Speed", 0 ],
			"obj-80::obj-92" : [ "live.text[40]", "live.text", 0 ],
			"obj-102::obj-27" : [ "Noise Volume", "Volume", 0 ],
			"obj-3::obj-33::obj-19::obj-11" : [ "receive_delta_out_channel_num[12]", "Output Channel", 0 ],
			"obj-3::obj-33::obj-22" : [ "number[18]", "number[18]", 0 ],
			"obj-80::obj-85::obj-79::obj-18" : [ "umenu[4]", "umenu", 0 ],
			"obj-80::obj-11" : [ "Repeat Rate Adjust[2]", "Rate", 0 ],
			"obj-3::obj-38::obj-24" : [ "live.toggle[237]", "live.toggle", 0 ],
			"obj-79::obj-272::obj-3" : [ "live.gain~[14]", "live.gain~[5]", 0 ],
			"obj-80::obj-64::obj-12" : [ "Metro On/Off[10]", "Metro On/Off", 0 ],
			"obj-102::obj-80" : [ "Manual Frequency Diff Per Pixel", "Freq Per Pixel", 0 ],
			"obj-3::obj-33::obj-18::obj-11" : [ "receive_delta_out_channel_num[13]", "Output Channel", 0 ],
			"obj-3::obj-33::obj-8" : [ "number[17]", "number[17]", 0 ],
			"obj-3::obj-12::obj-21" : [ "Slider 5 On", "Slider 5 On", 0 ],
			"obj-79::obj-274::obj-27" : [ "live.toggle[6]", "live.toggle", 0 ],
			"obj-79::obj-79" : [ "live.dial[10]", "live.dial", 0 ],
			"obj-80::obj-79::obj-79::obj-10" : [ "Manual Metro Rate[5]", "Rate", 0 ],
			"obj-80::obj-99::obj-78::obj-13" : [ "Quantize Metro Rate[4]", "Quantize Metro Rate", 0 ],
			"obj-80::obj-97" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-3::obj-47::obj-35" : [ "Ramp Go-To-Point[2]", "Ramp Go-To-Point", 0 ],
			"obj-3::obj-38::obj-23" : [ "live.toggle[85]", "live.toggle", 0 ],
			"obj-3::obj-38::obj-123" : [ "live.dial[23]", "live.dial", 0 ],
			"obj-3::obj-38::obj-103" : [ "live.dial[31]", "live.dial", 0 ],
			"obj-80::obj-86::obj-12" : [ "Metro On/Off[9]", "Metro On/Off", 0 ],
			"obj-80::obj-3" : [ "Channel1 On", "Channel1 On", 0 ],
			"obj-102::obj-79" : [ "Manual Root Frequency", "Root", 0 ],
			"obj-102::obj-74" : [ "live.toggle[87]", "live.toggle[1]", 0 ],
			"obj-3::obj-48::obj-62" : [ "number[22]", "number[1]", 0 ],
			"obj-3::obj-33::obj-7" : [ "number[16]", "number[16]", 0 ],
			"obj-3::obj-6::obj-15" : [ "Note Map Min", "Minimum", 0 ],
			"obj-3::obj-38::obj-110" : [ "live.dial[25]", "live.dial", 0 ],
			"obj-79::obj-275::obj-3" : [ "live.gain~[5]", "live.gain~[5]", 0 ],
			"obj-79::obj-275::obj-6" : [ "live.text[47]", "live.text[1]", 0 ],
			"obj-79::obj-276::obj-3" : [ "live.gain~[2]", "live.gain~[5]", 0 ],
			"obj-79::obj-6" : [ "Rhythm Length", "Count", 0 ],
			"obj-80::obj-79::obj-79::obj-31" : [ "live.text[21]", "live.text", 0 ],
			"obj-80::obj-103" : [ "live.numbox[4]", "live.numbox", 0 ],
			"obj-80::obj-118" : [ "Dry/Wet", "Dry/Wet", 0 ],
			"obj-80::obj-112" : [ "Feedback Reduce by DB", "Feedback Reduce by DB", 0 ],
			"obj-81::obj-39" : [ "Dry Volume", "Dry", 0 ],
			"obj-3::obj-33::obj-12::obj-2" : [ "receive_delta_on[17]", "receive_delta_on", 0 ],
			"obj-3::obj-38::obj-22" : [ "live.toggle[86]", "live.toggle", 0 ],
			"obj-80::obj-86::obj-10" : [ "Manual Metro Rate[9]", "Rate", 0 ],
			"obj-80::obj-46" : [ "Quantize Buffer Size", "Quantize Buffer Size", 0 ],
			"obj-3::obj-49::obj-117" : [ "LFO Intensity Number[2]", "Intensity Number", 0 ],
			"obj-3::obj-4::obj-16" : [ "Note Map Max[1]", "Maximum", 0 ],
			"obj-3::obj-7::obj-8" : [ "DB file selected[1]", "DB file selected", 0 ],
			"obj-3::obj-38::obj-37" : [ "live.dial[3]", "live.dial", 0 ],
			"obj-3::obj-38::obj-111" : [ "live.dial[26]", "live.dial", 0 ],
			"obj-80::obj-88::obj-79::obj-34" : [ "live.text[18]", "live.text", 0 ],
			"obj-173" : [ "live.gain~[20]", "live.gain~", 0 ],
			"obj-3::obj-33::obj-13::obj-156" : [ "Receive Delta Max Value[10]", "Maximum", 0 ],
			"obj-3::obj-2::obj-210" : [ "number[86]", "number[10]", 0 ],
			"obj-3::obj-38::obj-45" : [ "live.dial[6]", "live.dial", 0 ],
			"obj-3::obj-38::obj-64" : [ "live.numbox[10]", "live.numbox", 0 ],
			"obj-79::obj-274::obj-6" : [ "live.text[32]", "live.text[1]", 0 ],
			"obj-80::obj-89::obj-31" : [ "live.text[44]", "live.text", 0 ],
			"obj-3::obj-49::obj-35" : [ "LFO Modulation Amount[2]", "Modulation", 0 ],
			"obj-3::obj-7::obj-45" : [ "DB maximum output value", "DB maximum output value", 0 ],
			"obj-3::obj-38::obj-107" : [ "live.dial[27]", "live.dial", 0 ],
			"obj-3::obj-38::obj-72" : [ "live.numbox[9]", "live.numbox", 0 ],
			"obj-79::obj-271::obj-6" : [ "live.text[49]", "live.text[1]", 0 ],
			"obj-80::obj-98::obj-78::obj-56" : [ "Dropdown Control[1]", "Select", 0 ],
			"obj-80::obj-116" : [ "live.text[34]", "live.text", 0 ],
			"obj-3::obj-33::obj-15::obj-156" : [ "Receive Delta Max Value[15]", "Maximum", 0 ],
			"obj-3::obj-12::obj-24" : [ "Slider 8 On", "Slider 8 On", 0 ],
			"obj-79::obj-195" : [ "live.text[12]", "live.text", 0 ],
			"obj-80::obj-107::obj-78::obj-12" : [ "Metro On/Off[7]", "Metro On/Off", 0 ],
			"obj-80::obj-29" : [ "Quantize Buffer Size[1]", "Quantize Buffer Size", 0 ],
			"obj-80::obj-55" : [ "live.text[24]", "live.text", 0 ],
			"obj-102::obj-59" : [ "Video Offset Y", "Offset Y", 0 ],
			"obj-3::obj-49::obj-53" : [ "LFO High Ramp[2]", "High Ramp", 0 ],
			"obj-3::obj-12::obj-20" : [ "Slider 1 On", "Slider 1 On", 0 ],
			"obj-3::obj-38::obj-108" : [ "live.dial[28]", "live.dial", 0 ],
			"obj-79::obj-271::obj-3" : [ "live.gain~[17]", "live.gain~[5]", 0 ],
			"obj-80::obj-48.2::obj-51::obj-15" : [ "live.toggle[12]", "live.toggle", 0 ],
			"obj-80::obj-115" : [ "live.text[27]", "live.text", 0 ],
			"obj-102::obj-114" : [ "Offset Y", "Offset Y", 0 ],
			"obj-3::obj-33::obj-19::obj-155" : [ "Receive Delta Min Value[12]", "Minimum", 0 ],
			"obj-3::obj-38::obj-10" : [ "gain~", "gain~", 0 ],
			"obj-79::obj-271::obj-2" : [ "live.dial[16]", "live.dial[4]", 0 ],
			"obj-80::obj-85::obj-79::obj-31" : [ "live.text[23]", "live.text", 0 ],
			"obj-80::obj-135" : [ "Feedback offset", "Offset", 0 ],
			"obj-80::obj-78" : [ "live.text[37]", "live.text", 0 ],
			"obj-102::obj-3" : [ "Video Offset X", "Offset X", 0 ],
			"obj-3::obj-25::obj-117" : [ "LFO Intensity Number[1]", "Intensity Number", 0 ],
			"obj-79::obj-272::obj-1" : [ "live.gain~[15]", "live.gain~", 0 ],
			"obj-80::obj-64::obj-31" : [ "live.text[46]", "live.text", 0 ],
			"obj-80::obj-50" : [ "Feedback", "Feed", 0 ],
			"obj-3::obj-33::obj-18::obj-155" : [ "Receive Delta Min Value[13]", "Minimum", 0 ],
			"obj-3::obj-6::obj-8" : [ "live.toggle[29]", "live.toggle", 0 ],
			"obj-3::obj-38::obj-114" : [ "live.dial[17]", "live.dial", 0 ],
			"obj-3::obj-38::obj-33" : [ "live.dial[34]", "live.dial", 0 ],
			"obj-79::obj-274::obj-44" : [ "live.gain~[7]", "live.gain~[5]", 0 ],
			"obj-80::obj-85::obj-79::obj-10" : [ "Manual Metro Rate[6]", "Rate", 0 ],
			"obj-3::obj-47::obj-62" : [ "number[25]", "number[1]", 0 ],
			"obj-3::obj-7::obj-14" : [ "DB slide up rate", "DB slide up rate", 0 ],
			"obj-80::obj-64::obj-56" : [ "Dropdown Control[11]", "Select", 0 ],
			"obj-3::obj-33::obj-17::obj-11" : [ "receive_delta_out_channel_num[9]", "Output Channel", 0 ],
			"obj-79::obj-75" : [ "live.dial[8]", "live.dial", 0 ],
			"obj-80::obj-79::obj-79::obj-56" : [ "Dropdown Control[6]", "Select", 0 ],
			"obj-80::obj-5" : [ "Sample Playback Speed 2", "Speed", 0 ],
			"obj-80::obj-104" : [ "live.numbox[5]", "live.numbox", 0 ],
			"obj-80::obj-65" : [ "live.text[25]", "live.text", 0 ],
			"obj-3::obj-25::obj-53" : [ "LFO High Ramp[1]", "High Ramp", 0 ],
			"obj-3::obj-33::obj-12::obj-11" : [ "receive_delta_out_channel_num[16]", "Output Channel", 0 ],
			"obj-3::obj-33::obj-5" : [ "number[1]", "number", 0 ],
			"obj-79::obj-273::obj-6" : [ "live.text[33]", "live.text[1]", 0 ],
			"obj-79::obj-273::obj-27" : [ "live.toggle[7]", "live.toggle", 0 ],
			"obj-80::obj-86::obj-56" : [ "Dropdown Control[10]", "Select", 0 ],
			"obj-102::obj-28" : [ "Open Movie File", "Open Movie File", 0 ],
			"obj-3::obj-25::obj-133" : [ "LFO Y-Offset[1]", "Y-Offset", 0 ],
			"obj-3::obj-4::obj-15" : [ "Note Map Min[1]", "Minimum", 0 ],
			"obj-3::obj-12::obj-15" : [ "Slider 3 On", "Slider 3 On", 0 ],
			"obj-79::obj-272::obj-27" : [ "live.toggle[8]", "live.toggle", 0 ],
			"obj-79::obj-193" : [ "live.text[2]", "live.text", 0 ],
			"obj-80::obj-88::obj-79::obj-31" : [ "live.text[19]", "live.text", 0 ],
			"obj-80::obj-88::obj-79::obj-10" : [ "Manual Metro Rate[2]", "Rate", 0 ],
			"obj-81::obj-6" : [ "Module1 Volume", "Volume1", 0 ],
			"obj-171" : [ "live.gain~", "live.gain~", 0 ],
			"obj-3::obj-33::obj-13::obj-11" : [ "receive_delta_out_channel_num[10]", "Output Channel", 0 ],
			"obj-3::obj-33::obj-16::obj-156" : [ "Receive Delta Max Value", "Maximum", 0 ],
			"obj-3::obj-38::obj-120" : [ "live.dial[21]", "live.dial", 0 ],
			"obj-3::obj-38::obj-49" : [ "live.dial[33]", "live.dial", 0 ],
			"obj-80::obj-89::obj-10" : [ "Manual Metro Rate[8]", "Rate", 0 ],
			"obj-102::obj-12" : [ "Scale Freq Map", "Scale", 0 ],
			"obj-102::obj-124" : [ "Filter With Previous Samples", "Filter", 0 ],
			"obj-3::obj-49::obj-122" : [ "LFO Intensity[2]", "Intensity", 0 ],
			"obj-3::obj-25::obj-132" : [ "LFO Freq[1]", "Freq", 0 ],
			"obj-3::obj-38::obj-16" : [ "live.numbox[11]", "Amp ∆/T", 0 ],
			"obj-79::obj-276::obj-2" : [ "live.dial[1]", "live.dial[4]", 0 ],
			"obj-79::obj-77" : [ "live.dial[9]", "live.dial", 0 ],
			"obj-79::obj-199" : [ "live.text[11]", "live.text", 0 ],
			"obj-80::obj-98::obj-78::obj-10" : [ "Manual Metro Rate[1]", "Rate", 0 ],
			"obj-80::obj-81" : [ "live.numbox", "live.numbox", 0 ],
			"obj-80::obj-101" : [ "live.text[30]", "live.text", 0 ],
			"obj-3::obj-33::obj-15::obj-2" : [ "receive_delta_on[16]", "receive_delta_on", 0 ],
			"obj-3::obj-7::obj-26" : [ "live.toggle[82]", "live.toggle", 0 ],
			"obj-3::obj-38::obj-20" : [ "live.toggle[255]", "live.toggle", 0 ],
			"obj-80::obj-107::obj-78::obj-34" : [ "live.text[28]", "live.text", 0 ],
			"obj-80::obj-7" : [ "Bypass % 3", "Bypass", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_LFO.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ramp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_delta_to_CC.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_receive_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_multiplex.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ok.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mux_toggle_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mute.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "_note_mapping.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_db_data.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "001.txt",
				"bootpath" : "~/12c/12c_sandbox/db",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_slider.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "randomness_feedback.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "hilo_limit.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_subdivide.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ctrl_audio.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "2ch_Amp_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "amp_delta_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "in_phase.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "1ch_amp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_rhythm.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rhythm_lock.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "fold",
				"implicit" : 1
			}
, 			{
				"name" : "rhythm_cell.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "rhythm_selector.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "masterfader.maxpat",
				"bootpath" : "~/12c/12c_sandbox/mixer",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "playornot.maxpat",
				"bootpath" : "~/12c/12c_sandbox",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_stutter.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multi_stutter.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "stutter-redev.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "stutter-main.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/probability",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "markov_init.js",
				"bootpath" : "~/12c/12c_sandbox/probability/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_reverb.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_reverb_matrix.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_vid_aud.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "videocycle.maxpat",
				"bootpath" : "~/12c/12c_sandbox/video",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "matrix_row.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_slidermatrix.maxpat",
				"bootpath" : "~/12c/12c_sandbox/matrixtest",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
