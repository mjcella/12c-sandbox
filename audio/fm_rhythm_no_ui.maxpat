{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 34.0, 79.0, 1211.0, 683.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1415.558594, 1189.733276, 45.0, 22.0 ],
					"presentation_rect" : [ 1418.22522, 1191.666626, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1365.558594, 1189.733276, 45.0, 22.0 ],
					"presentation_rect" : [ 1368.22522, 1191.666626, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1314.558594, 1189.733276, 45.0, 22.0 ],
					"presentation_rect" : [ 1317.22522, 1191.666626, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1467.558594, 1115.666626, 45.0, 22.0 ],
					"presentation_rect" : [ 1468.22522, 1115.666626, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1415.558594, 1115.666626, 45.0, 22.0 ],
					"presentation_rect" : [ 1416.22522, 1115.666626, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1365.558594, 1115.666626, 45.0, 22.0 ],
					"presentation_rect" : [ 1366.22522, 1115.666626, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1314.558594, 1115.666626, 45.0, 22.0 ],
					"presentation_rect" : [ 1315.22522, 1115.666626, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1467.558594, 1039.0, 45.0, 22.0 ],
					"presentation_rect" : [ 1470.22522, 1045.333374, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1415.558594, 1039.0, 45.0, 22.0 ],
					"presentation_rect" : [ 1415.558594, 1046.0, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1365.558594, 1039.0, 45.0, 22.0 ],
					"presentation_rect" : [ 1367.891968, 1045.333374, 0.0, 0.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1314.558594, 1039.0, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 602.866638, -116.0, 57.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 93.866638, 979.566589, 57.0, 22.0 ],
					"style" : "",
					"text" : "_stepper"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-262",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2381.0, 90.299927, 108.0, 22.0 ],
					"style" : "",
					"text" : "send~ drum4_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-263",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2172.222168, 90.299927, 108.0, 22.0 ],
					"style" : "",
					"text" : "send~ drum3_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-399",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1963.444458, 90.299927, 104.0, 22.0 ],
					"style" : "",
					"text" : "send~ snare_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-400",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1754.666626, 90.299927, 94.0, 22.0 ],
					"style" : "",
					"text" : "send~ kick_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-398",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 541.0, 708.0, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 441.700073, 689.099915, 40.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.text[103]",
							"parameter_shortname" : "live.text[103]",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "val1", "val2" ]
						}

					}
,
					"text" : "quant",
					"texton" : "quant",
					"varname" : "live.text[2]"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 127.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-397",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 3397.233398, 1338.333252, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1928.252319, 955.999878, 151.400391, 25.80011 ],
					"varname" : "New waveform",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 127.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-395",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 3160.56665, 1338.333252, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1768.585815, 955.999878, 151.400391, 25.80011 ],
					"varname" : "Frequency",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 127.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-403",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2923.900146, 1338.333252, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1610.018921, 955.999878, 151.400391, 25.80011 ],
					"varname" : "Metro speed",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 127.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-396",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2687.233398, 1338.333252, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1451.018921, 955.999878, 151.400391, 25.80011 ],
					"varname" : "New amplitude",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.5, 6.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-358",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2526.333252, 1559.666626, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1220.333252, 957.700562, 151.400391, 25.80011 ],
					"varname" : "synth1 filter mod",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.5, 6.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-319",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2433.333252, 1559.666626, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1065.333252, 957.700562, 151.400391, 25.80011 ],
					"varname" : "synth1 filter q",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.5, 6.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-318",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2340.333252, 1559.666626, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 926.333252, 957.700562, 151.400391, 25.80011 ],
					"varname" : "synth1 filter gain",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 50.0, 20000.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-187",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2247.333252, 1559.666626, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 782.333252, 957.700562, 151.400391, 25.80011 ],
					"varname" : "synth1 filter frequency",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 127.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-82",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2154.333252, 1559.666626, 86.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 625.499878, 957.700562, 151.400391, 25.80011 ],
					"varname" : "synth1 frequency",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_synthbuild.maxpat",
					"numinlets" : 5,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2154.333252, 1618.666626, 391.0, 335.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 717.333435, 999.366943, 391.0, 335.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-393",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 421.333496, 335.999969, 82.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 171.333435, 599.066528, 54.0, 22.0 ],
					"style" : "",
					"text" : "_reverb"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-281",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 192.0, 317.0, 102.0, 22.0 ],
					"style" : "",
					"text" : "_dihedral.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ -1140.666504, 24.0, 60.0, 35.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 97.933411, 269.533356, 67.0, 22.0 ],
					"style" : "",
					"text" : "_dihedral2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-388",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1415.558594, 1225.399902, 53.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 11"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-389",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1365.558594, 1225.399902, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-390",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1314.558594, 1225.399902, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-383",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1467.558594, 1152.399902, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-384",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1415.558594, 1152.399902, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-385",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1365.558594, 1152.399902, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-386",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1314.558594, 1152.399902, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-382",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1467.558594, 1077.799927, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-381",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1418.06665, 1077.799927, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-380",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1365.558594, 1077.799927, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-379",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1314.558594, 1077.799927, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-368",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 602.866638, -73.666672, 142.0, 22.0 ],
					"style" : "",
					"text" : "send~ reversedelay_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-367",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 602.866638, -158.333328, 188.0, 22.0 ],
					"style" : "",
					"text" : "receive~ reversedelay_aud_input"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-363",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 367.399902, -51.566742, 101.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.333435, 974.066589, 85.0, 29.0 ],
					"style" : "",
					"text" : "stepper"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-364",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 293.399902, -48.066742, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 10, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-365",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 293.399902, -332.466705, 225.666672, 268.466675 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.333435, 717.500061, 221.0, 262.466644 ],
					"varname" : "reversedelay_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-366",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 293.399902, -14.066742, 175.0, 22.0 ],
					"style" : "",
					"text" : "send~ reversedelay_aud_input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-350",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 793.0, 1007.400024, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-351",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 677.0, 1007.400024, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-353",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 793.0, 972.733337, 109.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1254.284668, 59.266876, 155.600021, 22.999996 ],
					"varname" : "Slider2 qty",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-355",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 677.0, 972.733337, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1254.284668, 33.266876, 154.000046, 22.999996 ],
					"varname" : "Slider2 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-349",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 768.0, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-348",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 652.0, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-342",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 768.0, 897.333313, 109.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1098.884766, 59.266876, 155.600021, 22.999996 ],
					"varname" : "Slider1 qty",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-344",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 652.0, 897.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1099.684814, 34.266876, 154.000046, 22.999996 ],
					"varname" : "Slider1 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-332",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ -674.0, 520.833374, 29.5, 22.0 ],
					"style" : "",
					"text" : "0.8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-329",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -714.5, 520.833374, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-300",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ -772.0, 520.833374, 44.0, 22.0 ],
					"style" : "",
					"text" : "22500"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-289",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ -835.0, 471.333374, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -835.0, 520.833374, 51.0, 22.0 ],
					"style" : "",
					"text" : "mode 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.75, 6.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-362",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -389.0, 405.866669, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 237.807465, 1056.733154, 154.000046, 22.999996 ],
					"varname" : "Filter Gain",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.5, 10.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-361",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -220.0, 485.333374, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 237.807465, 1081.733154, 154.000046, 22.999996 ],
					"varname" : "Filter Q",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 50.0, 15000.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-360",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -570.0, 418.799988, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 237.807465, 1031.633301, 154.000046, 22.999996 ],
					"varname" : "Filter Freq",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-357",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ -753.0, 671.0, 115.0, 22.0 ],
					"style" : "",
					"text" : "receive~ filter_input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-356",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -294.0, 460.0, 18.0, 20.0 ],
					"style" : "",
					"text" : "q"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-354",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -382.0, 460.0, 34.0, 20.0 ],
					"style" : "",
					"text" : "gain"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-352",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -518.0, 485.333374, 32.0, 20.0 ],
					"style" : "",
					"text" : "freq"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-269",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -309.0, 485.333374, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-272",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -389.0, 485.333374, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-286",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -484.0, 485.333374, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-291",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -309.0, 520.833374, 55.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-294",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -389.0, 520.833374, 55.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-295",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -484.0, 520.833374, 57.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"id" : "obj-298",
					"linmarkers" : [ 0.0, 11025.0, 16537.5 ],
					"logmarkers" : [ 0.0, 100.0, 1000.0, 10000.0 ],
					"maxclass" : "filtergraph~",
					"nfilters" : 1,
					"numinlets" : 8,
					"numoutlets" : 7,
					"outlettype" : [ "list", "float", "float", "float", "float", "list", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -623.0, 562.333374, 360.0, 155.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 1112.733154, 252.000061, 141.199982 ],
					"setfilter" : [ 0, 1, 1, 0, 0, 22050.0, 1.0, 0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-345",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -704.0, 704.333374, 43.0, 23.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-346",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ -623.0, 798.833374, 92.0, 23.0 ],
					"style" : "",
					"text" : "biquad~"
				}

			}
, 			{
				"box" : 				{
					"attr" : "edit_mode",
					"attr_display" : 1,
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-347",
					"lock" : 1,
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -623.0, 497.833374, 83.0, 46.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 368.200043, 981.966736, 96.0, 46.0 ],
					"style" : "",
					"text_width" : 83.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-341",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2381.0, -133.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "i"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 1.0, 16.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-340",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2381.0, -162.999939, 108.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1614.044678, 30.766684, 160.315796, 27.999935 ],
					"varname" : "regen_row",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-339",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2174.0, -133.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "i"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 11.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-330",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 2174.0, -162.999939, 108.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1446.72876, 30.766684, 160.315796, 27.999935 ],
					"varname" : "matrix_tempo",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-327",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1100.558594, 1046.0, 155.0, 22.0 ],
					"style" : "",
					"text" : "receive~ master_aud_outR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-326",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 974.06665, 995.333313, 142.0, 22.0 ],
					"style" : "",
					"text" : "send~ master_aud_outR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-304",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1053.06665, 963.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "preset", "int", "preset", "int" ],
					"patching_rect" : [ 1013.0, 1288.0, 100.0, 40.0 ],
					"pattrstorage" : "all_pattrs",
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-323",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2050.333496, 183.000015, 29.5, 22.0 ],
					"style" : "",
					"text" : "80."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2771.666748, 1973.0, 129.0, 22.0 ],
					"style" : "",
					"text" : "send~ synthgen2_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2154.333252, 1960.0, 129.0, 22.0 ],
					"style" : "",
					"text" : "send~ synthgen1_aud"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-9",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_harmonic_synth.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2687.233398, 1372.666626, 729.0, 586.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1451.018921, 989.666626, 546.949707, 264.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-311",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1176.06665, 1132.733276, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Main Out",
							"parameter_shortname" : "Main Out",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "Main Out"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-309",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -1140.666504, 68.0, 116.0, 22.0 ],
					"style" : "",
					"text" : "send~ dihedral_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-306",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ -1140.666504, -17.066742, 162.0, 22.0 ],
					"style" : "",
					"text" : "receive~ dihedral_aud_input"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-285",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1126.333252, 62.433258, 85.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.333435, 266.033356, 85.0, 29.0 ],
					"style" : "",
					"text" : "dihedral"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-287",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1039.399902, 67.933258, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 7, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-301",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1039.399902, -216.466705, 227.666672, 268.466675 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.333435, 5.066711, 221.0, 262.466644 ],
					"varname" : "dihedral_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-305",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1039.399902, 101.933258, 149.0, 22.0 ],
					"style" : "",
					"text" : "send~ dihedral_aud_input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1128.0, 1306.0, 79.0, 22.0 ],
					"style" : "",
					"text" : "clientwindow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-303",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1013.0, 1347.599976, 189.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 45, 668, 512 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"style" : "",
					"text" : "pattrstorage all_pattrs @greedy 1",
					"varname" : "all_pattrs"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-302",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ -141.0, 321.666626, 58.0, 22.0 ],
					"restore" : 					{
						"Main Out" : [ -6.566929 ],
						"Stutter Rate" : [ 0.857714 ],
						"Stutter Speed" : [ 0.1 ],
						"live.text[2]" : [ 1.0 ]
					}
,
					"style" : "",
					"text" : "autopattr",
					"varname" : "u055009800"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-282",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 204.0, 1545.0, 44.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 494.093018, 7.066681, 44.058594, 20.0 ],
					"style" : "",
					"text" : "global"
				}

			}
, 			{
				"box" : 				{
					"automation" : "Auto",
					"automationon" : "Auto",
					"id" : "obj-278",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 89.0, 1545.0, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 636.151611, 6.066681, 36.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.text[204]",
							"parameter_shortname" : "live.text[203]",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Auto", "Auto" ]
						}

					}
,
					"text" : "Auto",
					"texton" : "Auto",
					"varname" : "live.text[6]"
				}

			}
, 			{
				"box" : 				{
					"automation" : "Limit",
					"automationon" : "Limit",
					"id" : "obj-274",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 89.0, 1609.0, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 598.151611, 6.066681, 34.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.text[203]",
							"parameter_shortname" : "live.text[203]",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Limit", "Limit" ]
						}

					}
,
					"text" : "Limit",
					"texton" : "Limit",
					"varname" : "live.text[7]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-270",
					"maxclass" : "number",
					"maximum" : 7,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 89.0, 1477.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 540.151611, 6.066681, 50.0, 22.0 ],
					"style" : "",
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 89.0, 1576.0, 154.0, 22.0 ],
					"style" : "",
					"text" : "s global_delta_auto_toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 89.0, 1638.0, 152.0, 22.0 ],
					"style" : "",
					"text" : "s global_delta_limit_toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-268",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 89.0, 1512.0, 139.0, 22.0 ],
					"style" : "",
					"text" : "s global_delta_speedlim"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.001, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-160",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 456.533508, 653.333374, 107.0, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 643.099915, 162.000015, 22.999996 ],
					"varname" : "stutter rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-258",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1866.483154, 169.500015, 54.0, 27.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1730.717651, 231.166718, 52.0, 27.0 ],
					"style" : "",
					"text" : "bpm"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1226.399902, 2135.399902, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-271",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "bang" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-223",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "bang" ],
									"patching_rect" : [ 50.0, 223.333328, 34.0, 22.0 ],
									"style" : "",
									"text" : "t 0 b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-222",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 187.333328, 34.0, 22.0 ],
									"style" : "",
									"text" : "gate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-192",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 50.0, 132.333328, 24.0, 22.0 ],
									"style" : "",
									"text" : "t b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-191",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 50.0, 100.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "change"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-267",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-268",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 85.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-269",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 305.333313, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-270",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 85.0, 305.333313, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-192", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-191", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-222", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 59.5, 169.333324, 74.5, 169.333324 ],
									"source" : [ "obj-192", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-223", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-222", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-269", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-223", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-270", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-223", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-191", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-267", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-222", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-268", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1682.0, -71.0, 61.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p requant"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1571.749878, -69.0, 65.250122, 20.0 ],
					"style" : "",
					"text" : "requantize"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-255",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1648.0, -71.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1485.512329, 74.099937, 17.0, 17.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 0.6 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-158",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 471.48584, 275.13324, 103.347473, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 9.200043, 646.099854, 152.93338, 22.999996 ],
					"varname" : "resonance",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 100.0, 3000.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-149",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 349.48584, 275.13324, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 9.200043, 623.099854, 152.93338, 22.999996 ],
					"varname" : "cutoff",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.1, 4.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-142",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 341.333496, 653.333374, 107.0, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 620.099915, 162.000015, 22.999996 ],
					"varname" : "speed",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 532.0, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-118",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 532.0, 897.333313, 109.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 944.724121, 59.266876, 155.600021, 22.999996 ],
					"varname" : "DB rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 420.0, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-114",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 420.0, 897.333313, 106.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 794.284912, 59.266876, 154.000046, 22.999996 ],
					"varname" : "Slider",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 309.0, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-110",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 309.0, 897.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 944.724121, 34.266876, 154.000046, 22.999996 ],
					"varname" : "Ramp2 Dur",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 194.48584, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-108",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 194.48584, 897.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 794.284912, 34.266876, 154.000046, 22.999996 ],
					"varname" : "Ramp1 Dur",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 79.333496, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-105",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 79.333496, 897.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 642.284912, 59.266876, 154.000046, 22.999996 ],
					"varname" : "LFO2 Off",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -35.028473, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-102",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -35.028473, 897.333313, 108.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 642.284912, 34.266876, 154.000046, 22.999996 ],
					"varname" : "LFO2 Int",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -146.999817, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-98",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -146.999817, 897.333313, 106.971344, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 489.151611, 59.266876, 154.000046, 22.999996 ],
					"varname" : "LFO1 Off",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -263.666504, 932.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-89",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -263.666504, 897.333313, 109.466576, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 489.151611, 33.266876, 154.000046, 22.999996 ],
					"varname" : "LFO1 int",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 144.333344, 1879.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "send delta"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 8,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 144.333344, 1848.666748, 137.0, 22.0 ],
					"style" : "",
					"text" : "pak 0. 0. 0. 0. 0. 0. 0. 0."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-96",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_delta.maxpat",
					"numinlets" : 14,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float", "float", "float", "float", "float", "float", "float", "float" ],
					"patching_rect" : [ -263.666504, 972.733337, 920.733154, 830.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 485.45166, 88.000031, 920.733154, 830.0 ],
					"varname" : "_delta",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1316.06665, 478.799927, 140.0, 22.0 ],
					"style" : "",
					"text" : "send~ reverb_aud_input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 578.833313, 275.13324, 153.0, 22.0 ],
					"style" : "",
					"text" : "receive~ reverb_aud_input"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-90",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1487.0, 897.0, 79.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 592.066528, 79.0, 29.0 ],
					"style" : "",
					"text" : "stutter"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-88",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 994.06665, 791.0, 57.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 346.366547, 304.533264, 57.0, 29.0 ],
					"style" : "",
					"text" : "main"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-85",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1128.399902, 442.0, 73.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 981.966736, 49.0, 29.0 ],
					"style" : "",
					"text" : "filter"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-84",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1408.06665, 442.0, 74.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.333435, 595.566528, 74.0, 29.0 ],
					"style" : "",
					"text" : "reverb"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 341.333496, 683.5, 196.5, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 668.099915, 196.5, 20.0 ],
					"style" : "",
					"text" : "speed         rate        bars         gen"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 508.0, 708.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 409.866547, 689.099915, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "number",
					"maximum" : 16,
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 453.333496, 708.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 355.200043, 689.099915, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-51",
					"maxclass" : "flonum",
					"maximum" : 2.0,
					"minimum" : 0.01,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 398.333496, 708.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 300.200043, 689.099915, 50.0, 22.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "number[7]",
							"parameter_shortname" : "number[6]",
							"parameter_type" : 3,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 1.0 ],
							"parameter_invisible" : 1
						}

					}
,
					"style" : "",
					"varname" : "Stutter Rate"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-50",
					"maxclass" : "flonum",
					"maximum" : 4.0,
					"minimum" : 0.1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 341.333496, 708.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 689.099915, 50.0, 22.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "number[6]",
							"parameter_shortname" : "number[6]",
							"parameter_type" : 3,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 1.0 ],
							"parameter_invisible" : 1
						}

					}
,
					"style" : "",
					"varname" : "Stutter Speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 328.833496, 744.0, 209.0, 22.0 ],
					"style" : "",
					"text" : "stutter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1053.06665, 901.400024, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1399.06665, 901.400024, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1316.06665, 442.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1045.158569, 449.000061, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1045.158569, 481.799988, 102.0, 22.0 ],
					"style" : "",
					"text" : "send~ filter_input"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 5, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-53",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1045.158569, 175.533356, 227.0, 259.466644 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 717.500061, 221.0, 262.466644 ],
					"varname" : "filter_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1316.06665, 175.533356, 226.666672, 259.466644 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.333435, 335.533264, 221.0, 264.466644 ],
					"varname" : "reverb_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 6, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-204",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1399.06665, 623.000061, 230.666672, 264.466614 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 335.533264, 221.0, 262.466644 ],
					"varname" : "stutter_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ -1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-203",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui_panning.maxpat",
					"numinlets" : 0,
					"numoutlets" : 13,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 1056.06665, 572.000061, 225.0, 308.333252 ],
					"presentation" : 1,
					"presentation_rect" : [ 252.73349, 5.066711, 224.266113, 303.799988 ],
					"varname" : "main_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 27.466736, 415.133331, 123.0, 22.0 ],
					"style" : "",
					"text" : "send~ resample_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.466736, 167.799988, 141.0, 22.0 ],
					"style" : "",
					"text" : "send~ snarepattern_aud"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-225",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_drummatrix2.maxpat",
					"numinlets" : 4,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 1754.666626, -104.333328, 645.333374, 171.999985 ],
					"presentation" : 1,
					"presentation_rect" : [ 1437.044556, 54.766685, 821.333374, 171.999985 ],
					"varname" : "_drummatrix2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 328.833496, 777.266724, 107.0, 22.0 ],
					"style" : "",
					"text" : "send~ stutter_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1399.06665, 935.400024, 119.0, 22.0 ],
					"style" : "",
					"text" : "send~ stutter_sends"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 328.833496, 621.333374, 132.0, 22.0 ],
					"style" : "",
					"text" : "receive~ stutter_sends"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1533.658569, 1016.733276, 89.0, 22.0 ],
					"style" : "",
					"text" : "loadmess start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "list", "list" ],
					"patching_rect" : [ 1046.558594, 1101.733276, 73.0, 22.0 ],
					"style" : "",
					"text" : "omx.comp~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-178",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1282.399902, 1793.333374, 88.0, 22.0 ],
					"style" : "",
					"text" : "record~ stutter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-171",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1282.399902, 1747.666626, 146.0, 22.0 ],
					"style" : "",
					"text" : "receive~ master_aud_out"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1456.41394, 1747.666626, 120.0, 22.0 ],
					"style" : "",
					"text" : "receive~ drum4_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1456.41394, 1709.666626, 120.0, 22.0 ],
					"style" : "",
					"text" : "receive~ drum3_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 421.333496, 369.866669, 108.0, 22.0 ],
					"style" : "",
					"text" : "send~ reverb_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -623.0, 837.333313, 96.0, 22.0 ],
					"style" : "",
					"text" : "send~ filter_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1046.558594, 1073.399902, 153.0, 22.0 ],
					"style" : "",
					"text" : "receive~ master_aud_outL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1589.666504, 1747.666626, 116.0, 22.0 ],
					"style" : "",
					"text" : "receive~ snare_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1589.666504, 1709.666626, 107.0, 22.0 ],
					"style" : "",
					"text" : "receive~ kick_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 974.06665, 934.0, 140.0, 22.0 ],
					"style" : "",
					"text" : "send~ master_aud_outL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 40.466736, 103.366699, 151.0, 22.0 ],
					"style" : "",
					"text" : "resample snarepattern 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1589.666504, 1793.833374, 170.0, 22.0 ],
					"style" : "",
					"text" : "record~ snarepattern @loop 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 1767.83313, 1709.666626, 159.0, 22.0 ],
					"style" : "",
					"text" : "buffer~ snarepattern 1000 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 27.466736, 374.866669, 146.0, 22.0 ],
					"style" : "",
					"text" : "resample resample 10 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2050.333496, 217.000015, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1711.33313, 1603.666626, 48.333332, 35.0 ],
					"style" : "",
					"text" : "delay 2000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1767.83313, 1671.666626, 93.0, 22.0 ],
					"style" : "",
					"text" : "sizeinsamps $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1767.83313, 1603.666626, 29.5, 22.0 ],
					"style" : "",
					"text" : "1n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1767.83313, 1634.666626, 166.0, 22.0 ],
					"style" : "",
					"text" : "translate notevalues samples"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1711.33313, 1649.999878, 29.5, 22.0 ],
					"style" : "",
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1767.83313, 1566.666626, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1226.399902, 2166.333496, 152.0, 22.0 ],
					"style" : "",
					"text" : "record~ resample @loop 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 1935.333252, 1709.666626, 141.0, 22.0 ],
					"style" : "",
					"text" : "buffer~ resample 1000 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 69.0, 165.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 207.25, 134.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "83"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 207.25, 190.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "tempo $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 86.25, 190.0, 115.0, 22.0 ],
									"style" : "",
									"text" : "metro 4n @active 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 190.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 107.75, 100.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-77",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 207.25, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-78",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 108.5, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-79",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 207.25, 272.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 117.25, 183.0, 59.5, 183.0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 117.25, 128.0, 216.75, 128.0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-77", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1933.83313, 223.000015, 83.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p transportutil"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 36.0,
					"format" : 6,
					"id" : "obj-52",
					"maxclass" : "flonum",
					"maximum" : 1000.0,
					"minimum" : 0.05,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1933.83313, 160.500015, 88.0, 49.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1713.517456, 274.93338, 102.0, 49.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 9,
					"outlettype" : [ "int", "int", "float", "float", "float", "", "int", "float", "" ],
					"patching_rect" : [ 1933.83313, 251.000015, 103.0, 22.0 ],
					"style" : "",
					"text" : "transport"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-381", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 88.833496, 962.666694, -46.074238, 962.666694 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -825.5, 552.0, -613.5, 552.0 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 203.98584, 962.666694, 23.289851, 962.666694 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 318.5, 962.666694, 92.65394, 962.666694 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 429.5, 962.666694, 162.018029, 962.666694 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 541.5, 961.333361, 231.382117, 961.333361 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-379", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1067.0, 1324.058594, 1067.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1068.0, 1375.058594, 1068.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-381", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1067.0, 1427.56665, 1067.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1068.0, 1477.058594, 1068.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-383", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1109.0, 1477.058594, 1109.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-384", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1107.0, 1425.058594, 1107.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1106.0, 1375.058594, 1106.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-386", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1106.0, 1324.058594, 1106.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-388", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1140.0, 1425.058594, 1140.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-389", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1141.0, 1375.058594, 1141.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1543.158569, 1142.0, 1324.058594, 1142.0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-393", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 358.98584, 318.0, 462.333496, 318.0 ],
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-155", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-393", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 480.98584, 320.0, 472.833496, 320.0 ],
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 466.033508, 691.20001, 407.833496, 691.20001 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-383", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-171", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1134.233317, 893.0, 1131.0, 893.0, 1131.0, 1018.0, 1425.058594, 1018.0 ],
					"source" : [ "obj-203", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1151.399983, 1018.0, 1477.058594, 1018.0 ],
					"source" : [ "obj-203", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1220.06665, 893.0, 1130.0, 893.0, 1130.0, 1018.0, 1477.058594, 1018.0 ],
					"source" : [ "obj-203", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1202.899983, 893.0, 1129.0, 893.0, 1129.0, 1018.0, 1425.058594, 1018.0 ],
					"source" : [ "obj-203", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1185.733317, 892.0, 1131.0, 892.0, 1131.0, 1018.0, 1375.058594, 1018.0 ],
					"source" : [ "obj-203", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1168.56665, 891.0, 1127.0, 891.0, 1127.0, 1020.0, 1324.058594, 1020.0 ],
					"source" : [ "obj-203", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1271.56665, 893.0, 1133.0, 893.0, 1133.0, 1017.0, 1425.058594, 1017.0 ],
					"source" : [ "obj-203", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1254.399983, 893.0, 1129.0, 893.0, 1129.0, 1017.0, 1375.058594, 1017.0 ],
					"source" : [ "obj-203", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1099.899983, 890.0, 1130.0, 890.0, 1130.0, 1015.0, 1324.058594, 1015.0 ],
					"source" : [ "obj-203", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1237.233317, 891.0, 1131.0, 891.0, 1131.0, 1018.0, 1324.058594, 1018.0 ],
					"source" : [ "obj-203", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-304", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1082.733317, 889.0, 1040.0, 889.0, 1040.0, 959.0, 1062.56665, 959.0 ],
					"source" : [ "obj-203", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-203", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1117.06665, 891.0, 1133.0, 891.0, 1133.0, 1016.0, 1375.058594, 1016.0 ],
					"source" : [ "obj-203", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1777.33313, 1599.000106, 1720.83313, 1599.000106 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-219", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-384", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-262", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-225", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-225", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-399", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-225", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-400", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-303", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-236", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-386", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1657.5, -38.0, 1677.0, -38.0, 1677.0, -83.0, 1733.5, -83.0 ],
					"source" : [ "obj-255", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-388", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-368", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-291", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1720.83313, 1782.000152, 1291.899902, 1782.000152 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1720.83313, 1782.0, 1495.0, 1782.0, 1495.0, 2161.0, 1235.899902, 2161.0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1720.83313, 1782.00018, 1599.166504, 1782.00018 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-270", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1733.5, -40.0, 1747.0, -40.0, 1747.0, -115.0, 1972.944417, -115.0 ],
					"source" : [ "obj-271", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-255", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1691.5, -38.0, 1678.0, -38.0, 1678.0, -83.0, 1657.5, -83.0 ],
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-294", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-272", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-265", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-295", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-286", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-305", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-289", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-300", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -825.5, 512.0, -762.5, 512.0 ],
					"source" : [ "obj-289", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-329", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -825.5, 512.0, -705.0, 512.0, -705.0, 519.0 ],
					"source" : [ "obj-289", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-332", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -825.5, 510.0, -664.5, 510.0 ],
					"source" : [ "obj-289", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-389", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -299.5, 554.0, -272.5, 554.0 ],
					"source" : [ "obj-291", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -379.5, 553.0, -321.214286, 553.0 ],
					"source" : [ "obj-294", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -474.5, 553.0, -369.928571, 553.0 ],
					"source" : [ "obj-295", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-269", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -443.0, 747.0, -229.0, 747.0, -229.0, 450.0, -299.5, 450.0 ],
					"source" : [ "obj-298", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -499.833333, 736.0, -228.0, 736.0, -228.0, 450.0, -379.5, 450.0 ],
					"source" : [ "obj-298", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-286", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -556.666667, 726.0, -227.0, 726.0, -227.0, 450.0, -474.5, 450.0 ],
					"source" : [ "obj-298", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-346", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-298", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-379", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-295", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -762.5, 559.0, -645.0, 559.0, -645.0, 482.0, -531.0, 482.0, -531.0, 513.0, -474.5, 513.0 ],
					"source" : [ "obj-300", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-301", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-302", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-302", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-326", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1062.56665, 991.0, 983.56665, 991.0 ],
					"source" : [ "obj-304", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-306", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-318", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-319", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-323", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2059.833496, 245.666672, 2043.666626, 245.666672, 2043.666626, 174.666672, 2059.833496, 174.666672 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2059.833496, 209.666672, 2033.666626, 209.666672, 2033.666626, 150.666672, 1943.33313, 150.666672 ],
					"source" : [ "obj-323", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-327", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-294", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -705.0, 567.0, -637.0, 567.0, -637.0, 469.0, -405.0, 469.0, -405.0, 511.0, -379.5, 511.0 ],
					"source" : [ "obj-329", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-330", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-291", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -664.5, 578.0, -631.0, 578.0, -631.0, 461.0, -321.0, 461.0, -321.0, 513.0, -299.5, 513.0 ],
					"source" : [ "obj-332", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-339", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-341", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-340", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-341", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-349", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-342", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-348", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-344", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-346", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -694.5, 734.0, -613.5, 734.0 ],
					"source" : [ "obj-345", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-346", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-347", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 10 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 661.5, 962.0, 439.474384, 962.0 ],
					"source" : [ "obj-348", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 11 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 777.5, 961.0, 508.838472, 961.0 ],
					"source" : [ "obj-349", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1943.33313, 285.999995, 1748.0, 285.999995, 1748.0, 36.0, 1691.5, 36.0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 13 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 802.5, 1044.0, 671.0, 1044.0, 671.0, 960.0, 647.56665, 960.0 ],
					"source" : [ "obj-350", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 12 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 686.5, 1039.0, 669.0, 1039.0, 669.0, 960.0, 578.202561, 960.0 ],
					"source" : [ "obj-351", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-350", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-353", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-351", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-355", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-346", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -743.5, 735.0, -613.5, 735.0 ],
					"source" : [ "obj-357", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-358", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-295", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -560.5, 461.0, -524.0, 461.0, -524.0, 515.0, -474.5, 515.0 ],
					"source" : [ "obj-360", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-291", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -210.5, 513.0, -299.5, 513.0 ],
					"source" : [ "obj-361", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-294", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -379.5, 443.0, -397.0, 443.0, -397.0, 514.0, -379.5, 514.0 ],
					"source" : [ "obj-362", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-366", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-364", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-364", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-365", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-367", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-393", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-395", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-396", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-397", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 550.5, 739.0, 528.333496, 739.0 ],
					"source" : [ "obj-398", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-403", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1777.33313, 1703.666732, 1944.833252, 1703.666732 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 350.833496, 735.0, 376.333496, 735.0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 407.833496, 736.0, 414.333496, 736.0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1943.33313, 216.499995, 1747.0, 216.499995, 1747.0, -126.000004, 1764.166626, -126.000004 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-311", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1074.058594, 1150.0, 1164.0, 1150.0, 1164.0, 1102.0, 1214.56665, 1102.0 ],
					"source" : [ "obj-55", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-311", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1056.058594, 1141.0, 1163.0, 1141.0, 1163.0, 1115.0, 1185.56665, 1115.0 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-309", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 462.833496, 735.0, 452.333496, 735.0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 517.5, 738.0, 490.333496, 738.0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1062.56665, 929.0, 983.56665, 929.0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2007.33313, 242.73333, 2022.800151, 242.73333, 2022.800151, 150.933329, 1943.33313, 150.933329 ],
					"source" : [ "obj-80", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-393", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 588.333313, 321.0, 493.833496, 321.0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-393", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 588.333313, 318.0, 483.333496, 318.0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 647.56665, 1817.0, 271.833344, 1817.0 ],
					"source" : [ "obj-96", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 518.747628, 1817.0, 254.976201, 1817.0 ],
					"source" : [ "obj-96", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 389.928606, 1817.0, 238.119058, 1817.0 ],
					"source" : [ "obj-96", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 261.109584, 1817.0, 221.261915, 1817.0 ],
					"source" : [ "obj-96", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 132.290562, 1817.0, 204.404773, 1817.0 ],
					"source" : [ "obj-96", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 3.47154, 1816.0, 187.54763, 1816.0 ],
					"source" : [ "obj-96", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -125.347482, 1816.0, 170.690487, 1816.0 ],
					"source" : [ "obj-96", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -254.166504, 1817.0, 153.833344, 1817.0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -137.499817, 961.333361, -184.802415, 961.333361 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-225::obj-382::obj-2" : [ "rslider[70]", "rslider[15]", 0 ],
			"obj-28::obj-2::obj-43" : [ "Number of Bars[3]", "Bars", 0 ],
			"obj-96::obj-2::obj-36::obj-4" : [ "Random Mux", "Random Mux", 0 ],
			"obj-96::obj-38::obj-108" : [ "live.dial[28]", "live.dial", 0 ],
			"obj-96::obj-38::obj-23" : [ "live.toggle[85]", "live.toggle", 0 ],
			"obj-89::obj-8::obj-56" : [ "Dropdown Control[127]", "Select", 0 ],
			"obj-108::obj-9" : [ "live.text[264]", "live.text", 0 ],
			"obj-149::obj-8::obj-31" : [ "live.text[319]", "live.text", 0 ],
			"obj-360::obj-8::obj-12" : [ "Metro On/Off[18]", "Metro On/Off", 0 ],
			"obj-353::obj-187" : [ "rslider[94]", "rslider", 0 ],
			"obj-281::obj-356" : [ "live.numbox[7]", "live.numbox[7]", 0 ],
			"obj-281::obj-246" : [ "Scramble 32nd Note", "Scramble", 0 ],
			"obj-319::obj-8::obj-18" : [ "umenu[66]", "umenu", 0 ],
			"obj-396::obj-8::obj-13" : [ "Quantize Metro Rate[73]", "Quantize Metro Rate", 0 ],
			"obj-225::obj-374::obj-8" : [ "Velocity Initial Variance Minimum[14]", "Velocity Initial Variance Minimum", 0 ],
			"obj-225::obj-312" : [ "root note number", "root number", 0 ],
			"obj-96::obj-33::obj-14::obj-2" : [ "receive_delta_on[4]", "receive_delta_on", 0 ],
			"obj-96::obj-38::obj-13" : [ "live.numbox[12]", "live.numbox", 0 ],
			"obj-102::obj-30" : [ "live.text[325]", "live.text[1]", 0 ],
			"obj-118::obj-8::obj-13" : [ "Quantize Metro Rate[81]", "Quantize Metro Rate", 0 ],
			"obj-362::obj-187" : [ "rslider[91]", "rslider", 0 ],
			"obj-342::obj-187" : [ "rslider[92]", "rslider", 0 ],
			"obj-281::obj-361::obj-288::obj-12" : [ "Metro On/Off[105]", "Metro On/Off", 0 ],
			"obj-281::obj-280" : [ "Note Speed Multislider", "Note Speed Multislider", 0 ],
			"obj-82::obj-142" : [ "deltachannel[66]", "deltachannel", 0 ],
			"obj-397::obj-8::obj-10" : [ "Manual Metro Rate[49]", "Rate", 0 ],
			"obj-225::obj-381::obj-2" : [ "rslider[20]", "rslider[15]", 0 ],
			"obj-96::obj-49::obj-133" : [ "LFO Y-Offset[2]", "Y-Offset", 0 ],
			"obj-96::obj-33::obj-5" : [ "number[1]", "number", 0 ],
			"obj-96::obj-38::obj-104" : [ "live.dial[29]", "live.dial", 0 ],
			"obj-96::obj-38::obj-22" : [ "live.toggle[86]", "live.toggle", 0 ],
			"obj-89::obj-8::obj-18" : [ "umenu[73]", "umenu", 0 ],
			"obj-149::obj-142" : [ "deltachannel[28]", "deltachannel", 0 ],
			"obj-360::obj-142" : [ "deltachannel[53]", "deltachannel", 0 ],
			"obj-281::obj-351::obj-288::obj-10" : [ "Manual Metro Rate[55]", "Rate", 0 ],
			"obj-281::obj-275" : [ "16th Note Playback Speed", "Playback Speed", 0 ],
			"obj-281::obj-357" : [ "live.text[22]", "live.text[16]", 0 ],
			"obj-281::obj-180" : [ "32nd Note random factor", "32nd Note random factor", 0 ],
			"obj-319::obj-8::obj-13" : [ "Quantize Metro Rate[86]", "Quantize Metro Rate", 0 ],
			"obj-403::obj-8::obj-10" : [ "Manual Metro Rate[47]", "Rate", 0 ],
			"obj-225::obj-375::obj-2" : [ "rslider[81]", "rslider[15]", 0 ],
			"obj-96::obj-33::obj-19::obj-156" : [ "Receive Delta Max Value[3]", "Maximum", 0 ],
			"obj-96::obj-33::obj-10" : [ "number[21]", "number[21]", 0 ],
			"obj-96::obj-38::obj-114" : [ "live.dial[17]", "live.dial", 0 ],
			"obj-96::obj-38::obj-10" : [ "gain~", "gain~", 0 ],
			"obj-105::obj-8::obj-10" : [ "Manual Metro Rate[100]", "Rate", 0 ],
			"obj-118::obj-9" : [ "live.text[256]", "live.text", 0 ],
			"obj-330::obj-8::obj-18" : [ "umenu[123]", "umenu", 0 ],
			"obj-344::obj-8::obj-56" : [ "Dropdown Control[17]", "Select", 0 ],
			"obj-355::obj-8::obj-34" : [ "live.text[395]", "live.text", 0 ],
			"obj-281::obj-361::obj-288::obj-18" : [ "umenu[101]", "umenu", 0 ],
			"obj-187::obj-8::obj-13" : [ "Quantize Metro Rate[130]", "Quantize Metro Rate", 0 ],
			"obj-397::obj-9" : [ "live.text[208]", "live.text", 0 ],
			"obj-225::obj-322" : [ "Regen Mode", "Regen Mode", 0 ],
			"obj-225::obj-77" : [ "live.numbox[2]", "live.numbox[2]", 0 ],
			"obj-96::obj-49::obj-117" : [ "LFO Intensity Number[2]", "Intensity Number", 0 ],
			"obj-96::obj-38::obj-105" : [ "live.dial[30]", "live.dial", 0 ],
			"obj-89::obj-30" : [ "live.text[333]", "live.text[1]", 0 ],
			"obj-110::obj-8::obj-56" : [ "Dropdown Control[122]", "Select", 0 ],
			"obj-158::obj-8::obj-13" : [ "Quantize Metro Rate[38]", "Quantize Metro Rate", 0 ],
			"obj-361::obj-8::obj-31" : [ "live.text[382]", "live.text", 0 ],
			"obj-6::obj-123" : [ "16th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-281::obj-351::obj-288::obj-12" : [ "Metro On/Off[109]", "Metro On/Off", 0 ],
			"obj-281::obj-298" : [ "8th Note Map Reset", "Note Map Reset", 0 ],
			"obj-403::obj-8::obj-31" : [ "live.text[97]", "live.text", 0 ],
			"obj-225::obj-378::obj-8" : [ "Velocity Initial Variance Minimum[9]", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-33::obj-18::obj-156" : [ "Receive Delta Max Value[2]", "Maximum", 0 ],
			"obj-96::obj-33::obj-11" : [ "number[20]", "number[20]", 0 ],
			"obj-105::obj-8::obj-12" : [ "Metro On/Off[123]", "Metro On/Off", 0 ],
			"obj-142::obj-8::obj-31" : [ "live.text[173]", "live.text", 0 ],
			"obj-330::obj-142" : [ "deltachannel[50]", "deltachannel", 0 ],
			"obj-344::obj-187" : [ "rslider[93]", "rslider", 0 ],
			"obj-355::obj-8::obj-13" : [ "Quantize Metro Rate[66]", "Quantize Metro Rate", 0 ],
			"obj-6::obj-432" : [ "toggle", "toggle", 0 ],
			"obj-6::obj-124" : [ "8th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-281::obj-358::obj-288::obj-10" : [ "Manual Metro Rate[51]", "Rate", 0 ],
			"obj-281::obj-25" : [ "Volume Multislider", "Volume Multislider", 0 ],
			"obj-187::obj-142" : [ "deltachannel[67]", "deltachannel", 0 ],
			"obj-358::obj-8::obj-31" : [ "live.text[447]", "live.text", 0 ],
			"obj-155::obj-4::obj-10" : [ "Manual Metro Rate[110]", "Rate", 0 ],
			"obj-96::obj-48::obj-43" : [ "function[1]", "function", 0 ],
			"obj-96::obj-12::obj-22" : [ "Slider 6 On", "Slider 6 On", 0 ],
			"obj-96::obj-38::obj-103" : [ "live.dial[31]", "live.dial", 0 ],
			"obj-96::obj-38::obj-16" : [ "live.numbox[11]", "Amp ∆/T", 0 ],
			"obj-98::obj-8::obj-10" : [ "Manual Metro Rate[102]", "Rate", 0 ],
			"obj-110::obj-9" : [ "live.text[260]", "live.text", 0 ],
			"obj-158::obj-9" : [ "live.text[133]", "live.text", 0 ],
			"obj-9::obj-23" : [ "live.text[140]", "New waveform", 0 ],
			"obj-361::obj-8::obj-10" : [ "Manual Metro Rate[111]", "Rate", 0 ],
			"obj-281::obj-352::obj-288::obj-18" : [ "umenu[103]", "umenu", 0 ],
			"obj-393::obj-41" : [ "Range[2]", "Range", 0 ],
			"obj-403::obj-187" : [ "rslider[109]", "rslider", 0 ],
			"obj-225::obj-388::obj-8" : [ "Velocity Initial Variance Minimum[7]", "Velocity Initial Variance Minimum", 0 ],
			"obj-204::obj-68" : [ "Mixer / Send UI[20]", "Mixer / Send UI", 0 ],
			"obj-96::obj-33::obj-17::obj-156" : [ "Receive Delta Max Value[1]", "Maximum", 0 ],
			"obj-96::obj-33::obj-21" : [ "number[19]", "number[19]", 0 ],
			"obj-105::obj-187" : [ "rslider[50]", "rslider", 0 ],
			"obj-142::obj-8::obj-12" : [ "Metro On/Off[49]", "Metro On/Off", 0 ],
			"obj-340::obj-8::obj-18" : [ "umenu[124]", "umenu", 0 ],
			"obj-342::obj-8::obj-56" : [ "Dropdown Control[16]", "Select", 0 ],
			"obj-355::obj-142" : [ "deltachannel[59]", "deltachannel", 0 ],
			"obj-281::obj-360" : [ "live.text[153]", "live.text[16]", 0 ],
			"obj-281::obj-291" : [ "Manual Sample Duration", "Buffer", 0 ],
			"obj-318::obj-8::obj-13" : [ "Quantize Metro Rate[72]", "Quantize Metro Rate", 0 ],
			"obj-358::obj-8::obj-12" : [ "Metro On/Off[44]", "Metro On/Off", 0 ],
			"obj-44::obj-4::obj-31" : [ "live.text[197]", "live.text", 0 ],
			"obj-225::obj-537" : [ "drummatrix_reverse_row[1]", "drummatrix_reverse_row", 0 ],
			"obj-96::obj-25::obj-55" : [ "LFO Low Ramp[1]", "Low Ramp", 0 ],
			"obj-96::obj-38::obj-102" : [ "live.dial[32]", "live.dial", 0 ],
			"obj-96::obj-38::obj-120" : [ "live.dial[21]", "live.dial", 0 ],
			"obj-96::obj-319::obj-68" : [ "Mixer / Send UI[14]", "Mixer / Send UI", 0 ],
			"obj-98::obj-12" : [ "ratecontrol[41]", "ratecontrol", 0 ],
			"obj-114::obj-8::obj-10" : [ "Manual Metro Rate[57]", "Rate", 0 ],
			"obj-160::obj-8::obj-34" : [ "live.text[248]", "live.text", 0 ],
			"obj-361::obj-9" : [ "live.text[380]", "live.text", 0 ],
			"obj-6::obj-127" : [ "Full Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-281::obj-355::obj-288::obj-56" : [ "Dropdown Control[110]", "Select", 0 ],
			"obj-281::obj-203" : [ "8th Note Pan", "Pan", 0 ],
			"obj-393::obj-40" : [ "live.text[426]", "live.text", 0 ],
			"obj-395::obj-8::obj-12" : [ "Metro On/Off[54]", "Metro On/Off", 0 ],
			"obj-225::obj-385::obj-8" : [ "Velocity Initial Variance Minimum[4]", "Velocity Initial Variance Minimum", 0 ],
			"obj-225::obj-381::obj-8" : [ "Velocity Initial Variance Minimum", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-25::obj-191" : [ "LFO Freq Number[1]", "LFO Freq Number", 0 ],
			"obj-96::obj-33::obj-22" : [ "number[18]", "number[18]", 0 ],
			"obj-96::obj-4::obj-97" : [ "live.menu[5]", "live.menu", 0 ],
			"obj-96::obj-6::obj-45" : [ "number", "number", 0 ],
			"obj-96::obj-7::obj-26" : [ "live.toggle[82]", "live.toggle", 0 ],
			"obj-96::obj-12::obj-12" : [ "Slider +/- Range", "Slider +/- Range", 0 ],
			"obj-108::obj-8::obj-10" : [ "Manual Metro Rate[99]", "Rate", 0 ],
			"obj-142::obj-142" : [ "deltachannel[29]", "deltachannel", 0 ],
			"obj-340::obj-9" : [ "live.text[366]", "live.text", 0 ],
			"obj-353::obj-8::obj-12" : [ "Metro On/Off[84]", "Metro On/Off", 0 ],
			"obj-6::obj-154" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-281::obj-256" : [ "Random All", "Random", 0 ],
			"obj-318::obj-8::obj-56" : [ "Dropdown Control[139]", "Select", 0 ],
			"obj-358::obj-9" : [ "live.text[445]", "live.text", 0 ],
			"obj-44::obj-4::obj-34" : [ "live.text[375]", "live.text", 0 ],
			"obj-96::obj-47::obj-62" : [ "number[22]", "number[1]", 0 ],
			"obj-96::obj-25::obj-53" : [ "LFO High Ramp[1]", "High Ramp", 0 ],
			"obj-96::obj-33::obj-13::obj-156" : [ "Receive Delta Max Value[6]", "Maximum", 0 ],
			"obj-96::obj-38::obj-49" : [ "live.dial[33]", "live.dial", 0 ],
			"obj-102::obj-8::obj-56" : [ "Dropdown Control[125]", "Select", 0 ],
			"obj-114::obj-187" : [ "rslider[47]", "rslider", 0 ],
			"obj-160::obj-9" : [ "live.text[247]", "live.text", 0 ],
			"obj-362::obj-8::obj-13" : [ "Quantize Metro Rate[24]", "Quantize Metro Rate", 0 ],
			"obj-281::obj-364::obj-288::obj-31" : [ "live.text[188]", "live.text", 0 ],
			"obj-82::obj-8::obj-31" : [ "live.text[431]", "live.text", 0 ],
			"obj-395::obj-187" : [ "rslider[110]", "rslider", 0 ],
			"obj-225::obj-383::obj-2" : [ "rslider[71]", "rslider[15]", 0 ],
			"obj-225::obj-392" : [ "live.toggle[8]", "live.toggle[8]", 0 ],
			"obj-28::obj-2::obj-15" : [ "live.toggle[9]", "live.toggle", 0 ],
			"obj-96::obj-33::obj-8" : [ "number[17]", "number[17]", 0 ],
			"obj-96::obj-38::obj-70" : [ "live.toggle[16]", "live.toggle", 0 ],
			"obj-108::obj-142" : [ "deltachannel[39]", "deltachannel", 0 ],
			"obj-149::obj-8::obj-56" : [ "Dropdown Control[113]", "Select", 0 ],
			"obj-360::obj-8::obj-18" : [ "umenu[13]", "umenu", 0 ],
			"obj-353::obj-9" : [ "live.text[390]", "live.text", 0 ],
			"obj-281::obj-337" : [ "live.text[172]", "live.text[16]", 0 ],
			"obj-281::obj-314" : [ "Channel Mute[1]", "Channel Mute", 0 ],
			"obj-318::obj-9" : [ "live.text[437]", "live.text", 0 ],
			"obj-396::obj-8::obj-12" : [ "Metro On/Off[138]", "Metro On/Off", 0 ],
			"obj-225::obj-373::obj-2" : [ "rslider[84]", "rslider[15]", 0 ],
			"obj-225::obj-16" : [ "note variance", "note variance", 0 ],
			"obj-96::obj-25::obj-122" : [ "LFO Intensity[1]", "Intensity", 0 ],
			"obj-96::obj-33::obj-15::obj-11" : [ "receive_delta_out_channel_num[5]", "Output Channel", 0 ],
			"obj-102::obj-8::obj-34" : [ "live.text[327]", "live.text", 0 ],
			"obj-118::obj-8::obj-18" : [ "umenu[47]", "umenu", 0 ],
			"obj-301::obj-68" : [ "Mixer / Send UI[16]", "Mixer / Send UI", 0 ],
			"obj-362::obj-142" : [ "deltachannel[55]", "deltachannel", 0 ],
			"obj-342::obj-8::obj-12" : [ "Metro On/Off[69]", "Metro On/Off", 0 ],
			"obj-6::obj-140" : [ "live.toggle[17]", "live.toggle", 0 ],
			"obj-281::obj-364::obj-288::obj-34" : [ "live.text[187]", "live.text", 0 ],
			"obj-82::obj-187" : [ "rslider[102]", "rslider", 0 ],
			"obj-397::obj-8::obj-34" : [ "live.text[209]", "live.text", 0 ],
			"obj-96::obj-49::obj-22" : [ "LFO On[2]", "On", 0 ],
			"obj-96::obj-33::obj-7" : [ "number[16]", "number[16]", 0 ],
			"obj-96::obj-7::obj-15" : [ "DB slide down rate", "DB slide down rate", 0 ],
			"obj-89::obj-8::obj-31" : [ "live.text[336]", "live.text", 0 ],
			"obj-149::obj-187" : [ "rslider[38]", "rslider", 0 ],
			"obj-360::obj-8::obj-31" : [ "live.text[378]", "live.text", 0 ],
			"obj-281::obj-366" : [ "live.text[31]", "live.text[16]", 0 ],
			"obj-319::obj-8::obj-34" : [ "live.text[442]", "live.text", 0 ],
			"obj-396::obj-187" : [ "rslider[108]", "rslider", 0 ],
			"obj-260::obj-349" : [ "Quantized Sample Duration[2]", "Quantized Sample Duration", 0 ],
			"obj-225::obj-376::obj-2" : [ "rslider[82]", "rslider[15]", 0 ],
			"obj-96::obj-33::obj-14::obj-155" : [ "Receive Delta Min Value[4]", "Minimum", 0 ],
			"obj-96::obj-33::obj-16::obj-156" : [ "Receive Delta Max Value", "Maximum", 0 ],
			"obj-96::obj-12::obj-17" : [ "Slider All On", "Slider All On", 0 ],
			"obj-96::obj-38::obj-72" : [ "live.numbox[9]", "live.numbox", 0 ],
			"obj-96::obj-38::obj-39" : [ "live.dial[4]", "live.dial", 0 ],
			"obj-96::obj-38::obj-113" : [ "live.dial[24]", "live.dial", 0 ],
			"obj-102::obj-187" : [ "rslider[51]", "rslider", 0 ],
			"obj-118::obj-8::obj-31" : [ "live.text[43]", "live.text", 0 ],
			"obj-330::obj-8::obj-31" : [ "live.text[196]", "live.text", 0 ],
			"obj-344::obj-8::obj-12" : [ "Metro On/Off[83]", "Metro On/Off", 0 ],
			"obj-342::obj-142" : [ "deltachannel[56]", "deltachannel", 0 ],
			"obj-281::obj-361::obj-288::obj-10" : [ "Manual Metro Rate[52]", "Rate", 0 ],
			"obj-187::obj-8::obj-56" : [ "Dropdown Control[86]", "Select", 0 ],
			"obj-397::obj-8::obj-56" : [ "Dropdown Control[51]", "Select", 0 ],
			"obj-311" : [ "Main Out", "Main Out", 0 ],
			"obj-225::obj-100" : [ "Note Number Range", "Note Number Range", 0 ],
			"obj-96::obj-49::obj-55" : [ "LFO Low Ramp[2]", "Low Ramp", 0 ],
			"obj-96::obj-33::obj-6" : [ "number[15]", "number[15]", 0 ],
			"obj-89::obj-142" : [ "deltachannel[43]", "deltachannel", 0 ],
			"obj-110::obj-8::obj-34" : [ "live.text[261]", "live.text", 0 ],
			"obj-158::obj-8::obj-12" : [ "Metro On/Off[12]", "Metro On/Off", 0 ],
			"obj-360::obj-9" : [ "live.text[376]", "live.text", 0 ],
			"obj-6::obj-273" : [ "Master", "Master", 0 ],
			"obj-6::obj-264" : [ "live.gain~[6]", "live.gain~[6]", 0 ],
			"obj-281::obj-351::obj-288::obj-56" : [ "Dropdown Control[112]", "Select", 0 ],
			"obj-281::obj-255" : [ "Scramble 8th Note", "Scramble", 0 ],
			"obj-319::obj-9" : [ "live.text[441]", "live.text", 0 ],
			"obj-396::obj-9" : [ "live.text[409]", "live.text", 0 ],
			"obj-403::obj-8::obj-13" : [ "Quantize Metro Rate[54]", "Quantize Metro Rate", 0 ],
			"obj-260::obj-48" : [ "live.menu[4]", "live.menu", 0 ],
			"obj-225::obj-379::obj-2" : [ "rslider[79]", "rslider[15]", 0 ],
			"obj-96::obj-33::obj-18::obj-155" : [ "Receive Delta Min Value[2]", "Minimum", 0 ],
			"obj-96::obj-7::obj-39" : [ "DB minimum output value", "DB minimum output value", 0 ],
			"obj-105::obj-8::obj-56" : [ "Dropdown Control[124]", "Select", 0 ],
			"obj-118::obj-30" : [ "live.text[255]", "live.text[1]", 0 ],
			"obj-330::obj-30" : [ "live.text[193]", "live.text[1]", 0 ],
			"obj-344::obj-30" : [ "live.text[199]", "live.text[1]", 0 ],
			"obj-355::obj-8::obj-10" : [ "Manual Metro Rate[17]", "Rate", 0 ],
			"obj-281::obj-358::obj-288::obj-12" : [ "Metro On/Off[104]", "Metro On/Off", 0 ],
			"obj-281::obj-286" : [ "Gain", "Gain", 0 ],
			"obj-281::obj-84" : [ "32nd Note Matrix", "32nd Note Matrix", 0 ],
			"obj-187::obj-8::obj-18" : [ "umenu[83]", "umenu", 0 ],
			"obj-319::obj-142" : [ "deltachannel[69]", "deltachannel", 0 ],
			"obj-397::obj-12" : [ "ratecontrol[73]", "ratecontrol", 0 ],
			"obj-203::obj-113" : [ "panning", "panning", 0 ],
			"obj-96::obj-48::obj-7" : [ "Ramp Duration[2]", "Ramp Duration", 0 ],
			"obj-96::obj-25::obj-35" : [ "LFO Modulation Amount[1]", "Modulation", 0 ],
			"obj-96::obj-38::obj-20" : [ "live.toggle[255]", "live.toggle", 0 ],
			"obj-98::obj-8::obj-56" : [ "Dropdown Control[126]", "Select", 0 ],
			"obj-110::obj-142" : [ "deltachannel[38]", "deltachannel", 0 ],
			"obj-158::obj-12" : [ "ratecontrol[18]", "ratecontrol", 0 ],
			"obj-361::obj-8::obj-12" : [ "Metro On/Off[67]", "Metro On/Off", 0 ],
			"obj-6::obj-26" : [ "Feed", "Feed", 0 ],
			"obj-281::obj-352::obj-288::obj-12" : [ "Metro On/Off[108]", "Metro On/Off", 0 ],
			"obj-281::obj-247" : [ "1/2 Note random factor", "1/2 Note random factor", 0 ],
			"obj-281::obj-101" : [ "Manual Sample Length", "Manual Sample Length", 0 ],
			"obj-393::obj-6" : [ "Module1 Volume[2]", "Vol1", 0 ],
			"obj-403::obj-9" : [ "live.text[76]", "live.text", 0 ],
			"obj-225::obj-377::obj-8" : [ "Velocity Initial Variance Minimum[8]", "Velocity Initial Variance Minimum", 0 ],
			"obj-225::obj-68" : [ "number[205]", "number[1]", 0 ],
			"obj-96::obj-33::obj-17::obj-11" : [ "receive_delta_out_channel_num[1]", "Output Channel", 0 ],
			"obj-105::obj-12" : [ "ratecontrol[39]", "ratecontrol", 0 ],
			"obj-142::obj-8::obj-56" : [ "Dropdown Control[13]", "Select", 0 ],
			"obj-340::obj-8::obj-13" : [ "Quantize Metro Rate[63]", "Quantize Metro Rate", 0 ],
			"obj-342::obj-8::obj-13" : [ "Quantize Metro Rate[26]", "Quantize Metro Rate", 0 ],
			"obj-355::obj-9" : [ "live.text[394]", "live.text", 0 ],
			"obj-281::obj-358::obj-288::obj-34" : [ "live.text[271]", "live.text", 0 ],
			"obj-187::obj-9" : [ "live.text[433]", "live.text", 0 ],
			"obj-358::obj-8::obj-34" : [ "live.text[446]", "live.text", 0 ],
			"obj-155::obj-4::obj-13" : [ "Quantize Metro Rate[50]", "Quantize Metro Rate", 0 ],
			"obj-225::obj-173" : [ "drummatrix_regen_probability", "drummatrix_regen_probability", 0 ],
			"obj-96::obj-48::obj-56" : [ "Ramp Speed[2]", "Ramp Speed", 0 ],
			"obj-96::obj-25::obj-7" : [ "LFO Freq Menu[1]", "LFO Freq Menu", 0 ],
			"obj-96::obj-2::obj-210" : [ "number[86]", "number[10]", 0 ],
			"obj-98::obj-8::obj-13" : [ "Quantize Metro Rate[124]", "Quantize Metro Rate", 0 ],
			"obj-114::obj-8::obj-13" : [ "Quantize Metro Rate[82]", "Quantize Metro Rate", 0 ],
			"obj-160::obj-8::obj-18" : [ "umenu[62]", "umenu", 0 ],
			"obj-361::obj-12" : [ "ratecontrol[53]", "ratecontrol", 0 ],
			"obj-281::obj-355::obj-288::obj-13" : [ "Quantize Metro Rate[106]", "Quantize Metro Rate", 0 ],
			"obj-393::obj-39" : [ "Dry Volume[2]", "Dry", 0 ],
			"obj-395::obj-8::obj-10" : [ "Manual Metro Rate[48]", "Rate", 0 ],
			"obj-225::obj-386::obj-2" : [ "rslider[74]", "rslider[15]", 0 ],
			"obj-28::obj-90" : [ "live.text[101]", "live.text[101]", 0 ],
			"obj-96::obj-11::obj-15" : [ "live.toggle[87]", "live.toggle", 0 ],
			"obj-108::obj-8::obj-12" : [ "Metro On/Off[122]", "Metro On/Off", 0 ],
			"obj-142::obj-9" : [ "live.text[167]", "live.text", 0 ],
			"obj-340::obj-8::obj-34" : [ "live.text[367]", "live.text", 0 ],
			"obj-353::obj-8::obj-10" : [ "Manual Metro Rate[9]", "Rate", 0 ],
			"obj-6::obj-291" : [ "Manual Sample Duration[1]", "Buffer", 0 ],
			"obj-281::obj-253" : [ "Scramble 16th Note", "Scramble", 0 ],
			"obj-281::obj-5" : [ "32nd Note Pan", "Pan", 0 ],
			"obj-318::obj-8::obj-12" : [ "Metro On/Off[135]", "Metro On/Off", 0 ],
			"obj-358::obj-30" : [ "live.text[444]", "live.text[1]", 0 ],
			"obj-44::obj-4::obj-13" : [ "Quantize Metro Rate[64]", "Quantize Metro Rate", 0 ],
			"obj-225::obj-124" : [ "drummatrix_shuffle_max", "drummatrix_shuffle_max", 0 ],
			"obj-28::obj-73" : [ "Sample Playback Speed[1]", "Speed", 0 ],
			"obj-96::obj-33::obj-12::obj-11" : [ "receive_delta_out_channel_num[7]", "Output Channel", 0 ],
			"obj-96::obj-12::obj-4" : [ "Slider Rate", "Slider Rate", 0 ],
			"obj-96::obj-12::obj-18" : [ "Slider 4 On", "Slider 4 On", 0 ],
			"obj-98::obj-142" : [ "deltachannel[42]", "deltachannel", 0 ],
			"obj-114::obj-8::obj-12" : [ "Metro On/Off[82]", "Metro On/Off", 0 ],
			"obj-160::obj-8::obj-12" : [ "Metro On/Off[47]", "Metro On/Off", 0 ],
			"obj-362::obj-8::obj-34" : [ "live.text[384]", "live.text", 0 ],
			"obj-281::obj-355::obj-288::obj-31" : [ "live.text[274]", "live.text", 0 ],
			"obj-281::obj-270" : [ "Full Note Manual Note Speed On", "Manual Speed On", 0 ],
			"obj-82::obj-8::obj-56" : [ "Dropdown Control[138]", "Select", 0 ],
			"obj-395::obj-8::obj-13" : [ "Quantize Metro Rate[55]", "Quantize Metro Rate", 0 ],
			"obj-225::obj-384::obj-8" : [ "Velocity Initial Variance Minimum[3]", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-38::obj-27" : [ "live.dial[36]", "live.dial", 0 ],
			"obj-96::obj-38::obj-64" : [ "live.numbox[10]", "live.numbox", 0 ],
			"obj-108::obj-30" : [ "live.text[263]", "live.text[1]", 0 ],
			"obj-149::obj-8::obj-12" : [ "Metro On/Off[48]", "Metro On/Off", 0 ],
			"obj-340::obj-12" : [ "ratecontrol[50]", "ratecontrol", 0 ],
			"obj-353::obj-30" : [ "live.text[389]", "live.text[1]", 0 ],
			"obj-281::obj-339" : [ "live.numbox[14]", "live.numbox[7]", 0 ],
			"obj-318::obj-142" : [ "deltachannel[68]", "deltachannel", 0 ],
			"obj-396::obj-8::obj-31" : [ "live.text[451]", "live.text", 0 ],
			"obj-225::obj-7" : [ "note numbers", "note numbers", 0 ],
			"obj-96::obj-33::obj-15::obj-155" : [ "Receive Delta Min Value[5]", "Minimum", 0 ],
			"obj-96::obj-6::obj-39" : [ "live.tab", "live.tab", 0 ],
			"obj-96::obj-7::obj-49" : [ "DB frame rate", "rate (ms)", 0 ],
			"obj-96::obj-38::obj-123" : [ "live.dial[23]", "live.dial", 0 ],
			"obj-96::obj-38::obj-117" : [ "live.dial[19]", "live.dial", 0 ],
			"obj-102::obj-8::obj-12" : [ "Metro On/Off[124]", "Metro On/Off", 0 ],
			"obj-114::obj-142" : [ "deltachannel[37]", "deltachannel", 0 ],
			"obj-160::obj-30" : [ "live.text[246]", "live.text[1]", 0 ],
			"obj-340::obj-8::obj-10" : [ "Manual Metro Rate[6]", "Rate", 0 ],
			"obj-362::obj-30" : [ "live.text[383]", "live.text[1]", 0 ],
			"obj-342::obj-8::obj-18" : [ "umenu[59]", "umenu", 0 ],
			"obj-281::obj-364::obj-288::obj-56" : [ "Dropdown Control[109]", "Select", 0 ],
			"obj-281::obj-269" : [ "1/2 Note Playback Speed", "Playback Speed", 0 ],
			"obj-82::obj-8::obj-18" : [ "umenu[82]", "umenu", 0 ],
			"obj-395::obj-30" : [ "live.text[98]", "live.text[1]", 0 ],
			"obj-28::obj-2::obj-57" : [ "Smooth Output[3]", "Smooth Output", 0 ],
			"obj-96::obj-7::obj-22" : [ "live.toggle[83]", "live.toggle", 0 ],
			"obj-96::obj-11::obj-57" : [ "Smooth Output[2]", "Smooth Output", 0 ],
			"obj-96::obj-38::obj-88" : [ "live.toggle[15]", "live.toggle", 0 ],
			"obj-89::obj-8::obj-12" : [ "Metro On/Off[126]", "Metro On/Off", 0 ],
			"obj-110::obj-8::obj-13" : [ "Quantize Metro Rate[61]", "Quantize Metro Rate", 0 ],
			"obj-149::obj-12" : [ "ratecontrol[27]", "ratecontrol", 0 ],
			"obj-360::obj-8::obj-10" : [ "Manual Metro Rate[113]", "Rate", 0 ],
			"obj-365::obj-68" : [ "Mixer / Send UI[15]", "Mixer / Send UI", 0 ],
			"obj-6::obj-152" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-6::obj-35" : [ "Complete speed control", "live.numbox", 0 ],
			"obj-281::obj-354" : [ "live.text[159]", "live.text[16]", 0 ],
			"obj-319::obj-8::obj-56" : [ "Dropdown Control[33]", "Select", 0 ],
			"obj-396::obj-30" : [ "live.text[408]", "live.text[1]", 0 ],
			"obj-398" : [ "live.text[103]", "live.text[103]", 0 ],
			"obj-225::obj-374::obj-2" : [ "rslider[83]", "rslider[15]", 0 ],
			"obj-96::obj-33::obj-14::obj-156" : [ "Receive Delta Max Value[4]", "Maximum", 0 ],
			"obj-102::obj-12" : [ "ratecontrol[40]", "ratecontrol", 0 ],
			"obj-118::obj-8::obj-34" : [ "live.text[257]", "live.text", 0 ],
			"obj-330::obj-8::obj-13" : [ "Quantize Metro Rate[62]", "Quantize Metro Rate", 0 ],
			"obj-344::obj-8::obj-18" : [ "umenu[63]", "umenu", 0 ],
			"obj-342::obj-12" : [ "ratecontrol[55]", "ratecontrol", 0 ],
			"obj-281::obj-361::obj-288::obj-34" : [ "live.text[154]", "live.text", 0 ],
			"obj-281::obj-263" : [ "8th Note Manual Note Speed On", "Note Speed On", 0 ],
			"obj-82::obj-9" : [ "live.text[429]", "live.text", 0 ],
			"obj-397::obj-8::obj-13" : [ "Quantize Metro Rate[56]", "Quantize Metro Rate", 0 ],
			"obj-96::obj-49::obj-132" : [ "LFO Freq[2]", "Freq", 0 ],
			"obj-89::obj-8::obj-10" : [ "Manual Metro Rate[103]", "Rate", 0 ],
			"obj-110::obj-8::obj-10" : [ "Manual Metro Rate[68]", "Rate", 0 ],
			"obj-158::obj-8::obj-31" : [ "live.text[135]", "live.text", 0 ],
			"obj-360::obj-30" : [ "live.text[369]", "live.text[1]", 0 ],
			"obj-281::obj-351::obj-288::obj-31" : [ "live.text[161]", "live.text", 0 ],
			"obj-281::obj-349" : [ "Quantized Sample Duration", "Quantized Sample Duration", 0 ],
			"obj-281::obj-302" : [ "1/2 Note Map Reset", "Note Map Reset", 0 ],
			"obj-319::obj-8::obj-10" : [ "Manual Metro Rate[81]", "Rate", 0 ],
			"obj-403::obj-8::obj-56" : [ "Dropdown Control[142]", "Select", 0 ],
			"obj-274" : [ "live.text[203]", "live.text[203]", 0 ],
			"obj-225::obj-380::obj-2" : [ "rslider[80]", "rslider[15]", 0 ],
			"obj-225::obj-10" : [ "live.toggle[205]", "live.toggle[2]", 0 ],
			"obj-225::obj-76" : [ "live.numbox[1]", "live.numbox[1]", 0 ],
			"obj-96::obj-33::obj-19::obj-2" : [ "receive_delta_on[3]", "receive_delta_on", 0 ],
			"obj-105::obj-8::obj-34" : [ "live.text[323]", "live.text", 0 ],
			"obj-118::obj-12" : [ "ratecontrol[35]", "ratecontrol", 0 ],
			"obj-330::obj-8::obj-10" : [ "Manual Metro Rate[112]", "Rate", 0 ],
			"obj-344::obj-8::obj-31" : [ "live.text[267]", "live.text", 0 ],
			"obj-355::obj-8::obj-12" : [ "Metro On/Off[129]", "Metro On/Off", 0 ],
			"obj-281::obj-361::obj-288::obj-56" : [ "Dropdown Control[108]", "Select", 0 ],
			"obj-281::obj-265" : [ "8th Note Playback Speed", "Playback Speed", 0 ],
			"obj-187::obj-8::obj-12" : [ "Metro On/Off[134]", "Metro On/Off", 0 ],
			"obj-397::obj-187" : [ "rslider[111]", "rslider", 0 ],
			"obj-225::obj-14" : [ "Shuffle note numbers", "Shuffle note numbers", 0 ],
			"obj-96::obj-49::obj-35" : [ "LFO Modulation Amount[2]", "Modulation", 0 ],
			"obj-96::obj-12::obj-23" : [ "Slider 7 On", "Slider 7 On", 0 ],
			"obj-89::obj-9" : [ "live.text[334]", "live.text", 0 ],
			"obj-110::obj-8::obj-31" : [ "live.text[262]", "live.text", 0 ],
			"obj-158::obj-8::obj-18" : [ "umenu[105]", "umenu", 0 ],
			"obj-361::obj-8::obj-13" : [ "Quantize Metro Rate[33]", "Quantize Metro Rate", 0 ],
			"obj-6::obj-126" : [ "2nd Note Playback Speed", "Note Speed", 0 ],
			"obj-281::obj-352::obj-288::obj-34" : [ "live.text[157]", "live.text", 0 ],
			"obj-393::obj-3" : [ "multislider[14]", "multislider", 0 ],
			"obj-403::obj-8::obj-12" : [ "Metro On/Off[34]", "Metro On/Off", 0 ],
			"obj-260::obj-104" : [ "Master[1]", "Master", 0 ],
			"obj-225::obj-378::obj-2" : [ "rslider[78]", "rslider[15]", 0 ],
			"obj-96::obj-33::obj-18::obj-11" : [ "receive_delta_out_channel_num[2]", "Output Channel", 0 ],
			"obj-96::obj-4::obj-39" : [ "live.tab[1]", "live.tab", 0 ],
			"obj-105::obj-30" : [ "live.text[321]", "live.text[1]", 0 ],
			"obj-142::obj-8::obj-13" : [ "Quantize Metro Rate[108]", "Quantize Metro Rate", 0 ],
			"obj-330::obj-12" : [ "ratecontrol[49]", "ratecontrol", 0 ],
			"obj-344::obj-9" : [ "live.text[200]", "live.text", 0 ],
			"obj-355::obj-30" : [ "live.text[393]", "live.text[1]", 0 ],
			"obj-281::obj-358::obj-288::obj-18" : [ "umenu[100]", "umenu", 0 ],
			"obj-281::obj-362" : [ "live.numbox[5]", "live.numbox[7]", 0 ],
			"obj-187::obj-12" : [ "ratecontrol[66]", "ratecontrol", 0 ],
			"obj-358::obj-8::obj-13" : [ "Quantize Metro Rate[87]", "Quantize Metro Rate", 0 ],
			"obj-155::obj-4::obj-34" : [ "live.text[361]", "live.text", 0 ],
			"obj-96::obj-48::obj-62" : [ "number[9]", "number[1]", 0 ],
			"obj-96::obj-38::obj-119" : [ "live.dial[20]", "live.dial", 0 ],
			"obj-98::obj-8::obj-12" : [ "Metro On/Off[125]", "Metro On/Off", 0 ],
			"obj-110::obj-187" : [ "rslider[48]", "rslider", 0 ],
			"obj-158::obj-187" : [ "rslider[37]", "rslider", 0 ],
			"obj-361::obj-142" : [ "deltachannel[54]", "deltachannel", 0 ],
			"obj-6::obj-151" : [ "live.gain~", "live.gain~", 0 ],
			"obj-281::obj-352::obj-288::obj-13" : [ "Quantize Metro Rate[107]", "Quantize Metro Rate", 0 ],
			"obj-281::obj-271" : [ "Full Note Playback Speed", "Playback Speed", 0 ],
			"obj-393::obj-10" : [ "Volume2 Cutoff[2]", "Volume2 Cutoff", 0 ],
			"obj-403::obj-12" : [ "ratecontrol[70]", "ratecontrol", 0 ],
			"obj-225::obj-387::obj-8" : [ "Velocity Initial Variance Minimum[6]", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-7::obj-56" : [ "DB loop speed (notes)[1]", "DB loop speed (notes)", 0 ],
			"obj-108::obj-8::obj-13" : [ "Quantize Metro Rate[121]", "Quantize Metro Rate", 0 ],
			"obj-142::obj-12" : [ "ratecontrol[28]", "ratecontrol", 0 ],
			"obj-340::obj-8::obj-12" : [ "Metro On/Off[13]", "Metro On/Off", 0 ],
			"obj-353::obj-8::obj-34" : [ "live.text[391]", "live.text", 0 ],
			"obj-6::obj-155" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-281::obj-254" : [ "Random Full note", "Random", 0 ],
			"obj-318::obj-8::obj-18" : [ "umenu[84]", "umenu", 0 ],
			"obj-358::obj-142" : [ "deltachannel[70]", "deltachannel", 0 ],
			"obj-44::obj-4::obj-10" : [ "Manual Metro Rate[7]", "Rate", 0 ],
			"obj-225::obj-75" : [ "live.numbox", "live.numbox", 0 ],
			"obj-96::obj-25::obj-133" : [ "LFO Y-Offset[1]", "Y-Offset", 0 ],
			"obj-96::obj-33::obj-12::obj-155" : [ "Receive Delta Min Value[7]", "Minimum", 0 ],
			"obj-98::obj-187" : [ "rslider[52]", "rslider", 0 ],
			"obj-114::obj-8::obj-56" : [ "Dropdown Control[121]", "Select", 0 ],
			"obj-160::obj-8::obj-31" : [ "live.text[249]", "live.text", 0 ],
			"obj-362::obj-8::obj-31" : [ "live.text[385]", "live.text", 0 ],
			"obj-6::obj-125" : [ "4th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-281::obj-355::obj-288::obj-10" : [ "Manual Metro Rate[53]", "Rate", 0 ],
			"obj-393::obj-33" : [ "live.text[427]", "live.text", 0 ],
			"obj-395::obj-8::obj-56" : [ "Dropdown Control[143]", "Select", 0 ],
			"obj-225::obj-385::obj-2" : [ "rslider[73]", "rslider[15]", 0 ],
			"obj-96::obj-7::obj-8" : [ "DB file selected[1]", "DB file selected", 0 ],
			"obj-108::obj-8::obj-56" : [ "Dropdown Control[123]", "Select", 0 ],
			"obj-149::obj-8::obj-34" : [ "live.text[138]", "live.text", 0 ],
			"obj-340::obj-142" : [ "deltachannel[51]", "deltachannel", 0 ],
			"obj-353::obj-8::obj-31" : [ "live.text[392]", "live.text", 0 ],
			"obj-6::obj-122" : [ "32nd Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-281::obj-209" : [ "1/2 Note Pan", "Pan", 0 ],
			"obj-281::obj-257" : [ "Scramble 4th Note", "Scramble", 0 ],
			"obj-281::obj-94" : [ "32nd Note Map", "32nd Note Map", 0 ],
			"obj-318::obj-8::obj-34" : [ "live.text[438]", "live.text", 0 ],
			"obj-396::obj-8::obj-18" : [ "umenu[129]", "umenu", 0 ],
			"obj-44::obj-4::obj-12" : [ "Metro On/Off[14]", "Metro On/Off", 0 ],
			"obj-225::obj-311" : [ "manual BPM mode", "manual BPM", 0 ],
			"obj-96::obj-25::obj-132" : [ "LFO Freq[1]", "Freq", 0 ],
			"obj-96::obj-33::obj-13::obj-155" : [ "Receive Delta Min Value[6]", "Minimum", 0 ],
			"obj-102::obj-8::obj-10" : [ "Manual Metro Rate[101]", "Rate", 0 ],
			"obj-114::obj-12" : [ "ratecontrol[36]", "ratecontrol", 0 ],
			"obj-160::obj-142" : [ "deltachannel[15]", "deltachannel", 0 ],
			"obj-362::obj-8::obj-12" : [ "Metro On/Off[68]", "Metro On/Off", 0 ],
			"obj-281::obj-364::obj-288::obj-18" : [ "umenu[102]", "umenu", 0 ],
			"obj-82::obj-8::obj-13" : [ "Quantize Metro Rate[129]", "Quantize Metro Rate", 0 ],
			"obj-395::obj-142" : [ "deltachannel[73]", "deltachannel", 0 ],
			"obj-225::obj-382::obj-8" : [ "Velocity Initial Variance Minimum[1]", "Velocity Initial Variance Minimum", 0 ],
			"obj-108::obj-12" : [ "ratecontrol[38]", "ratecontrol", 0 ],
			"obj-149::obj-8::obj-13" : [ "Quantize Metro Rate[15]", "Quantize Metro Rate", 0 ],
			"obj-360::obj-8::obj-56" : [ "Dropdown Control[131]", "Select", 0 ],
			"obj-353::obj-12" : [ "ratecontrol[57]", "ratecontrol", 0 ],
			"obj-281::obj-259" : [ "Scramble 1/2 Note", "Scramble", 0 ],
			"obj-281::obj-274" : [ "16th Note Manual Note Speed On", "Note Speed On", 0 ],
			"obj-318::obj-30" : [ "live.text[436]", "live.text[1]", 0 ],
			"obj-396::obj-8::obj-10" : [ "Manual Metro Rate[63]", "Rate", 0 ],
			"obj-225::obj-373::obj-8" : [ "Velocity Initial Variance Minimum[15]", "Velocity Initial Variance Minimum", 0 ],
			"obj-225::obj-198" : [ "note volume", "note volume", 0 ],
			"obj-96::obj-33::obj-15::obj-2" : [ "receive_delta_on[5]", "receive_delta_on", 0 ],
			"obj-96::obj-12::obj-5" : [ "Slider 2 On", "Slider 2 On", 0 ],
			"obj-96::obj-38::obj-44" : [ "live.dial[5]", "live.dial", 0 ],
			"obj-102::obj-9" : [ "live.text[326]", "live.text", 0 ],
			"obj-118::obj-8::obj-56" : [ "Dropdown Control[118]", "Select", 0 ],
			"obj-362::obj-12" : [ "ratecontrol[54]", "ratecontrol", 0 ],
			"obj-342::obj-30" : [ "live.text[386]", "live.text[1]", 0 ],
			"obj-281::obj-364::obj-288::obj-13" : [ "Quantize Metro Rate[105]", "Quantize Metro Rate", 0 ],
			"obj-281::obj-40" : [ "live.toggle", "live.toggle", 0 ],
			"obj-281::obj-243" : [ "4th Note random factor", "4th Note random factor", 0 ],
			"obj-82::obj-12" : [ "ratecontrol[65]", "ratecontrol", 0 ],
			"obj-397::obj-8::obj-31" : [ "live.text[210]", "live.text", 0 ],
			"obj-96::obj-49::obj-122" : [ "LFO Intensity[2]", "Intensity", 0 ],
			"obj-96::obj-2::obj-36::obj-123" : [ "Shuffle Mux", "Shuffle Mux", 0 ],
			"obj-89::obj-8::obj-34" : [ "live.text[335]", "live.text", 0 ],
			"obj-149::obj-30" : [ "live.text[136]", "live.text[1]", 0 ],
			"obj-360::obj-12" : [ "ratecontrol[52]", "ratecontrol", 0 ],
			"obj-281::obj-351::obj-288::obj-34" : [ "live.text[160]", "live.text", 0 ],
			"obj-281::obj-177" : [ "Random 32nd notes", "Random", 0 ],
			"obj-319::obj-8::obj-31" : [ "live.text[443]", "live.text", 0 ],
			"obj-396::obj-142" : [ "deltachannel[72]", "deltachannel", 0 ],
			"obj-260::obj-18" : [ "rslider", "rslider", 0 ],
			"obj-225::obj-375::obj-8" : [ "Velocity Initial Variance Minimum[12]", "Velocity Initial Variance Minimum", 0 ],
			"obj-225::obj-43" : [ "number[204]", "number[2]", 0 ],
			"obj-96::obj-33::obj-19::obj-11" : [ "receive_delta_out_channel_num[3]", "Output Channel", 0 ],
			"obj-96::obj-7::obj-45" : [ "DB maximum output value", "DB maximum output value", 0 ],
			"obj-105::obj-8::obj-18" : [ "umenu[121]", "umenu", 0 ],
			"obj-118::obj-142" : [ "deltachannel[36]", "deltachannel", 0 ],
			"obj-330::obj-8::obj-56" : [ "Dropdown Control[82]", "Select", 0 ],
			"obj-344::obj-8::obj-10" : [ "Manual Metro Rate[116]", "Rate", 0 ],
			"obj-355::obj-8::obj-56" : [ "Dropdown Control[71]", "Select", 0 ],
			"obj-281::obj-361::obj-288::obj-31" : [ "live.text[185]", "live.text", 0 ],
			"obj-187::obj-8::obj-31" : [ "live.text[435]", "live.text", 0 ],
			"obj-397::obj-30" : [ "live.text[207]", "live.text[1]", 0 ],
			"obj-225::obj-82" : [ "Randomize note number", "Randomize note number", 0 ],
			"obj-96::obj-49::obj-53" : [ "LFO High Ramp[2]", "High Ramp", 0 ],
			"obj-96::obj-47::obj-56" : [ "Ramp Speed[1]", "Ramp Speed", 0 ],
			"obj-96::obj-38::obj-21" : [ "live.toggle[254]", "live.toggle", 0 ],
			"obj-89::obj-12" : [ "ratecontrol[42]", "ratecontrol", 0 ],
			"obj-110::obj-8::obj-18" : [ "umenu[119]", "umenu", 0 ],
			"obj-158::obj-8::obj-56" : [ "Dropdown Control[7]", "Select", 0 ],
			"obj-361::obj-8::obj-34" : [ "live.text[381]", "live.text", 0 ],
			"obj-281::obj-351::obj-288::obj-18" : [ "umenu[48]", "umenu", 0 ],
			"obj-281::obj-146" : [ "Playback Speed", "Speed", 0 ],
			"obj-281::obj-74" : [ "live.text[275]", "live.text", 0 ],
			"obj-319::obj-187" : [ "rslider[105]", "rslider", 0 ],
			"obj-403::obj-8::obj-18" : [ "umenu[81]", "umenu", 0 ],
			"obj-260::obj-291" : [ "Manual Sample Duration[2]", "Buffer", 0 ],
			"obj-225::obj-379::obj-8" : [ "Velocity Initial Variance Minimum[10]", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-47::obj-35" : [ "Ramp Go-To-Point[1]", "Ramp Go-To-Point", 0 ],
			"obj-96::obj-33::obj-18::obj-2" : [ "receive_delta_on[2]", "receive_delta_on", 0 ],
			"obj-105::obj-8::obj-13" : [ "Quantize Metro Rate[122]", "Quantize Metro Rate", 0 ],
			"obj-142::obj-8::obj-34" : [ "live.text[168]", "live.text", 0 ],
			"obj-330::obj-9" : [ "live.text[194]", "live.text", 0 ],
			"obj-344::obj-142" : [ "deltachannel[57]", "deltachannel", 0 ],
			"obj-355::obj-8::obj-31" : [ "live.text[205]", "live.text", 0 ],
			"obj-281::obj-358::obj-288::obj-13" : [ "Quantize Metro Rate[103]", "Quantize Metro Rate", 0 ],
			"obj-187::obj-8::obj-10" : [ "Manual Metro Rate[71]", "Rate", 0 ],
			"obj-358::obj-8::obj-18" : [ "umenu[85]", "umenu", 0 ],
			"obj-155::obj-4::obj-31" : [ "live.text[362]", "live.text", 0 ],
			"obj-98::obj-8::obj-31" : [ "live.text[332]", "live.text", 0 ],
			"obj-110::obj-12" : [ "ratecontrol[37]", "ratecontrol", 0 ],
			"obj-158::obj-142" : [ "deltachannel[27]", "deltachannel", 0 ],
			"obj-361::obj-8::obj-56" : [ "Dropdown Control[132]", "Select", 0 ],
			"obj-6::obj-195" : [ "multislider[1]", "multislider", 0 ],
			"obj-281::obj-352::obj-288::obj-10" : [ "Manual Metro Rate[54]", "Rate", 0 ],
			"obj-281::obj-46" : [ "Full Note Gain", "Gain", 0 ],
			"obj-393::obj-9" : [ "multislider[13]", "multislider", 0 ],
			"obj-403::obj-142" : [ "deltachannel[71]", "deltachannel", 0 ],
			"obj-260::obj-91" : [ "rslider[1]", "rslider[1]", 0 ],
			"obj-225::obj-388::obj-2" : [ "rslider[76]", "rslider[15]", 0 ],
			"obj-96::obj-48::obj-1" : [ "live.toggle[5]", "live.toggle", 0 ],
			"obj-96::obj-47::obj-43" : [ "function[4]", "function", 0 ],
			"obj-96::obj-33::obj-17::obj-155" : [ "Receive Delta Min Value[1]", "Minimum", 0 ],
			"obj-96::obj-38::obj-33" : [ "live.dial[34]", "live.dial", 0 ],
			"obj-105::obj-9" : [ "live.text[322]", "live.text", 0 ],
			"obj-142::obj-8::obj-18" : [ "umenu[117]", "umenu", 0 ],
			"obj-340::obj-8::obj-31" : [ "live.text[368]", "live.text", 0 ],
			"obj-342::obj-8::obj-34" : [ "live.text[388]", "live.text", 0 ],
			"obj-355::obj-12" : [ "ratecontrol[58]", "ratecontrol", 0 ],
			"obj-281::obj-241" : [ "Random 8th notes", "Random", 0 ],
			"obj-281::obj-185" : [ "16th Note random factor", "16th Note random factor", 0 ],
			"obj-281::obj-359" : [ "live.numbox[4]", "live.numbox[7]", 0 ],
			"obj-281::obj-83" : [ "16th Note Matrix", "16th Note Matrix", 0 ],
			"obj-187::obj-187" : [ "rslider[103]", "rslider", 0 ],
			"obj-358::obj-8::obj-56" : [ "Dropdown Control[44]", "Select", 0 ],
			"obj-155::obj-4::obj-12" : [ "Metro On/Off[51]", "Metro On/Off", 0 ],
			"obj-225::obj-529" : [ "drummatrix_reverse_row", "drummatrix_reverse_row", 0 ],
			"obj-96::obj-33::obj-12::obj-156" : [ "Receive Delta Max Value[7]", "Maximum", 0 ],
			"obj-96::obj-6::obj-97" : [ "live.menu", "live.menu", 0 ],
			"obj-96::obj-12::obj-21" : [ "Slider 5 On", "Slider 5 On", 0 ],
			"obj-98::obj-30" : [ "live.text[329]", "live.text[1]", 0 ],
			"obj-114::obj-8::obj-34" : [ "live.text[107]", "live.text", 0 ],
			"obj-160::obj-8::obj-13" : [ "Quantize Metro Rate[14]", "Quantize Metro Rate", 0 ],
			"obj-9::obj-9" : [ "live.text[139]", "New waveform", 0 ],
			"obj-361::obj-30" : [ "live.text[379]", "live.text[1]", 0 ],
			"obj-281::obj-355::obj-288::obj-12" : [ "Metro On/Off[107]", "Metro On/Off", 0 ],
			"obj-281::obj-45" : [ "1/2 Note Gain", "Gain", 0 ],
			"obj-393::obj-24" : [ "live.menu[3]", "live.menu", 0 ],
			"obj-395::obj-8::obj-18" : [ "umenu[86]", "umenu", 0 ],
			"obj-260::obj-80" : [ "live.menu[1]", "live.menu", 0 ],
			"obj-225::obj-386::obj-8" : [ "Velocity Initial Variance Minimum[5]", "Velocity Initial Variance Minimum", 0 ],
			"obj-4::obj-68" : [ "Mixer / Send UI[19]", "Mixer / Send UI", 0 ],
			"obj-28::obj-91" : [ "live.text[102]", "live.text[101]", 0 ],
			"obj-96::obj-47::obj-1" : [ "live.toggle[4]", "live.toggle", 0 ],
			"obj-96::obj-4::obj-45" : [ "number[8]", "number", 0 ],
			"obj-96::obj-38::obj-30" : [ "live.dial[35]", "live.dial", 0 ],
			"obj-108::obj-8::obj-31" : [ "live.text[266]", "live.text", 0 ],
			"obj-142::obj-187" : [ "rslider[39]", "rslider", 0 ],
			"obj-340::obj-187" : [ "rslider[87]", "rslider", 0 ],
			"obj-353::obj-8::obj-56" : [ "Dropdown Control[18]", "Select", 0 ],
			"obj-6::obj-349" : [ "Quantized Sample Duration[1]", "Quantized Sample Duration", 0 ],
			"obj-281::obj-353" : [ "live.numbox[13]", "live.numbox[7]", 0 ],
			"obj-281::obj-134" : [ "8th Note Matrix", "8th Note Matrix", 0 ],
			"obj-318::obj-8::obj-10" : [ "Manual Metro Rate[72]", "Rate", 0 ],
			"obj-358::obj-12" : [ "ratecontrol[69]", "ratecontrol", 0 ],
			"obj-44::obj-4::obj-56" : [ "Dropdown Control[84]", "Select", 0 ],
			"obj-96::obj-33::obj-13::obj-2" : [ "receive_delta_on[6]", "receive_delta_on", 0 ],
			"obj-96::obj-38::obj-52" : [ "gain~[1]", "gain~[1]", 0 ],
			"obj-96::obj-38::obj-116" : [ "live.dial[18]", "live.dial", 0 ],
			"obj-96::obj-38::obj-122" : [ "live.dial[22]", "live.dial", 0 ],
			"obj-102::obj-8::obj-31" : [ "live.text[328]", "live.text", 0 ],
			"obj-114::obj-9" : [ "live.text[58]", "live.text", 0 ],
			"obj-160::obj-8::obj-56" : [ "Dropdown Control[66]", "Select", 0 ],
			"obj-362::obj-8::obj-56" : [ "Dropdown Control[15]", "Select", 0 ],
			"obj-281::obj-355::obj-288::obj-18" : [ "umenu[53]", "umenu", 0 ],
			"obj-281::obj-44" : [ "4th Note Gain", "Gain", 0 ],
			"obj-82::obj-8::obj-34" : [ "live.text[430]", "live.text", 0 ],
			"obj-395::obj-12" : [ "ratecontrol[72]", "ratecontrol", 0 ],
			"obj-225::obj-383::obj-8" : [ "Velocity Initial Variance Minimum[2]", "Velocity Initial Variance Minimum", 0 ],
			"obj-225::obj-79" : [ "live.toggle[3]", "live.toggle[3]", 0 ],
			"obj-108::obj-187" : [ "rslider[49]", "rslider", 0 ],
			"obj-149::obj-8::obj-10" : [ "Manual Metro Rate[95]", "Rate", 0 ],
			"obj-360::obj-8::obj-34" : [ "live.text[377]", "live.text", 0 ],
			"obj-353::obj-142" : [ "deltachannel[58]", "deltachannel", 0 ],
			"obj-6::obj-153" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-281::obj-244" : [ "Random 4th notes", "Random", 0 ],
			"obj-281::obj-179" : [ "4th Note Matrix", "4th Note Matrix", 0 ],
			"obj-318::obj-12" : [ "ratecontrol[67]", "ratecontrol", 0 ],
			"obj-396::obj-8::obj-34" : [ "live.text[410]", "live.text", 0 ],
			"obj-225::obj-283::obj-2" : [ "rslider[85]", "rslider[15]", 0 ],
			"obj-225::obj-211" : [ "note speed probability", "note speed probability", 0 ],
			"obj-28::obj-22" : [ "Repeat Rate Adjust[1]", "Rate", 0 ],
			"obj-96::obj-47::obj-50" : [ "number[23]", "number[2]", 0 ],
			"obj-96::obj-25::obj-22" : [ "LFO On[1]", "On", 0 ],
			"obj-96::obj-33::obj-15::obj-156" : [ "Receive Delta Max Value[5]", "Maximum", 0 ],
			"obj-96::obj-33::obj-16::obj-2" : [ "receive_delta_on", "receive_delta_on", 0 ],
			"obj-96::obj-11::obj-43" : [ "Number of Bars[2]", "Bars", 0 ],
			"obj-102::obj-8::obj-18" : [ "umenu[122]", "umenu", 0 ],
			"obj-118::obj-8::obj-10" : [ "Manual Metro Rate[46]", "Rate", 0 ],
			"obj-160::obj-187" : [ "rslider[34]", "rslider", 0 ],
			"obj-9::obj-4" : [ "live.text", "Reset pitch", 0 ],
			"obj-362::obj-9" : [ "live.text[363]", "live.text", 0 ],
			"obj-342::obj-8::obj-31" : [ "live.text[198]", "live.text", 0 ],
			"obj-6::obj-265" : [ "multislider", "multislider", 0 ],
			"obj-281::obj-364::obj-288::obj-10" : [ "Manual Metro Rate[90]", "Rate", 0 ],
			"obj-281::obj-43" : [ "8th Note Gain", "Gain", 0 ],
			"obj-281::obj-296" : [ "16th Note Map Reset", "Note Map Reset", 0 ],
			"obj-82::obj-8::obj-10" : [ "Manual Metro Rate[70]", "Rate", 0 ],
			"obj-397::obj-8::obj-12" : [ "Metro On/Off[55]", "Metro On/Off", 0 ],
			"obj-89::obj-8::obj-13" : [ "Quantize Metro Rate[125]", "Quantize Metro Rate", 0 ],
			"obj-149::obj-9" : [ "live.text[137]", "live.text", 0 ],
			"obj-360::obj-8::obj-13" : [ "Quantize Metro Rate[17]", "Quantize Metro Rate", 0 ],
			"obj-6::obj-74" : [ "live.text[425]", "live.text", 0 ],
			"obj-281::obj-300" : [ "4th Note Map Reset", "Note Map Reset", 0 ],
			"obj-281::obj-183" : [ "1/2 Note Matrix", "1/2 Note Matrix", 0 ],
			"obj-281::obj-240" : [ "Channel Mute", "Channel Mute", 0 ],
			"obj-281::obj-363" : [ "live.text[186]", "live.text[16]", 0 ],
			"obj-281::obj-365" : [ "live.numbox[6]", "live.numbox[7]", 0 ],
			"obj-319::obj-8::obj-12" : [ "Metro On/Off[136]", "Metro On/Off", 0 ],
			"obj-396::obj-12" : [ "ratecontrol[71]", "ratecontrol", 0 ],
			"obj-225::obj-376::obj-8" : [ "Velocity Initial Variance Minimum[13]", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-47::obj-7" : [ "Ramp Duration[1]", "Ramp Duration", 0 ],
			"obj-96::obj-33::obj-14::obj-11" : [ "receive_delta_out_channel_num[4]", "Output Channel", 0 ],
			"obj-102::obj-142" : [ "deltachannel[41]", "deltachannel", 0 ],
			"obj-118::obj-8::obj-12" : [ "Metro On/Off[81]", "Metro On/Off", 0 ],
			"obj-330::obj-8::obj-12" : [ "Metro On/Off[66]", "Metro On/Off", 0 ],
			"obj-344::obj-8::obj-34" : [ "live.text[201]", "live.text", 0 ],
			"obj-342::obj-9" : [ "live.text[387]", "live.text", 0 ],
			"obj-281::obj-361::obj-288::obj-13" : [ "Quantize Metro Rate[104]", "Quantize Metro Rate", 0 ],
			"obj-281::obj-195" : [ "16th Note Pan", "Pan", 0 ],
			"obj-281::obj-42" : [ "16th Note Gain", "Gain", 0 ],
			"obj-82::obj-30" : [ "live.text[428]", "live.text[1]", 0 ],
			"obj-397::obj-8::obj-18" : [ "umenu[130]", "umenu", 0 ],
			"obj-51" : [ "number[7]", "number[6]", 0 ],
			"obj-225::obj-81" : [ "Loop Control", "Loop Control", 0 ],
			"obj-225::obj-204" : [ "live.toggle[206]", "live.toggle[1]", 0 ],
			"obj-96::obj-49::obj-7" : [ "LFO Freq Menu[2]", "LFO Freq Menu", 0 ],
			"obj-96::obj-7::obj-14" : [ "DB slide up rate", "DB slide up rate", 0 ],
			"obj-96::obj-12::obj-24" : [ "Slider 8 On", "Slider 8 On", 0 ],
			"obj-89::obj-187" : [ "rslider[53]", "rslider", 0 ],
			"obj-110::obj-8::obj-12" : [ "Metro On/Off[62]", "Metro On/Off", 0 ],
			"obj-158::obj-8::obj-10" : [ "Manual Metro Rate[80]", "Rate", 0 ],
			"obj-360::obj-187" : [ "rslider[89]", "rslider", 0 ],
			"obj-281::obj-351::obj-288::obj-13" : [ "Quantize Metro Rate[53]", "Quantize Metro Rate", 0 ],
			"obj-281::obj-252" : [ "Full Note Matrix", "Full Note Matrix", 0 ],
			"obj-281::obj-211" : [ "4th Note Pan", "Pan", 0 ],
			"obj-281::obj-248" : [ "Random 1/2 notes", "Random", 0 ],
			"obj-281::obj-133" : [ "1/2 Note Map", "1/2 Note Map", 0 ],
			"obj-319::obj-12" : [ "ratecontrol[68]", "ratecontrol", 0 ],
			"obj-403::obj-8::obj-34" : [ "live.text[96]", "live.text", 0 ],
			"obj-278" : [ "live.text[204]", "live.text[203]", 0 ],
			"obj-225::obj-380::obj-8" : [ "Velocity Initial Variance Minimum[11]", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-25::obj-117" : [ "LFO Intensity Number[1]", "Intensity Number", 0 ],
			"obj-96::obj-33::obj-19::obj-155" : [ "Receive Delta Min Value[3]", "Minimum", 0 ],
			"obj-96::obj-33::obj-16::obj-155" : [ "Receive Delta Min Value", "Minimum", 0 ],
			"obj-96::obj-12::obj-20" : [ "Slider 1 On", "Slider 1 On", 0 ],
			"obj-96::obj-38::obj-37" : [ "live.dial[3]", "live.dial", 0 ],
			"obj-105::obj-8::obj-31" : [ "live.text[324]", "live.text", 0 ],
			"obj-118::obj-187" : [ "rslider[46]", "rslider", 0 ],
			"obj-330::obj-8::obj-34" : [ "live.text[195]", "live.text", 0 ],
			"obj-344::obj-8::obj-13" : [ "Quantize Metro Rate[30]", "Quantize Metro Rate", 0 ],
			"obj-355::obj-8::obj-18" : [ "umenu[64]", "umenu", 0 ],
			"obj-281::obj-358::obj-288::obj-56" : [ "Dropdown Control[107]", "Select", 0 ],
			"obj-281::obj-206" : [ "Full Note Pan", "Pan", 0 ],
			"obj-281::obj-41" : [ "32nd Note Gain", "Gain", 0 ],
			"obj-187::obj-8::obj-34" : [ "live.text[434]", "live.text", 0 ],
			"obj-319::obj-30" : [ "live.text[440]", "live.text[1]", 0 ],
			"obj-397::obj-142" : [ "deltachannel[74]", "deltachannel", 0 ],
			"obj-50" : [ "number[6]", "number[6]", 0 ],
			"obj-155::obj-4::obj-18" : [ "umenu[58]", "umenu", 0 ],
			"obj-96::obj-49::obj-191" : [ "LFO Freq Number[2]", "LFO Freq Number", 0 ],
			"obj-96::obj-33::obj-16::obj-11" : [ "receive_delta_out_channel_num", "Output Channel", 0 ],
			"obj-98::obj-8::obj-34" : [ "live.text[331]", "live.text", 0 ],
			"obj-110::obj-30" : [ "live.text[259]", "live.text[1]", 0 ],
			"obj-158::obj-8::obj-34" : [ "live.text[134]", "live.text", 0 ],
			"obj-361::obj-8::obj-18" : [ "umenu[16]", "umenu", 0 ],
			"obj-281::obj-352::obj-288::obj-56" : [ "Dropdown Control[111]", "Select", 0 ],
			"obj-281::obj-132" : [ "4th Note Map", "4th Note Map", 0 ],
			"obj-281::obj-272" : [ "4th Note Manual Note Speed On", "Note Speed On", 0 ],
			"obj-403::obj-30" : [ "live.text[75]", "live.text[1]", 0 ],
			"obj-225::obj-377::obj-2" : [ "rslider[77]", "rslider[15]", 0 ],
			"obj-203::obj-68" : [ "Mixer / Send UI[9]", "Mixer / Send UI", 0 ],
			"obj-96::obj-33::obj-17::obj-2" : [ "receive_delta_on[1]", "receive_delta_on", 0 ],
			"obj-96::obj-38::obj-110" : [ "live.dial[25]", "live.dial", 0 ],
			"obj-105::obj-142" : [ "deltachannel[40]", "deltachannel", 0 ],
			"obj-142::obj-8::obj-10" : [ "Manual Metro Rate[96]", "Rate", 0 ],
			"obj-330::obj-187" : [ "rslider[86]", "rslider", 0 ],
			"obj-344::obj-12" : [ "ratecontrol[56]", "ratecontrol", 0 ],
			"obj-355::obj-187" : [ "rslider[95]", "rslider", 0 ],
			"obj-6::obj-156" : [ "live.gain~[5]", "live.gain~", 0 ],
			"obj-281::obj-358::obj-288::obj-31" : [ "live.text[272]", "live.text", 0 ],
			"obj-281::obj-295" : [ "32nd Note Map Reset", "Note Map Reset", 0 ],
			"obj-281::obj-2" : [ "32nd Note Manual Note Speed On", "Note Speed On", 0 ],
			"obj-187::obj-30" : [ "live.text[432]", "live.text[1]", 0 ],
			"obj-358::obj-8::obj-10" : [ "Manual Metro Rate[82]", "Rate", 0 ],
			"obj-155::obj-4::obj-56" : [ "Dropdown Control[9]", "Select", 0 ],
			"obj-225::obj-41" : [ "drummatrix_quantized_tempo", "tempo", 0 ],
			"obj-28::obj-42" : [ "Buffer Size[1]", "Buffer Size", 0 ],
			"obj-96::obj-48::obj-50" : [ "number[10]", "number[2]", 0 ],
			"obj-96::obj-48::obj-35" : [ "Ramp Go-To-Point[2]", "Ramp Go-To-Point", 0 ],
			"obj-98::obj-8::obj-18" : [ "umenu[72]", "umenu", 0 ],
			"obj-114::obj-8::obj-18" : [ "umenu[49]", "umenu", 0 ],
			"obj-158::obj-30" : [ "live.text[132]", "live.text[1]", 0 ],
			"obj-361::obj-187" : [ "rslider[90]", "rslider", 0 ],
			"obj-281::obj-352::obj-288::obj-31" : [ "live.text[158]", "live.text", 0 ],
			"obj-281::obj-273" : [ "4th Note Playback Speed", "Playback Speed", 0 ],
			"obj-281::obj-305" : [ "Feed[1]", "Feed", 0 ],
			"obj-281::obj-119" : [ "8th Note Map", "8th Note Map", 0 ],
			"obj-393::obj-16" : [ "multislider[12]", "multislider[2]", 0 ],
			"obj-395::obj-8::obj-34" : [ "live.text[100]", "live.text", 0 ],
			"obj-225::obj-387::obj-2" : [ "rslider[75]", "rslider[15]", 0 ],
			"obj-96::obj-38::obj-111" : [ "live.dial[26]", "live.dial", 0 ],
			"obj-96::obj-38::obj-25" : [ "live.toggle[236]", "live.toggle", 0 ],
			"obj-108::obj-8::obj-18" : [ "umenu[120]", "umenu", 0 ],
			"obj-142::obj-30" : [ "live.text[166]", "live.text[1]", 0 ],
			"obj-9::obj-3" : [ "live.text[78]", "live.text[78]", 0 ],
			"obj-340::obj-8::obj-56" : [ "Dropdown Control[83]", "Select", 0 ],
			"obj-353::obj-8::obj-13" : [ "Quantize Metro Rate[65]", "Quantize Metro Rate", 0 ],
			"obj-281::obj-1" : [ "32nd Note Playback Speed", "Note Speed", 0 ],
			"obj-281::obj-212" : [ "Random 16th notes", "Random", 0 ],
			"obj-281::obj-237" : [ "8th Note random factor", "8th Note random factor", 0 ],
			"obj-318::obj-8::obj-31" : [ "live.text[439]", "live.text", 0 ],
			"obj-358::obj-187" : [ "rslider[106]", "rslider", 0 ],
			"obj-44::obj-4::obj-18" : [ "umenu[127]", "umenu", 0 ],
			"obj-28::obj-29" : [ "Repeat Rate Toggle[1]", "Sync Rate Type", 0 ],
			"obj-96::obj-33::obj-12::obj-2" : [ "receive_delta_on[7]", "receive_delta_on", 0 ],
			"obj-98::obj-9" : [ "live.text[330]", "live.text", 0 ],
			"obj-114::obj-8::obj-31" : [ "live.text[258]", "live.text", 0 ],
			"obj-160::obj-8::obj-10" : [ "Manual Metro Rate[42]", "Rate", 0 ],
			"obj-362::obj-8::obj-18" : [ "umenu[17]", "umenu", 0 ],
			"obj-281::obj-355::obj-288::obj-34" : [ "live.text[273]", "live.text", 0 ],
			"obj-281::obj-118" : [ "16th Note Map", "16th Note Map", 0 ],
			"obj-281::obj-268" : [ "1/2 Note Manual Note Speed On", "Note Speed On", 0 ],
			"obj-393::obj-87" : [ "Module2 Volume[2]", "Vol2", 0 ],
			"obj-395::obj-8::obj-31" : [ "live.text[206]", "live.text", 0 ],
			"obj-225::obj-384::obj-2" : [ "rslider[72]", "rslider[15]", 0 ],
			"obj-53::obj-68" : [ "Mixer / Send UI[18]", "Mixer / Send UI", 0 ],
			"obj-96::obj-38::obj-24" : [ "live.toggle[237]", "live.toggle", 0 ],
			"obj-96::obj-38::obj-107" : [ "live.dial[27]", "live.dial", 0 ],
			"obj-108::obj-8::obj-34" : [ "live.text[265]", "live.text", 0 ],
			"obj-149::obj-8::obj-18" : [ "umenu[106]", "umenu", 0 ],
			"obj-340::obj-30" : [ "live.text[365]", "live.text[1]", 0 ],
			"obj-353::obj-8::obj-18" : [ "umenu[52]", "umenu", 0 ],
			"obj-281::obj-258" : [ "Scramble All", "Scramble", 0 ],
			"obj-318::obj-187" : [ "rslider[104]", "rslider", 0 ],
			"obj-396::obj-8::obj-56" : [ "Dropdown Control[141]", "Select", 0 ],
			"obj-225::obj-283::obj-8" : [ "Velocity Initial Variance Minimum[16]", "Velocity Initial Variance Minimum", 0 ],
			"obj-225::obj-1" : [ "new row z depth", "new row z depth", 0 ],
			"obj-225::obj-20" : [ "live.numbox[3]", "live.numbox[3]", 0 ],
			"obj-96::obj-33::obj-13::obj-11" : [ "receive_delta_out_channel_num[6]", "Output Channel", 0 ],
			"obj-96::obj-12::obj-15" : [ "Slider 3 On", "Slider 3 On", 0 ],
			"obj-96::obj-38::obj-45" : [ "live.dial[6]", "live.dial", 0 ],
			"obj-102::obj-8::obj-13" : [ "Quantize Metro Rate[123]", "Quantize Metro Rate", 0 ],
			"obj-114::obj-30" : [ "live.text[57]", "live.text[1]", 0 ],
			"obj-160::obj-12" : [ "ratecontrol[15]", "ratecontrol", 0 ],
			"obj-362::obj-8::obj-10" : [ "Manual Metro Rate[114]", "Rate", 0 ],
			"obj-342::obj-8::obj-10" : [ "Manual Metro Rate[115]", "Rate", 0 ],
			"obj-281::obj-364::obj-288::obj-12" : [ "Metro On/Off[106]", "Metro On/Off", 0 ],
			"obj-82::obj-8::obj-12" : [ "Metro On/Off[133]", "Metro On/Off", 0 ],
			"obj-395::obj-9" : [ "live.text[99]", "live.text", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "resample.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/probability",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "markov_init.js",
				"bootpath" : "~/12c/12c_sandbox/probability/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_drummatrix2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "drummatrix_cell.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "matrix_note.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI/drum_matrix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "note_variance.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI/drum_matrix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "solo.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "mute.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "lock.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "drummatrix_patterns.json",
				"bootpath" : "~/12c/12c_sandbox/instruments/mpc",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "velocity_variance.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI/drum_matrix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_ui_panning.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_output_channel.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "stutter.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_subdivide.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_LFO.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ramp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_delta_to_CC.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_receive_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_multiplex.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ok.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mux_toggle_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_slider2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_db_data.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "001.txt",
				"bootpath" : "~/12c/12c_sandbox/db",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_slider.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "randomness_feedback.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "hilo_limit.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ctrl_audio.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "2ch_Amp_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "amp_delta_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "in_phase.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "1ch_amp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bpatcher_name.js",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_harmonic_synth.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_dihedral2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "create_slice_msg.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dihed_rand.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dihed_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dh_markov2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jam_change.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_dihedral.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "fractal_beat2.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "scramble_matrix_row.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "randomize_matrix_row.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_reverb.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_reverb_matrix.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_synthbuild.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_oscillator.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_filter.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_stepper.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "hipass.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
