{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 49.0, 79.0, 1067.0, 683.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-52",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 309.991882, 1011.0, 565.0, 47.0 ],
					"style" : "",
					"text" : "This connects the Saffire inputs 1-8 to Live via soundflower. While admittedly a bit hacky, I could not get multi-output devices to work reliably in Mac OS, so I'm sending everything to Live from Max via soundflower, which means I need to receive Saffire inputs here and send them to Live accordingly."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 824.983765, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 751.983765, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 19"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 676.650391, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 18"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 601.983765, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 527.991882, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 454.991882, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 15"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 382.658539, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 309.991882, 1102.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 13"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 824.983765, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 751.983765, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 676.650391, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 601.983765, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 527.991882, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 454.991882, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 382.658539, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 309.991882, 1067.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "adc~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1291.466797, 857.666687, 36.0, 22.0 ],
					"style" : "",
					"text" : "*~ 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1291.466797, 894.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 13"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1291.466797, 823.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1378.233276, 785.0, 111.666672, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.800049, 342.533264, 52.533394, 29.0 ],
					"style" : "",
					"text" : "verb"
				}

			}
, 			{
				"box" : 				{
					"args" : [ -1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-19",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui_panning.maxpat",
					"numinlets" : 0,
					"numoutlets" : 13,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 1291.466797, 518.533325, 229.0, 259.466644 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.800049, 207.233154, 224.266113, 144.466644 ],
					"varname" : "midiverb_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 789.399963, 823.0, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 12"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.25, 2.25 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-7",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1356.333252, 303.433258, 103.347473, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 74.266663, 210.533356, 152.93338, 22.999996 ],
					"varname" : "Dihedral Speed",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 370.666656, 433.666687, 58.0, 22.0 ],
					"restore" : 					{
						"Filter" : [ 1, 0, 1, 1, 0, 0, 3033.799316, 1.052274, 0.8 ],
						"Stutter Rate" : [ 1.0 ],
						"Stutter Speed" : [ 1.0 ],
						"live.text[103]" : [ 1.0 ]
					}
,
					"style" : "",
					"text" : "autopattr",
					"varname" : "u214003079"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 42.516876, 501.533386, 218.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 45, 749, 737 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"style" : "",
					"text" : "pattrstorage @savemode 1 @greedy 1",
					"varname" : "u963000711"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "mixer" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_preset_select.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 42.516876, 433.666687, 315.0, 59.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.333435, 0.766876, 453.666168, 59.0 ],
					"varname" : "_preset_select",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1036.033447, 22.53331, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 525.06665, 539.799561, 154.000046, 22.999996 ],
					"varname" : "Playback Speed",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1036.033447, 63.63327, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 525.06665, 515.399414, 154.000046, 22.999996 ],
					"varname" : "Metro Speed",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 1,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-332",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 211.0, 160.833389, 29.5, 22.0 ],
									"style" : "",
									"text" : "0.8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-329",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 170.5, 160.833389, 29.5, 22.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-300",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 113.0, 160.833389, 44.0, 22.0 ],
									"style" : "",
									"text" : "22500"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-289",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 50.0, 111.333374, 60.0, 22.0 ],
									"style" : "",
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 160.833389, 51.0, 22.0 ],
									"style" : "",
									"text" : "mode 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-356",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 590.999939, 100.0, 18.0, 20.0 ],
									"style" : "",
									"text" : "q"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-354",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 502.999939, 100.0, 34.0, 20.0 ],
									"style" : "",
									"text" : "gain"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-352",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 366.999939, 125.333389, 32.0, 20.0 ],
									"style" : "",
									"text" : "freq"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-269",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 575.999939, 125.333389, 48.0, 23.0 ],
									"style" : "",
									"text" : "set $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-272",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 495.999939, 125.333389, 48.0, 23.0 ],
									"style" : "",
									"text" : "set $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-286",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 400.999939, 125.333389, 48.0, 23.0 ],
									"style" : "",
									"text" : "set $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-291",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 575.999939, 160.833389, 55.0, 23.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-294",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 495.999939, 160.833389, 55.0, 23.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-295",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 400.999939, 160.833389, 57.0, 23.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-45",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 400.999939, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-46",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 435.999939, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-47",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 495.999939, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-48",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 530.999939, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-49",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 575.999939, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-54",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 610.999939, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-56",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 281.400146, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-58",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 400.999939, 281.400146, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-59",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 495.999939, 281.400146, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-60",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 575.999939, 281.400146, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-291", 0 ],
									"disabled" : 0,
									"hidden" : 1,
									"source" : [ "obj-269", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-294", 0 ],
									"disabled" : 0,
									"hidden" : 1,
									"source" : [ "obj-272", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-295", 0 ],
									"disabled" : 0,
									"hidden" : 1,
									"source" : [ "obj-286", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-289", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-300", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 59.5, 152.000015, 122.5, 152.000015 ],
									"source" : [ "obj-289", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-329", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 59.5, 152.000015, 179.999969, 152.000015, 180.0, 159.000015 ],
									"source" : [ "obj-289", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-332", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 59.5, 150.000015, 220.5, 150.000015 ],
									"source" : [ "obj-289", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-291", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-294", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-295", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-295", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 122.5, 199.000015, 239.999969, 199.000015, 239.999969, 122.000015, 353.999969, 122.000015, 353.999969, 153.000015, 410.499939, 153.000015 ],
									"source" : [ "obj-300", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-294", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 180.0, 207.000015, 247.999969, 207.000015, 247.999969, 109.000015, 479.999969, 109.000015, 479.999969, 151.000015, 505.499939, 151.000015 ],
									"source" : [ "obj-329", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-291", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 220.5, 218.000015, 253.999969, 218.000015, 253.999969, 101.000015, 563.999969, 101.000015, 563.999969, 153.000015, 585.499939, 153.000015 ],
									"source" : [ "obj-332", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-286", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-295", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-272", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-294", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-269", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-291", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-54", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 169.000168, 124.933273, 71.5, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p init"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1956.891846, 640.266479, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1906.891846, 640.266479, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1855.891846, 640.266479, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2008.891846, 566.199829, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1956.891846, 566.199829, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1906.891846, 566.199829, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1855.891846, 566.199829, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2008.891846, 489.533203, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1956.891846, 489.533203, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1906.891846, 489.533203, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1855.891846, 489.533203, 45.0, 22.0 ],
					"style" : "",
					"text" : "hipass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1046.5, 152.333328, 57.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.800049, 543.699707, 57.0, 22.0 ],
					"style" : "",
					"text" : "_stepper",
					"varname" : "_stepper"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-398",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 403.666656, 636.333313, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 225.200043, 582.233093, 40.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "live.text[103]",
							"parameter_shortname" : "live.text[103]",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 1 ]
						}

					}
,
					"text" : "quant",
					"texton" : "quant",
					"varname" : "live.text[103]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1275.399902, 338.799927, 80.047424, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 10.933435, 235.633301, 67.0, 22.0 ],
					"style" : "",
					"text" : "_dihedral2",
					"varname" : "_dihedral2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-388",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1956.891846, 675.933105, 53.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 11"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-389",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1906.891846, 675.933105, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-390",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1855.891846, 675.933105, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-383",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2008.891846, 602.933105, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-384",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1956.891846, 602.933105, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-385",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1906.891846, 602.933105, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-386",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1855.891846, 602.933105, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-382",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2008.891846, 528.33313, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-381",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1959.399902, 528.33313, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-380",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1906.891846, 528.33313, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-379",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1855.891846, 528.33313, 47.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-368",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1046.5, 194.666656, 114.0, 22.0 ],
					"style" : "",
					"text" : "send~ stepper_aud"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-363",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 870.066589, 303.433258, 101.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.800049, 512.699707, 52.533394, 29.0 ],
					"style" : "",
					"text" : "step"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-364",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 796.066589, 306.933258, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 10, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-365",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 796.066589, 22.53331, 225.666672, 268.466675 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.800049, 369.533264, 221.0, 144.466644 ],
					"varname" : "stepper_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.75, 6.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-362",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.80011, 32.166565, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 296.199982, 235.633301, 154.000046, 22.999996 ],
					"varname" : "Filter Gain",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.5, 10.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-361",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.80011, 71.63327, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 296.199982, 260.633301, 154.000046, 22.999996 ],
					"varname" : "Filter Q",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 50.0, 15000.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-360",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 128.516876, 32.166565, 152.466583, 24.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 296.199982, 211.233154, 154.000046, 22.999996 ],
					"varname" : "Filter Freq",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"id" : "obj-298",
					"linmarkers" : [ 0.0, 11025.0, 16537.5 ],
					"logmarkers" : [ 0.0, 100.0, 1000.0, 10000.0 ],
					"maxclass" : "filtergraph~",
					"nfilters" : 1,
					"numinlets" : 8,
					"numoutlets" : 7,
					"outlettype" : [ "list", "float", "float", "float", "float", "list", "int" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 65.000153, 178.099915, 360.0, 155.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 229.200043, 285.533325, 221.0, 56.93335 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Filter",
							"parameter_shortname" : "Filter",
							"parameter_type" : 3,
							"parameter_initial" : [ 1, 0, 1, 1, 0, 0, 3422.510742, 1.277545, 0.8 ],
							"parameter_invisible" : 1
						}

					}
,
					"setfilter" : [ 0, 1, 1, 0, 0, 3033.799316, 1.052274, 0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ],
					"style" : "",
					"varname" : "Filter"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-346",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 65.000153, 361.13324, 92.0, 23.0 ],
					"style" : "",
					"text" : "biquad~"
				}

			}
, 			{
				"box" : 				{
					"attr" : "edit_mode",
					"attr_display" : 1,
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"id" : "obj-347",
					"lock" : 1,
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 65.000153, 112.933273, 83.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 229.200043, 235.633301, 63.0, 40.0 ],
					"style" : "",
					"text_width" : 83.0,
					"varname" : "FilterType"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-309",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1275.399902, 369.799927, 116.0, 22.0 ],
					"style" : "",
					"text" : "send~ dihedral_aud"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-285",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1272.923584, 397.433289, 85.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 2.333435, 207.533356, 80.0, 29.0 ],
					"style" : "",
					"text" : "dihedra"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-287",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1275.399902, 306.933289, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 7, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-301",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1275.399902, 22.53331, 227.666672, 268.466675 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.333435, 64.066711, 221.0, 144.466644 ],
					"varname" : "dihedral_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.001, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-160",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 319.200165, 581.666687, 107.0, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 286.200043, 534.233093, 162.000015, 22.999996 ],
					"varname" : "stutter rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.1, 4.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-142",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 204.000153, 581.666687, 107.0, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 286.200043, 512.699707, 162.000015, 22.999996 ],
					"varname" : "speed",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-90",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 557.091919, 787.533325, 79.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 225.200043, 509.699707, 67.0, 29.0 ],
					"style" : "",
					"text" : "stutter"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-88",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1535.399902, 241.533188, 57.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 542.200012, 33.066711, 57.0, 29.0 ],
					"style" : "",
					"text" : "main"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-85",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 552.399902, 289.0, 73.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 243.200043, 207.233154, 49.0, 29.0 ],
					"style" : "",
					"text" : "filter"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 20.0,
					"id" : "obj-84",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 881.399963, 785.0, 74.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.333435, 509.699707, 74.0, 29.0 ],
					"style" : "",
					"text" : "clouds"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 204.000153, 611.833313, 196.5, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 266.700073, 559.233093, 196.5, 20.0 ],
					"style" : "",
					"text" : "speed         rate        bars         gen"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 370.666656, 636.333313, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 430.866547, 581.233093, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "number",
					"maximum" : 16,
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 316.000153, 636.333313, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 370.700073, 581.233093, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-51",
					"maxclass" : "flonum",
					"maximum" : 2.0,
					"minimum" : 0.01,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 261.000153, 636.333313, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 318.700073, 581.233093, 50.0, 22.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "number[7]",
							"parameter_shortname" : "number[6]",
							"parameter_type" : 3,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 1.0 ],
							"parameter_invisible" : 1
						}

					}
,
					"style" : "",
					"varname" : "Stutter Rate"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-50",
					"maxclass" : "flonum",
					"maximum" : 4.0,
					"minimum" : 0.1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 204.000153, 636.333313, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 266.700073, 581.233093, 50.0, 22.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "number[6]",
							"parameter_shortname" : "number[6]",
							"parameter_type" : 3,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 1.0 ],
							"parameter_invisible" : 1
						}

					}
,
					"style" : "",
					"varname" : "Stutter Speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 191.500153, 672.333313, 209.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 229.200043, 540.699707, 55.0, 22.0 ],
					"style" : "",
					"text" : "stutter",
					"varname" : "stutter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 469.158539, 791.93335, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 789.399963, 785.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 469.158539, 296.000061, 66.0, 22.0 ],
					"style" : "",
					"text" : "clip~ -1. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 5, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-53",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 469.158539, 23.53331, 227.0, 259.466644 ],
					"presentation" : 1,
					"presentation_rect" : [ 229.200043, 64.066711, 221.0, 144.466644 ],
					"varname" : "filter_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 789.399963, 518.533325, 226.666672, 259.466644 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.333435, 369.533264, 221.0, 144.466644 ],
					"varname" : "reverb_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 6, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0, -70.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-204",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 469.158539, 513.533386, 230.666672, 264.466614 ],
					"presentation" : 1,
					"presentation_rect" : [ 229.200043, 369.533264, 221.0, 142.466644 ],
					"varname" : "stutter_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ -1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-203",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "mixer_ui_panning.maxpat",
					"numinlets" : 0,
					"numoutlets" : 13,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 1597.399902, 22.53331, 225.0, 142.333252 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.800049, 64.066711, 224.266113, 144.466644 ],
					"varname" : "main_mixer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 191.500153, 705.600037, 107.0, 22.0 ],
					"style" : "",
					"text" : "send~ stutter_aud"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2074.991699, 467.266479, 89.0, 22.0 ],
					"style" : "",
					"text" : "loadmess start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 65.000153, 390.033203, 96.0, 22.0 ],
					"style" : "",
					"text" : "send~ filter_aud"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.923178, 0.969813, 0.949165, 0.964511 ],
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-64",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 222.000153, 798.333374, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -0.199957, -0.233124, 711.351562, 605.233154 ],
					"proportion" : 0.39,
					"rounded" : 0,
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1045.533447, 54.0, 1217.0, 54.0, 1217.0, 142.0, 1094.0, 142.0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-381", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-379", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 517.533203, 1865.391846, 517.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 518.533203, 1916.391846, 518.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-381", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 517.533203, 1968.899902, 517.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-382", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 518.533203, 2018.391846, 518.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-383", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 559.533203, 2018.391846, 559.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-384", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 557.533203, 1966.391846, 557.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 556.533203, 1916.391846, 556.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-386", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 556.533203, 1865.391846, 556.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-388", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 590.533203, 1966.391846, 590.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-389", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 591.533203, 1916.391846, 591.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2084.491699, 592.533203, 1865.391846, 592.533203 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 328.700165, 619.533323, 270.500153, 619.533323 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-383", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1045.533447, 102.0, 1033.0, 102.0, 1033.0, 140.0, 1075.0, 140.0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1675.566569, 343.533203, 1672.333252, 343.533203, 1672.333252, 468.533203, 1966.391846, 468.533203 ],
					"source" : [ "obj-203", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1692.733235, 468.533203, 2018.391846, 468.533203 ],
					"source" : [ "obj-203", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1761.399902, 343.533203, 1671.333252, 343.533203, 1671.333252, 468.533203, 2018.391846, 468.533203 ],
					"source" : [ "obj-203", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1744.233235, 343.533203, 1670.333252, 343.533203, 1670.333252, 468.533203, 1966.391846, 468.533203 ],
					"source" : [ "obj-203", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1727.066569, 342.533203, 1672.333252, 342.533203, 1672.333252, 468.533203, 1916.391846, 468.533203 ],
					"source" : [ "obj-203", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1709.899902, 341.533203, 1668.333252, 341.533203, 1668.333252, 470.533203, 1865.391846, 470.533203 ],
					"source" : [ "obj-203", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1812.899902, 343.533203, 1674.333252, 343.533203, 1674.333252, 467.533203, 1966.391846, 467.533203 ],
					"source" : [ "obj-203", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1795.733235, 343.533203, 1670.333252, 343.533203, 1670.333252, 467.533203, 1916.391846, 467.533203 ],
					"source" : [ "obj-203", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1641.233235, 340.533203, 1671.333252, 340.533203, 1671.333252, 465.533203, 1865.391846, 465.533203 ],
					"source" : [ "obj-203", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1778.566569, 341.533203, 1672.333252, 341.533203, 1672.333252, 468.533203, 1865.391846, 468.533203 ],
					"source" : [ "obj-203", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1658.399902, 341.533203, 1674.333252, 341.533203, 1674.333252, 466.533203, 1916.391846, 466.533203 ],
					"source" : [ "obj-203", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-384", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-386", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-388", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-368", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-389", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-346", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-298", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 245.000153, 349.800032, 456.266761, 349.800032, 456.266761, 104.200029, 220.500168, 104.200029 ],
					"source" : [ "obj-298", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 188.16682, 342.600032, 444.266761, 342.600032, 444.266761, 115.400029, 199.500168, 115.400029 ],
					"source" : [ "obj-298", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 131.333486, 339.400032, 53.866755, 339.400032, 53.866755, 104.200029, 178.500168, 104.200029 ],
					"source" : [ "obj-298", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-379", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-301", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-346", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-347", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 138.016876, 90.600028, 189.000168, 90.600028 ],
					"source" : [ "obj-360", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 297.30011, 110.600029, 231.000168, 110.600029 ],
					"source" : [ "obj-361", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 297.30011, 62.600028, 210.000168, 62.600028 ],
					"source" : [ "obj-362", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 805.566589, 342.0, 1031.0, 342.0, 1031.0, 144.0, 1056.0, 144.0 ],
					"source" : [ "obj-364", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-364", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-365", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 413.166656, 667.333313, 391.000153, 667.333313 ],
					"source" : [ "obj-398", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 52.016876, 532.0, 34.0, 532.0, 34.0, 424.0, 52.016876, 424.0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 213.500153, 663.333313, 239.000153, 663.333313 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 270.500153, 664.333313, 277.000153, 664.333313 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-309", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 231.000168, 159.400029, 415.500153, 159.400029 ],
					"source" : [ "obj-62", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 213.500168, 165.00003, 366.785867, 165.00003 ],
					"source" : [ "obj-62", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 196.000168, 169.00003, 318.071582, 169.00003 ],
					"source" : [ "obj-62", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 178.500168, 165.00003, 74.500153, 165.00003 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 325.500153, 663.333313, 315.000153, 663.333313 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 380.166656, 666.333313, 353.000153, 666.333313 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-346", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 478.658539, 351.0, 74.0, 351.0, 74.500153, 360.0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1365.833252, 333.0, 1345.947326, 333.0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 478.658539, 823.0, 461.0, 823.0, 461.0, 572.0, 201.000153, 572.0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-360::obj-8::obj-56" : [ "Dropdown Control[3]", "Select", 0 ],
			"obj-6::obj-155" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-260::obj-104" : [ "Master[1]", "Master", 0 ],
			"obj-28::obj-90" : [ "Rate quantize", "Rate quantize", 0 ],
			"obj-160::obj-8::obj-34" : [ "live.text[40]", "live.text", 0 ],
			"obj-361::obj-8::obj-34" : [ "live.text[11]", "live.text", 0 ],
			"obj-1::obj-8::obj-13" : [ "Quantize Metro Rate[7]", "Quantize Metro Rate", 0 ],
			"obj-28::obj-91" : [ "Speed quantize", "Speed quantize", 0 ],
			"obj-362::obj-8::obj-34" : [ "live.text[15]", "live.text", 0 ],
			"obj-6::obj-126" : [ "2nd Note Playback Speed", "Note Speed", 0 ],
			"obj-260::obj-291" : [ "Manual Sample Duration[2]", "Buffer", 0 ],
			"obj-1::obj-30" : [ "live.text[66]", "live.text[1]", 0 ],
			"obj-19::obj-68" : [ "Mixer / Send UI[8]", "Mixer / Send UI", 0 ],
			"obj-360::obj-187" : [ "rslider[7]", "rslider", 0 ],
			"obj-6::obj-84" : [ "DH1", "DH1", 0 ],
			"obj-7::obj-8::obj-18" : [ "umenu[8]", "umenu", 0 ],
			"obj-28::obj-2::obj-21" : [ "Jam Bars Button", "Jam Bars Button", 0 ],
			"obj-260::obj-349" : [ "Quantized Sample Duration[2]", "Quantized Sample Duration", 0 ],
			"obj-2::obj-8::obj-56" : [ "Dropdown Control[6]", "Select", 0 ],
			"obj-2::obj-187" : [ "rslider[10]", "rslider", 0 ],
			"obj-142::obj-8::obj-13" : [ "Quantize Metro Rate[16]", "Quantize Metro Rate", 0 ],
			"obj-142::obj-8::obj-34" : [ "live.text[33]", "live.text", 0 ],
			"obj-6::obj-91" : [ "DH8", "DH8", 0 ],
			"obj-2::obj-8::obj-31" : [ "live.text[44]", "live.text", 0 ],
			"obj-28::obj-2::obj-30" : [ "Loop Jam On/Off", "Loop Jam On/Off", 0 ],
			"obj-361::obj-30" : [ "live.text[14]", "live.text[1]", 0 ],
			"obj-362::obj-30" : [ "live.text[17]", "live.text[1]", 0 ],
			"obj-1::obj-187" : [ "rslider[11]", "rslider", 0 ],
			"obj-7::obj-9" : [ "live.text[50]", "live.text", 0 ],
			"obj-4::obj-68" : [ "Mixer / Send UI[2]", "Mixer / Send UI", 0 ],
			"obj-6::obj-220" : [ "DHNotes8", "DHNotes8", 0 ],
			"obj-260::obj-18" : [ "rslider[117]", "rslider", 0 ],
			"obj-51" : [ "number[7]", "number[6]", 0 ],
			"obj-160::obj-8::obj-10" : [ "Manual Metro Rate[3]", "Rate", 0 ],
			"obj-160::obj-9" : [ "live.text[10]", "live.text", 0 ],
			"obj-360::obj-8::obj-10" : [ "Manual Metro Rate[4]", "Rate", 0 ],
			"obj-53::obj-68" : [ "Mixer / Send UI[3]", "Mixer / Send UI", 0 ],
			"obj-28::obj-85" : [ "live.text", "live.text", 0 ],
			"obj-142::obj-9" : [ "live.text[39]", "live.text", 0 ],
			"obj-6::obj-153" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-7::obj-8::obj-13" : [ "Quantize Metro Rate[8]", "Quantize Metro Rate", 0 ],
			"obj-28::obj-42" : [ "Buffer Size", "Buffer Size", 0 ],
			"obj-360::obj-8::obj-12" : [ "Metro On/Off[4]", "Metro On/Off", 0 ],
			"obj-361::obj-8::obj-31" : [ "live.text[12]", "live.text", 0 ],
			"obj-362::obj-12" : [ "ratecontrol[10]", "ratecontrol", 0 ],
			"obj-2::obj-8::obj-34" : [ "live.text[45]", "live.text", 0 ],
			"obj-142::obj-8::obj-10" : [ "Manual Metro Rate[11]", "Rate", 0 ],
			"obj-7::obj-30" : [ "live.text[51]", "live.text[1]", 0 ],
			"obj-19::obj-113" : [ "panning[2]", "panning", 0 ],
			"obj-28::obj-73" : [ "Sample Playback Pitch", "Pitch", 0 ],
			"obj-360::obj-8::obj-18" : [ "umenu[3]", "umenu", 0 ],
			"obj-361::obj-8::obj-56" : [ "Dropdown Control[4]", "Select", 0 ],
			"obj-361::obj-8::obj-10" : [ "Manual Metro Rate[5]", "Rate", 0 ],
			"obj-6::obj-124" : [ "8th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-28::obj-2::obj-11" : [ "Gen New Bars Button", "Gen New Bars Button", 0 ],
			"obj-160::obj-8::obj-12" : [ "Metro On/Off[3]", "Metro On/Off", 0 ],
			"obj-362::obj-187" : [ "rslider[9]", "rslider", 0 ],
			"obj-260::obj-16" : [ "NoteAmp", "NoteAmp", 0 ],
			"obj-360::obj-12" : [ "ratecontrol[8]", "ratecontrol", 0 ],
			"obj-7::obj-187" : [ "rslider[12]", "rslider", 0 ],
			"obj-28::obj-2::obj-27" : [ "Loop Jam Bars", "Loop Jam Bars", 0 ],
			"obj-28::obj-2::obj-26" : [ "Values", "Values", 0 ],
			"obj-361::obj-12" : [ "ratecontrol[9]", "ratecontrol", 0 ],
			"obj-142::obj-12" : [ "ratecontrol[4]", "ratecontrol", 0 ],
			"obj-6::obj-182" : [ "DHNotes32", "DHNotes32", 0 ],
			"obj-2::obj-30" : [ "live.text[46]", "live.text[1]", 0 ],
			"obj-28::obj-2::obj-45" : [ "Range Control", "Range Control", 0 ],
			"obj-28::obj-2::obj-15" : [ "Loop On/Off[8]", "Loop On/Off", 0 ],
			"obj-362::obj-8::obj-10" : [ "Manual Metro Rate[6]", "Rate", 0 ],
			"obj-6::obj-349" : [ "Quantized Sample Duration[1]", "Quantized Sample Duration", 0 ],
			"obj-6::obj-93" : [ "multislider[3]", "multislider[3]", 0 ],
			"obj-160::obj-8::obj-56" : [ "Dropdown Control[2]", "Select", 0 ],
			"obj-6::obj-156" : [ "live.gain~[5]", "live.gain~", 0 ],
			"obj-7::obj-8::obj-12" : [ "Metro On/Off[9]", "Metro On/Off", 0 ],
			"obj-7::obj-8::obj-56" : [ "Dropdown Control[8]", "Select", 0 ],
			"obj-203::obj-68" : [ "Mixer / Send UI[6]", "Mixer / Send UI", 0 ],
			"obj-28::obj-22" : [ "Repeat Rate Adjust", "Rate", 0 ],
			"obj-301::obj-68" : [ "Mixer / Send UI[4]", "Mixer / Send UI", 0 ],
			"obj-361::obj-8::obj-13" : [ "Quantize Metro Rate[4]", "Quantize Metro Rate", 0 ],
			"obj-2::obj-9" : [ "live.text[47]", "live.text", 0 ],
			"obj-7::obj-8::obj-10" : [ "Manual Metro Rate[13]", "Rate", 0 ],
			"obj-6::obj-122" : [ "32nd Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-6::obj-273" : [ "Master", "Master", 0 ],
			"obj-1::obj-8::obj-31" : [ "live.text[65]", "live.text", 0 ],
			"obj-160::obj-8::obj-13" : [ "Quantize Metro Rate[17]", "Quantize Metro Rate", 0 ],
			"obj-6::obj-92" : [ "DH16", "DH16", 0 ],
			"obj-2::obj-8::obj-13" : [ "Quantize Metro Rate[6]", "Quantize Metro Rate", 0 ],
			"obj-28::obj-2::obj-25" : [ "Jam Bars On/Off", "Jam Bars On/Off", 0 ],
			"obj-2::obj-12" : [ "ratecontrol[11]", "ratecontrol", 0 ],
			"obj-360::obj-8::obj-31" : [ "live.text[42]", "live.text", 0 ],
			"obj-6::obj-195" : [ "DHLoop", "DHLoop", 0 ],
			"obj-1::obj-9" : [ "live.text[67]", "live.text", 0 ],
			"obj-7::obj-8::obj-31" : [ "live.text[49]", "live.text", 0 ],
			"obj-7::obj-12" : [ "ratecontrol[13]", "ratecontrol", 0 ],
			"obj-362::obj-9" : [ "live.text[18]", "live.text", 0 ],
			"obj-204::obj-68" : [ "Mixer / Send UI[1]", "Mixer / Send UI", 0 ],
			"obj-142::obj-8::obj-12" : [ "Metro On/Off[16]", "Metro On/Off", 0 ],
			"obj-6::obj-127" : [ "Full Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-6::obj-154" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-28::obj-103" : [ "Rate Multislider", "Rate Multislider", 0 ],
			"obj-6::obj-265" : [ "multislider", "multislider", 0 ],
			"obj-260::obj-48" : [ "live.menu[4]", "live.menu", 0 ],
			"obj-360::obj-30" : [ "live.text[43]", "live.text[1]", 0 ],
			"obj-298" : [ "Filter", "Filter", 0 ],
			"obj-28::obj-76" : [ "Speed Multislider", "Speed Multislider", 0 ],
			"obj-142::obj-187" : [ "rslider[5]", "rslider", 0 ],
			"obj-160::obj-8::obj-18" : [ "umenu[2]", "umenu", 0 ],
			"obj-6::obj-123" : [ "16th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-362::obj-8::obj-18" : [ "umenu[5]", "umenu", 0 ],
			"obj-7::obj-8::obj-34" : [ "live.text[68]", "live.text", 0 ],
			"obj-362::obj-8::obj-31" : [ "live.text[16]", "live.text", 0 ],
			"obj-365::obj-68" : [ "Mixer / Send UI[7]", "Mixer / Send UI", 0 ],
			"obj-6::obj-35" : [ "Complete speed control", "live.numbox", 0 ],
			"obj-260::obj-113" : [ "number[29]", "number[29]", 0 ],
			"obj-260::obj-163" : [ "number[28]", "number[28]", 0 ],
			"obj-160::obj-187" : [ "rslider[6]", "rslider", 0 ],
			"obj-260::obj-17" : [ "NoteLocation", "NoteLocation", 0 ],
			"obj-28::obj-101" : [ "live.text[63]", "live.text", 0 ],
			"obj-6::obj-90" : [ "DH4", "DH4", 0 ],
			"obj-6::obj-26" : [ "Feed", "Feed", 0 ],
			"obj-28::obj-2::obj-28" : [ "Loop Jam Bars Button", "Loop Jam Bars Button", 0 ],
			"obj-142::obj-8::obj-56" : [ "Dropdown Control[14]", "Select", 0 ],
			"obj-360::obj-8::obj-13" : [ "Quantize Metro Rate[3]", "Quantize Metro Rate", 0 ],
			"obj-6::obj-74" : [ "live.text[425]", "live.text", 0 ],
			"obj-28::obj-2::obj-18" : [ "Jam Bars[4]", "Jam Bars", 0 ],
			"obj-142::obj-8::obj-31" : [ "live.text[34]", "live.text", 0 ],
			"obj-160::obj-8::obj-31" : [ "live.text[35]", "live.text", 0 ],
			"obj-6::obj-223" : [ "DHNotes4", "DHNotes4", 0 ],
			"obj-1::obj-8::obj-34" : [ "live.text[48]", "live.text", 0 ],
			"obj-50" : [ "number[6]", "number[6]", 0 ],
			"obj-203::obj-113" : [ "panning[1]", "panning", 0 ],
			"obj-28::obj-2::obj-57" : [ "Smooth Output", "Smooth Output", 0 ],
			"obj-1::obj-8::obj-10" : [ "Manual Metro Rate[12]", "Rate", 0 ],
			"obj-362::obj-8::obj-56" : [ "Dropdown Control[5]", "Select", 0 ],
			"obj-6::obj-152" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-398" : [ "live.text[103]", "live.text[103]", 0 ],
			"obj-1::obj-8::obj-12" : [ "Metro On/Off[8]", "Metro On/Off", 0 ],
			"obj-6::obj-264" : [ "live.gain~[6]", "live.gain~[6]", 0 ],
			"obj-2::obj-8::obj-12" : [ "Metro On/Off[7]", "Metro On/Off", 0 ],
			"obj-28::obj-29" : [ "Repeat Rate Toggle", "Repeat Rate Toggle", 0 ],
			"obj-361::obj-187" : [ "rslider[8]", "rslider", 0 ],
			"obj-2::obj-8::obj-18" : [ "umenu[6]", "umenu", 0 ],
			"obj-2::obj-8::obj-10" : [ "Manual Metro Rate[7]", "Rate", 0 ],
			"obj-361::obj-8::obj-18" : [ "umenu[4]", "umenu", 0 ],
			"obj-6::obj-125" : [ "4th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-142::obj-8::obj-18" : [ "umenu[14]", "umenu", 0 ],
			"obj-142::obj-30" : [ "live.text[38]", "live.text[1]", 0 ],
			"obj-360::obj-9" : [ "live.text[64]", "live.text", 0 ],
			"obj-362::obj-8::obj-12" : [ "Metro On/Off[6]", "Metro On/Off", 0 ],
			"obj-260::obj-12" : [ "MetroSpeed", "MetroSpeed", 0 ],
			"obj-1::obj-8::obj-18" : [ "umenu[7]", "umenu", 0 ],
			"obj-6::obj-89" : [ "DH2", "DH2", 0 ],
			"obj-6::obj-151" : [ "live.gain~", "live.gain~", 0 ],
			"obj-28::obj-2::obj-20" : [ "Loop Controller", "Loop Controller", 0 ],
			"obj-6::obj-140" : [ "live.toggle[18]", "live.toggle", 0 ],
			"obj-260::obj-63" : [ "playback speed", "playback speed", 0 ],
			"obj-1::obj-8::obj-56" : [ "Dropdown Control[7]", "Select", 0 ],
			"obj-160::obj-12" : [ "ratecontrol[7]", "ratecontrol", 0 ],
			"obj-360::obj-8::obj-34" : [ "live.text[41]", "live.text", 0 ],
			"obj-361::obj-8::obj-12" : [ "Metro On/Off[5]", "Metro On/Off", 0 ],
			"obj-6::obj-432" : [ "toggle", "toggle", 0 ],
			"obj-6::obj-212" : [ "DHNotes16", "DHNotes16", 0 ],
			"obj-1::obj-12" : [ "ratecontrol[12]", "ratecontrol", 0 ],
			"obj-28::obj-2::obj-43" : [ "Number of Bars", "Bars", 0 ],
			"obj-160::obj-30" : [ "live.text[9]", "live.text[1]", 0 ],
			"obj-361::obj-9" : [ "live.text[13]", "live.text", 0 ],
			"obj-362::obj-8::obj-13" : [ "Quantize Metro Rate[5]", "Quantize Metro Rate", 0 ],
			"obj-6::obj-291" : [ "Manual Sample Duration[1]", "Buffer", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "mixer_ui_panning.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_output_channel.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_feedback_reduction.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "stutter.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_subdivide.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jam_change.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_markov2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/probability",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "markov_init.js",
				"bootpath" : "~/12c/12c_sandbox/probability/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bpatcher_name.js",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_dihedral2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "create_slice_msg.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dihed_rand.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dihed_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dh_markov2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_stepper.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "hipass.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_preset_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "filename concat",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
