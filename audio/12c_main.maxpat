{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 45.0, 79.0, 1201.0, 658.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-3",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_main_instruments.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1706.0, 161.000061, 841.0, 1162.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.292969, 670.599976, 2372.666748, 496.200073 ],
					"varname" : "_main_instruments",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_main_mixer_sends.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 936.5, 646.000061, 696.800049, 606.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.292969, 85.066681, 715.192017, 586.533325 ],
					"varname" : "_main_mixer_sends",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ -122.514145, 67.533356, 58.0, 22.0 ],
					"restore" : 					{
						"live.text[1]" : [ 0.0 ],
						"live.text[2]" : [ 0.0 ]
					}
,
					"style" : "",
					"text" : "autopattr",
					"varname" : "u379008488"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-81",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 82.733398, -69.333313, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -108.666748, -33.333313, 54.0, 22.0 ],
					"style" : "",
					"text" : "store $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -106.666748, -69.333313, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -42.0, -33.333313, 91.0, 22.0 ],
					"style" : "",
					"text" : "storagewindow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -42.0, 2.0, 249.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 45, 749, 737 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"style" : "",
					"text" : "pattrstorage basic @savemode 1 @greedy 1",
					"varname" : "basic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 977.0, 496.400024, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-37",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 977.0, 461.733398, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1646.333252, 59.266872, 154.000046, 22.999996 ],
					"varname" : "DB2 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 977.0, 336.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-34",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 977.0, 301.333374, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1646.333252, 34.266876, 154.000046, 22.999996 ],
					"varname" : "DB1 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 972.0, 575.400024, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-31",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 972.0, 540.733398, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1483.819092, 59.266872, 154.000046, 22.999996 ],
					"varname" : "Slider4 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.0, 575.400024, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-25",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.0, 540.733398, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1327.819092, 59.266876, 154.000046, 22.999996 ],
					"varname" : "Slider3 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 704.0, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-16",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 704.0, 377.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1483.819092, 34.266876, 154.000046, 22.999996 ],
					"varname" : "Ramp4 Dur",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 589.48584, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-20",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 589.48584, 377.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1327.819092, 34.266876, 154.000046, 22.999996 ],
					"varname" : "Ramp3 Dur",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-351",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.0, 487.400024, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-355",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.0, 452.733337, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1168.390747, 59.266876, 154.000046, 22.999996 ],
					"varname" : "Slider2 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-282",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 21.0, 237.000061, 44.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 717.759705, 7.066681, 44.058594, 20.0 ],
					"style" : "",
					"text" : "global"
				}

			}
, 			{
				"box" : 				{
					"automation" : "Auto",
					"automationon" : "Auto",
					"id" : "obj-278",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ -94.000031, 237.000061, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 859.818237, 6.066681, 36.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.text[204]",
							"parameter_shortname" : "live.text[203]",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Auto", "Auto" ]
						}

					}
,
					"text" : "Auto",
					"texton" : "Auto",
					"varname" : "live.text[1]"
				}

			}
, 			{
				"box" : 				{
					"automation" : "Limit",
					"automationon" : "Limit",
					"id" : "obj-274",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ -94.000031, 301.000061, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 821.818237, 6.066681, 34.0, 20.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.text[203]",
							"parameter_shortname" : "live.text[203]",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Limit", "Limit" ]
						}

					}
,
					"text" : "Limit",
					"texton" : "Limit",
					"varname" : "live.text[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-270",
					"maxclass" : "number",
					"maximum" : 7,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ -94.000031, 169.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 763.818298, 6.066681, 50.0, 22.0 ],
					"style" : "",
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -94.000031, 268.000061, 154.0, 22.0 ],
					"style" : "",
					"text" : "s global_delta_auto_toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -94.000031, 330.000061, 152.0, 22.0 ],
					"style" : "",
					"text" : "s global_delta_limit_toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-268",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -94.000031, 204.0, 139.0, 22.0 ],
					"style" : "",
					"text" : "s global_delta_speedlim"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 826.0, 336.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-114",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 826.0, 301.333313, 106.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1017.951538, 59.266876, 154.000046, 22.999996 ],
					"varname" : "Slider1 Rate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 474.0, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-110",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 474.0, 377.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1168.390747, 34.266876, 154.000046, 22.999996 ],
					"varname" : "Ramp2 Dur",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 359.48584, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-108",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 359.48584, 377.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 1017.951538, 34.266876, 154.000046, 22.999996 ],
					"varname" : "Ramp1 Dur",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 244.333496, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-105",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 244.333496, 377.333313, 107.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 865.951538, 59.266876, 154.000046, 22.999996 ],
					"varname" : "LFO2 Off",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 129.971527, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-102",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 129.971527, 377.333313, 108.800049, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 865.951538, 34.266876, 154.000046, 22.999996 ],
					"varname" : "LFO2 Int",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 18.000183, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-98",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 18.000183, 377.333313, 106.971344, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 714.818298, 59.266876, 154.000046, 22.999996 ],
					"varname" : "LFO1 Off",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -98.666504, 412.0, 92.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 2. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-89",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -98.666504, 377.333313, 109.466576, 22.999996 ],
					"presentation" : 1,
					"presentation_rect" : [ 714.818298, 33.266876, 154.000046, 22.999996 ],
					"varname" : "LFO1 int",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 309.333344, 1359.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "send delta"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 8,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 309.333344, 1328.666748, 137.0, 22.0 ],
					"style" : "",
					"text" : "pak 0. 0. 0. 0. 0. 0. 0. 0."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-96",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_delta.maxpat",
					"numinlets" : 14,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ -98.666504, 452.733337, 920.733154, 830.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 710.118347, 87.000031, 1439.933105, 584.599976 ],
					"varname" : "_delta",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 139.471527, 441.0, 49.561674, 441.0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 253.833496, 442.666694, 118.925762, 442.666694 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 368.98584, 442.666694, 188.289851, 442.666694 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 483.5, 442.666694, 257.65394, 442.666694 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 8 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 835.5, 444.0, 465.746206, 444.0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 713.5, 441.0, 396.382117, 441.0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 598.98584, 442.0, 327.018029, 442.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 10 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 858.5, 605.0, 834.0, 605.0, 834.0, 446.0, 604.474384, 446.0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 11 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 981.5, 608.0, 836.0, 608.0, 836.0, 444.0, 673.838472, 444.0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-270", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-265", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 12 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 986.5, 440.0, 743.202561, 440.0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 9 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 858.5, 519.0, 834.0, 519.0, 834.0, 440.0, 535.110295, 440.0 ],
					"source" : [ "obj-351", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-351", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-355", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 13 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 986.5, 528.0, 834.0, 528.0, 834.0, 446.0, 812.56665, 446.0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 812.56665, 1297.0, 436.833344, 1297.0 ],
					"source" : [ "obj-96", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 683.747628, 1297.0, 419.976201, 1297.0 ],
					"source" : [ "obj-96", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 554.928606, 1297.0, 403.119058, 1297.0 ],
					"source" : [ "obj-96", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 426.109584, 1297.0, 386.261915, 1297.0 ],
					"source" : [ "obj-96", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 297.290562, 1297.0, 369.404773, 1297.0 ],
					"source" : [ "obj-96", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 168.47154, 1296.0, 352.54763, 1296.0 ],
					"source" : [ "obj-96", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 39.652518, 1296.0, 335.690487, 1296.0 ],
					"source" : [ "obj-96", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ -89.166504, 1297.0, 318.833344, 1297.0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 27.500183, 442.0, -19.802415, 442.0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-98", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-96::obj-38::obj-13" : [ "Freq ∆ Time[1]", "Freq ∆ Time", 0 ],
			"obj-102::obj-9" : [ "live.text[326]", "live.text", 0 ],
			"obj-355::obj-8::obj-18" : [ "umenu[64]", "umenu", 0 ],
			"obj-34::obj-8::obj-56" : [ "Dropdown Control[2]", "Select", 0 ],
			"obj-1::obj-142::obj-8::obj-31" : [ "live.text[399]", "live.text", 0 ],
			"obj-1::obj-360::obj-8::obj-18" : [ "umenu[10]", "umenu", 0 ],
			"obj-1::obj-6::obj-26" : [ "Feed", "Feed", 0 ],
			"obj-1::obj-260::obj-18" : [ "rslider[117]", "rslider", 0 ],
			"obj-1::obj-1::obj-8::obj-31" : [ "live.text[69]", "live.text", 0 ],
			"obj-3::obj-225::obj-382::obj-2" : [ "rslider[19]", "rslider[15]", 0 ],
			"obj-3::obj-318::obj-8::obj-13" : [ "Quantize Metro Rate[15]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-397::obj-8::obj-13" : [ "Quantize Metro Rate[11]", "Quantize Metro Rate", 0 ],
			"obj-96::obj-25::obj-55" : [ "LFO Low Ramp[1]", "Low Ramp", 0 ],
			"obj-96::obj-80::obj-97" : [ "live.menu[2]", "live.menu", 0 ],
			"obj-89::obj-12" : [ "ratecontrol[42]", "ratecontrol", 0 ],
			"obj-110::obj-8::obj-10" : [ "Manual Metro Rate[68]", "Rate", 0 ],
			"obj-25::obj-8::obj-10" : [ "Manual Metro Rate[19]", "Rate", 0 ],
			"obj-1::obj-158::obj-30" : [ "live.text[85]", "live.text[1]", 0 ],
			"obj-1::obj-6::obj-92" : [ "DH16", "DH16", 0 ],
			"obj-3::obj-225::obj-374::obj-8" : [ "Velocity Initial Variance Minimum[14]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-340::obj-8::obj-34" : [ "live.text[131]", "live.text", 0 ],
			"obj-3::obj-358::obj-8::obj-10" : [ "Manual Metro Rate[24]", "Rate", 0 ],
			"obj-96::obj-7::obj-8" : [ "DB file selected[1]", "DB file selected", 0 ],
			"obj-96::obj-84::obj-21" : [ "Jam Bars Button[1]", "Jam Bars Button", 0 ],
			"obj-105::obj-8::obj-34" : [ "live.text[323]", "live.text", 0 ],
			"obj-355::obj-12" : [ "ratecontrol[58]", "ratecontrol", 0 ],
			"obj-16::obj-8::obj-56" : [ "Dropdown Control[14]", "Select", 0 ],
			"obj-34::obj-8::obj-10" : [ "Manual Metro Rate[46]", "Rate", 0 ],
			"obj-1::obj-142::obj-12" : [ "ratecontrol[19]", "ratecontrol", 0 ],
			"obj-1::obj-360::obj-187" : [ "rslider[115]", "rslider", 0 ],
			"obj-1::obj-1::obj-187" : [ "rslider[9]", "rslider", 0 ],
			"obj-3::obj-225::obj-537" : [ "drummatrix_reverse_row[1]", "drummatrix_reverse_row", 0 ],
			"obj-3::obj-397::obj-9" : [ "live.text[385]", "live.text", 0 ],
			"obj-96::obj-49::obj-53" : [ "LFO High Ramp[2]", "High Ramp", 0 ],
			"obj-96::obj-2::obj-36::obj-123" : [ "Shuffle Mux", "Shuffle Mux", 0 ],
			"obj-96::obj-6::obj-21" : [ "Jam Bars[1]", "Jam Bars", 0 ],
			"obj-98::obj-8::obj-34" : [ "live.text[331]", "live.text", 0 ],
			"obj-110::obj-8::obj-12" : [ "Metro On/Off[62]", "Metro On/Off", 0 ],
			"obj-25::obj-8::obj-12" : [ "Metro On/Off[69]", "Metro On/Off", 0 ],
			"obj-34::obj-12" : [ "ratecontrol[4]", "ratecontrol", 0 ],
			"obj-1::obj-28::obj-2::obj-18" : [ "Jam Bars[9]", "Jam Bars", 0 ],
			"obj-1::obj-160::obj-8::obj-13" : [ "Quantize Metro Rate[24]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-6::obj-195" : [ "DHLoop", "DHLoop", 0 ],
			"obj-3::obj-225::obj-375::obj-8" : [ "Velocity Initial Variance Minimum[12]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-340::obj-187" : [ "rslider[17]", "rslider", 0 ],
			"obj-3::obj-358::obj-30" : [ "live.text[98]", "live.text[1]", 0 ],
			"obj-3::obj-4::obj-44" : [ "Metro Speed", "Metro Speed", 0 ],
			"obj-96::obj-11::obj-20" : [ "Loop Controller", "Loop Controller", 0 ],
			"obj-96::obj-75::obj-13" : [ "Freq ∆ Time", "Freq ∆ Time", 0 ],
			"obj-96::obj-79::obj-56" : [ "Ramp Speed[3]", "Ramp Speed", 0 ],
			"obj-96::obj-84::obj-28" : [ "Loop Jam Bars Button[1]", "Loop Jam Bars Button", 0 ],
			"obj-105::obj-8::obj-18" : [ "umenu[121]", "umenu", 0 ],
			"obj-1::obj-149::obj-8::obj-10" : [ "Manual Metro Rate[96]", "Rate", 0 ],
			"obj-1::obj-361::obj-8::obj-12" : [ "Metro On/Off[22]", "Metro On/Off", 0 ],
			"obj-1::obj-298" : [ "Filter", "Filter", 0 ],
			"obj-3::obj-9::obj-4" : [ "live.text[91]", "Reset pitch", 0 ],
			"obj-3::obj-319::obj-8::obj-10" : [ "Manual Metro Rate[25]", "Rate", 0 ],
			"obj-3::obj-14::obj-173" : [ "live.text[428]", "live.text", 0 ],
			"obj-96::obj-49::obj-35" : [ "LFO Modulation Amount[2]", "Modulation", 0 ],
			"obj-96::obj-33::obj-13::obj-32" : [ "rslider[1]", "rslider", 0 ],
			"obj-96::obj-7::obj-45" : [ "DB maximum output value", "DB maximum output value", 0 ],
			"obj-96::obj-82::obj-49" : [ "DB frame rate[1]", "rate (ms)", 0 ],
			"obj-98::obj-8::obj-10" : [ "Manual Metro Rate[102]", "Rate", 0 ],
			"obj-1::obj-28::obj-2::obj-11" : [ "Gen New Bars Button[3]", "Gen New Bars Button", 0 ],
			"obj-1::obj-160::obj-12" : [ "ratecontrol[11]", "ratecontrol", 0 ],
			"obj-1::obj-6::obj-127" : [ "Full Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-1::obj-7::obj-9" : [ "live.text[39]", "live.text", 0 ],
			"obj-3::obj-225::obj-378::obj-2" : [ "rslider[28]", "rslider[15]", 0 ],
			"obj-3::obj-225::obj-381::obj-2" : [ "rslider[20]", "rslider[15]", 0 ],
			"obj-3::obj-187::obj-8::obj-31" : [ "live.text[128]", "live.text", 0 ],
			"obj-3::obj-396::obj-8::obj-56" : [ "Dropdown Control[16]", "Select", 0 ],
			"obj-96::obj-48::obj-56" : [ "Ramp Speed[2]", "Ramp Speed", 0 ],
			"obj-96::obj-33::obj-17::obj-2" : [ "Flip Output On/Off[6]", "Flip Output On/Off", 0 ],
			"obj-96::obj-4::obj-19" : [ "Smooth Output On/Off", "Smooth Output On/Off", 0 ],
			"obj-96::obj-78::obj-1" : [ "live.toggle[13]", "live.toggle", 0 ],
			"obj-105::obj-12" : [ "ratecontrol[39]", "ratecontrol", 0 ],
			"obj-20::obj-8::obj-12" : [ "Metro On/Off[70]", "Metro On/Off", 0 ],
			"obj-37::obj-8::obj-18" : [ "umenu[5]", "umenu", 0 ],
			"obj-1::obj-149::obj-8::obj-12" : [ "Metro On/Off[49]", "Metro On/Off", 0 ],
			"obj-1::obj-361::obj-187" : [ "rslider[91]", "rslider", 0 ],
			"obj-1::obj-6::obj-140" : [ "live.toggle[18]", "live.toggle", 0 ],
			"obj-1::obj-260::obj-291" : [ "Manual Sample Duration[2]", "Buffer", 0 ],
			"obj-1::obj-7::obj-8::obj-13" : [ "Quantize Metro Rate[126]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-225::obj-312" : [ "Root Note Number", "root number", 0 ],
			"obj-3::obj-330::obj-8::obj-18" : [ "umenu[78]", "umenu", 0 ],
			"obj-3::obj-319::obj-30" : [ "live.text[108]", "live.text[1]", 0 ],
			"obj-3::obj-14::obj-40" : [ "Note Length Minimum[1]", "Note Length Minimum", 0 ],
			"obj-96::obj-11::obj-57" : [ "Smooth Output[5]", "Smooth Output", 0 ],
			"obj-98::obj-12" : [ "ratecontrol[41]", "ratecontrol", 0 ],
			"obj-114::obj-8::obj-56" : [ "Dropdown Control[121]", "Select", 0 ],
			"obj-31::obj-8::obj-10" : [ "Manual Metro Rate[9]", "Rate", 0 ],
			"obj-1::obj-28::obj-2::obj-30" : [ "Loop Jam On/Off[3]", "Loop Jam On/Off", 0 ],
			"obj-1::obj-6::obj-154" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-1::obj-2::obj-8::obj-34" : [ "live.text[72]", "live.text", 0 ],
			"obj-3::obj-225::obj-388::obj-2" : [ "rslider[26]", "rslider[15]", 0 ],
			"obj-3::obj-187::obj-8::obj-12" : [ "Metro On/Off[76]", "Metro On/Off", 0 ],
			"obj-3::obj-396::obj-8::obj-31" : [ "live.text[124]", "live.text", 0 ],
			"obj-96::obj-47::obj-13" : [ "Duration (ms)[1]", "Duration (ms)", 0 ],
			"obj-96::obj-33::obj-16::obj-32" : [ "rslider[7]", "rslider", 0 ],
			"obj-96::obj-85::obj-27" : [ "Loop Jam Bars[2]", "Loop Jam Bars", 0 ],
			"obj-108::obj-8::obj-34" : [ "live.text[265]", "live.text", 0 ],
			"obj-20::obj-187" : [ "rslider[112]", "rslider", 0 ],
			"obj-37::obj-187" : [ "rslider[92]", "rslider", 0 ],
			"obj-1::obj-28::obj-103" : [ "Rate Multislider", "Rate Multislider", 0 ],
			"obj-1::obj-260::obj-349" : [ "Quantized Sample Duration[2]", "Quantized Sample Duration", 0 ],
			"obj-1::obj-7::obj-12" : [ "ratecontrol[13]", "ratecontrol", 0 ],
			"obj-3::obj-225::obj-14" : [ "Shuffle note numbers", "Shuffle note numbers", 0 ],
			"obj-3::obj-330::obj-8::obj-34" : [ "live.text[134]", "live.text", 0 ],
			"obj-3::obj-358::obj-8::obj-18" : [ "umenu[20]", "umenu", 0 ],
			"obj-3::obj-4::obj-3" : [ "Fold Mode", "Fold Mode", 0 ],
			"obj-102::obj-8::obj-18" : [ "umenu[122]", "umenu", 0 ],
			"obj-114::obj-187" : [ "rslider[47]", "rslider", 0 ],
			"obj-31::obj-12" : [ "ratecontrol[3]", "ratecontrol", 0 ],
			"obj-1::obj-28::obj-73" : [ "Sample Playback Speed", "Speed", 0 ],
			"obj-1::obj-360::obj-8::obj-10" : [ "Manual Metro Rate[52]", "Rate", 0 ],
			"obj-1::obj-2::obj-12" : [ "ratecontrol[7]", "ratecontrol", 0 ],
			"obj-3::obj-225::obj-385::obj-2" : [ "rslider[23]", "rslider[15]", 0 ],
			"obj-96::obj-81::obj-97" : [ "live.menu[1]", "live.menu", 0 ],
			"obj-96::obj-85::obj-57" : [ "Smooth Output[2]", "Smooth Output", 0 ],
			"obj-89::obj-8::obj-12" : [ "Metro On/Off[126]", "Metro On/Off", 0 ],
			"obj-108::obj-12" : [ "ratecontrol[38]", "ratecontrol", 0 ],
			"obj-16::obj-187" : [ "rslider[64]", "rslider", 0 ],
			"obj-1::obj-204::obj-68" : [ "Mixer / Send UI[11]", "Mixer / Send UI", 0 ],
			"obj-1::obj-28::obj-76" : [ "Speed Multislider", "Speed Multislider", 0 ],
			"obj-1::obj-158::obj-8::obj-10" : [ "Manual Metro Rate[95]", "Rate", 0 ],
			"obj-1::obj-362::obj-8::obj-56" : [ "Dropdown Control[5]", "Select", 0 ],
			"obj-1::obj-6::obj-123" : [ "16th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-96::obj-33::obj-14::obj-32" : [ "rslider[3]", "rslider", 0 ],
			"obj-96::obj-38::obj-22" : [ "Rampsmooth Down Time[1]", "Rampsmooth Down Time", 0 ],
			"obj-96::obj-83::obj-50" : [ "Mixer[1]", "Mixer", 0 ],
			"obj-102::obj-187" : [ "rslider[51]", "rslider", 0 ],
			"obj-355::obj-8::obj-34" : [ "live.text[395]", "live.text", 0 ],
			"obj-1::obj-142::obj-8::obj-12" : [ "Metro On/Off[50]", "Metro On/Off", 0 ],
			"obj-1::obj-1::obj-8::obj-13" : [ "Quantize Metro Rate[6]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-1::obj-8::obj-12" : [ "Metro On/Off[71]", "Metro On/Off", 0 ],
			"obj-3::obj-225::obj-383::obj-2" : [ "rslider[21]", "rslider[15]", 0 ],
			"obj-3::obj-225::obj-77" : [ "live.numbox[2]", "live.numbox[2]", 0 ],
			"obj-3::obj-318::obj-8::obj-10" : [ "Manual Metro Rate[26]", "Rate", 0 ],
			"obj-3::obj-397::obj-8::obj-31" : [ "live.text[338]", "live.text", 0 ],
			"obj-96::obj-80::obj-19" : [ "Smooth Output On/Off[3]", "Smooth Output On/Off", 0 ],
			"obj-1::obj-158::obj-12" : [ "ratecontrol[12]", "ratecontrol", 0 ],
			"obj-1::obj-362::obj-30" : [ "live.text[74]", "live.text[1]", 0 ],
			"obj-1::obj-6::obj-35" : [ "Complete speed control", "live.numbox", 0 ],
			"obj-3::obj-225::obj-373::obj-2" : [ "rslider[34]", "rslider[15]", 0 ],
			"obj-3::obj-225::obj-381::obj-8" : [ "Velocity Initial Variance Minimum", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-9::obj-23" : [ "live.text[209]", "New waveform", 0 ],
			"obj-3::obj-340::obj-8::obj-12" : [ "Metro On/Off[77]", "Metro On/Off", 0 ],
			"obj-96::obj-33::obj-19::obj-9" : [ "Delta Channel Menu[4]", "Delta Channel Menu", 0 ],
			"obj-96::obj-84::obj-45" : [ "Range Control[1]", "Range Control", 0 ],
			"obj-355::obj-8::obj-31" : [ "live.text[205]", "live.text", 0 ],
			"obj-34::obj-8::obj-13" : [ "Quantize Metro Rate[3]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-142::obj-30" : [ "live.text[382]", "live.text[1]", 0 ],
			"obj-1::obj-360::obj-30" : [ "live.text[82]", "live.text[1]", 0 ],
			"obj-1::obj-6::obj-74" : [ "live.text[425]", "live.text", 0 ],
			"obj-1::obj-6::obj-151" : [ "live.gain~", "live.gain~", 0 ],
			"obj-1::obj-1::obj-8::obj-34" : [ "live.text[68]", "live.text", 0 ],
			"obj-3::obj-318::obj-30" : [ "live.text[112]", "live.text[1]", 0 ],
			"obj-3::obj-397::obj-12" : [ "ratecontrol[14]", "ratecontrol", 0 ],
			"obj-96::obj-6::obj-19" : [ "Smooth Output On/Off[1]", "Smooth Output On/Off", 0 ],
			"obj-96::obj-7::obj-13" : [ "SlideDown", "SlideDown", 0 ],
			"obj-96::obj-80::obj-15" : [ "Jam On/Off[3]", "Jam On/Off", 0 ],
			"obj-110::obj-8::obj-13" : [ "Quantize Metro Rate[61]", "Quantize Metro Rate", 0 ],
			"obj-16::obj-8::obj-31" : [ "live.text[34]", "live.text", 0 ],
			"obj-25::obj-8::obj-34" : [ "live.text[48]", "live.text", 0 ],
			"obj-37::obj-8::obj-34" : [ "live.text[63]", "live.text", 0 ],
			"obj-1::obj-28::obj-2::obj-25" : [ "Jam Bars On/Off[3]", "Jam Bars On/Off", 0 ],
			"obj-1::obj-160::obj-8::obj-31" : [ "live.text[84]", "live.text", 0 ],
			"obj-1::obj-6::obj-90" : [ "DH4", "DH4", 0 ],
			"obj-3::obj-225::obj-376::obj-2" : [ "rslider[32]", "rslider[15]", 0 ],
			"obj-3::obj-340::obj-9" : [ "live.text[130]", "live.text", 0 ],
			"obj-3::obj-358::obj-187" : [ "rslider[13]", "rslider", 0 ],
			"obj-96::obj-49::obj-7" : [ "LFO Freq Menu[2]", "LFO Freq Menu", 0 ],
			"obj-96::obj-33::obj-18::obj-2" : [ "Flip Output On/Off[5]", "Flip Output On/Off", 0 ],
			"obj-96::obj-7::obj-11" : [ "Direction", "Direction", 0 ],
			"obj-96::obj-11::obj-21" : [ "Jam Bars Button", "Jam Bars Button", 0 ],
			"obj-96::obj-84::obj-25" : [ "Jam Bars On/Off[1]", "Jam Bars On/Off", 0 ],
			"obj-105::obj-8::obj-31" : [ "live.text[324]", "live.text", 0 ],
			"obj-355::obj-30" : [ "live.text[393]", "live.text[1]", 0 ],
			"obj-16::obj-8::obj-12" : [ "Metro On/Off[16]", "Metro On/Off", 0 ],
			"obj-34::obj-187" : [ "rslider[114]", "rslider", 0 ],
			"obj-3::obj-225::obj-75" : [ "live.numbox", "live.numbox", 0 ],
			"obj-3::obj-225::obj-322" : [ "Regen Mode", "Regen Mode", 0 ],
			"obj-3::obj-319::obj-8::obj-12" : [ "Metro On/Off[74]", "Metro On/Off", 0 ],
			"obj-3::obj-14::obj-38" : [ "Number of Steps[1]", "Number of Steps", 0 ],
			"obj-96::obj-49::obj-55" : [ "LFO Low Ramp[2]", "Low Ramp", 0 ],
			"obj-96::obj-82::obj-8" : [ "DB file selected[2]", "DB file selected", 0 ],
			"obj-98::obj-8::obj-12" : [ "Metro On/Off[125]", "Metro On/Off", 0 ],
			"obj-110::obj-30" : [ "live.text[259]", "live.text[1]", 0 ],
			"obj-25::obj-187" : [ "rslider[8]", "rslider", 0 ],
			"obj-1::obj-28::obj-2::obj-43" : [ "Number of Bars[1]", "Bars", 0 ],
			"obj-1::obj-160::obj-8::obj-10" : [ "Manual Metro Rate[22]", "Rate", 0 ],
			"obj-1::obj-6::obj-432" : [ "toggle", "toggle", 0 ],
			"obj-1::obj-6::obj-223" : [ "DHNotes4", "DHNotes4", 0 ],
			"obj-1::obj-50" : [ "number[6]", "number[6]", 0 ],
			"obj-3::obj-225::obj-379::obj-2" : [ "rslider[29]", "rslider[15]", 0 ],
			"obj-96::obj-33::obj-18::obj-32" : [ "rslider[5]", "rslider", 0 ],
			"obj-96::obj-33::obj-17::obj-11" : [ "receive_delta_out_channel_num[1]", "Output Channel", 0 ],
			"obj-96::obj-11::obj-30" : [ "Loop Jam On/Off", "Loop Jam On/Off", 0 ],
			"obj-96::obj-75::obj-24" : [ "Rampsmooth Up Time", "Rampsmooth Up Time", 0 ],
			"obj-96::obj-84::obj-57" : [ "Smooth Output[3]", "Smooth Output", 0 ],
			"obj-105::obj-30" : [ "live.text[321]", "live.text[1]", 0 ],
			"obj-20::obj-8::obj-34" : [ "live.text[270]", "live.text", 0 ],
			"obj-37::obj-8::obj-13" : [ "Quantize Metro Rate[4]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-149::obj-8::obj-34" : [ "live.text[452]", "live.text", 0 ],
			"obj-1::obj-361::obj-8::obj-18" : [ "umenu[9]", "umenu", 0 ],
			"obj-1::obj-7::obj-8::obj-34" : [ "live.text[90]", "live.text", 0 ],
			"obj-3::obj-319::obj-8::obj-18" : [ "umenu[74]", "umenu", 0 ],
			"obj-3::obj-14::obj-32" : [ "live.text[337]", "live.text[1]", 0 ],
			"obj-96::obj-33::obj-13::obj-11" : [ "receive_delta_out_channel_num[6]", "Output Channel", 0 ],
			"obj-96::obj-82::obj-56" : [ "DB loop speed (notes)[2]", "DB loop speed (notes)", 0 ],
			"obj-98::obj-187" : [ "rslider[52]", "rslider", 0 ],
			"obj-114::obj-8::obj-34" : [ "live.text[107]", "live.text", 0 ],
			"obj-31::obj-8::obj-56" : [ "Dropdown Control[1]", "Select", 0 ],
			"obj-1::obj-28::obj-2::obj-15" : [ "live.toggle[90]", "live.toggle", 0 ],
			"obj-1::obj-160::obj-30" : [ "live.text[369]", "live.text[1]", 0 ],
			"obj-1::obj-6::obj-152" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-1::obj-2::obj-8::obj-56" : [ "Dropdown Control[4]", "Select", 0 ],
			"obj-3::obj-225::obj-377::obj-8" : [ "Velocity Initial Variance Minimum[8]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-187::obj-8::obj-10" : [ "Manual Metro Rate[27]", "Rate", 0 ],
			"obj-3::obj-396::obj-8::obj-18" : [ "umenu[19]", "umenu", 0 ],
			"obj-96::obj-33::obj-16::obj-2" : [ "Flip Output On/Off[7]", "Flip Output On/Off", 0 ],
			"obj-96::obj-78::obj-43" : [ "function[6]", "function", 0 ],
			"obj-96::obj-85::obj-15" : [ "live.toggle[91]", "live.toggle", 0 ],
			"obj-108::obj-8::obj-31" : [ "live.text[266]", "live.text", 0 ],
			"obj-20::obj-8::obj-31" : [ "live.text[271]", "live.text", 0 ],
			"obj-37::obj-12" : [ "ratecontrol[5]", "ratecontrol", 0 ],
			"obj-1::obj-149::obj-30" : [ "live.text[449]", "live.text[1]", 0 ],
			"obj-1::obj-361::obj-12" : [ "ratecontrol[9]", "ratecontrol", 0 ],
			"obj-1::obj-260::obj-16" : [ "NoteAmp", "NoteAmp", 0 ],
			"obj-1::obj-7::obj-8::obj-18" : [ "umenu[16]", "umenu", 0 ],
			"obj-3::obj-330::obj-8::obj-10" : [ "Manual Metro Rate[29]", "Rate", 0 ],
			"obj-3::obj-14::obj-7" : [ "Step Sequencer[1]", "Step Sequencer", 0 ],
			"obj-3::obj-4::obj-173" : [ "live.text[97]", "live.text", 0 ],
			"obj-3::obj-4::obj-198" : [ "New Sequence Every On/Off", "New Sequence Every On/Off", 0 ],
			"obj-274" : [ "live.text[203]", "live.text[203]", 0 ],
			"obj-96::obj-33::obj-15::obj-2" : [ "Flip Output On/Off[2]", "Flip Output On/Off", 0 ],
			"obj-102::obj-8::obj-56" : [ "Dropdown Control[125]", "Select", 0 ],
			"obj-114::obj-8::obj-13" : [ "Quantize Metro Rate[82]", "Quantize Metro Rate", 0 ],
			"obj-31::obj-8::obj-12" : [ "Metro On/Off[19]", "Metro On/Off", 0 ],
			"obj-1::obj-6::obj-264" : [ "live.gain~[6]", "live.gain~[6]", 0 ],
			"obj-1::obj-2::obj-8::obj-10" : [ "Manual Metro Rate[12]", "Rate", 0 ],
			"obj-3::obj-225::obj-386::obj-2" : [ "rslider[24]", "rslider[15]", 0 ],
			"obj-3::obj-5::obj-157" : [ "Amplitude", "Amplitude", 0 ],
			"obj-3::obj-187::obj-9" : [ "live.text[126]", "live.text", 0 ],
			"obj-3::obj-396::obj-187" : [ "rslider[12]", "rslider", 0 ],
			"obj-96::obj-47::obj-56" : [ "Ramp Speed[1]", "Ramp Speed", 0 ],
			"obj-96::obj-81::obj-12" : [ "Slider Qty[2]", "Slider Qty", 0 ],
			"obj-96::obj-85::obj-11" : [ "Gen New Bars Button[2]", "Gen New Bars Button", 0 ],
			"obj-89::obj-8::obj-13" : [ "Quantize Metro Rate[125]", "Quantize Metro Rate", 0 ],
			"obj-108::obj-8::obj-10" : [ "Manual Metro Rate[99]", "Rate", 0 ],
			"obj-1::obj-28::obj-29" : [ "Repeat Rate Toggle", "Repeat Rate Toggle", 0 ],
			"obj-1::obj-158::obj-8::obj-31" : [ "live.text[88]", "live.text", 0 ],
			"obj-1::obj-362::obj-8::obj-10" : [ "Manual Metro Rate[21]", "Rate", 0 ],
			"obj-3::obj-225::obj-412" : [ "Weird Mode On/Off", "Weird Mode On/Off", 0 ],
			"obj-3::obj-330::obj-9" : [ "live.text[116]", "live.text", 0 ],
			"obj-3::obj-358::obj-8::obj-12" : [ "Metro On/Off[73]", "Metro On/Off", 0 ],
			"obj-96::obj-33::obj-14::obj-9" : [ "Delta Channel Menu[3]", "Delta Channel Menu", 0 ],
			"obj-96::obj-11::obj-18" : [ "Jam Bars[4]", "Jam Bars", 0 ],
			"obj-96::obj-83::obj-36::obj-4" : [ "Random Mux[1]", "Random Mux", 0 ],
			"obj-102::obj-8::obj-10" : [ "Manual Metro Rate[101]", "Rate", 0 ],
			"obj-31::obj-9" : [ "live.text[50]", "live.text", 0 ],
			"obj-1::obj-142::obj-8::obj-34" : [ "live.text[383]", "live.text", 0 ],
			"obj-3::obj-225::obj-384::obj-8" : [ "Velocity Initial Variance Minimum[3]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-225::obj-311" : [ "Manual BPM mode", "manual BPM", 0 ],
			"obj-3::obj-318::obj-8::obj-34" : [ "live.text[114]", "live.text", 0 ],
			"obj-3::obj-397::obj-8::obj-34" : [ "live.text[386]", "live.text", 0 ],
			"obj-96::obj-25::obj-7" : [ "LFO Freq Menu[1]", "LFO Freq Menu", 0 ],
			"obj-96::obj-7::obj-56" : [ "DB loop speed (notes)[1]", "DB loop speed (notes)", 0 ],
			"obj-96::obj-85::obj-26" : [ "Values[2]", "Values", 0 ],
			"obj-89::obj-8::obj-31" : [ "live.text[336]", "live.text", 0 ],
			"obj-108::obj-9" : [ "live.text[264]", "live.text", 0 ],
			"obj-16::obj-9" : [ "live.text[121]", "live.text", 0 ],
			"obj-1::obj-4::obj-68" : [ "Mixer / Send UI[10]", "Mixer / Send UI", 0 ],
			"obj-1::obj-158::obj-8::obj-34" : [ "live.text[87]", "live.text", 0 ],
			"obj-1::obj-362::obj-12" : [ "ratecontrol[8]", "ratecontrol", 0 ],
			"obj-1::obj-6::obj-125" : [ "4th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-3::obj-340::obj-8::obj-31" : [ "live.text[132]", "live.text", 0 ],
			"obj-3::obj-4::obj-193" : [ "Random or Increment New Sequence", "Random or Increment New Sequence", 0 ],
			"obj-96::obj-33::obj-19::obj-32" : [ "rslider[4]", "rslider", 0 ],
			"obj-96::obj-38::obj-24" : [ "Rampsmooth Up Time[1]", "Rampsmooth Up Time", 0 ],
			"obj-96::obj-84::obj-30" : [ "Loop Jam On/Off[1]", "Loop Jam On/Off", 0 ],
			"obj-102::obj-12" : [ "ratecontrol[40]", "ratecontrol", 0 ],
			"obj-355::obj-8::obj-56" : [ "Dropdown Control[71]", "Select", 0 ],
			"obj-34::obj-8::obj-31" : [ "live.text[62]", "live.text", 0 ],
			"obj-1::obj-142::obj-8::obj-13" : [ "Quantize Metro Rate[27]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-360::obj-8::obj-12" : [ "Metro On/Off[23]", "Metro On/Off", 0 ],
			"obj-1::obj-1::obj-8::obj-10" : [ "Manual Metro Rate[20]", "Rate", 0 ],
			"obj-3::obj-225::obj-100" : [ "Note Number Range", "Note Number Range", 0 ],
			"obj-3::obj-318::obj-8::obj-12" : [ "Metro On/Off[75]", "Metro On/Off", 0 ],
			"obj-3::obj-397::obj-8::obj-56" : [ "Dropdown Control[15]", "Select", 0 ],
			"obj-96::obj-25::obj-35" : [ "LFO Modulation Amount[1]", "Modulation", 0 ],
			"obj-96::obj-2::obj-36::obj-4" : [ "Random Mux", "Random Mux", 0 ],
			"obj-96::obj-6::obj-9" : [ "Manual Rate[1]", "Manual Rate", 0 ],
			"obj-96::obj-80::obj-9" : [ "Manual Rate[3]", "Manual Rate", 0 ],
			"obj-89::obj-9" : [ "live.text[334]", "live.text", 0 ],
			"obj-110::obj-8::obj-34" : [ "live.text[261]", "live.text", 0 ],
			"obj-25::obj-8::obj-18" : [ "umenu[2]", "umenu", 0 ],
			"obj-1::obj-6::obj-89" : [ "DH2", "DH2", 0 ],
			"obj-3::obj-225::obj-374::obj-2" : [ "rslider[33]", "rslider[15]", 0 ],
			"obj-3::obj-340::obj-8::obj-10" : [ "Manual Metro Rate[28]", "Rate", 0 ],
			"obj-3::obj-358::obj-8::obj-31" : [ "live.text[101]", "live.text", 0 ],
			"obj-96::obj-6::obj-97" : [ "live.menu", "live.menu", 0 ],
			"obj-96::obj-11::obj-11" : [ "Gen New Bars Button", "Gen New Bars Button", 0 ],
			"obj-96::obj-84::obj-18" : [ "Jam Bars[7]", "Jam Bars", 0 ],
			"obj-105::obj-8::obj-13" : [ "Quantize Metro Rate[122]", "Quantize Metro Rate", 0 ],
			"obj-355::obj-187" : [ "rslider[95]", "rslider", 0 ],
			"obj-34::obj-9" : [ "live.text[60]", "live.text", 0 ],
			"obj-1::obj-28::obj-101" : [ "live.text[1]", "live.text", 0 ],
			"obj-1::obj-142::obj-9" : [ "live.text[398]", "live.text", 0 ],
			"obj-1::obj-360::obj-9" : [ "live.text[396]", "live.text", 0 ],
			"obj-1::obj-260::obj-113" : [ "number[29]", "number[29]", 0 ],
			"obj-1::obj-1::obj-12" : [ "ratecontrol[6]", "ratecontrol", 0 ],
			"obj-3::obj-225::obj-41" : [ "QuantizedTempo", "tempo", 0 ],
			"obj-96::obj-2::obj-50" : [ "Mixer", "Mixer", 0 ],
			"obj-96::obj-82::obj-11" : [ "Direction[1]", "Direction", 0 ],
			"obj-98::obj-8::obj-13" : [ "Quantize Metro Rate[124]", "Quantize Metro Rate", 0 ],
			"obj-110::obj-12" : [ "ratecontrol[37]", "ratecontrol", 0 ],
			"obj-25::obj-9" : [ "live.text[47]", "live.text", 0 ],
			"obj-1::obj-28::obj-2::obj-20" : [ "Loop Controller[3]", "Loop Controller", 0 ],
			"obj-1::obj-160::obj-8::obj-12" : [ "Metro On/Off[47]", "Metro On/Off", 0 ],
			"obj-1::obj-6::obj-212" : [ "DHNotes16", "DHNotes16", 0 ],
			"obj-3::obj-225::obj-380::obj-2" : [ "rslider[30]", "rslider[15]", 0 ],
			"obj-3::obj-358::obj-9" : [ "live.text[99]", "live.text", 0 ],
			"obj-96::obj-4::obj-15" : [ "Jam On/Off", "Jam On/Off", 0 ],
			"obj-96::obj-11::obj-27" : [ "Loop Jam Bars", "Loop Jam Bars", 0 ],
			"obj-96::obj-75::obj-6" : [ "Master Volume", "Master Volume", 0 ],
			"obj-96::obj-79::obj-43" : [ "function[5]", "function", 0 ],
			"obj-96::obj-84::obj-20" : [ "Loop Controller[1]", "Loop Controller", 0 ],
			"obj-37::obj-8::obj-10" : [ "Manual Metro Rate[50]", "Rate", 0 ],
			"obj-1::obj-149::obj-8::obj-31" : [ "live.text[453]", "live.text", 0 ],
			"obj-1::obj-361::obj-8::obj-31" : [ "live.text[81]", "live.text", 0 ],
			"obj-3::obj-319::obj-8::obj-56" : [ "Dropdown Control[73]", "Select", 0 ],
			"obj-3::obj-14::obj-29" : [ "Display Mode[1]", "Display Mode", 0 ],
			"obj-96::obj-49::obj-22" : [ "LFO On[2]", "On", 0 ],
			"obj-96::obj-33::obj-13::obj-9" : [ "Delta Channel Menu[1]", "Delta Channel Menu", 0 ],
			"obj-96::obj-82::obj-45" : [ "DB maximum output value[1]", "DB maximum output value", 0 ],
			"obj-98::obj-8::obj-18" : [ "umenu[72]", "umenu", 0 ],
			"obj-1::obj-28::obj-2::obj-28" : [ "Loop Jam Bars Button[3]", "Loop Jam Bars Button", 0 ],
			"obj-1::obj-160::obj-9" : [ "live.text[397]", "live.text", 0 ],
			"obj-1::obj-6::obj-93" : [ "multislider[3]", "multislider[3]", 0 ],
			"obj-1::obj-6::obj-291" : [ "Manual Sample Duration[1]", "Buffer", 0 ],
			"obj-3::obj-225::obj-378::obj-8" : [ "Velocity Initial Variance Minimum[9]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-187::obj-8::obj-34" : [ "live.text[127]", "live.text", 0 ],
			"obj-3::obj-396::obj-8::obj-10" : [ "Manual Metro Rate[23]", "Rate", 0 ],
			"obj-3::obj-4::obj-40" : [ "Note Length Minimum", "Note Length Minimum", 0 ],
			"obj-96::obj-48::obj-1" : [ "live.toggle[5]", "live.toggle", 0 ],
			"obj-96::obj-2::obj-36::obj-1" : [ "MatrixCtrl", "MatrixCtrl", 0 ],
			"obj-96::obj-11::obj-45" : [ "Range Control", "Range Control", 0 ],
			"obj-96::obj-78::obj-56" : [ "Ramp Speed[4]", "Ramp Speed", 0 ],
			"obj-96::obj-85::obj-30" : [ "Loop Jam On/Off[2]", "Loop Jam On/Off", 0 ],
			"obj-20::obj-8::obj-10" : [ "Manual Metro Rate[18]", "Rate", 0 ],
			"obj-37::obj-8::obj-31" : [ "live.text[388]", "live.text", 0 ],
			"obj-1::obj-28::obj-85" : [ "live.text", "live.text", 0 ],
			"obj-1::obj-149::obj-187" : [ "rslider[116]", "rslider", 0 ],
			"obj-1::obj-361::obj-30" : [ "live.text[78]", "live.text[1]", 0 ],
			"obj-1::obj-6::obj-273" : [ "Master", "Master", 0 ],
			"obj-1::obj-260::obj-48" : [ "live.menu[4]", "live.menu", 0 ],
			"obj-1::obj-7::obj-8::obj-10" : [ "Manual Metro Rate[1]", "Rate", 0 ],
			"obj-3::obj-330::obj-8::obj-12" : [ "Metro On/Off[24]", "Metro On/Off", 0 ],
			"obj-3::obj-319::obj-187" : [ "rslider[14]", "rslider", 0 ],
			"obj-3::obj-14::obj-3" : [ "Fold Mode[1]", "Fold Mode", 0 ],
			"obj-96::obj-48::obj-13" : [ "Duration (ms)", "Duration (ms)", 0 ],
			"obj-96::obj-33::obj-15::obj-32" : [ "rslider[2]", "rslider", 0 ],
			"obj-96::obj-11::obj-15" : [ "live.toggle[89]", "live.toggle", 0 ],
			"obj-96::obj-83::obj-36::obj-123" : [ "Shuffle Mux[1]", "Shuffle Mux", 0 ],
			"obj-114::obj-8::obj-12" : [ "Metro On/Off[82]", "Metro On/Off", 0 ],
			"obj-31::obj-8::obj-13" : [ "Quantize Metro Rate[2]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-301::obj-68" : [ "Mixer / Send UI[8]", "Mixer / Send UI", 0 ],
			"obj-1::obj-6::obj-155" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-1::obj-2::obj-8::obj-18" : [ "umenu[7]", "umenu", 0 ],
			"obj-3::obj-225::obj-387::obj-8" : [ "Velocity Initial Variance Minimum[6]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-187::obj-30" : [ "live.text[125]", "live.text[1]", 0 ],
			"obj-3::obj-396::obj-9" : [ "live.text[122]", "live.text", 0 ],
			"obj-96::obj-47::obj-43" : [ "function[4]", "function", 0 ],
			"obj-96::obj-33::obj-16::obj-11" : [ "receive_delta_out_channel_num", "Output Channel", 0 ],
			"obj-96::obj-81::obj-21" : [ "Jam Bars[5]", "Jam Bars", 0 ],
			"obj-96::obj-85::obj-20" : [ "Loop Controller[2]", "Loop Controller", 0 ],
			"obj-89::obj-8::obj-10" : [ "Manual Metro Rate[103]", "Rate", 0 ],
			"obj-108::obj-8::obj-18" : [ "umenu[120]", "umenu", 0 ],
			"obj-20::obj-9" : [ "live.text[269]", "live.text", 0 ],
			"obj-1::obj-28::obj-90" : [ "Rate quantize", "Rate quantize", 0 ],
			"obj-1::obj-362::obj-8::obj-31" : [ "live.text[77]", "live.text", 0 ],
			"obj-1::obj-7::obj-187" : [ "rslider[10]", "rslider", 0 ],
			"obj-3::obj-330::obj-30" : [ "live.text[133]", "live.text[1]", 0 ],
			"obj-3::obj-358::obj-8::obj-56" : [ "Dropdown Control[72]", "Select", 0 ],
			"obj-96::obj-38::obj-74::obj-68" : [ "Mixer / Send UI[13]", "Mixer / Send UI", 0 ],
			"obj-102::obj-8::obj-13" : [ "Quantize Metro Rate[123]", "Quantize Metro Rate", 0 ],
			"obj-114::obj-12" : [ "ratecontrol[36]", "ratecontrol", 0 ],
			"obj-20::obj-8::obj-18" : [ "umenu[1]", "umenu", 0 ],
			"obj-1::obj-360::obj-8::obj-34" : [ "live.text[378]", "live.text", 0 ],
			"obj-1::obj-2::obj-30" : [ "live.text[70]", "live.text[1]", 0 ],
			"obj-3::obj-225::obj-385::obj-8" : [ "Velocity Initial Variance Minimum[4]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-225::obj-1" : [ "new row z depth", "new row z depth", 0 ],
			"obj-3::obj-225::obj-20" : [ "live.numbox[3]", "live.numbox[3]", 0 ],
			"obj-96::obj-25::obj-133" : [ "LFO Y-Offset[1]", "Y-Offset", 0 ],
			"obj-96::obj-33::obj-12::obj-2" : [ "Flip Output On/Off", "Flip Output On/Off", 0 ],
			"obj-96::obj-81::obj-19" : [ "Smooth Output On/Off[2]", "Smooth Output On/Off", 0 ],
			"obj-96::obj-85::obj-25" : [ "Jam Bars On/Off[2]", "Jam Bars On/Off", 0 ],
			"obj-89::obj-8::obj-18" : [ "umenu[73]", "umenu", 0 ],
			"obj-108::obj-30" : [ "live.text[263]", "live.text[1]", 0 ],
			"obj-16::obj-30" : [ "live.text[120]", "live.text[1]", 0 ],
			"obj-1::obj-28::obj-91" : [ "Speed quantize", "Speed quantize", 0 ],
			"obj-1::obj-158::obj-8::obj-13" : [ "Quantize Metro Rate[25]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-362::obj-8::obj-34" : [ "live.text[76]", "live.text", 0 ],
			"obj-1::obj-6::obj-126" : [ "2nd Note Playback Speed", "Note Speed", 0 ],
			"obj-3::obj-4::obj-32" : [ "live.text[136]", "live.text[1]", 0 ],
			"obj-96::obj-38::obj-6" : [ "Master Volume[1]", "Master Volume", 0 ],
			"obj-102::obj-30" : [ "live.text[325]", "live.text[1]", 0 ],
			"obj-355::obj-8::obj-13" : [ "Quantize Metro Rate[66]", "Quantize Metro Rate", 0 ],
			"obj-34::obj-8::obj-18" : [ "umenu[4]", "umenu", 0 ],
			"obj-1::obj-142::obj-8::obj-56" : [ "Dropdown Control[13]", "Select", 0 ],
			"obj-1::obj-1::obj-8::obj-56" : [ "Dropdown Control[66]", "Select", 0 ],
			"obj-1::obj-398" : [ "live.text[103]", "live.text[103]", 0 ],
			"obj-3::obj-225::obj-382::obj-8" : [ "Velocity Initial Variance Minimum[1]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-318::obj-8::obj-31" : [ "live.text[115]", "live.text", 0 ],
			"obj-3::obj-397::obj-8::obj-12" : [ "Metro On/Off[1]", "Metro On/Off", 0 ],
			"obj-96::obj-25::obj-22" : [ "LFO On[1]", "On", 0 ],
			"obj-96::obj-80::obj-12" : [ "Slider Qty[3]", "Slider Qty", 0 ],
			"obj-89::obj-30" : [ "live.text[333]", "live.text[1]", 0 ],
			"obj-110::obj-8::obj-18" : [ "umenu[119]", "umenu", 0 ],
			"obj-16::obj-8::obj-34" : [ "live.text[33]", "live.text", 0 ],
			"obj-25::obj-8::obj-31" : [ "live.text[49]", "live.text", 0 ],
			"obj-1::obj-53::obj-68" : [ "Mixer / Send UI[9]", "Mixer / Send UI", 0 ],
			"obj-1::obj-158::obj-187" : [ "rslider[96]", "rslider", 0 ],
			"obj-1::obj-362::obj-187" : [ "rslider[90]", "rslider", 0 ],
			"obj-1::obj-6::obj-84" : [ "DH1", "DH1", 0 ],
			"obj-1::obj-260::obj-163" : [ "number[28]", "number[28]", 0 ],
			"obj-1::obj-7::obj-30" : [ "live.text[38]", "live.text[1]", 0 ],
			"obj-3::obj-225::obj-373::obj-8" : [ "Velocity Initial Variance Minimum[15]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-340::obj-8::obj-18" : [ "umenu[77]", "umenu", 0 ],
			"obj-96::obj-33::obj-19::obj-2" : [ "Flip Output On/Off[4]", "Flip Output On/Off", 0 ],
			"obj-96::obj-75::obj-74::obj-68" : [ "Mixer / Send UI[12]", "Mixer / Send UI", 0 ],
			"obj-96::obj-84::obj-27" : [ "Loop Jam Bars[1]", "Loop Jam Bars", 0 ],
			"obj-105::obj-8::obj-10" : [ "Manual Metro Rate[100]", "Rate", 0 ],
			"obj-355::obj-8::obj-12" : [ "Metro On/Off[129]", "Metro On/Off", 0 ],
			"obj-34::obj-8::obj-12" : [ "Metro On/Off[20]", "Metro On/Off", 0 ],
			"obj-1::obj-142::obj-187" : [ "rslider[94]", "rslider", 0 ],
			"obj-1::obj-360::obj-12" : [ "ratecontrol[10]", "ratecontrol", 0 ],
			"obj-1::obj-1::obj-30" : [ "live.text[66]", "live.text[1]", 0 ],
			"obj-3::obj-225::obj-529" : [ "drummatrix_reverse_row", "drummatrix_reverse_row", 0 ],
			"obj-3::obj-318::obj-9" : [ "live.text[113]", "live.text", 0 ],
			"obj-3::obj-397::obj-30" : [ "live.text[339]", "live.text[1]", 0 ],
			"obj-96::obj-6::obj-12" : [ "Slider Qty[1]", "Slider Qty", 0 ],
			"obj-110::obj-8::obj-56" : [ "Dropdown Control[122]", "Select", 0 ],
			"obj-25::obj-8::obj-56" : [ "Dropdown Control[67]", "Select", 0 ],
			"obj-1::obj-28::obj-2::obj-27" : [ "Loop Jam Bars[3]", "Loop Jam Bars", 0 ],
			"obj-1::obj-160::obj-8::obj-18" : [ "umenu[11]", "umenu", 0 ],
			"obj-1::obj-6::obj-91" : [ "DH8", "DH8", 0 ],
			"obj-3::obj-225::obj-375::obj-2" : [ "rslider[31]", "rslider[15]", 0 ],
			"obj-3::obj-340::obj-30" : [ "live.text[129]", "live.text[1]", 0 ],
			"obj-3::obj-358::obj-12" : [ "ratecontrol[21]", "ratecontrol", 0 ],
			"obj-96::obj-33::obj-18::obj-9" : [ "Delta Channel Menu[5]", "Delta Channel Menu", 0 ],
			"obj-96::obj-11::obj-25" : [ "Jam Bars On/Off", "Jam Bars On/Off", 0 ],
			"obj-96::obj-75::obj-16" : [ "Amp ∆ Time", "Amp ∆/T", 0 ],
			"obj-96::obj-79::obj-13" : [ "Duration (ms)[2]", "Duration (ms)", 0 ],
			"obj-96::obj-84::obj-15" : [ "live.toggle[16]", "live.toggle", 0 ],
			"obj-105::obj-8::obj-12" : [ "Metro On/Off[123]", "Metro On/Off", 0 ],
			"obj-1::obj-149::obj-8::obj-56" : [ "Dropdown Control[10]", "Select", 0 ],
			"obj-1::obj-361::obj-8::obj-13" : [ "Quantize Metro Rate[9]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-225::obj-211" : [ "note speed probability", "note speed probability", 0 ],
			"obj-3::obj-9::obj-61" : [ "AmplitudeStepper", "AmplitudeStepper", 0 ],
			"obj-3::obj-319::obj-8::obj-31" : [ "live.text[111]", "live.text", 0 ],
			"obj-3::obj-14::obj-198" : [ "New Sequence Every On/Off[1]", "New Sequence Every On/Off", 0 ],
			"obj-96::obj-49::obj-133" : [ "LFO Y-Offset[2]", "Y-Offset", 0 ],
			"obj-96::obj-7::obj-15" : [ "DB slide down rate", "DB slide down rate", 0 ],
			"obj-96::obj-82::obj-68" : [ "File[1]", "File", 0 ],
			"obj-98::obj-8::obj-56" : [ "Dropdown Control[126]", "Select", 0 ],
			"obj-110::obj-187" : [ "rslider[48]", "rslider", 0 ],
			"obj-25::obj-30" : [ "live.text[44]", "live.text[1]", 0 ],
			"obj-1::obj-28::obj-2::obj-57" : [ "Smooth Output[1]", "Smooth Output", 0 ],
			"obj-1::obj-160::obj-8::obj-34" : [ "live.text[83]", "live.text", 0 ],
			"obj-1::obj-6::obj-220" : [ "DHNotes8", "DHNotes8", 0 ],
			"obj-1::obj-51" : [ "number[7]", "number[6]", 0 ],
			"obj-3::obj-225::obj-379::obj-8" : [ "Velocity Initial Variance Minimum[10]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-187::obj-8::obj-13" : [ "Quantize Metro Rate[17]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-318::obj-187" : [ "rslider[15]", "rslider", 0 ],
			"obj-3::obj-396::obj-8::obj-34" : [ "live.text[123]", "live.text", 0 ],
			"obj-3::obj-4::obj-7" : [ "Step Sequencer", "Step Sequencer", 0 ],
			"obj-96::obj-33::obj-17::obj-9" : [ "Delta Channel Menu[6]", "Delta Channel Menu", 0 ],
			"obj-96::obj-4::obj-12" : [ "Slider Qty", "Slider Qty", 0 ],
			"obj-105::obj-9" : [ "live.text[322]", "live.text", 0 ],
			"obj-20::obj-8::obj-56" : [ "Dropdown Control[87]", "Select", 0 ],
			"obj-16::obj-8::obj-10" : [ "Manual Metro Rate[11]", "Rate", 0 ],
			"obj-37::obj-8::obj-56" : [ "Dropdown Control[3]", "Select", 0 ],
			"obj-1::obj-149::obj-8::obj-18" : [ "umenu[15]", "umenu", 0 ],
			"obj-1::obj-361::obj-8::obj-34" : [ "live.text[80]", "live.text", 0 ],
			"obj-1::obj-7::obj-8::obj-56" : [ "Dropdown Control[128]", "Select", 0 ],
			"obj-3::obj-330::obj-8::obj-13" : [ "Quantize Metro Rate[19]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-319::obj-12" : [ "ratecontrol[43]", "ratecontrol", 0 ],
			"obj-3::obj-14::obj-44" : [ "Metro Speed[1]", "Metro Speed", 0 ],
			"obj-96::obj-2::obj-40::obj-210" : [ "number[86]", "number[10]", 0 ],
			"obj-98::obj-9" : [ "live.text[330]", "live.text", 0 ],
			"obj-114::obj-8::obj-10" : [ "Manual Metro Rate[57]", "Rate", 0 ],
			"obj-31::obj-8::obj-18" : [ "umenu[3]", "umenu", 0 ],
			"obj-1::obj-28::obj-2::obj-45" : [ "Range Control[3]", "Range Control", 0 ],
			"obj-1::obj-6::obj-153" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-1::obj-2::obj-8::obj-12" : [ "Metro On/Off[12]", "Metro On/Off", 0 ],
			"obj-3::obj-225::obj-388::obj-8" : [ "Velocity Initial Variance Minimum[7]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-187::obj-8::obj-18" : [ "umenu[76]", "umenu", 0 ],
			"obj-3::obj-396::obj-8::obj-13" : [ "Quantize Metro Rate[12]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-4::obj-29" : [ "Display Mode", "Display Mode", 0 ],
			"obj-96::obj-33::obj-16::obj-9" : [ "Delta Channel Menu[7]", "Delta Channel Menu", 0 ],
			"obj-96::obj-85::obj-28" : [ "Loop Jam Bars Button[2]", "Loop Jam Bars Button", 0 ],
			"obj-108::obj-8::obj-12" : [ "Metro On/Off[122]", "Metro On/Off", 0 ],
			"obj-20::obj-30" : [ "live.text[268]", "live.text[1]", 0 ],
			"obj-37::obj-9" : [ "live.text[46]", "live.text", 0 ],
			"obj-1::obj-28::obj-42" : [ "Buffer Size", "Buffer Size", 0 ],
			"obj-1::obj-149::obj-12" : [ "ratecontrol[18]", "ratecontrol", 0 ],
			"obj-1::obj-260::obj-17" : [ "NoteLocation", "NoteLocation", 0 ],
			"obj-1::obj-7::obj-8::obj-31" : [ "live.text[89]", "live.text", 0 ],
			"obj-3::obj-225::obj-7" : [ "note numbers", "note numbers", 0 ],
			"obj-3::obj-330::obj-8::obj-31" : [ "live.text[135]", "live.text", 0 ],
			"obj-3::obj-4::obj-38" : [ "Number of Steps", "Number of Steps", 0 ],
			"obj-278" : [ "live.text[204]", "live.text[203]", 0 ],
			"obj-96::obj-33::obj-15::obj-9" : [ "Delta Channel Menu[2]", "Delta Channel Menu", 0 ],
			"obj-102::obj-8::obj-34" : [ "live.text[327]", "live.text", 0 ],
			"obj-114::obj-9" : [ "live.text[58]", "live.text", 0 ],
			"obj-31::obj-30" : [ "live.text[45]", "live.text[1]", 0 ],
			"obj-1::obj-360::obj-8::obj-56" : [ "Dropdown Control[7]", "Select", 0 ],
			"obj-1::obj-2::obj-9" : [ "live.text[71]", "live.text", 0 ],
			"obj-3::obj-225::obj-386::obj-8" : [ "Velocity Initial Variance Minimum[5]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-187::obj-187" : [ "rslider[16]", "rslider", 0 ],
			"obj-3::obj-396::obj-12" : [ "ratecontrol[20]", "ratecontrol", 0 ],
			"obj-96::obj-33::obj-12::obj-9" : [ "Delta Channel Menu", "Delta Channel Menu", 0 ],
			"obj-96::obj-11::obj-26" : [ "Values", "Values", 0 ],
			"obj-96::obj-81::obj-15" : [ "Jam On/Off[2]", "Jam On/Off", 0 ],
			"obj-96::obj-85::obj-45" : [ "Range Control[2]", "Range Control", 0 ],
			"obj-89::obj-8::obj-34" : [ "live.text[335]", "live.text", 0 ],
			"obj-108::obj-8::obj-56" : [ "Dropdown Control[123]", "Select", 0 ],
			"obj-1::obj-203::obj-68" : [ "Mixer / Send UI[6]", "Mixer / Send UI", 0 ],
			"obj-1::obj-158::obj-8::obj-12" : [ "Metro On/Off[48]", "Metro On/Off", 0 ],
			"obj-1::obj-362::obj-8::obj-13" : [ "Quantize Metro Rate[8]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-330::obj-12" : [ "ratecontrol[59]", "ratecontrol", 0 ],
			"obj-96::obj-33::obj-14::obj-11" : [ "receive_delta_out_channel_num[4]", "Output Channel", 0 ],
			"obj-96::obj-38::obj-16" : [ "Amp ∆ Time[1]", "Amp ∆/T", 0 ],
			"obj-96::obj-83::obj-40::obj-210" : [ "number[27]", "number[10]", 0 ],
			"obj-102::obj-8::obj-12" : [ "Metro On/Off[124]", "Metro On/Off", 0 ],
			"obj-16::obj-8::obj-18" : [ "umenu[14]", "umenu", 0 ],
			"obj-1::obj-142::obj-8::obj-18" : [ "umenu[17]", "umenu", 0 ],
			"obj-3::obj-225::obj-383::obj-8" : [ "Velocity Initial Variance Minimum[2]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-318::obj-8::obj-56" : [ "Dropdown Control[17]", "Select", 0 ],
			"obj-3::obj-397::obj-8::obj-10" : [ "Manual Metro Rate[105]", "Rate", 0 ],
			"obj-96::obj-25::obj-53" : [ "LFO High Ramp[1]", "High Ramp", 0 ],
			"obj-96::obj-4::obj-97" : [ "live.menu[5]", "live.menu", 0 ],
			"obj-89::obj-187" : [ "rslider[53]", "rslider", 0 ],
			"obj-1::obj-203::obj-113" : [ "panning[1]", "panning", 0 ],
			"obj-1::obj-158::obj-9" : [ "live.text[86]", "live.text", 0 ],
			"obj-1::obj-362::obj-9" : [ "live.text[75]", "live.text", 0 ],
			"obj-1::obj-6::obj-124" : [ "8th Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-3::obj-225::obj-283::obj-2" : [ "rslider[35]", "rslider[15]", 0 ],
			"obj-3::obj-340::obj-8::obj-13" : [ "Quantize Metro Rate[18]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-4::obj-108" : [ "New Sequence Every", "New Sequence Every", 0 ],
			"obj-96::obj-33::obj-12::obj-32" : [ "rslider", "rslider", 0 ],
			"obj-96::obj-33::obj-19::obj-11" : [ "receive_delta_out_channel_num[3]", "Output Channel", 0 ],
			"obj-96::obj-84::obj-11" : [ "Gen New Bars Button[1]", "Gen New Bars Button", 0 ],
			"obj-355::obj-8::obj-10" : [ "Manual Metro Rate[17]", "Rate", 0 ],
			"obj-16::obj-8::obj-13" : [ "Quantize Metro Rate[16]", "Quantize Metro Rate", 0 ],
			"obj-34::obj-8::obj-34" : [ "live.text[61]", "live.text", 0 ],
			"obj-1::obj-142::obj-8::obj-10" : [ "Manual Metro Rate[104]", "Rate", 0 ],
			"obj-1::obj-360::obj-8::obj-31" : [ "live.text[384]", "live.text", 0 ],
			"obj-1::obj-361::obj-8::obj-56" : [ "Dropdown Control[6]", "Select", 0 ],
			"obj-1::obj-1::obj-8::obj-18" : [ "umenu[6]", "umenu", 0 ],
			"obj-3::obj-318::obj-12" : [ "ratecontrol[44]", "ratecontrol", 0 ],
			"obj-3::obj-397::obj-187" : [ "rslider[11]", "rslider", 0 ],
			"obj-96::obj-49::obj-132" : [ "LFO Freq[2]", "Freq", 0 ],
			"obj-96::obj-25::obj-132" : [ "LFO Freq[1]", "Freq", 0 ],
			"obj-96::obj-6::obj-15" : [ "Jam On/Off[1]", "Jam On/Off", 0 ],
			"obj-96::obj-80::obj-21" : [ "Jam Bars[6]", "Jam Bars", 0 ],
			"obj-110::obj-8::obj-31" : [ "live.text[262]", "live.text", 0 ],
			"obj-25::obj-8::obj-13" : [ "Quantize Metro Rate[1]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-28::obj-2::obj-21" : [ "Jam Bars Button[3]", "Jam Bars Button", 0 ],
			"obj-1::obj-365::obj-68" : [ "Mixer / Send UI[7]", "Mixer / Send UI", 0 ],
			"obj-3::obj-225::obj-376::obj-8" : [ "Velocity Initial Variance Minimum[13]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-225::obj-16" : [ "note variance", "note variance", 0 ],
			"obj-3::obj-9::obj-9" : [ "live.text[210]", "New waveform", 0 ],
			"obj-3::obj-340::obj-12" : [ "ratecontrol[46]", "ratecontrol", 0 ],
			"obj-3::obj-358::obj-8::obj-13" : [ "Quantize Metro Rate[13]", "Quantize Metro Rate", 0 ],
			"obj-96::obj-33::obj-18::obj-11" : [ "receive_delta_out_channel_num[2]", "Output Channel", 0 ],
			"obj-96::obj-4::obj-21" : [ "Jam Bars", "Jam Bars", 0 ],
			"obj-96::obj-84::obj-43" : [ "Number of Bars[3]", "Bars", 0 ],
			"obj-105::obj-8::obj-56" : [ "Dropdown Control[124]", "Select", 0 ],
			"obj-355::obj-9" : [ "live.text[394]", "live.text", 0 ],
			"obj-34::obj-30" : [ "live.text[59]", "live.text[1]", 0 ],
			"obj-1::obj-1::obj-9" : [ "live.text[67]", "live.text", 0 ],
			"obj-3::obj-225::obj-82" : [ "Randomize note number", "Randomize note number", 0 ],
			"obj-3::obj-225::obj-124" : [ "drummatrix_shuffle_max", "drummatrix_shuffle_max", 0 ],
			"obj-3::obj-319::obj-8::obj-13" : [ "Quantize Metro Rate[14]", "Quantize Metro Rate", 0 ],
			"obj-96::obj-49::obj-122" : [ "LFO Intensity[2]", "Intensity", 0 ],
			"obj-96::obj-33::obj-12::obj-11" : [ "receive_delta_out_channel_num[7]", "Output Channel", 0 ],
			"obj-96::obj-7::obj-68" : [ "File", "File", 0 ],
			"obj-96::obj-82::obj-15" : [ "DB slide down rate[1]", "DB slide down rate", 0 ],
			"obj-98::obj-8::obj-31" : [ "live.text[332]", "live.text", 0 ],
			"obj-110::obj-9" : [ "live.text[260]", "live.text", 0 ],
			"obj-25::obj-12" : [ "ratecontrol[2]", "ratecontrol", 0 ],
			"obj-1::obj-28::obj-2::obj-26" : [ "Values[3]", "Values", 0 ],
			"obj-1::obj-160::obj-8::obj-56" : [ "Dropdown Control[8]", "Select", 0 ],
			"obj-1::obj-6::obj-182" : [ "DHNotes32", "DHNotes32", 0 ],
			"obj-3::obj-225::obj-380::obj-8" : [ "Velocity Initial Variance Minimum[11]", "Velocity Initial Variance Minimum", 0 ],
			"obj-96::obj-33::obj-17::obj-32" : [ "rslider[6]", "rslider", 0 ],
			"obj-96::obj-4::obj-9" : [ "Manual Rate", "Manual Rate", 0 ],
			"obj-96::obj-11::obj-28" : [ "Loop Jam Bars Button", "Loop Jam Bars Button", 0 ],
			"obj-96::obj-75::obj-22" : [ "Rampsmooth Down Time", "Rampsmooth Down Time", 0 ],
			"obj-96::obj-79::obj-1" : [ "live.toggle[12]", "live.toggle", 0 ],
			"obj-96::obj-84::obj-26" : [ "Values[1]", "Values", 0 ],
			"obj-105::obj-187" : [ "rslider[50]", "rslider", 0 ],
			"obj-37::obj-8::obj-12" : [ "Metro On/Off[21]", "Metro On/Off", 0 ],
			"obj-1::obj-149::obj-8::obj-13" : [ "Quantize Metro Rate[26]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-361::obj-8::obj-10" : [ "Manual Metro Rate[51]", "Rate", 0 ],
			"obj-3::obj-225::obj-10" : [ "Reset Note ∆ Button", "Reset Note ∆ Button", 0 ],
			"obj-3::obj-319::obj-8::obj-34" : [ "live.text[110]", "live.text", 0 ],
			"obj-3::obj-14::obj-193" : [ "Random or Increment New Sequence[1]", "Random or Increment New Sequence", 0 ],
			"obj-96::obj-33::obj-13::obj-2" : [ "Flip Output On/Off[1]", "Flip Output On/Off", 0 ],
			"obj-96::obj-7::obj-49" : [ "DB frame rate", "rate (ms)", 0 ],
			"obj-96::obj-82::obj-13" : [ "SlideDown[1]", "SlideDown", 0 ],
			"obj-98::obj-30" : [ "live.text[329]", "live.text[1]", 0 ],
			"obj-114::obj-8::obj-18" : [ "umenu[49]", "umenu", 0 ],
			"obj-31::obj-8::obj-34" : [ "live.text[51]", "live.text", 0 ],
			"obj-1::obj-160::obj-187" : [ "rslider[93]", "rslider", 0 ],
			"obj-1::obj-6::obj-349" : [ "Quantized Sample Duration[1]", "Quantized Sample Duration", 0 ],
			"obj-1::obj-2::obj-8::obj-31" : [ "live.text[73]", "live.text", 0 ],
			"obj-3::obj-225::obj-377::obj-2" : [ "rslider[27]", "rslider[15]", 0 ],
			"obj-3::obj-187::obj-8::obj-56" : [ "Dropdown Control[18]", "Select", 0 ],
			"obj-3::obj-396::obj-8::obj-12" : [ "Metro On/Off[72]", "Metro On/Off", 0 ],
			"obj-96::obj-48::obj-43" : [ "function[1]", "function", 0 ],
			"obj-96::obj-78::obj-13" : [ "Duration (ms)[3]", "Duration (ms)", 0 ],
			"obj-96::obj-85::obj-18" : [ "Jam Bars[8]", "Jam Bars", 0 ],
			"obj-20::obj-8::obj-13" : [ "Quantize Metro Rate[131]", "Quantize Metro Rate", 0 ],
			"obj-37::obj-30" : [ "live.text[448]", "live.text[1]", 0 ],
			"obj-1::obj-149::obj-9" : [ "live.text[379]", "live.text", 0 ],
			"obj-1::obj-361::obj-9" : [ "live.text[79]", "live.text", 0 ],
			"obj-1::obj-260::obj-12" : [ "MetroSpeed", "MetroSpeed", 0 ],
			"obj-1::obj-7::obj-8::obj-12" : [ "Metro On/Off[127]", "Metro On/Off", 0 ],
			"obj-3::obj-330::obj-8::obj-56" : [ "Dropdown Control[20]", "Select", 0 ],
			"obj-3::obj-319::obj-9" : [ "live.text[109]", "live.text", 0 ],
			"obj-3::obj-14::obj-108" : [ "New Sequence Every[1]", "New Sequence Every", 0 ],
			"obj-96::obj-33::obj-15::obj-11" : [ "receive_delta_out_channel_num[5]", "Output Channel", 0 ],
			"obj-96::obj-11::obj-43" : [ "Number of Bars[5]", "Bars", 0 ],
			"obj-96::obj-83::obj-36::obj-1" : [ "MatrixCtrl[1]", "MatrixCtrl", 0 ],
			"obj-114::obj-8::obj-31" : [ "live.text[258]", "live.text", 0 ],
			"obj-31::obj-8::obj-31" : [ "live.text[52]", "live.text", 0 ],
			"obj-1::obj-6::obj-156" : [ "live.gain~[5]", "live.gain~", 0 ],
			"obj-1::obj-2::obj-8::obj-13" : [ "Quantize Metro Rate[7]", "Quantize Metro Rate", 0 ],
			"obj-3::obj-225::obj-387::obj-2" : [ "rslider[25]", "rslider[15]", 0 ],
			"obj-3::obj-187::obj-12" : [ "ratecontrol[45]", "ratecontrol", 0 ],
			"obj-3::obj-396::obj-30" : [ "live.text[92]", "live.text[1]", 0 ],
			"obj-96::obj-47::obj-1" : [ "live.toggle[4]", "live.toggle", 0 ],
			"obj-96::obj-81::obj-9" : [ "Manual Rate[2]", "Manual Rate", 0 ],
			"obj-96::obj-85::obj-43" : [ "Number of Bars[2]", "Bars", 0 ],
			"obj-108::obj-8::obj-13" : [ "Quantize Metro Rate[121]", "Quantize Metro Rate", 0 ],
			"obj-20::obj-12" : [ "ratecontrol[1]", "ratecontrol", 0 ],
			"obj-1::obj-28::obj-22" : [ "Repeat Rate Adjust", "Rate", 0 ],
			"obj-1::obj-158::obj-8::obj-56" : [ "Dropdown Control[9]", "Select", 0 ],
			"obj-1::obj-362::obj-8::obj-12" : [ "Metro On/Off[18]", "Metro On/Off", 0 ],
			"obj-1::obj-260::obj-63" : [ "playback speed", "playback speed", 0 ],
			"obj-1::obj-260::obj-104" : [ "Master[1]", "Master", 0 ],
			"obj-3::obj-330::obj-187" : [ "rslider[18]", "rslider", 0 ],
			"obj-3::obj-358::obj-8::obj-34" : [ "live.text[100]", "live.text", 0 ],
			"obj-96::obj-33::obj-14::obj-2" : [ "Flip Output On/Off[3]", "Flip Output On/Off", 0 ],
			"obj-102::obj-8::obj-31" : [ "live.text[328]", "live.text", 0 ],
			"obj-114::obj-30" : [ "live.text[57]", "live.text[1]", 0 ],
			"obj-31::obj-187" : [ "rslider[113]", "rslider", 0 ],
			"obj-1::obj-360::obj-8::obj-13" : [ "Quantize Metro Rate[10]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-2::obj-187" : [ "rslider[89]", "rslider", 0 ],
			"obj-3::obj-225::obj-384::obj-2" : [ "rslider[22]", "rslider[15]", 0 ],
			"obj-3::obj-225::obj-198" : [ "note volume", "note volume", 0 ],
			"obj-3::obj-225::obj-81" : [ "Loop Control", "Loop Control", 0 ],
			"obj-3::obj-318::obj-8::obj-18" : [ "umenu[75]", "umenu", 0 ],
			"obj-3::obj-397::obj-8::obj-18" : [ "umenu[18]", "umenu", 0 ],
			"obj-96::obj-25::obj-122" : [ "LFO Intensity[1]", "Intensity", 0 ],
			"obj-96::obj-85::obj-21" : [ "Jam Bars Button[2]", "Jam Bars Button", 0 ],
			"obj-89::obj-8::obj-56" : [ "Dropdown Control[127]", "Select", 0 ],
			"obj-108::obj-187" : [ "rslider[49]", "rslider", 0 ],
			"obj-16::obj-12" : [ "ratecontrol[26]", "ratecontrol", 0 ],
			"obj-1::obj-158::obj-8::obj-18" : [ "umenu[12]", "umenu", 0 ],
			"obj-1::obj-362::obj-8::obj-18" : [ "umenu[8]", "umenu", 0 ],
			"obj-1::obj-6::obj-265" : [ "multislider", "multislider", 0 ],
			"obj-1::obj-6::obj-122" : [ "32nd Note Playback Speed[1]", "Note Speed", 0 ],
			"obj-3::obj-225::obj-283::obj-8" : [ "Velocity Initial Variance Minimum[16]", "Velocity Initial Variance Minimum", 0 ],
			"obj-3::obj-225::obj-173" : [ "drummatrix_regen_probability", "drummatrix_regen_probability", 0 ],
			"obj-3::obj-340::obj-8::obj-56" : [ "Dropdown Control[19]", "Select", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_LFO.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ramp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_delta_to_CC.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_receive_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_multiplex.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ok.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mux_toggle_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mute.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "_slider2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jam_change.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_db_data.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "001.txt",
				"bootpath" : "~/12c/12c_sandbox/db",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_subdivide.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_markov2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ctrl_audio2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "amp_delta_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "1ch_amp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_output_channel.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_feedback_reduction.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_preset_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "filename concat",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/probability",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "markov_init.js",
				"bootpath" : "~/12c/12c_sandbox/probability/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bpatcher_name.js",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_main_mixer_sends.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_ui_panning.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "stutter.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_dihedral2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "create_slice_msg.maxpat",
				"bootpath" : "~/12c/12c_sandbox/m4l",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dihed_rand.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dihed_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dh_markov2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_stepper.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "hipass.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_cv.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_main_instruments.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_drummatrix2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "drummatrix_cell.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "matrix_note.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI/drum_matrix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "note_variance.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI/drum_matrix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "velocity_range.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "solo.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "lock.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "velocity_variance.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI/drum_matrix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_harmonic_synth.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_synthbuild.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_oscillator.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_filter.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio/synth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_bass_stepper.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "midiscale.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "vb.pitch~.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
