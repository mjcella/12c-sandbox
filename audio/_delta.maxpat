{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 56.0, 83.0, 1150.0, 654.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2467.201416, -189.88765, 55.0, 22.0 ],
					"presentation_rect" : [ 2467.415771, -189.88765, 0.0, 0.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2343.820312, -189.88765, 55.0, 22.0 ],
					"presentation_rect" : [ 2340.449463, -189.88765, 0.0, 0.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2220.224854, -189.88765, 55.0, 22.0 ],
					"presentation_rect" : [ 2219.101318, -189.88765, 0.0, 0.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2091.011475, -189.88765, 55.0, 22.0 ],
					"presentation_rect" : [ 2087.640625, -189.88765, 0.0, 0.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1966.292358, -189.88765, 55.0, 22.0 ],
					"presentation_rect" : [ 1964.045166, -189.88765, 0.0, 0.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1839.32605, -189.88765, 55.0, 22.0 ],
					"presentation_rect" : [ 1837.078857, -191.011246, 0.0, 0.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1716.854126, -189.88765, 55.0, 22.0 ],
					"presentation_rect" : [ 1710.112427, -192.134842, 0.0, 0.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1589.887695, -189.88765, 55.0, 22.0 ],
					"style" : "",
					"text" : "delta_cv"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2467.201416, -158.809082, 55.730339, 22.0 ],
					"style" : "",
					"text" : "dac~ 28"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2343.820312, -158.809082, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 27"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2220.224854, -158.809082, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 26"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2091.011475, -158.809082, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 25"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1966.292358, -158.809082, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1839.32605, -158.809082, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 23"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1716.854126, -158.809082, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1589.887695, -158.809082, 54.0, 22.0 ],
					"style" : "",
					"text" : "dac~ 21"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1406.333374, -410.333344, 218.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 45, 749, 737 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"style" : "",
					"text" : "pattrstorage @savemode 1 @greedy 1",
					"varname" : "u052003144"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "delta" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_preset_select.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1406.333374, -477.93338, 474.666626, 57.933388 ],
					"presentation" : 1,
					"presentation_rect" : [ 950.599243, 19.399952, 455.999969, 57.933388 ],
					"varname" : "_preset_select",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 535.710938, -409.0, 125.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0.0001 50."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 413.0, -409.0, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 1. 1000."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 344.0, -446.0, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 1. 1000."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-85",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_subdivide.maxpat",
					"numinlets" : 0,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1091.168091, 241.563675, 516.169312, 116.603027 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.616699, 426.125183, 509.565826, 113.346558 ],
					"varname" : "Subdivide2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-84",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_subdivide.maxpat",
					"numinlets" : 0,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1091.168091, 393.633728, 516.169312, 116.536331 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.616699, 538.221741, 509.565826, 117.01326 ],
					"varname" : "Subdivide3",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-83",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_multiplex.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 757.500061, -259.670441, 642.166626, 78.561798 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.56665, 243.730194, 655.800049, 70.966972 ],
					"varname" : "Mux2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-82",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_db_data.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1396.674194, 612.322021, 254.726593, 53.677906 ],
					"presentation" : 1,
					"presentation_rect" : [ 910.599243, 122.333313, 256.583282, 51.763252 ],
					"varname" : "DB2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-80",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_slider2.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 315.582428, 631.850159, 256.58429, 75.34082 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.616699, 243.230194, 254.000961, 71.466972 ],
					"varname" : "Slider2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-81",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_slider2.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 315.582428, 762.516846, 256.58429, 75.34082 ],
					"presentation" : 1,
					"presentation_rect" : [ 910.599243, 243.230194, 256.583282, 71.466972 ],
					"varname" : "Slider4",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-78",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ramp.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 363.475677, 21.333405, 322.324402, 173.333328 ],
					"presentation" : 1,
					"presentation_rect" : [ 331.533356, 482.346558, 323.333344, 172.096573 ],
					"varname" : "Ramp4",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-79",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ramp.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 34.249084, 21.333405, 315.190979, 173.333328 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.56665, 482.346558, 330.466705, 172.096573 ],
					"varname" : "Ramp3",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-75",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ctrl_audio2.maxpat",
					"numinlets" : 0,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "int", "", "float" ],
					"patching_rect" : [ 363.475677, 232.666656, 279.333344, 265.836761 ],
					"presentation" : 1,
					"presentation_rect" : [ 1164.182495, 122.833313, 306.555267, 271.166656 ],
					"varname" : "CC Audio2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"comment" : "DB 2 Rate",
					"id" : "obj-36",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 674.036743, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "DB 1 rate ",
					"id" : "obj-40",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 634.710938, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Slider 4 Rate",
					"id" : "obj-28",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 596.508667, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Slider 3 Rate",
					"id" : "obj-22",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 557.182861, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Slider 2 Rate",
					"id" : "obj-9",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 508.733368, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Slider 1 Rate",
					"id" : "obj-8",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 469.733368, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 8",
					"id" : "obj-58",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2429.583496, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 7",
					"id" : "obj-61",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2303.869141, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 6",
					"id" : "obj-64",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2178.154785, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 5",
					"id" : "obj-67",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2052.44043, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 4",
					"id" : "obj-55",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1926.726196, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 3",
					"id" : "obj-51",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1801.011963, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 2",
					"id" : "obj-44",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1675.297607, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Delta Out 1",
					"id" : "obj-43",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1549.583374, -162.809082, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-38",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ctrl_audio2.maxpat",
					"numinlets" : 0,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "int", "", "float" ],
					"patching_rect" : [ 34.249084, 232.666656, 282.0, 266.836761 ],
					"presentation" : 1,
					"presentation_rect" : [ 1164.182495, 385.568359, 306.555267, 271.166656 ],
					"varname" : "CC Audio1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 523.710938, -452.666687, 125.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 0.0001 50."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.0, -409.0, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 1. 1000."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 213.0, -446.0, 112.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. 1. 1000."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 157.333344, -409.0, 123.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -128. 128."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 81.333336, -446.0, 123.0, 22.0 ],
					"style" : "",
					"text" : "scale 0. 1. -128. 128."
				}

			}
, 			{
				"box" : 				{
					"comment" : "Ramp 4 Duration",
					"id" : "obj-24",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 413.0, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Ramp 3 Duration",
					"id" : "obj-23",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 344.0, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Ramp 2 Duration",
					"id" : "obj-21",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.0, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "Ramp 1 Duration",
					"id" : "obj-20",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 213.0, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "LFO 2 Offset",
					"id" : "obj-18",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 157.333344, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "LFO 2 Intensity",
					"id" : "obj-17",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 121.333336, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "LFO 1 Offset",
					"id" : "obj-14",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 81.333336, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "LFO 1 Intensity",
					"id" : "obj-13",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 30.0, -509.666626, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-11",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_subdivide.maxpat",
					"numinlets" : 0,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1091.168091, 97.63372, 516.169312, 120.536331 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.616699, 314.197174, 509.565826, 112.928024 ],
					"varname" : "Subdivide1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-7",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_db_data.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1110.007446, 612.322021, 254.726593, 53.677906 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.366699, 122.333313, 256.583282, 51.763252 ],
					"varname" : "DB1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_slider2.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 34.249084, 631.850159, 256.58429, 75.34082 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.366699, 172.096558, 254.250961, 72.633636 ],
					"varname" : "Slider1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_slider2.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 34.249084, 762.516846, 256.58429, 75.34082 ],
					"presentation" : 1,
					"presentation_rect" : [ 910.599243, 172.096558, 256.583282, 72.633636 ],
					"varname" : "Slider3",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_multiplex.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 757.500061, -355.40448, 643.0, 80.808983 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.56665, 173.096558, 659.800049, 73.633636 ],
					"varname" : "Mux1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1064.0, -64.33326, 69.0, 22.0 ],
					"style" : "",
					"text" : "delay 2000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1064.0, -29.999977, 97.0, 22.0 ],
					"style" : "",
					"text" : "zoomfactor 0.89"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1064.0, -98.999939, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1043.199951, -127.200005, 150.0, 20.0 ],
					"style" : "",
					"text" : "fits my screen well :D"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1064.0, 2.733343, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-33",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_delta_to_CC.maxpat",
					"numinlets" : 0,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 1549.583374, -333.198486, 899.0, 127.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.56665, -4.618408, 1461.837769, 127.416672 ],
					"varname" : "DeltaReceiveContainer",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 785.250061, -391.666656, 115.0, 22.0 ],
					"style" : "",
					"text" : "s delta_module_out"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1484.083374, -29.966648, 37.0, 22.0 ],
					"style" : "",
					"text" : "adc~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1432.833374, -71.466652, 89.0, 22.0 ],
					"style" : "",
					"text" : "loadmess start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1432.833374, -29.966648, 37.0, 22.0 ],
					"style" : "",
					"text" : "dac~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"linecount" : 12,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1260.333374, -114.85392, 139.333328, 167.0 ],
					"style" : "",
					"text" : "1 - LFO 1\n2 - LFO 2\n3 - Ramp 1\n4 - Ramp 2\n5 - Ramp 3\n6 - Ramp 4\n7-10 - CC audio 1\n11-14 - CC audio 2\n15-18 - sliders 1-4\n19-20 - DB data out 1-2\n21-22 - Mux 1-2\n23-25 - subdivides 1-3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 25,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 757.500061, -431.333344, 363.0, 22.0 ],
					"style" : "",
					"text" : "pak 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-25",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_LFO.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 30.0, -263.699982, 328.266693, 54.400002 ],
					"presentation" : 1,
					"presentation_rect" : [ 331.100037, 122.333313, 329.600037, 51.763252 ],
					"varname" : "LFO2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-47",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ramp.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 27.115662, -174.666611, 322.324402, 173.333328 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.56665, 314.197174, 330.466705, 172.096573 ],
					"varname" : "Ramp1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-48",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ramp.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 363.475677, -174.666611, 315.190979, 173.333328 ],
					"presentation" : 1,
					"presentation_rect" : [ 331.033356, 314.197174, 330.333344, 172.096573 ],
					"varname" : "Ramp2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-49",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_LFO.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 30.0, -337.266663, 328.266693, 50.933331 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.56665, 122.333313, 327.533386, 51.763252 ],
					"varname" : "LFO1",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 22 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1100.668091, 225.33334, 1074.666699, 225.33334, 1074.666699, 182.666672, 785.333357, 174.666672, 785.333357, -98.66667, 737.333355, -98.66667, 737.333355, -453.333347, 1082.333394, -453.333347 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1415.833374, -378.666678, 1396.000042, -378.666678, 1396.000042, -490.666681, 1415.833374, -490.666681 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 353.5, -416.000012, 18.666667, -416.000012, 18.666667, 10.666667, 43.749084, 10.666667 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 130.833336, -454.400007, 65.600001, -454.400007, 65.600001, -362.400005, 20.0, -362.400005, 20.0, -274.666672, 39.5, -274.666672 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 166.833344, -453.600007, 208.800003, -453.600007, 208.800003, -421.600006, 166.833344, -421.600006 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 767.000061, -371.0, 747.0, -371.0, 747.0, -268.0, 767.000061, -268.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 20 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 767.000061, -284.000008, 734.666689, -284.000008, 734.666689, -457.333347, 1053.666728, -457.333347 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 296.5, -455.200007, 334.400005, -455.200007, 334.400005, -416.000006, 296.5, -416.000006 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 566.682861, -470.666681, 6.666667, -470.666681, 6.666667, 753.333356, 43.749084, 753.333356 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 422.5, -457.333347, 464.000014, -457.333347, 464.000014, -422.666679, 422.5, -422.666679 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 39.5, -200.000006, 734.666689, -200.000006, 734.666689, -449.333347, 781.333394, -449.333347 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 90.833336, -362.400005, 348.766693, -362.400005 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 166.833344, -369.600006, 672.80001, -369.600006, 672.80001, -272.533338, 348.766693, -272.533338 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 606.008667, -470.666681, 8.0, -470.666681, 8.0, 754.666689, 325.082428, 754.666689 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 422.5, -382.666678, 18.666667, -382.666678, 18.666667, 13.333334, 372.975677, 13.333334 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 222.5, -414.400006, 16.0, -414.400006, 16.0, -184.266671, 372.975677, -184.266671 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 296.5, -376.000011, 10.666667, -376.000011, 10.666667, -192.000006, 36.615662, -192.000006 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1559.083374, -201.123612, 1599.387695, -201.123612 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1684.79766, -200.000016, 1726.354126, -200.000016 ],
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1810.511945, -201.123612, 1848.82605, -201.123612 ],
					"source" : [ "obj-33", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1936.226231, -198.87642, 1975.792358, -198.87642 ],
					"source" : [ "obj-33", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2061.940517, -201.123612, 2100.511475, -201.123612 ],
					"source" : [ "obj-33", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2187.654803, -200.000016, 2229.724854, -200.000016 ],
					"source" : [ "obj-33", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2313.369088, -201.123612, 2353.320312, -201.123612 ],
					"source" : [ "obj-33", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 2439.083374, -200.000016, 2476.701416, -200.000016 ],
					"source" : [ "obj-33", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 545.210938, -380.000011, 713.333355, -380.000011, 713.333355, 146.666671, 1074.666699, 146.666671, 1074.666699, 598.666685, 1406.174194, 598.666685 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 683.536743, -417.333346, 545.210938, -417.333346 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 533.210938, -426.666679, 713.333355, -426.666679, 713.333355, 149.333338, 1073.333365, 149.333338, 1073.333365, 597.333351, 1119.507446, 597.333351 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 13 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 306.749084, 573.33335, 1036.000031, 573.33335, 1036.000031, 177.333339, 785.333357, 177.333339, 785.333357, -97.333336, 737.333355, -97.333336, 737.333355, -453.333347, 953.333394, -453.333347 ],
					"source" : [ "obj-38", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 12 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 219.082417, 572.000017, 1033.333364, 572.000017, 1033.333364, 178.666672, 788.000023, 178.666672, 788.000023, -96.000003, 737.333355, -96.000003, 737.333355, -453.333347, 939.000061, -453.333347 ],
					"source" : [ "obj-38", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 11 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 131.415751, 573.33335, 1034.666698, 573.33335, 1034.666698, 177.333339, 785.333357, 177.333339, 785.333357, -97.333336, 737.333355, -97.333336, 737.333355, -454.66668, 924.666728, -454.66668 ],
					"source" : [ "obj-38", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 10 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 43.749084, 573.33335, 1036.000031, 573.33335, 1036.000031, 176.000005, 788.000023, 176.000005, 788.000023, -97.333336, 738.666689, -97.333336, 738.666689, -453.333347, 910.333394, -453.333347 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 15 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 43.749084, 861.333359, 589.333351, 861.333359, 589.333351, 733.333355, 1058.666698, 733.333355, 1058.666698, 176.000005, 785.333357, 176.000005, 785.333357, -97.333336, 738.666689, -97.333336, 738.666689, -97.333336, 738.666689, -456.000014, 982.000061, -456.000014 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 644.210938, -465.333347, 533.210938, -465.333347 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 36.615662, 9.333334, 736.000022, 9.333334, 736.000022, -449.333347, 795.666728, -449.333347 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 372.975677, 8.0, 734.666689, 8.0, 734.666689, -449.333347, 810.000061, -449.333347 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 39.5, -278.666675, 734.666689, -278.666675, 734.666689, -448.000013, 767.000061, -448.000013 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 14 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 43.749084, 734.666689, 1056.000031, 734.666689, 1056.000031, 176.000005, 782.66669, 176.000005, 782.66669, -96.000003, 737.333355, -96.000003, 737.333355, -456.000014, 967.666728, -456.000014 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 18 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1119.507446, 678.65174, 1073.033794, 678.65174, 1073.033794, 179.775295, 784.269726, 179.775295, 784.269726, -96.629221, 738.202306, -96.629221, 738.202306, -449.438238, 1025.000061, -458.427003 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 9 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 633.309021, 572.800017, 1033.333364, 572.800017, 1033.333364, 178.666672, 784.000023, 178.666672, 784.000023, -98.66667, 737.333355, -98.66667, 737.333355, -453.333347, 896.000061, -453.333347 ],
					"source" : [ "obj-75", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 8 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 546.53124, 571.200017, 1033.333364, 571.200017, 1033.333364, 178.666672, 785.333357, 178.666672, 785.333357, -96.000003, 737.333355, -96.000003, 737.333355, -453.333347, 881.666728, -453.333347 ],
					"source" : [ "obj-75", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 459.753458, 573.33335, 1032.000031, 573.33335, 1032.000031, 181.333339, 785.333357, 181.333339, 785.333357, -96.000003, 737.333355, -96.000003, 737.333355, -452.000013, 867.333394, -452.000013 ],
					"source" : [ "obj-75", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 372.975677, 572.53335, 1032.000031, 572.53335, 1032.000031, 182.666672, 784.000023, 182.666672, 784.000023, -96.000003, 737.333355, -96.000003, 737.333355, -450.66668, 853.000061, -450.66668 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 372.975677, 205.333339, 738.666689, 205.333339, 738.666689, -450.66668, 838.666728, -450.66668 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 43.749084, 206.666673, 734.666689, 206.666673, 734.666689, -450.66668, 824.333394, -450.66668 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 479.233368, -468.000014, 4.0, -468.000014, 4.0, 614.666685, 43.749084, 614.666685 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 16 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 325.082428, 734.666689, 1060.000032, 734.666689, 1060.000032, 174.666672, 784.000023, 174.666672, 784.000023, -96.000003, 736.000022, -96.000003, 736.000022, -96.000003, 736.000022, -456.000014, 996.333394, -456.000014 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 17 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 325.082428, 861.333359, 590.666684, 861.333359, 590.666684, 732.000022, 1058.666698, 732.000022, 1058.666698, 174.666672, 785.333357, 174.666672, 785.333357, -97.333336, 736.000022, -97.333336, 736.000022, -458.66668, 1010.666728, -458.66668 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 19 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1406.174194, 678.65174, 1073.033794, 678.65174, 1073.033794, 180.898891, 783.14613, 180.898891, 783.14613, -96.629221, 735.955115, -96.629221, 735.955115, -458.427003, 1039.333394, -458.427003 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 21 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 767.000061, -177.333339, 736.000022, -177.333339, 736.000022, -456.000014, 1068.000061, -456.000014 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 24 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1100.668091, 524.000016, 1060.000032, 524.000016, 1060.000032, 177.333339, 788.000023, 177.333339, 788.000023, -96.000003, 734.666689, -96.000003, 734.666689, -456.000014, 1111.000061, -456.000014 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 23 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1100.668091, 369.333344, 1057.333365, 369.333344, 1057.333365, 177.333339, 786.66669, 177.333339, 786.66669, -96.000003, 737.333355, -96.000003, 737.333355, -454.66668, 1096.666728, -454.66668 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 518.233368, -469.333347, 4.0, -469.333347, 4.0, 616.000018, 325.082428, 616.000018 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1442.333374, -37.866664, 1493.583374, -37.866664 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-25::obj-22" : [ "LFO On[1]", "On", 0 ],
			"obj-84::obj-21" : [ "Jam Bars Button[1]", "Jam Bars Button", 0 ],
			"obj-33::obj-18::obj-11" : [ "Delta Channel Number[2]", "Delta Channel Number", 0 ],
			"obj-4::obj-97" : [ "Quantized Rate", "Quantized Rate", 0 ],
			"obj-81::obj-15" : [ "Jam On/Off[2]", "Jam On/Off", 0 ],
			"obj-80::obj-15" : [ "Jam On/Off[3]", "Jam On/Off", 0 ],
			"obj-33::obj-12::obj-2" : [ "Flip Output On/Off[7]", "Flip Output On/Off", 0 ],
			"obj-38::obj-22" : [ "Rampsmooth Down Time", "Rampsmooth Down Time", 0 ],
			"obj-84::obj-25" : [ "Jam Bars On/Off[1]", "Jam Bars On/Off", 0 ],
			"obj-49::obj-53" : [ "High Ramp", "Hi Ramp", 0 ],
			"obj-33::obj-17::obj-2" : [ "Flip Output On/Off[1]", "Flip Output On/Off", 0 ],
			"obj-82::obj-8" : [ "DB file selected[2]", "DB file selected", 0 ],
			"obj-48::obj-56" : [ "Duration (note)[3]", "Duration (note)", 0 ],
			"obj-85::obj-11" : [ "Gen New Bars Button[2]", "Gen New Bars Button", 0 ],
			"obj-49::obj-133" : [ "Y-Offset", "Y +/-", 0 ],
			"obj-82::obj-11" : [ "Direction[1]", "Direction", 0 ],
			"obj-33::obj-12::obj-32" : [ "rslider[7]", "rslider", 0 ],
			"obj-75::obj-16" : [ "Amp ∆ Time[1]", "Amp ∆/T", 0 ],
			"obj-85::obj-26" : [ "Values[2]", "Values", 0 ],
			"obj-11::obj-21" : [ "Jam Bars Button", "Jam Bars Button", 0 ],
			"obj-83::obj-36::obj-123" : [ "Shuffle Mux[1]", "Shuffle Mux", 0 ],
			"obj-79::obj-13" : [ "Duration (ms)[2]", "Duration (ms)", 0 ],
			"obj-85::obj-21" : [ "Jam Bars Button[2]", "Jam Bars Button", 0 ],
			"obj-6::obj-12" : [ "Slider Qty[1]", "Slider Qty", 0 ],
			"obj-11::obj-30" : [ "Loop Jam On/Off", "Loop Jam On/Off", 0 ],
			"obj-7::obj-15" : [ "SlideUp", "SlideUp", 0 ],
			"obj-78::obj-13" : [ "Duration (ms)[3]", "Duration (ms)", 0 ],
			"obj-25::obj-35" : [ "Modulation Amount[1]", "Modulation", 0 ],
			"obj-2::obj-36::obj-123" : [ "Shuffle Mux", "Shuffle Mux", 0 ],
			"obj-84::obj-43" : [ "Number of Bars[1]", "Bars", 0 ],
			"obj-4::obj-15" : [ "Jam On/Off", "Jam On/Off", 0 ],
			"obj-81::obj-21" : [ "Jam Bars[5]", "Jam Bars", 0 ],
			"obj-80::obj-19" : [ "Smooth Output On/Off[3]", "Smooth Output On/Off", 0 ],
			"obj-38::obj-13" : [ "Freq ∆ Time", "Freq ∆ Time", 0 ],
			"obj-84::obj-57" : [ "Smooth Output[1]", "Smooth Output", 0 ],
			"obj-33::obj-16::obj-11" : [ "Delta Channel Number", "Delta Channel Number", 0 ],
			"obj-49::obj-132" : [ "Freq", "Freq", 0 ],
			"obj-33::obj-12::obj-9" : [ "Delta Channel Menu[7]", "Delta Channel Menu", 0 ],
			"obj-49::obj-35" : [ "Modulation Amount", "Modulation", 0 ],
			"obj-82::obj-13" : [ "SlideDown[1]", "SlideDown", 0 ],
			"obj-33::obj-13::obj-9" : [ "Delta Channel Menu[6]", "Delta Channel Menu", 0 ],
			"obj-75::obj-6" : [ "Master Volume[1]", "Master Volume", 0 ],
			"obj-85::obj-20" : [ "Loop Controller[2]", "Loop Controller", 0 ],
			"obj-11::obj-11" : [ "Gen New Bars Button", "Gen New Bars Button", 0 ],
			"obj-33::obj-15::obj-9" : [ "Delta Channel Menu[5]", "Delta Channel Menu", 0 ],
			"obj-33::obj-17::obj-32" : [ "rslider[1]", "rslider", 0 ],
			"obj-79::obj-43" : [ "function[2]", "function", 0 ],
			"obj-85::obj-25" : [ "Jam Bars On/Off[2]", "Jam Bars On/Off", 0 ],
			"obj-47::obj-1" : [ "Loop On/Off[1]", "Loop On/Off", 0 ],
			"obj-11::obj-27" : [ "Loop Jam Bars", "Loop Jam Bars", 0 ],
			"obj-11::obj-26" : [ "Values", "Values", 0 ],
			"obj-83::obj-50" : [ "Mixer[1]", "Mixer", 0 ],
			"obj-33::obj-14::obj-2" : [ "Flip Output On/Off[4]", "Flip Output On/Off", 0 ],
			"obj-7::obj-11" : [ "Direction", "Direction", 0 ],
			"obj-25::obj-133" : [ "Y-Offset[1]", "Y +/-", 0 ],
			"obj-6::obj-97" : [ "Quantized Rate[1]", "Quantized Rate", 0 ],
			"obj-11::obj-45" : [ "Range Control", "Range Control", 0 ],
			"obj-84::obj-26" : [ "Values[1]", "Values", 0 ],
			"obj-48::obj-43" : [ "function[3]", "function", 0 ],
			"obj-33::obj-19::obj-11" : [ "Delta Channel Number[3]", "Delta Channel Number", 0 ],
			"obj-11::obj-15" : [ "Loop On/Off[8]", "Loop On/Off", 0 ],
			"obj-25::obj-132" : [ "Freq[1]", "Freq", 0 ],
			"obj-84::obj-15" : [ "Loop On/Off[10]", "Loop On/Off", 0 ],
			"obj-33::obj-18::obj-9" : [ "Delta Channel Menu[2]", "Delta Channel Menu", 0 ],
			"obj-4::obj-12" : [ "Slider Qty", "Slider Qty", 0 ],
			"obj-81::obj-97" : [ "Quantized Rate[2]", "Quantized Rate", 0 ],
			"obj-80::obj-97" : [ "Quantized Rate[3]", "Quantized Rate", 0 ],
			"obj-38::obj-74::obj-68" : [ "Mixer / Send UI[6]", "Mixer / Send UI", 0 ],
			"obj-38::obj-24" : [ "Rampsmooth Up Time", "Rampsmooth Up Time", 0 ],
			"obj-84::obj-18" : [ "Jam Bars[7]", "Jam Bars", 0 ],
			"obj-49::obj-22" : [ "LFO On", "On", 0 ],
			"obj-33::obj-17::obj-9" : [ "Delta Channel Menu[1]", "Delta Channel Menu", 0 ],
			"obj-33::obj-16::obj-32" : [ "rslider", "rslider", 0 ],
			"obj-82::obj-49" : [ "Rate[1]", "rate", 0 ],
			"obj-33::obj-13::obj-2" : [ "Flip Output On/Off[6]", "Flip Output On/Off", 0 ],
			"obj-75::obj-22" : [ "Rampsmooth Down Time[1]", "Rampsmooth Down Time", 0 ],
			"obj-85::obj-45" : [ "Range Control[2]", "Range Control", 0 ],
			"obj-82::obj-15" : [ "SlideUp[1]", "SlideUp", 0 ],
			"obj-33::obj-15::obj-2" : [ "Flip Output On/Off[5]", "Flip Output On/Off", 0 ],
			"obj-33::obj-14::obj-32" : [ "rslider[4]", "rslider", 0 ],
			"obj-85::obj-57" : [ "Smooth Output[2]", "Smooth Output", 0 ],
			"obj-47::obj-56" : [ "Duration (note)[4]", "Duration (note)", 0 ],
			"obj-11::obj-25" : [ "Jam Bars On/Off", "Jam Bars On/Off", 0 ],
			"obj-83::obj-36::obj-4" : [ "Random Mux[1]", "Random Mux", 0 ],
			"obj-83::obj-40::obj-210" : [ "number[1]", "number[10]", 0 ],
			"obj-33::obj-14::obj-9" : [ "Delta Channel Menu[4]", "Delta Channel Menu", 0 ],
			"obj-79::obj-1" : [ "Loop On/Off[2]", "Loop On/Off", 0 ],
			"obj-85::obj-15" : [ "Loop On/Off[11]", "Loop On/Off", 0 ],
			"obj-2::obj-36::obj-4" : [ "Random Mux", "Random Mux", 0 ],
			"obj-6::obj-19" : [ "Smooth Output On/Off[1]", "Smooth Output On/Off", 0 ],
			"obj-7::obj-45" : [ "Loop On/Off", "Loop On/Off", 0 ],
			"obj-84::obj-45" : [ "Range Control[1]", "Range Control", 0 ],
			"obj-33::obj-19::obj-32" : [ "rslider[3]", "rslider", 0 ],
			"obj-2::obj-50" : [ "Mixer", "Mixer", 0 ],
			"obj-78::obj-43" : [ "function[4]", "function", 0 ],
			"obj-81::obj-12" : [ "Slider Qty[2]", "Slider Qty", 0 ],
			"obj-25::obj-7" : [ "LFO Freq Menu[3]", "LFO Freq Menu", 0 ],
			"obj-84::obj-27" : [ "Loop Jam Bars[1]", "Loop Jam Bars", 0 ],
			"obj-48::obj-1" : [ "Loop On/Off[3]", "Loop On/Off", 0 ],
			"obj-4::obj-9" : [ "Manual Rate", "Manual Rate", 0 ],
			"obj-81::obj-19" : [ "Smooth Output On/Off[2]", "Smooth Output On/Off", 0 ],
			"obj-80::obj-12" : [ "Slider Qty[3]", "Slider Qty", 0 ],
			"obj-38::obj-6" : [ "Master Volume", "Master Volume", 0 ],
			"obj-84::obj-28" : [ "Loop Jam Bars Button[1]", "Loop Jam Bars Button", 0 ],
			"obj-33::obj-16::obj-2" : [ "Flip Output On/Off", "Flip Output On/Off", 0 ],
			"obj-48::obj-13" : [ "Duration (ms)", "Duration (ms)", 0 ],
			"obj-33::obj-15::obj-32" : [ "rslider[5]", "rslider", 0 ],
			"obj-85::obj-18" : [ "Jam Bars[8]", "Jam Bars", 0 ],
			"obj-82::obj-68" : [ "File[1]", "File", 0 ],
			"obj-75::obj-74::obj-68" : [ "Mixer / Send UI[7]", "Mixer / Send UI", 0 ],
			"obj-75::obj-24" : [ "Rampsmooth Up Time[1]", "Rampsmooth Up Time", 0 ],
			"obj-85::obj-28" : [ "Loop Jam Bars Button[2]", "Loop Jam Bars Button", 0 ],
			"obj-33::obj-13::obj-32" : [ "rslider[6]", "rslider", 0 ],
			"obj-4::obj-21" : [ "Jam Bars", "Jam Bars", 0 ],
			"obj-79::obj-56" : [ "Duration (note)[5]", "Duration (note)", 0 ],
			"obj-85::obj-43" : [ "Number of Bars[2]", "Bars", 0 ],
			"obj-47::obj-43" : [ "function[1]", "function", 0 ],
			"obj-6::obj-9" : [ "Manual Rate[1]", "Manual Rate", 0 ],
			"obj-11::obj-28" : [ "Loop Jam Bars Button", "Loop Jam Bars Button", 0 ],
			"obj-7::obj-13" : [ "SlideDown", "SlideDown", 0 ],
			"obj-11::obj-18" : [ "Jam Bars[4]", "Jam Bars", 0 ],
			"obj-78::obj-1" : [ "Loop On/Off[4]", "Loop On/Off", 0 ],
			"obj-25::obj-55" : [ "Low Ramp[1]", "Lo Ramp", 0 ],
			"obj-6::obj-21" : [ "Jam Bars[1]", "Jam Bars", 0 ],
			"obj-7::obj-68" : [ "File", "File", 0 ],
			"obj-11::obj-57" : [ "Smooth Output", "Smooth Output", 0 ],
			"obj-84::obj-30" : [ "Loop Jam On/Off[1]", "Loop Jam On/Off", 0 ],
			"obj-33::obj-19::obj-9" : [ "Delta Channel Menu[3]", "Delta Channel Menu", 0 ],
			"obj-80::obj-21" : [ "Jam Bars[6]", "Jam Bars", 0 ],
			"obj-25::obj-122" : [ "Amplitude[1]", "Amp", 0 ],
			"obj-2::obj-40::obj-210" : [ "number[86]", "number[10]", 0 ],
			"obj-38::obj-16" : [ "Amp ∆ Time", "Amp ∆/T", 0 ],
			"obj-84::obj-11" : [ "Gen New Bars Button[1]", "Gen New Bars Button", 0 ],
			"obj-33::obj-18::obj-2" : [ "Flip Output On/Off[2]", "Flip Output On/Off", 0 ],
			"obj-33::obj-16::obj-9" : [ "Delta Channel Menu", "Delta Channel Menu", 0 ],
			"obj-4::obj-19" : [ "Smooth Output On/Off", "Smooth Output On/Off", 0 ],
			"obj-7::obj-49" : [ "Rate", "rate", 0 ],
			"obj-81::obj-9" : [ "Manual Rate[2]", "Manual Rate", 0 ],
			"obj-80::obj-9" : [ "Manual Rate[3]", "Manual Rate", 0 ],
			"obj-49::obj-7" : [ "LFO Freq Menu[2]", "LFO Freq Menu", 0 ],
			"obj-33::obj-12::obj-11" : [ "Delta Channel Number[7]", "Delta Channel Number", 0 ],
			"obj-49::obj-55" : [ "Low Ramp", "Lo Ramp", 0 ],
			"obj-33::obj-17::obj-11" : [ "Delta Channel Number[1]", "Delta Channel Number", 0 ],
			"obj-82::obj-45" : [ "Loop On/Off[9]", "Loop On/Off", 0 ],
			"obj-33::obj-13::obj-11" : [ "Delta Channel Number[6]", "Delta Channel Number", 0 ],
			"obj-75::obj-13" : [ "Freq ∆ Time[1]", "Freq ∆ Time", 0 ],
			"obj-85::obj-30" : [ "Loop Jam On/Off[2]", "Loop Jam On/Off", 0 ],
			"obj-82::obj-56" : [ "DB loop speed (notes)[2]", "DB loop speed (notes)", 0 ],
			"obj-33::obj-15::obj-11" : [ "Delta Channel Number[5]", "Delta Channel Number", 0 ],
			"obj-33::obj-18::obj-32" : [ "rslider[2]", "rslider", 0 ],
			"obj-7::obj-8" : [ "DB file selected[1]", "DB file selected", 0 ],
			"obj-85::obj-27" : [ "Loop Jam Bars[2]", "Loop Jam Bars", 0 ],
			"obj-47::obj-13" : [ "Duration (ms)[1]", "Duration (ms)", 0 ],
			"obj-2::obj-36::obj-1" : [ "MatrixCtrl", "MatrixCtrl", 0 ],
			"obj-7::obj-56" : [ "DB loop speed (notes)[1]", "DB loop speed (notes)", 0 ],
			"obj-11::obj-20" : [ "Loop Controller", "Loop Controller", 0 ],
			"obj-83::obj-36::obj-1" : [ "MatrixCtrl[1]", "MatrixCtrl", 0 ],
			"obj-49::obj-122" : [ "Amplitude", "Amp", 0 ],
			"obj-33::obj-14::obj-11" : [ "Delta Channel Number[4]", "Delta Channel Number", 0 ],
			"obj-25::obj-53" : [ "High Ramp[1]", "Hi Ramp", 0 ],
			"obj-6::obj-15" : [ "Jam On/Off[1]", "Jam On/Off", 0 ],
			"obj-11::obj-43" : [ "Number of Bars", "Bars", 0 ],
			"obj-84::obj-20" : [ "Loop Controller[1]", "Loop Controller", 0 ],
			"obj-33::obj-19::obj-2" : [ "Flip Output On/Off[3]", "Flip Output On/Off", 0 ],
			"obj-78::obj-56" : [ "Duration (note)[6]", "Duration (note)", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "_LFO.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ramp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_delta_to_CC.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_receive_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_multiplex.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ok.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mux_toggle_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mute.png",
				"bootpath" : "~/12c/12c_sandbox/img",
				"type" : "PNG ",
				"implicit" : 1
			}
, 			{
				"name" : "_slider2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jam_change.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_db_data.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "001.txt",
				"bootpath" : "~/12c/12c_sandbox/db",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_subdivide.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_markov2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ctrl_audio2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "amp_delta_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "1ch_amp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_output_channel.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mixer_feedback_reduction.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_preset_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "filename concat",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "delta_cv.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "vb.pitch~.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "buttonGreen-1",
				"default" : 				{
					"bgcolor" : [ 0.043137, 0.364706, 0.094118, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rsliderGold",
				"default" : 				{
					"color" : [ 0.646639, 0.821777, 0.854593, 1.0 ],
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
