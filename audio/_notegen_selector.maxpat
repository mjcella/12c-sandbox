{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 344.0, 203.0, 98.0, 64.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-2",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 258.5, 100.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-1",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 258.5, 233.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "live.dial",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 258.5, 171.0, 34.0, 27.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.166672, 15.666664, 34.0, 27.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "notegen_rate",
							"parameter_shortname" : "notegen_rate",
							"parameter_type" : 1,
							"parameter_mmax" : 8.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 1 ],
							"parameter_unitstyle" : 0,
							"parameter_units" : "2n, 4n, 4nt, 8n 8nt 16n 16nt slowgen fastgen"
						}

					}
,
					"showname" : 0,
					"shownumber" : 0,
					"varname" : "notegen_rate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 280.666687, 196.0, 52.666668, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.333359, 40.666672, 52.666668, 20.0 ],
					"style" : "",
					"text" : "fast gen"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 287.333374, 182.0, 57.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 46.000046, 26.666664, 57.0, 20.0 ],
					"style" : "",
					"text" : "slow gen"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 285.333374, 171.0, 36.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 44.000046, 15.666664, 36.0, 20.0 ],
					"style" : "",
					"text" : "16nt"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 280.666687, 157.666672, 29.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.333359, 2.333336, 29.0, 20.0 ],
					"style" : "",
					"text" : "16n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 262.333344, 151.666672, 26.333334, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 21.000015, -3.666664, 26.333334, 20.0 ],
					"style" : "",
					"text" : "8nt"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 247.0, 154.0, 22.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.666672, -1.333336, 22.0, 20.0 ],
					"style" : "",
					"text" : "8n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 238.333328, 169.0, 26.333334, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -3.0, 13.666664, 26.333334, 20.0 ],
					"style" : "",
					"text" : "4nt"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 240.333328, 182.0, 22.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.0, 26.666664, 22.0, 20.0 ],
					"style" : "",
					"text" : "4n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 251.0, 196.0, 25.333334, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 9.666672, 40.666672, 25.333334, 20.0 ],
					"style" : "",
					"text" : "2n"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
 ]
	}

}
