{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 63.0, 79.0, 1026.0, 597.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"bgcolor" : [ 0.501961, 0.717647, 0.764706, 1.0 ],
					"buffername" : "synth_voice",
					"gridcolor" : [ 0.352941, 0.337255, 0.521569, 1.0 ],
					"id" : "obj-29",
					"maxclass" : "waveform~",
					"numinlets" : 5,
					"numoutlets" : 6,
					"outlettype" : [ "float", "float", "float", "float", "list", "" ],
					"patching_rect" : [ 2282.0, 2129.202881, 217.0, 31.594055 ],
					"presentation" : 1,
					"presentation_rect" : [ 192.0, 137.0, 237.0, 48.594055 ],
					"selectioncolor" : [ 0.313726, 0.498039, 0.807843, 0.0 ],
					"setunit" : 1,
					"style" : "",
					"waveformcolor" : [ 0.082353, 0.25098, 0.431373, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 2503.0, 2134.0, 115.0, 22.0 ],
					"style" : "",
					"text" : "buffer~ synth_voice"
				}

			}
, 			{
				"box" : 				{
					"comment" : "(signal) gen_synth2 audio",
					"id" : "obj-79",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 379.333344, 649.333374, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "(signal) gen_synth1 audio",
					"id" : "obj-78",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 26.0, 649.333374, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-75",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_gen_synth_controls.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 379.333344, 401.0, 341.0, 185.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 379.333344, 401.0, 341.0, 185.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-72",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_gen_synth_controls.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 26.0, 401.0, 341.0, 185.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 26.0, 401.0, 341.0, 185.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_scale_notegen.maxpat",
					"numinlets" : 0,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 26.0, 19.0, 536.0, 372.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 26.0, 19.0, 536.0, 372.0 ],
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 552.5, 397.600006, 388.833344, 397.600006 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-75", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-1::obj-12::obj-5::obj-56" : [ "Dropdown Control[3]", "Select", 0 ],
			"obj-1::obj-16::obj-8::obj-31" : [ "live.text[16]", "live.text", 0 ],
			"obj-1::obj-19::obj-8::obj-13" : [ "Quantize Metro Rate[8]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-17::obj-31::obj-20::obj-34" : [ "live.text[33]", "live.text", 0 ],
			"obj-1::obj-17::obj-31::obj-20::obj-10" : [ "Manual Metro Rate[11]", "Rate", 0 ],
			"obj-1::obj-3::obj-8::obj-34" : [ "live.text[11]", "live.text", 0 ],
			"obj-1::obj-19::obj-9" : [ "live.text[22]", "live.text", 0 ],
			"obj-1::obj-19::obj-8::obj-12" : [ "Metro On/Off[8]", "Metro On/Off", 0 ],
			"obj-1::obj-3::obj-8::obj-12" : [ "Metro On/Off[5]", "Metro On/Off", 0 ],
			"obj-1::obj-12::obj-31::obj-20::obj-13" : [ "Quantize Metro Rate[2]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-20::obj-187" : [ "rslider[4]", "rslider", 0 ],
			"obj-1::obj-20::obj-8::obj-34" : [ "live.text[27]", "live.text", 0 ],
			"obj-1::obj-18::obj-30" : [ "live.text[17]", "live.text[1]", 0 ],
			"obj-1::obj-17::obj-5::obj-56" : [ "Dropdown Control[1]", "Select", 0 ],
			"obj-1::obj-5" : [ "scale2 note diff amount", "scale2 note diff amount", 0 ],
			"obj-1::obj-14" : [ "notegen1 root", "notegen1 root", 0 ],
			"obj-1::obj-12::obj-31::obj-20::obj-34" : [ "live.text[3]", "live.text", 0 ],
			"obj-1::obj-2::obj-8::obj-13" : [ "Quantize Metro Rate[4]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-2::obj-187" : [ "rslider[51]", "rslider", 0 ],
			"obj-1::obj-20::obj-8::obj-10" : [ "Manual Metro Rate[9]", "Rate", 0 ],
			"obj-1::obj-3::obj-9" : [ "live.text[10]", "live.text", 0 ],
			"obj-1::obj-7::obj-13" : [ "notegen_rate[1]", "notegen_rate", 0 ],
			"obj-1::obj-2::obj-30" : [ "live.text[162]", "live.text[1]", 0 ],
			"obj-1::obj-20::obj-30" : [ "live.text[25]", "live.text[1]", 0 ],
			"obj-1::obj-3::obj-8::obj-10" : [ "Manual Metro Rate[5]", "Rate", 0 ],
			"obj-1::obj-17::obj-31::obj-20::obj-13" : [ "Quantize Metro Rate[16]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-16::obj-8::obj-12" : [ "Metro On/Off[6]", "Metro On/Off", 0 ],
			"obj-1::obj-38" : [ "notegen1 octave select", "notegen1 octave select", 0 ],
			"obj-1::obj-12::obj-31::obj-20::obj-12" : [ "Metro On/Off[2]", "Metro On/Off", 0 ],
			"obj-1::obj-19::obj-8::obj-31" : [ "live.text[24]", "live.text", 0 ],
			"obj-1::obj-18::obj-8::obj-12" : [ "Metro On/Off[7]", "Metro On/Off", 0 ],
			"obj-1::obj-17::obj-5::obj-13" : [ "Quantize Metro Rate[1]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-18::obj-8::obj-13" : [ "Quantize Metro Rate[7]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-18::obj-187" : [ "rslider[2]", "rslider", 0 ],
			"obj-1::obj-20::obj-8::obj-56" : [ "Dropdown Control[18]", "Select", 0 ],
			"obj-1::obj-17::obj-5::obj-34" : [ "live.text[1]", "live.text", 0 ],
			"obj-1::obj-16::obj-9" : [ "live.text[14]", "live.text", 0 ],
			"obj-1::obj-12::obj-5::obj-31" : [ "live.text[6]", "live.text", 0 ],
			"obj-1::obj-55" : [ "notegen2 octave select", "notegen2 octave select", 0 ],
			"obj-1::obj-17::obj-5::obj-18" : [ "umenu[1]", "umenu", 0 ],
			"obj-1::obj-2::obj-8::obj-12" : [ "Metro On/Off[4]", "Metro On/Off", 0 ],
			"obj-1::obj-2::obj-8::obj-10" : [ "Manual Metro Rate[4]", "Rate", 0 ],
			"obj-1::obj-18::obj-8::obj-34" : [ "live.text[19]", "live.text", 0 ],
			"obj-1::obj-18::obj-9" : [ "live.text[18]", "live.text", 0 ],
			"obj-1::obj-12::obj-5::obj-12" : [ "Metro On/Off[3]", "Metro On/Off", 0 ],
			"obj-1::obj-19::obj-30" : [ "live.text[21]", "live.text[1]", 0 ],
			"obj-1::obj-20::obj-9" : [ "live.text[26]", "live.text", 0 ],
			"obj-1::obj-2::obj-9" : [ "live.text[163]", "live.text", 0 ],
			"obj-1::obj-4" : [ "notegen1 manual speed", "notegen1 manual speed", 0 ],
			"obj-1::obj-3::obj-8::obj-31" : [ "live.text[12]", "live.text", 0 ],
			"obj-1::obj-18::obj-8::obj-10" : [ "Manual Metro Rate[7]", "Rate", 0 ],
			"obj-1::obj-18::obj-8::obj-31" : [ "live.text[20]", "live.text", 0 ],
			"obj-1::obj-12::obj-31::obj-20::obj-10" : [ "Manual Metro Rate[2]", "Rate", 0 ],
			"obj-1::obj-16::obj-8::obj-10" : [ "Manual Metro Rate[6]", "Rate", 0 ],
			"obj-1::obj-32" : [ "scale2 note qty minimum", "scale2 note qty minimum", 0 ],
			"obj-1::obj-17::obj-5::obj-12" : [ "Metro On/Off[1]", "Metro On/Off", 0 ],
			"obj-1::obj-16::obj-187" : [ "rslider[1]", "rslider", 0 ],
			"obj-1::obj-9::obj-13" : [ "notegen_rate", "notegen_rate", 0 ],
			"obj-1::obj-19::obj-8::obj-10" : [ "Manual Metro Rate[8]", "Rate", 0 ],
			"obj-1::obj-12::obj-31::obj-20::obj-31" : [ "live.text[4]", "live.text", 0 ],
			"obj-1::obj-12::obj-5::obj-10" : [ "Manual Metro Rate[3]", "Rate", 0 ],
			"obj-1::obj-16::obj-8::obj-56" : [ "Dropdown Control[15]", "Select", 0 ],
			"obj-1::obj-17::obj-31::obj-20::obj-18" : [ "umenu[14]", "umenu", 0 ],
			"obj-1::obj-2::obj-8::obj-18" : [ "umenu[3]", "umenu", 0 ],
			"obj-1::obj-16::obj-8::obj-13" : [ "Quantize Metro Rate[6]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-20::obj-8::obj-13" : [ "Quantize Metro Rate[9]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-17::obj-31::obj-20::obj-31" : [ "live.text[34]", "live.text", 0 ],
			"obj-1::obj-16::obj-8::obj-18" : [ "umenu[5]", "umenu", 0 ],
			"obj-1::obj-12::obj-5::obj-18" : [ "umenu[2]", "umenu", 0 ],
			"obj-1::obj-17::obj-31::obj-20::obj-12" : [ "Metro On/Off[16]", "Metro On/Off", 0 ],
			"obj-1::obj-3::obj-30" : [ "live.text[9]", "live.text[1]", 0 ],
			"obj-1::obj-20::obj-8::obj-31" : [ "live.text[28]", "live.text", 0 ],
			"obj-1::obj-17::obj-5::obj-10" : [ "Manual Metro Rate[1]", "Rate", 0 ],
			"obj-1::obj-19::obj-8::obj-34" : [ "live.text[23]", "live.text", 0 ],
			"obj-1::obj-17::obj-5::obj-31" : [ "live.text[2]", "live.text", 0 ],
			"obj-1::obj-17::obj-31::obj-20::obj-56" : [ "Dropdown Control[14]", "Select", 0 ],
			"obj-1::obj-19::obj-8::obj-56" : [ "Dropdown Control[17]", "Select", 0 ],
			"obj-1::obj-6" : [ "notegen2 root", "notegen2 root", 0 ],
			"obj-1::obj-2::obj-8::obj-31" : [ "live.text[8]", "live.text", 0 ],
			"obj-1::obj-2::obj-8::obj-34" : [ "live.text[7]", "live.text", 0 ],
			"obj-1::obj-18::obj-8::obj-56" : [ "Dropdown Control[16]", "Select", 0 ],
			"obj-1::obj-2::obj-8::obj-56" : [ "Dropdown Control[4]", "Select", 0 ],
			"obj-1::obj-3::obj-8::obj-56" : [ "Dropdown Control[5]", "Select", 0 ],
			"obj-1::obj-16::obj-8::obj-34" : [ "live.text[15]", "live.text", 0 ],
			"obj-1::obj-3::obj-8::obj-13" : [ "Quantize Metro Rate[5]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-12::obj-5::obj-13" : [ "Quantize Metro Rate[3]", "Quantize Metro Rate", 0 ],
			"obj-1::obj-21" : [ "notegen2 manual speed", "notegen2 manual speed", 0 ],
			"obj-1::obj-20::obj-8::obj-12" : [ "Metro On/Off[9]", "Metro On/Off", 0 ],
			"obj-1::obj-3::obj-187" : [ "rslider[52]", "rslider", 0 ],
			"obj-1::obj-12::obj-31::obj-20::obj-56" : [ "Dropdown Control[2]", "Select", 0 ],
			"obj-1::obj-19::obj-187" : [ "rslider[3]", "rslider", 0 ],
			"obj-1::obj-16::obj-30" : [ "live.text[13]", "live.text[1]", 0 ],
			"obj-1::obj-12::obj-31::obj-20::obj-18" : [ "umenu[15]", "umenu", 0 ],
			"obj-1::obj-19::obj-8::obj-18" : [ "umenu[17]", "umenu", 0 ],
			"obj-1::obj-12::obj-5::obj-34" : [ "live.text[5]", "live.text", 0 ],
			"obj-1::obj-3::obj-8::obj-18" : [ "umenu[4]", "umenu", 0 ],
			"obj-1::obj-18::obj-8::obj-18" : [ "umenu[16]", "umenu", 0 ],
			"obj-1::obj-20::obj-8::obj-18" : [ "umenu[18]", "umenu", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "_scale_notegen.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "scale_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "scale_matches.js",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "keyselect.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "notegen.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/probability",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "markov_init.js",
				"bootpath" : "~/12c/12c_sandbox/probability/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_notegen_selector.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "octave_select.js",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bpatcher_name.js",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_gen_synth_controls.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "gen_synth_voice.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
