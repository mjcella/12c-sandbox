{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 104.0, 941.0, 662.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 673.18597412109375, 62.645835876464844, 67.0, 22.0 ],
					"text" : "delay 5000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 898.1650390625, 233.486221313476562, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 44.477607727050781, 47.429470062255859, 36.034786224365234, 12.450361251831055 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_type" : 2,
							"parameter_longname" : "select_random_IR",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 1.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "select_random_IR"
						}

					}
,
					"text" : "random",
					"texton" : "random",
					"varname" : "select_random_IR"
				}

			}
, 			{
				"box" : 				{
					"automation" : "Record",
					"automationon" : "Record",
					"id" : "obj-35",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 143.634170532226562, 85.645835876464844, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -0.965480089187622, 35.111660003662109, 21.548110961914062, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "Record", "Record" ],
							"parameter_type" : 2,
							"parameter_longname" : "Record",
							"parameter_mmax" : 1.0,
							"parameter_shortname" : "Record"
						}

					}
,
					"text" : "Record",
					"texton" : "Record",
					"varname" : "Freeze"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 89.5, 198.75, 126.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.884506225585938, 35.343170166015625, 15.0, 15.0 ],
					"text" : "↓"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-48",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 108.75, 207.75, 126.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 69.986213684082031, 35.368759155273438, 15.0, 15.0 ],
					"text" : "↑"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 164.529998779296875, 521.134765625, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-30",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 164.529998779296875, 556.6392822265625, 39.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.016326904296875, 36.585296630859375, 32.75, 13.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_type" : 0,
							"parameter_unitstyle" : 0,
							"parameter_longname" : "lpg_slidedown[2]",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 44100.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "lpg_slidedown"
						}

					}
,
					"varname" : "lpg_slidedown"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 357.617645263671875, 604.89813232421875, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-40",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 210.094512939453125, 556.6392822265625, 39.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 47.118034362792969, 36.585296630859375, 32.75, 13.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_type" : 0,
							"parameter_unitstyle" : 0,
							"parameter_longname" : "lpg_slideup[2]",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 44100.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "lpg_slideup"
						}

					}
,
					"varname" : "lpg_slideup"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 357.617645263671875, 579.25433349609375, 29.5, 22.0 ],
					"text" : "> 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 196.433212280273438, 647.336181640625, 78.0, 22.0 ],
					"text" : "selector~ 2 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-42",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 3,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int", "", "" ],
					"patching_rect" : [ 328.2843017578125, 549.87225341796875, 107.0, 22.999996185302734 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.016326904296875, 14.016036987304688, 102.416694641113281, 22.999996185302734 ],
					"varname" : "Verb LP Gate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 255.4332275390625, 604.89813232421875, 41.0, 22.0 ],
					"text" : "lpgate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "bang" ],
					"patching_rect" : [ 143.634170532226562, 108.474571228027344, 29.5, 22.0 ],
					"text" : "t i b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"linecount" : 10,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 860.0, 413.178375244140625, 152.0, 141.0 ],
					"text" : "^ this used to be connected to this text field>> \nbut i removed it because it was wreaking havoc on pattr interpolation, and i'm rarely if ever loading random files into the convolution verb here. just not worth it."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 756.03594970703125, 94.348915100097656, 133.0, 49.0 ],
					"restore" : [ 174 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0,
						"parameter_mappable" : 0
					}
,
					"text" : "pattr ir_folder_wrapper @default_interp thresh 0.5",
					"varname" : "ir_folder_wrapper"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 547.11309814453125, 10.135135650634766, 90.0, 22.0 ],
					"text" : "loadmess 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 547.11309814453125, 38.875564575195312, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 67.497360229492188, -0.172664642333984, 36.5, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_type" : 0,
							"parameter_unitstyle" : 2,
							"parameter_mmin" : 250.0,
							"parameter_longname" : "buff_len",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 5000.0,
							"parameter_initial" : [ 1000 ],
							"parameter_shortname" : "buff_len"
						}

					}
,
					"varname" : "buff_len"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 193.216232299804688, 180.567550659179688, 42.0, 22.0 ],
					"text" : "edge~"
				}

			}
, 			{
				"box" : 				{
					"comment" : "(signal) input signal",
					"id" : "obj-68",
					"index" : 3,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 446.952239990234375, 78.145835876464844, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 143.634170532226562, 48.681354522705078, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 547.270263671875, 62.645835876464844, 122.0, 22.0 ],
					"text" : "translate ms samples"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 547.270263671875, 88.645835876464844, 91.0, 22.0 ],
					"text" : "sizeinsamps $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 193.125, 136.70269775390625, 154.0, 35.0 ],
					"text" : "record~ convolution_buffer @loop 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 245.9285888671875, 476.52423095703125, 112.0, 22.0 ],
					"text" : "limi~ @threshold -4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "int", "int", "bang", "bang", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 59.0, 1212.0, 707.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 50.0, 100.0, 41.0, 22.0 ],
									"text" : "t s 0 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-71",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 50.0, 254.251251220703125, 32.0, 22.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 50.0, 222.28387451171875, 54.0, 22.0 ],
									"text" : "delay 60"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 50.0, 139.485260009765625, 31.0, 22.0 ],
									"text" : "t b s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 62.0, 176.157623291015625, 65.0, 22.0 ],
									"text" : "replace $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-32",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-34",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 85.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-38",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 336.251220703125, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-39",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 85.0, 336.251220703125, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-40",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 120.0, 336.251220703125, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-41",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 155.0, 336.251220703125, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-42",
									"index" : 5,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 190.0, 336.251220703125, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-17", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-17", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"source" : [ "obj-36", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"source" : [ "obj-71", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-71", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 713.68597412109375, 370.731689453125, 97.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p onBufferSelect"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 504.559051513671875, 377.731689453125, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 77.965324401855469, 35.903766632080078, 25.828990936279297, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_type" : 0,
							"parameter_unitstyle" : 7,
							"parameter_mmin" : -48.0,
							"parameter_longname" : "IR_transpose",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 24.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "IR_transpose"
						}

					}
,
					"varname" : "IR_transpose"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 64.5, 123.145835876464844, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 79.0, 1212.0, 687.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 53.421562194824219, 131.553985595703125, 29.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 124.041221618652344, 208.262908935546875, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 53.421562194824219, 208.262908935546875, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 53.421562194824219, 171.561859130859375, 160.23931884765625, 22.0 ],
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 53.421562194824219, 97.434867858886719, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-57",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 53.421562194824219, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-58",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 53.421562194824219, 256.72625732421875, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-43", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 1 ],
									"midpoints" : [ 62.921562194824219, 239.743592172861099, 37.179487556219101, 239.743592172861099, 37.179487556219101, 88.888889789581299, 73.421562194824219, 88.888889789581299 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 1 ],
									"midpoints" : [ 133.541221618652344, 239.743592172861099, 221.367523610591888, 239.743592172861099, 221.367523610591888, 86.324787199497223, 73.421562194824219, 86.324787199497223 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"midpoints" : [ 73.421562194824219, 160.683762311935425, 48.717949211597443, 160.683762311935425, 48.717949211597443, 245.72649821639061, 62.921562194824219, 245.72649821639061 ],
									"source" : [ "obj-54", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-57", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 64.5, 229.856948852539062, 127.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p invertToggleOnBang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 106.166671752929688, 397.853668212890625, 29.5, 22.0 ],
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 106.166664123535156, 334.620574951171875, 135.0, 22.0 ],
					"text" : "multiconvolve~ 1 1 zero"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "", "signal", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 59.0, 1212.0, 707.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-53",
									"linecount" : 15,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 255.0, 424.0, 151.0, 208.0 ],
									"text" : "since the multiconvolve~ object clicks when you send it a new buffer~, this runs a crossfade that switches channels every time a new buffer is loaded or transposed, which avoids the click because the old sample continues playing on the old channel and just gets faded out. a bit more CPU intensive but since i can't edit the multiconvolve~ object, this works"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 284.0, 166.0, 29.5, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 378.0, 107.0, 77.0, 22.0 ],
									"text" : "loadmess 10"
								}

							}
, 							{
								"box" : 								{
									"comment" : "(int 0-1) xfade destination",
									"id" : "obj-37",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 244.0, 26.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "ch1 set buffer",
									"id" : "obj-35",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 223.0, 360.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "ch1 xfade on/off",
									"id" : "obj-36",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 182.0, 360.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "ch2 set buffer",
									"id" : "obj-34",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 457.0, 360.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "ch1 xfade on/off",
									"id" : "obj-33",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 418.0, 360.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 457.0, 236.0, 126.0, 22.0 ],
									"text" : "set convolution_buffer"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 236.0, 126.0, 22.0 ],
									"text" : "set convolution_buffer"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "bang" ],
									"patching_rect" : [ 320.5, 107.0, 52.0, 22.0 ],
									"text" : "t b b b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 389.0, 236.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 320.5, 236.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 182.0, 305.0, 34.0, 22.0 ],
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "bang" ],
									"patching_rect" : [ 244.0, 107.0, 52.0, 22.0 ],
									"text" : "t b b b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 182.0, 236.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 257.0, 236.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 418.0, 305.0, 34.0, 22.0 ],
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 244.0, 72.0, 172.0, 22.0 ],
									"text" : "sel 0 1"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 286.5, 140.0, 293.5, 140.0 ],
									"source" : [ "obj-10", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 275.5, 200.0, 466.5, 200.0 ],
									"source" : [ "obj-10", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"midpoints" : [ 253.5, 220.0, 191.5, 220.0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"midpoints" : [ 293.5, 290.0, 206.5, 290.0 ],
									"order" : 1,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"midpoints" : [ 293.5, 290.0, 442.5, 290.0 ],
									"order" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"midpoints" : [ 387.5, 149.0, 304.0, 149.0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"midpoints" : [ 398.5, 275.0, 427.5, 275.0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"midpoints" : [ 330.0, 268.0, 191.5, 268.0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 363.0, 138.0, 293.5, 138.0 ],
									"source" : [ "obj-29", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"midpoints" : [ 341.0, 227.0, 398.5, 227.0 ],
									"source" : [ "obj-29", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"midpoints" : [ 352.0, 209.0, 59.5, 209.0 ],
									"source" : [ "obj-29", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"midpoints" : [ 59.5, 282.0, 232.5, 282.0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"midpoints" : [ 266.5, 281.0, 427.5, 281.0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 64.5, 261.166656494140625, 144.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p xfadeOnNewTranspose"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 426.952239990234375, 334.620574951171875, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 504.559051513671875, 334.620574951171875, 41.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 504.559051513671875, 436.236114501953125, 32.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 97.25, 51.142856597900391, 87.0, 22.0 ],
									"text" : "speedlim 2000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-90",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 165.5, 100.0, 29.5, 22.0 ],
									"text" : "* -1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 165.5, 139.0, 101.0, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0
									}
,
									"text" : "coll semitones.txt"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 97.25, 100.0, 37.0, 22.0 ],
									"text" : "* 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 97.25, 176.0, 62.0, 35.0 ],
									"text" : "pitchshiftcent $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 97.25, 227.999969482421875, 31.0, 22.0 ],
									"text" : "t b s"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-21",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 165.5, 176.0, 61.0, 22.0 ],
									"text" : "stretch $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "bang" ],
									"patching_rect" : [ 97.25, 267.999969482421875, 232.0, 22.0 ],
									"saved_object_attributes" : 									{
										"basictuning" : 440,
										"followglobaltempo" : 0,
										"formantcorrection" : 0,
										"mode" : "general",
										"originallength" : [ 1343.519985610616914, "ticks" ],
										"originaltempo" : 119.999999999999801,
										"pitchcorrection" : 0,
										"quality" : "better",
										"stretch" : [ 1.0 ]
									}
,
									"text" : "stretch~ convolution_buffer @readagain 1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-47",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 97.25, 2.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-48",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 310.25, 302.999969482421875, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"midpoints" : [ 106.75, 87.0, 175.0, 87.0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 175.0, 222.0, 106.75, 222.0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"midpoints" : [ 118.75, 257.0, 106.75, 257.0 ],
									"source" : [ "obj-23", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-26", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"source" : [ "obj-90", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 504.559051513671875, 404.7281494140625, 83.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p transposeIR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1056.08203125, 505.15679931640625, 150.0, 74.0 ],
					"text" : "If file used was manually loaded, its name is stored in this text UI so it can be recalled when a pattr is retrieved"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 969.1717529296875, 159.857986450195312, 22.0, 22.0 ],
					"varname" : "manually_loaded_buffer_flag"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 387.86968994140625, 179.3260498046875, 127.0, 22.0 ],
									"text" : "textcolor 0.4 0.4 0.4 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 400.86968994140625, 233.3260498046875, 127.0, 22.0 ],
									"text" : "textcolor 0.2 0.2 0.2 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 356.86968994140625, 269.3260498046875, 128.0, 22.0 ],
									"text" : "bgcolor 0.2 0.2 0.2 0.2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 253.924560546875, 117.463783264160156, 44.0, 22.0 ],
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 349.89361572265625, 149.891250610351562, 181.0, 22.0 ],
									"text" : "bgcolor 0.883333 0.807422 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 139.89361572265625, 222.463775634765625, 133.0, 22.0 ],
									"text" : "bgfillcolor 0.2 0.2 0.2 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 18.0, 168.326034545898438, 193.0, 22.0 ],
									"text" : "bgfillcolor 0.883333 0.807422 0. 1."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-30",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 253.924560546875, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-31",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 225.44683837890625, 296.3260498046875, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-32",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 311.39849853515625, 296.3260498046875, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 2,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 2,
									"source" : [ "obj-26", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"order" : 1,
									"source" : [ "obj-26", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 0,
									"source" : [ "obj-26", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 969.1717529296875, 187.031753540039062, 209.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p manually_selected_or_from_folder?"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.2 ],
					"fontsize" : 8.0,
					"id" : "obj-15",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 992.2244873046875, 236.856948852539062, 107.0, 15.0 ],
					"text" : "BM7 Small Ambience.aif",
					"textcolor" : [ 0.200000002980232, 0.200000002980232, 0.200000002980232, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 994.2244873046875, 377.62554931640625, 72.0, 22.0 ],
					"text" : "prepend set"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 994.2244873046875, 471.664337158203125, 59.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1071.2265625, 377.62554931640625, 99.208091735839844, 22.0 ],
					"text" : "loadmess clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"linecount" : 2,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 994.2244873046875, 438.178375244140625, 128.323715209960938, 22.000009536743164 ],
					"text" : "\"BM7 Small Ambience.aif\"",
					"varname" : "filename"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 867.403564453125, 377.62554931640625, 113.161727905273438, 22.0 ],
					"text" : "r pattr_start_hook"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 16.527030944824219, 14.875570297241211, 56.0, 22.0 ],
					"restore" : 					{
						"Freeze" : [ 0.0 ],
						"IR_transpose" : [ 0.0 ],
						"OpenFile" : [ 0.0 ],
						"buff_len" : [ 1000.0 ],
						"filename" : [ "BM7 Small Ambience.aif" ],
						"lpg_slidedown" : [ 0.0 ],
						"lpg_slideup" : [ 0.0 ],
						"manually_loaded_buffer_flag" : [ 0 ],
						"select_random_IR" : [ 0.0 ]
					}
,
					"text" : "autopattr",
					"varname" : "u801024385"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 840.03594970703125, 229.856948852539062, 49.0, 22.0 ],
					"text" : "random"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 786.14581298828125, 229.856948852539062, 29.5, 22.0 ],
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 245.9285888671875, 442.90277099609375, 30.0, 22.0 ],
					"text" : "*~ 1"
				}

			}
, 			{
				"box" : 				{
					"comment" : "(signal 0. - 1.) dry/wet",
					"id" : "obj-86",
					"index" : 2,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 406.0953369140625, 78.145835876464844, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 484.559051513671875, 92.145835876464844, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 77.93304443359375, 47.429470062255859, 25.924287796020508, 12.450361251831055 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_type" : 2,
							"parameter_longname" : "OpenFile",
							"parameter_mmax" : 1.0,
							"parameter_shortname" : "OpenFile"
						}

					}
,
					"text" : "open",
					"varname" : "OpenFile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 9,
					"outlettype" : [ "float", "list", "float", "float", "float", "float", "float", "", "int" ],
					"patching_rect" : [ 694.18597412109375, 486.64111328125, 136.0, 22.0 ],
					"text" : "info~ convolution_buffer"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 673.18597412109375, 10.135135650634766, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 484.559051513671875, 127.145835876464844, 48.0, 22.0 ],
					"text" : "replace"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 572.764892578125, 174.031753540039062, 95.0, 35.0 ],
					"text" : "loadmess types AIFF WAVE"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.883333, 0.807422, 0.0, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_color" : [ 0.883333, 0.807422, 0.0, 1.0 ],
					"bgfillcolor_color1" : [ 0.376470588235294, 0.384313725490196, 0.4, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196078431373, 0.309803921568627, 0.301960784313725, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"fontsize" : 6.0,
					"id" : "obj-25",
					"items" : [ "ABLCR Aetherizer 1.aif", ",", "ABLCR Aetherizer 2 L.aif", ",", "ABLCR Aetherizer 2 R.aif", ",", "ABLCR Aetherizer 3 L.aif", ",", "ABLCR Aetherizer 3 R.aif", ",", "ABLCR Aetherizer 4 L.aif", ",", "ABLCR Aetherizer 4 R.aif", ",", "ABLCR Air 1.aif", ",", "ABLCR Air 2.aif", ",", "ABLCR Bass Bin L.aif", ",", "ABLCR Bass Bin R.aif", ",", "ABLCR Cabinate in a Room.aif", ",", "ABLCR Carbonized Wood.aif", ",", "ABLCR Chord 1.aif", ",", "ABLCR Chord 2.aif", ",", "ABLCR Chord 3.aif", ",", "ABLCR Chord 4.aif", ",", "ABLCR Chord 5.aif", ",", "ABLCR Chord String.aif", ",", "ABLCR Chord Vocal.aif", ",", "ABLCR Club Ambience L.aif", ",", "ABLCR Club Ambience R.aif", ",", "ABLCR Frequency Panned.aif", ",", "ABLCR Ground Hum.aif", ",", "ABLCR Ice 1 In C.aif", ",", "ABLCR Ice 2.aif", ",", "ABLCR Ice 3.aif", ",", "ABLCR Ice 4 noise.aif", ",", "ABLCR Lush Razor 1.aif", ",", "ABLCR Lush Razor Coder 2 L.aif", ",", "ABLCR Lush Razor Coder 2 R.aif", ",", "ABLCR Lush Razor Coder 3 L.aif", ",", "ABLCR Lush Razor Coder 3 R.aif", ",", "ABLCR M2S 1.aif", ",", "ABLCR M2S 2.aif", ",", "ABLCR M2S 3 L.aif", ",", "ABLCR M2S 3 R.aif", ",", "ABLCR M2S 4.aif", ",", "ABLCR Moist Fork.aiff", ",", "ABLCR Mp3 Hats.aif", ",", "ABLCR Muffled Metal.aif", ",", "ABLCR Noise Gen L.aif", ",", "ABLCR Noise Gen R.aif", ",", "ABLCR Noise Raw Long L.aif", ",", "ABLCR Noise Raw Long R.aif", ",", "ABLCR Noise Short Room L.aif", ",", "ABLCR Noise Short Room R.aif", ",", "ABLCR Nu Dub Hall.aif", ",", "ABLCR Presence L.aif", ",", "ABLCR Presence R.aif", ",", "ABLCR PVC Tube.aif", ",", "ABLCR Razor Room.aif", ",", "ABLCR Recursive Plated.aiff", ",", "ABLCR Reverb Echo 1.aif", ",", "ABLCR Reverb Echo 2.aif", ",", "ABLCR Rubber Elephant L.aif", ",", "ABLCR Rubber Elephant R.aif", ",", "ABLCR Short Timestretch.aif", ",", "ABLCR Slapback.aif", ",", "ABLCR Snowy Concrete Yard.wav", ",", "ABLCR Strange Echo.aif", ",", "ABLCR Sweep 1 Dark.wav", ",", "ABLCR Sweep 2 Dark L.aif", ",", "ABLCR Sweep 2 Dark R.aif", ",", "ABLCR Sweep 3 Dark.wav", ",", "ABLCR Sweep 4 Med.wav", ",", "ABLCR Sweep 5 High.wav", ",", "ABLCR Sweep 6 High.wav", ",", "ABLCR Sweep 7 High Long.wav", ",", "ABLCR Sweep 8 Wah.wav", ",", "ABLCR Telefunken Humm.aif", ",", "ABLCR Time stretch Long.aif", ",", "ABLCR Timelapse.aif", ",", "ABLCR Transient Thunder.aiff", ",", "ABLCR Underwater Confessional L.aif", ",", "ABLCR Underwater Confessional R.aif", ",", "ABLCR Underwater.aif", ",", "Amazing Stereo Spring.aif", ",", "Amp Spring Bright.aif", ",", "Amp Spring Dull.aif", ",", "Amp Spring High.aif", ",", "Arundel Nave.wav", ",", "BM7 A&M Chamber.aif", ",", "BM7 Academy Yard.aif", ",", "BM7 Amb Chamber A.aif", ",", "BM7 Amb Chamber B.aif", ",", "BM7 Amsterdam Hall.aif", ",", "BM7 Arena.aif", ",", "BM7 Back Room.aif", ",", "BM7 Bass XXL.aif", ",", "BM7 Bath House.aif", ",", "BM7 Berliner Hall.aif", ",", "BM7 Blue Room.aif", ",", "BM7 Boston Hall A.aif", ",", "BM7 Boston Hall B.aif", ",", "BM7 Brass Hall.aif", ",", "BM7 Bright Plate.aif", ",", "BM7 Car Park.aif", ",", "BM7 Cavern.aif", ",", "BM7 CD Chamber.aif", ",", "BM7 CD Plate A.aif", ",", "BM7 CD Plate B.aif", ",", "BM7 Center Room.aif", ",", "BM7 Chicago Hall.aif", ",", "BM7 Cinema Room.aif", ",", "BM7 Class Room.aif", ",", "BM7 Clear Ambience.aif", ",", "BM7 Clear Hall.aif", ",", "BM7 Concert Hall.aif", ",", "BM7 Crystal Plate.aif", ",", "BM7 Dark Plate.aif", ",", "BM7 Deep Ambience.aif", ",", "BM7 Deep Chamber.aif", ",", "BM7 Deep Stone.aif", ",", "BM7 Dense Hall.aif", ",", "BM7 Dense Plate.aif", ",", "BM7 Djangos Room.aif", ",", "BM7 Drum & Chamber.aif", ",", "BM7 East Church.aif", ",", "BM7 Echo Plate.aif", ",", "BM7 Europa.aif", ",", "BM7 Fat Plate.aif", ",", "BM7 Front Room.aif", ",", "BM7 Gold Hall.aif", ",", "BM7 Gold Plate.aif", ",", "BM7 Hall Large.aif", ",", "BM7 Hall Medium.aif", ",", "BM7 Hall Small.aif", ",", "BM7 Heavy Ambience.aif", ",", "BM7 Heavy Room.aif", ",", "BM7 Hillside.aif", ",", "BM7 Kick Chamber.aif", ",", "BM7 Large & Bright Amb.aif", ",", "BM7 Large & Bright Chamb.aif", ",", "BM7 Large & Dark Amb.aif", ",", "BM7 Large & Dark Chamb.aif", ",", "BM7 Large & Dark.aif", ",", "BM7 Large & Deep.aif", ",", "BM7 Large & Near.aif", ",", "BM7 Large Ambience.aif", ",", "BM7 Large Chamber.aif", ",", "BM7 Large Plate.aif", ",", "BM7 Large Q Room.aif", ",", "BM7 Large Red Room.aif", ",", "BM7 Large Room.aif", ",", "BM7 Large Tiled.aif", ",", "BM7 Large Wooden.aif", ",", "BM7 London Plate.aif", ",", "BM7 Long Ambience.aif", ",", "BM7 Marble Foyer.aif", ",", "BM7 Med Ambience.aif", ",", "BM7 Medium & Bright.aif", ",", "BM7 Medium & Dark.aif", ",", "BM7 Medium & Deep.aif", ",", "BM7 Medium & Near.aif", ",", "BM7 Medium Chamber.aif", ",", "BM7 Medium Tiled.aif", ",", "BM7 Music Room.aif", ",", "BM7 North Church.aif", ",", "BM7 Old Chamber.aif", ",", "BM7 Old Plate.aif", ",", "BM7 Percussion Air.aif", ",", "BM7 Percussion.aif", ",", "BM7 Red Room.aif", ",", "BM7 Redwood Valley.aif", ",", "BM7 Rich Plate.aif", ",", "BM7 Sandors Hall.aif", ",", "BM7 Scoring Stage.aif", ",", "BM7 Silver Plate.aif", ",", "BM7 Small & Bright Amb.aif", ",", "BM7 Small & Bright Chamb.aif", ",", "BM7 Small & Dark Amb.aif", ",", "BM7 Small & Dark Chamb.aif", ",", "BM7 Small & Near.aif", ",", "BM7 Small Ambience.aif", ",", "BM7 Small Chamber.aif", ",", "BM7 Small Plate.aif", ",", "BM7 Small Q Room.aif", ",", "BM7 Small Room.aif", ",", "BM7 Small Tiled.aif", ",", "BM7 Small Vox Room.aif", ",", "BM7 Small Wooden.aif", ",", "BM7 Snare Chamber.aif", ",", "BM7 Snare Plate A.aif", ",", "BM7 Snare Plate B.aif", ",", "BM7 South Church.aif", ",", "BM7 Stone Quarry.aif", ",", "BM7 Studio A.aif", ",", "BM7 Studio B Close.aif", ",", "BM7 Studio B Far.aif", ",", "BM7 Studio D.aif", ",", "BM7 Studio E.aif", ",", "BM7 Studio.aif", ",", "BM7 Tangewood.aif", ",", "BM7 The ArchDuke.aif", ",", "BM7 Vienna Hall.aif", ",", "BM7 Vocal Chamber.aif", ",", "BM7 Vocal Plate.aif", ",", "BM7 West Church.aif", ",", "BM7 Worchester Hall.aif", ",", "BRX100 Gentle.aif", ",", "BRX100 Loud.aif", ",", "Cab and Spring.aif", ",", "Club Med.wav", ",", "Club Small.wav", ",", "DDRS78 a.aif", ",", "DDRS78 b.aif", ",", "DDRS78 c.aif", ",", "DDRS78 e.aif", ",", "Drum Booth Real.aif", ",", "Drum Booth.aif", ",", "Drum Echo.aif", ",", "Drum Gated.aif", ",", "Drum Plate.aif", ",", "Drum Room Ring.aif", ",", "Drum Room.aif", ",", "Dull Tapedelay Spring.aif", ",", "DVRS23 b.aif", ",", "DVRS23 Chord Hum.aif", ",", "DVRS23 Strange Spring.aif", ",", "ECRR 2000Ms Hall.aif", ",", "ECRR Acoustic Ambience Plate.aif", ",", "ECRR Acoustic Guitar.aif", ",", "ECRR Apartment Living.aif", ",", "ECRR Background.aif", ",", "ECRR Big Ambience.aif", ",", "ECRR Big Empty Stadium.aif", ",", "ECRR Club Galleria Plate.aif", ",", "ECRR Frozen Room.aif", ",", "ECRR Hall.aif", ",", "ECRR Lightpipe.aif", ",", "ECRR Master Plate.aif", ",", "ECRR Move Aside.aif", ",", "ECRR Muddy Factory.aif", ",", "ECRR Muffled Cinema.aif", ",", "ECRR Openverb.aif", ",", "ECRR Outdoors.aif", ",", "ECRR Point To Point.aif", ",", "ECRR Rusty Plate.aif", ",", "ECRR Silky Gold Plate.aif", ",", "ECRR Smokey Alto Plate.aif", ",", "ECRR Stereo Enhancement Plate.aif", ",", "ECRR Taj Mahal.aif", ",", "ECRR Talking Head.aif", ",", "ECRR Tight Spaces.aif", ",", "ECRR Tin Presence.aif", ",", "ECRR Very Large Cathedral.aif", ",", "ECRR Warm Cathedral.aif", ",", "ECRR Wood Plate.aif", ",", "ECRR Wooden Hall.aif", ",", "EDSP7500 Acoustic Room.aif", ",", "EDSP7500 Barking Chamber L.aif", ",", "EDSP7500 Barking Chamber R.aif", ",", "EDSP7500 Big Garage.aif", ",", "EDSP7500 Blackhole.aif", ",", "EDSP7500 Chorus Taps L.aif", ",", "EDSP7500 Chorus Taps R.aif", ",", "EDSP7500 Demon Delay.aif", ",", "EDSP7500 Desert Percussion.aif", ",", "EDSP7500 Dream Chamber L.aif", ",", "EDSP7500 Dream Chamber R.aif", ",", "EDSP7500 Gated Water Snare.aif", ",", "EDSP7500 GenesisII.aif", ",", "EDSP7500 Ghost Air.aif", ",", "EDSP7500 Kickback.aif", ",", "EDSP7500 Long Distance Call.aif", ",", "EDSP7500 Magic Echo.aif", ",", "EDSP7500 Masterverb Hall L.aif", ",", "EDSP7500 Masterverb Hall R.aif", ",", "EDSP7500 Masterverb Room L.aif", ",", "EDSP7500 Masterverb Room R.aif", ",", "EDSP7500 Phantom and Reverb A-D-F L.aif", ",", "EDSP7500 Phantom and Reverb A-D-F R.aif", ",", "EDSP7500 Phantom and Reverb C-G L.aif", ",", "EDSP7500 Phantom and Reverb C-G R.aif", ",", "EDSP7500 ReelRoom L.aif", ",", "EDSP7500 ReelRoom R.aif", ",", "EDSP7500 Reverse Nonlinear L.aif", ",", "EDSP7500 Reverse Nonlinear R.aif", ",", "EDSP7500 Sizzleverb L.aif", ",", "EDSP7500 Sizzleverb R.aif", ",", "EDSP7500 Small Crowd L.aif", ",", "EDSP7500 Small Crowd R.aif", ",", "EDSP7500 Splashverb L.aif", ",", "EDSP7500 Splashverb R.aif", ",", "EDSP7500 Wormhole L.aif", ",", "EDSP7500 Wormhole R.aif", ",", "Ely Chapel.wav", ",", "Ely Large Choir Hall.wav", ",", "Fake Clean Spring Wide.aif", ",", "Fake Long Spring Mono.aif", ",", "Fake Loose Spring Wide.aif", ",", "Falkland Tennis Court L.aif", ",", "Falkland Tennis Court R.aif", ",", "Farfi Spring Dirtier Narrow L.aif", ",", "Farfi Spring Dirtier Narrow R.aif", ",", "Farfi Spring Dirtier Wide L.aif", ",", "Farfi Spring Dirtier Wide R.aif", ",", "Farfi Spring Dirtier Wider L.aif", ",", "Farfi Spring Dirtier Wider R.aif", ",", "Farfi Spring Dirty Narrow.aif", ",", "Farfi Spring Hiss Narrow.aif", ",", "German Concert Hall L.aif", ",", "German Concert Hall R.aif", ",", "German Large Church L.aif", ",", "German Large Church R.aif", ",", "German Opera House L.aif", ",", "German Opera House R.aif", ",", "German Theater L.aif", ",", "German Theater R.aif", ",", "Giant Basilica.aif", ",", "Hamilton Mausoleum.aif", ",", "HIC100L Crash.aif", ",", "Hyper Cheap Sci-fi.aif", ",", "Hyper Dark Slapback.aif", ",", "IR Dach Plateau Omni.wav", ",", "IR Dach XY Oktava Fern.wav", ",", "IR Dach XY Oktava Nah.wav", ",", "IR Deserted Employee Bathroom - ORTF .wav", ",", "IR Hallraum Hinten AB TLM 102.wav", ",", "IR Hallraum Hinten ORTF auf Regal.wav", ",", "IR Hallraum Vorne Omni.wav", ",", "IR Hallraum Vorne ORTF 1 TLM 103.wav", ",", "Kick and Bass Ambience.aif", ",", "L960 Bathroom.aif", ",", "L960 Big Bathroom.aif", ",", "L960 Large Closet.aif", ",", "L960 Living Room.aif", ",", "L960 Warehouse.aif", ",", "Maes Howe 1.aif", ",", "Maes Howe 2.aif", ",", "Moist Crash.aiff", ",", "Moist Diffused Spring Echo 3 L.aif", ",", "Moist Diffused Spring Echo 3 R.aif", ",", "Moist Spring Mono.aif", ",", "Moist Wierd Wide.aiff", ",", "NYC Sportscenter 1.aif", ",", "NYC Sportscenter 2 L.aif", ",", "NYC Sportscenter 2 R.aif", ",", "Perc Plate.aif", ",", "Phipps Hall Huddersfield L.wav", ",", "Phipps Hall Huddersfield R.wav", ",", "Powerstation Berlin Bright L.aif", ",", "Powerstation Berlin Bright R.aif", ",", "Powerstation Berlin L.aif", ",", "Powerstation Berlin R.aif", ",", "Q2496 Cathedral.aif", ",", "Q2496 Chapel.aif", ",", "Q2496 Church.aif", ",", "Q2496 Concert H-A.aif", ",", "Q2496 Disco.aif", ",", "Q2496 Funk.aif", ",", "Q2496 Lounge Latin.aif", ",", "Q2496 Rnb Sweet.aif", ",", "Q2496 Rnb Today 1.aif", ",", "Q2496 Rnb Today 2.aif", ",", "Q2496 Theater.aif", ",", "RLND Plastic Reverse L.aif", ",", "RLND Plastic Reverse R.aif", ",", "RLND Reverse and Tail L.aif", ",", "RLND Reverse and Tail R.aif", ",", "RMX16 Ambience.aif", ",", "Seminar Room L.aif", ",", "Seminar Room R.aif", ",", "Snare Tap Gate Hard.aif", ",", "Snare Tap.aif", ",", "SNRA500 Plucky.aif", ",", "St Albans Cathedral 1.aif", ",", "St Albans Cathedral 2.aif", ",", "St Albans Cathedral 3 L.aif", ",", "St Albans Cathedral 3 R.aif", ",", "St Albans Cathedral 4.aif", ",", "St Albans Cathedral 5.aif", ",", "St Albans Cathedral 6 L.aif", ",", "St Albans Cathedral 6 R.aif", ",", "St Andrews Church 1.aif", ",", "St Andrews Church 2.aif", ",", "St Patricks Church 1 L.aif", ",", "St Patricks Church 1 R.aif", ",", "St Patricks Church 2 L.aif", ",", "St Patricks Church 2 R.aif", ",", "St Patricks Church 3 L.aif", ",", "St Patricks Church 3 R.aif", ",", "St Pauls Hall Huddersfield Far.aif", ",", "St Pauls Hall Huddersfield Near.aif", ",", "Stairwell.aif", ",", "Swede Plate 1.0s.aif", ",", "Swede Plate 2.0s.aif", ",", "Swede Plate 2.5s.aif", ",", "Swede Plate 3.0s.aif", ",", "Swede Plate 3.2s L.aif", ",", "Swede Plate 3.2s R.aif", ",", "Swede Plate 3.5s.aif", ",", "Swede Plate 4.0s.aif", ",", "Swede Plate 4.5s.aif", ",", "Swede Plate 5.0s.aif", ",", "Swedish Nuclear Reactor 1 L.aif", ",", "Swedish Nuclear Reactor 1 R.aif", ",", "Swedish Nuclear Reactor 2.aif", ",", "Swissecho Layered.aif", ",", "Swissecho RevR DelayL 1.aif", ",", "Swissecho RevR DelayL 2.aif", ",", "Swissecho RevR DelayL 3.aif", ",", "Swissecho RevR DelayL 4 L.aif", ",", "Swissecho RevR DelayL 4 R.aif", ",", "Swissecho Spring 1.5s Wide.aif", ",", "Swissecho Spring 3.5s Mono.aif", ",", "Swissecho Spring 3.5s Wide.aif", ",", "Synth Spring Mono.aif", ",", "Terrys Typing Room 1.aif", ",", "Terrys Typing Room 2 L.aif", ",", "Terrys Typing Room 2 R.aif", ",", "Terrys Warehouse 1.aif", ",", "Terrys Warehouse 2 L.aif", ",", "Terrys Warehouse 2 R.aif", ",", "Toms Gated.aif", ",", "TVC33 12 String Doubler.aif", ",", "TVC33 12 String Reverb.aif", ",", "TVC33 240Msgate.aif", ",", "TVC33 3 Car Garage.aif", ",", "TVC33 A Vocal Room.aif", ",", "TVC33 Acoustic Guitar Space.aif", ",", "TVC33 Amb Rock Lead Git.aif", ",", "TVC33 Announcer.aif", ",", "TVC33 Backing Vocal Gate.aif", ",", "TVC33 Bedroom.aif", ",", "TVC33 Big Dense Studio.aif", ",", "TVC33 Bossa Nova Perc Room.aif", ",", "TVC33 Bright Strings.aif", ",", "TVC33 Bright Theatre.aif", ",", "TVC33 Church Piano.aif", ",", "TVC33 Dance Snare.aif", ",", "TVC33 Darkinverse.aif", ",", "TVC33 Dialoge 2.aif", ",", "TVC33 Dialoge 3.aif", ",", "TVC33 Distance In Jungle.aif", ",", "TVC33 Drum Room Expander.aif", ",", "TVC33 Drum Wood Plate.aif", ",", "TVC33 Empty Indoor Pool.aif", ",", "TVC33 Empty Store.aif", ",", "TVC33 Enhancer Verb 2.aif", ",", "TVC33 Forrest In Autum.aif", ",", "TVC33 Gittinjiggywiddit.aif", ",", "TVC33 In A Cylinder.aif", ",", "TVC33 Jazzhall.aif", ",", "TVC33 Kick Bass Ambience.aif", ",", "TVC33 Live Vo Booth.aif", ",", "TVC33 Lucho 2015 Good Room.aif", ",", "TVC33 Medium Vocal Hall.aif", ",", "TVC33 Microuzi Gate.aif", ",", "TVC33 Mid Size Ambience.aif", ",", "TVC33 Mine-Corridor.aif", ",", "TVC33 Minimum Booth.aif", ",", "TVC33 Moveson.aif", ",", "TVC33 Nice Perc Ambience.aif", ",", "TVC33 Open Mics.aif", ",", "TVC33 Overhead Mics.aif", ",", "TVC33 Piano Hall 1St Row.aif", ",", "TVC33 Recording Booth.aif", ",", "TVC33 Rhodes Thicken.aif", ",", "TVC33 Room Conversation.aif", ",", "TVC33 Scoring Stage 1.aif", ",", "TVC33 Shimmer Mod Lite.aif", ",", "TVC33 Sky Parking Garage.aif", ",", "TVC33 Slap Back Reverb.aif", ",", "TVC33 Slopedown.aif", ",", "TVC33 Small Booth.aif", ",", "TVC33 Small Clear Room.aif", ",", "TVC33 Small Guitar Room.aif", ",", "TVC33 Small Natural Room.aif", ",", "TVC33 Small Wood Chamber.aif", ",", "TVC33 Snare Room Long.aif", ",", "TVC33 Soft Guitar Ambience.aif", ",", "TVC33 Sparkling Hall.aif", ",", "TVC33 Studio C.aif", ",", "TVC33 Studio Small.aif", ",", "TVC33 Tight & Natural.aif", ",", "TVC33 Tight Vocal.aif", ",", "TVC33 Tiled Room.aif", ",", "TVC33 Venue Warm 1.aif", ",", "TVC33 Very Small Ambience.aif", ",", "TVC33 Vocal Doubler.aif", ",", "TVC33 Vocal For Thin Voice.aif", ",", "Tyndall Bruce Monument 1.aif", ",", "Tyndall Bruce Monument 2 L.aif", ",", "Tyndall Bruce Monument 2 R.aif", ",", "UEMT250 0.4s.aif", ",", "UEMT250 0.6s.aif", ",", "UEMT250 0.8s.aif", ",", "UEMT250 2.0s.aif", ",", "UEMT250 2.2s.aif", ",", "UEMT250 2.5s.aif", ",", "UEMT250 2.8s.aif", ",", "UEMT250 4.0s.aif", ",", "UMSS282 Cloud.aif", ",", "UMSS282 Comb 1.aif", ",", "UMSS282 Comb 2.aif", ",", "UMSS282 Echo 1.aif", ",", "UMSS282 Echo 2.aif", ",", "UMSS282 Echo 3.aif", ",", "UMSS282 Echo Chamber.aif", ",", "UMSS282 Fatty.aif", ",", "UMSS282 Hall.aif", ",", "UMSS282 Quick Short Room.aif", ",", "UMSS282 Room 1.aif", ",", "UMSS282 Room 2.aif", ",", "UMSS282 Room 3.aif", ",", "UMSS282 Room 4.aif", ",", "UMSS282 Room 5.aif", ",", "UMSS282 Slap Room.aif", ",", "UMSS282 Slap.aif", ",", "UMSS282 Space Repeat 1.aif", ",", "UMSS282 Space Repeat 2.aif", ",", "UMSS282 Space Repeat 3.aif", ",", "UMSS282 Space Slap.aif", ",", "Wedge Arctic Echoes.aif", ",", "Wedge Big Cave.aif", ",", "Wedge Empty Hall.aif", ",", "Wedge Flying Saucer.aif", ",", "Wedge Glimmer.aif", ",", "Wedge Harlem1949.aif", ",", "Wedge Nashville.aif", ",", "Wedge Vocal Ballad.aif", ",", "Wood Room Large.wav", ",", "Wood Room Small.wav", ",", "Xperimental Cymbal.aif", ",", "Xperimental Dance Shaker.aif", ",", "Xperimental Kick Sub Long.aif", ",", "Xperimental Kick Sub.aif", ",", "Xperimental Ride.aif", ",", "Xperimental Sleigh bell.aif", ",", "Xperimental Snare Distance and Snap.aif", ",", "Xperimental Thud and Pedal.aif", ",", "Xperimental TR8o8 Closed HH.aif", ",", "York Minster Cathedral.aif", ",", "YR7 Large Hall.aif" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 673.18597412109375, 283.244354248046875, 100.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -0.010066151618958, 0.016036957502365, 68.715980529785156, 15.0 ],
					"textcolor" : [ 0.533333333333333, 0.533333333333333, 0.533333333333333, 1.0 ],
					"varname" : "IR Folder"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 673.18597412109375, 187.031753540039062, 248.0, 22.0 ],
					"text" : "sprintf %s12c_sandbox/2019/convolution/IRs"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 673.18597412109375, 157.857986450195312, 57.0, 22.0 ],
					"text" : "tosymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 673.18597412109375, 126.826240539550781, 67.0, 22.0 ],
					"text" : "12c_config"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 673.18597412109375, 229.856948852539062, 39.0, 22.0 ],
					"text" : "folder"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 276.244293212890625, 397.853668212890625, 29.5, 22.0 ],
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "bang", "bang", "", "int", "int" ],
					"patching_rect" : [ 484.559051513671875, 162.145828247070312, 61.0, 22.0 ],
					"text" : "t b b s 1 0"
				}

			}
, 			{
				"box" : 				{
					"comment" : "(signal) audio out",
					"id" : "obj-9",
					"index" : 1,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 196.433212280273438, 687.41766357421875, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 276.2442626953125, 254.666656494140625, 107.272735595703125, 35.0 ],
					"text" : "loadmess set convolution_buffer"
				}

			}
, 			{
				"box" : 				{
					"comment" : "(signal) audio input for impulse response",
					"id" : "obj-5",
					"index" : 1,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 193.125, 78.145835876464844, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 505.559051513671875, 266.14666748046875, 70.11810302734375, 49.0 ],
					"text" : "buffer~ convolution_buffer",
					"varname" : "convolution_buffer"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 276.2442626953125, 334.620574951171875, 135.0, 22.0 ],
					"text" : "multiconvolve~ 1 1 zero"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"midpoints" : [ 285.744293212890625, 431.506300687789917, 255.4285888671875, 431.506300687789917 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"midpoints" : [ 525.559051513671875, 193.252201914787292, 562.595458745956421, 193.252201914787292, 562.595458745956421, 153.657107025384903, 978.6717529296875, 153.657107025384903 ],
					"source" : [ "obj-10", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-10", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"midpoints" : [ 494.059051513671875, 217.1875, 74.0, 217.1875 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"midpoints" : [ 504.559051513671875, 242.257230877876282, 616.124647915363312, 242.257230877876282, 616.124647915363312, 479.910353302955627, 703.68597412109375, 479.910353302955627 ],
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"midpoints" : [ 536.059051513671875, 228.336914956569672, 492.569320976734161, 228.336914956569672, 492.569320976734161, 321.245707631111145, 514.059051513671875, 321.245707631111145 ],
					"source" : [ "obj-10", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 849.53594970703125, 273.853188157081604, 682.68597412109375, 273.853188157081604 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"midpoints" : [ 255.4285888671875, 542.245973467826843, 264.9332275390625, 542.245973467826843 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 1 ],
					"midpoints" : [ 255.4285888671875, 588.235276937484741, 235.433212280273438, 588.235276937484741 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"midpoints" : [ 806.14581298828125, 259.347455203533173, 832.912126421928406, 259.347455203533173, 832.912126421928406, 223.701001167297363, 879.53594970703125, 223.701001167297363 ],
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 795.64581298828125, 259.347455203533173, 780.958032011985779, 259.347455203533173, 780.958032011985779, 219.230779945850372, 849.53594970703125, 219.230779945850372 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 1 ],
					"midpoints" : [ 157.333333333333314, 328.770561993122101, 265.433035552501678, 328.770561993122101, 265.433035552501678, 383.960510939359665, 296.244293212890625, 383.960510939359665 ],
					"source" : [ "obj-20", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-20", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 1 ],
					"midpoints" : [ 74.0, 381.823385655879974, 126.166671752929688, 381.823385655879974 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 199.0, 306.334187835454941, 285.7442626953125, 306.334187835454941 ],
					"source" : [ "obj-20", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"midpoints" : [ 115.666671752929688, 432.482719004154205, 255.4285888671875, 432.482719004154205 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"midpoints" : [ 225.716232299804688, 217.567553043365479, 74.0, 217.567553043365479 ],
					"source" : [ "obj-23", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-25", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"midpoints" : [ 556.770263671875, 234.459443807601929, 515.059051513671875, 234.459443807601929 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"midpoints" : [ 1080.7265625, 424.542662441730499, 1003.7244873046875, 424.542662441730499 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 822.53594970703125, 167.0, 927.0, 167.0, 927.0, 214.0, 734.0, 214.0, 734.0, 274.0, 682.68597412109375, 274.0 ],
					"source" : [ "obj-29", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 3 ],
					"midpoints" : [ 174.029998779296875, 578.864630997180939, 286.9332275390625, 578.864630997180939 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"midpoints" : [ 1168.6717529296875, 221.954280316829681, 1001.7244873046875, 221.954280316829681 ],
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 978.6717529296875, 265.47802522778511, 682.68597412109375, 265.47802522778511 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"midpoints" : [ 153.134170532226562, 133.898308277130127, 202.625, 133.898308277130127 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 163.634170532226562, 142.372884750366211, 180.508478879928589, 142.372884750366211, 180.508478879928589, 125.423731803894043, 265.254243612289429, 125.423731803894043, 265.254243612289429, 35.593221187591553, 556.61309814453125, 35.593221187591553 ],
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 367.117645263671875, 638.861627876758575, 205.933212280273438, 638.861627876758575 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 2 ],
					"midpoints" : [ 219.594512939453125, 594.993663370609283, 279.599894205729186, 594.993663370609283 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-42", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 1 ],
					"midpoints" : [ 337.7843017578125, 586.0, 272.266560872395814, 586.0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"midpoints" : [ 723.18597412109375, 396.70807221531868, 705.590058326721191, 396.70807221531868, 705.590058326721191, 354.658383190631866, 937.888193726539612, 354.658383190631866, 937.888193726539612, 153.030210793018341, 978.6717529296875, 153.030210793018341 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"midpoints" : [ 801.18597412109375, 407.936513721942902, 628.571438312530518, 407.936513721942902, 628.571438312530518, 251.488097548484802, 515.059051513671875, 251.488097548484802 ],
					"source" : [ "obj-43", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"midpoints" : [ 762.18597412109375, 414.87567874789238, 650.43476128578186, 414.87567874789238, 650.43476128578186, 217.855065375566483, 74.0, 217.855065375566483 ],
					"source" : [ "obj-43", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"midpoints" : [ 781.68597412109375, 422.340419352054596, 703.68597412109375, 422.340419352054596 ],
					"source" : [ "obj-43", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"midpoints" : [ 742.68597412109375, 401.361113727092743, 556.416671007871628, 401.361113727092743, 556.416671007871628, 324.416667491197586, 514.059051513671875, 324.416667491197586 ],
					"source" : [ "obj-43", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 1 ],
					"midpoints" : [ 1003.7244873046875, 502.380960166454315, 836.507949471473694, 502.380960166454315, 836.507949471473694, 362.698418319225311, 801.18597412109375, 362.698418319225311 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 2 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"order" : 1,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"midpoints" : [ 174.029998779296875, 550.235598534345627, 219.594512939453125, 550.235598534345627 ],
					"order" : 0,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"midpoints" : [ 1003.7244873046875, 414.024180948734283, 987.033773750066757, 414.024180948734283, 987.033773750066757, 353.840195417404175, 953.406495213508606, 353.840195417404175, 953.406495213508606, 223.58225291967392, 1001.7244873046875, 223.58225291967392 ],
					"order" : 1,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"order" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"midpoints" : [ 527.059051513671875, 509.696095645427704, 55.228836953639984, 509.696095645427704, 55.228836953639984, 215.625, 74.0, 215.625 ],
					"source" : [ "obj-55", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"midpoints" : [ 514.059051513671875, 479.404286354780197, 703.68597412109375, 479.404286354780197 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 582.264892578125, 224.020824909210205, 682.68597412109375, 224.020824909210205 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 907.6650390625, 255.504565834999084, 896.788915991783142, 255.504565834999084, 896.788915991783142, 218.807321190834045, 849.53594970703125, 218.807321190834045 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"midpoints" : [ 285.7442626953125, 296.894498527050018, 115.666664123535156, 296.894498527050018 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"midpoints" : [ 456.452239990234375, 193.0, 238.023809552192688, 193.0, 238.023809552192688, 317.0, 115.666664123535156, 317.0 ],
					"order" : 1,
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 456.452239990234375, 193.0, 238.0, 193.0, 238.0, 318.0, 285.7442626953125, 318.0 ],
					"order" : 0,
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 702.68597412109375, 258.045053780078888, 772.687950253486633, 258.045053780078888, 772.687950253486633, 225.620686590671539, 795.64581298828125, 225.620686590671539 ],
					"source" : [ "obj-7", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"midpoints" : [ 806.06097412109375, 520.067710340023041, 858.544609993696213, 520.067710340023041, 858.544609993696213, 366.046062290668488, 1003.7244873046875, 366.046062290668488 ],
					"source" : [ "obj-70", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"midpoints" : [ 436.452239990234375, 365.001888990402222, 514.059051513671875, 365.001888990402222 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 1 ],
					"midpoints" : [ 415.5953369140625, 427.068847298622131, 266.4285888671875, 427.068847298622131 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
 ],
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "buttonGreen-1",
				"default" : 				{
					"bgcolor" : [ 0.043137, 0.364706, 0.094118, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "dUG Yello 01-1",
				"button" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ]
				}
,
				"toggle" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ]
				}
,
				"default" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ],
					"fontface" : [ 1 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.94902, 0.992157, 1.0, 1.0 ],
						"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"fontsize" : [ 10.0 ]
				}
,
				"newobj" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ],
					"fontface" : [ 1 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"fontsize" : [ 10.0 ]
				}
,
				"message" : 				{
					"fontface" : [ 1 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.94902, 0.992157, 1.0, 1.0 ],
						"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"fontsize" : [ 10.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "ksliderWhite",
				"default" : 				{
					"color" : [ 1.0, 1.0, 1.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjCyan-1",
				"default" : 				{
					"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"fontsize" : [ 12.059008 ],
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rsliderGold",
				"default" : 				{
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
					"color" : [ 0.646639, 0.821777, 0.854593, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "tap",
				"default" : 				{
					"fontname" : [ "Lato Light" ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
