{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 0,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 580.0, 79.0, 288.0, 687.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 10.0,
					"id" : "obj-121",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 6.382978916168213, 86.322189331054688, 152.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.343465805053711, 78.723411560058594, 27.0, 18.0 ],
					"text" : "trig"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1191.0, -43.0, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 348.74993896484375, -60.295200347900391, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 387.7044677734375, 57.622993469238281, 32.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 400.7044677734375, 17.954645156860352, 74.0, 22.0 ],
					"text" : "snapshot~ 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ -100.0, 100.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 3,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int", "", "" ],
					"patching_rect" : [ 400.7044677734375, -25.427127838134766, 107.800048828125, 24.999996185302734 ],
					"presentation" : 1,
					"presentation_rect" : [ 97.007919311523438, 76.503631591796875, 101.986625671386719, 23.982097625732422 ],
					"varname" : "gravity_y_∆",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 8.0,
					"id" : "obj-88",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.282424926757812, 22.407308578491211, 55.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.202163696289062, 34.849372863769531, 72.616195678710938, 15.0 ],
					"text" : "x          y          z",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 8.0,
					"id" : "obj-80",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 132.341262817382812, -2.533874034881592, 44.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.202163696289062, 55.511829376220703, 72.469749450683594, 15.0 ],
					"text" : "impulse force",
					"textcolor" : [ 0.843137264251709, 0.843137264251709, 0.843137264251709, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 8.0,
					"id" : "obj-11",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -0.501888394355774, 64.622993469238281, 44.0, 24.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 264.860137939453125, 43.065238952636719, 42.502418518066406, 24.0 ],
					"text" : "wall energy"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "16n" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-114",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "hard_sync_via_delta.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 845.10455322265625, 45.952381134033203, 142.5, 18.759035110473633 ],
					"presentation" : 1,
					"presentation_rect" : [ 48.012619018554688, 79.503631591796875, 51.01190185546875, 14.642949104309082 ],
					"varname" : "Delta Retrigger",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1100.6583251953125, -23.783544540405273, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1123.6741943359375, 22.269847869873047, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 198.068069458007812, 103.976783752441406, 29.156852722167969, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_unitstyle" : 1,
							"parameter_longname" : "restitution_manual",
							"parameter_mmax" : 2.0,
							"parameter_shortname" : "restitution_manual",
							"parameter_type" : 0
						}

					}
,
					"varname" : "restitution_manual"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1161.161376953125, 96.057731628417969, 32.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1326.984130859375, -27.672433853149414, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1350.0, 18.380956649780273, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 198.068069458007812, 126.15411376953125, 29.156852722167969, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_unitstyle" : 1,
							"parameter_longname" : "scale_manual",
							"parameter_mmax" : 2.0,
							"parameter_shortname" : "scale_manual",
							"parameter_type" : 0
						}

					}
,
					"varname" : "scale_manual"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.4871826171875, 92.168838500976562, 32.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1400.4871826171875, 56.389381408691406, 74.0, 22.0 ],
					"text" : "snapshot~ 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-92",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 3,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int", "", "" ],
					"patching_rect" : [ 1400.4871826171875, 13.380958557128906, 107.800048828125, 24.999996185302734 ],
					"presentation" : 1,
					"presentation_rect" : [ 97.007919311523438, 121.663063049316406, 101.986625671386719, 23.982097625732422 ],
					"varname" : "scale",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1174.161376953125, 56.389381408691406, 74.0, 22.0 ],
					"text" : "snapshot~ 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0.0, 2.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-38",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 3,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int", "", "" ],
					"patching_rect" : [ 1174.161376953125, 14.558219909667969, 107.800048828125, 24.999996185302734 ],
					"presentation" : 1,
					"presentation_rect" : [ 97.007919311523438, 99.485733032226562, 101.986625671386719, 23.982097625732422 ],
					"varname" : "energy",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -88.0, 337.579742431640625, 61.0, 22.0 ],
					"text" : "pipe 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ -88.0, 286.0, 31.0, 22.0 ],
					"text" : "t 1 s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 32.0, 413.333343505859375, 31.0, 22.0 ],
					"text" : "t b s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -104.666656494140625, 247.0, 109.0, 22.0 ],
					"text" : "drawto phys_world"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 32.0, 378.0, 157.0, 22.0 ],
					"text" : "loadmess name phys_world"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "jit.pwindow",
					"name" : "phys_world",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 32.0, 457.166656494140625, 80.0, 60.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.202163696289062, 94.174278259277344, 72.616195678710938, 52.937538146972656 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 629.5, 125.0, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 265.99334716796875, 64.616966247558594, 29.156852722167969, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_unitstyle" : 1,
							"parameter_longname" : "global_restitution",
							"parameter_mmax" : 2.0,
							"parameter_shortname" : "global_restitution",
							"parameter_type" : 0
						}

					}
,
					"varname" : "global_restitution"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 8.0,
					"id" : "obj-75",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 247.24993896484375, 117.380958557128906, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 95.304054260253906, 61.849372863769531, 38.479339599609375, 15.0 ],
					"text" : "gravity"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 404.204498291015625, 168.5, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 198.068069458007812, 61.976783752441406, 29.1568603515625, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_unitstyle" : 1,
							"parameter_mmin" : -100.0,
							"parameter_longname" : "gravity_z",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 100.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "gravity_z",
							"parameter_type" : 0
						}

					}
,
					"varname" : "gravity_z"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 348.74993896484375, -20.427129745483398, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 164.068069458007812, 61.849372863769531, 29.1568603515625, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_unitstyle" : 1,
							"parameter_mmin" : -100.0,
							"parameter_longname" : "gravity_y",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 100.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "gravity_y",
							"parameter_type" : 0
						}

					}
,
					"varname" : "gravity_y"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 293.24993896484375, 117.380958557128906, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "live.numbox",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 293.24993896484375, 168.5, 44.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 129.068069458007812, 61.849372863769531, 29.160053253173828, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_unitstyle" : 1,
							"parameter_mmin" : -100.0,
							"parameter_longname" : "gravity_x",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 100.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "gravity_x",
							"parameter_type" : 0
						}

					}
,
					"varname" : "gravity_x"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"linecount" : 6,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 302.74993896484375, 268.5, 150.0, 87.0 ],
					"text" : "TODO eventually maybe.\nlogic to generate impules on collision that can be sent to delta module. i think it would replace wand1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1400.4871826171875, 176.1417236328125, 54.0, 22.0 ],
					"text" : "pack f f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1400.4871826171875, 205.81500244140625, 87.0, 22.0 ],
					"text" : "scale $1 $2 $3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "float", "float", "float" ],
					"patching_rect" : [ 1400.4871826171875, 142.2423095703125, 54.0, 22.0 ],
					"text" : "t f f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1161.161376953125, 248.692352294921875, 150.0, 22.0 ],
					"text" : "s universal_atom_property"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"linecount" : 7,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 977.41943359375, 957.15057373046875, 150.0, 114.0 ],
					"text" : "TODO: connect to delta module, prob build better ui for selecting channel. also maybe think if this is even worth it. i think it is but idk. it would replace wand 2\n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 950.00006103515625, 885.0, 37.634407043457031, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 67,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 981.5, 916.0, 712.0, 22.0 ],
					"text" : "selector~ 66"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 22,
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 1454.0, 885.0, 239.5, 22.0 ],
					"text" : "mc.unpack~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 22,
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 1222.648193359375, 885.0, 239.5, 22.0 ],
					"text" : "mc.unpack~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 22,
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 992.0, 885.0, 239.5, 22.0 ],
					"text" : "mc.unpack~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 884.0, 187.380950927734375, 100.0, 22.0 ],
					"text" : "impulse $1 $2 $3"
				}

			}
, 			{
				"box" : 				{
					"contdata" : 1,
					"id" : "obj-82",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 946.0, 80.389381408691406, 23.0, 59.9915771484375 ],
					"presentation" : 1,
					"presentation_rect" : [ 73.2021484375, 36.20892333984375, 22.616207122802734, 44.111259460449219 ],
					"setminmax" : [ -100.0, 100.0 ],
					"slidercolor" : [ 0.807843137254902, 0.898039215686275, 0.909803921568627, 0.38 ],
					"varname" : "impulse_z"
				}

			}
, 			{
				"box" : 				{
					"contdata" : 1,
					"id" : "obj-81",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 913.0, 80.389381408691406, 23.0, 59.9915771484375 ],
					"presentation" : 1,
					"presentation_rect" : [ 48.012619018554688, 36.20892333984375, 22.995296478271484, 44.111259460449219 ],
					"setminmax" : [ -100.0, 100.0 ],
					"slidercolor" : [ 0.807843137254902, 0.898039215686275, 0.909803921568627, 0.38 ],
					"varname" : "impulse_y"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 884.0, 151.380950927734375, 81.0, 22.0 ],
					"text" : "pak f f f"
				}

			}
, 			{
				"box" : 				{
					"contdata" : 1,
					"id" : "obj-21",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 884.0, 80.389381408691406, 23.0, 59.9915771484375 ],
					"presentation" : 1,
					"presentation_rect" : [ 23.202163696289062, 36.20892333984375, 22.995296478271484, 44.111259460449219 ],
					"setminmax" : [ -100.0, 100.0 ],
					"slidercolor" : [ 0.807843137254902, 0.898039215686275, 0.909803921568627, 0.38 ],
					"varname" : "impulse_x"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 884.0, 219.380950927734375, 91.0, 22.0 ],
					"text" : "s phys_impulse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 689.8846435546875, 30.072870254516602, 95.0, 22.0 ],
					"text" : "s impulse_gates"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-86",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_impulse_select.maxpat",
					"numinlets" : 0,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 689.8846435546875, -43.927131652832031, 134.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 95.007919311523438, 37.368091583251953, 132.217010498046875, 23.481281280517578 ],
					"varname" : "impulse_gates",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 100.0, 251.0, 22.0 ],
									"text" : "pak 0. 0. 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 139.0, 94.0, 22.0 ],
									"text" : "gravity $1 $2 $3"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-79",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-80",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 166.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-81",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 282.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-82",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 221.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 2 ],
									"source" : [ "obj-81", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 293.24993896484375, 199.0, 130.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p gravity_logic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 629.5, 150.74639892578125, 77.0, 22.0 ],
					"text" : "restitution $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 713.51348876953125, 933.0, 18.0, 20.0 ],
					"text" : "z"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 472.972930908203125, 942.43975830078125, 18.0, 20.0 ],
					"text" : "y"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 206.756744384765625, 942.43975830078125, 18.0, 20.0 ],
					"text" : "x"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1335.0, 508.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1335.0, 389.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1335.0, 449.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 22 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1335.0, 330.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 21 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1185.0, 748.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1185.0, 629.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1185.0, 508.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1185.0, 389.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1185.0, 689.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 20 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1185.0, 570.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 19 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1185.0, 449.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 18 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1185.0, 330.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 17 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1035.0, 747.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1035.0, 628.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1035.0, 507.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 1035.0, 388.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1035.0, 688.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 16 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1035.0, 569.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 15 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1035.0, 448.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 14 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1035.0, 329.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 13 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 885.0, 748.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 885.0, 629.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 885.0, 508.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 885.0, 389.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 689.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 12 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 570.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 11 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 449.0, 124.0, 49.0 ],
					"text" : "phys_atom @name 10 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 330.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 9 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 735.0, 748.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 735.0, 629.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 735.0, 508.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 735.0, 389.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 735.0, 689.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 8 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 735.0, 570.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 7 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 735.0, 449.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 6 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 735.0, 330.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 5 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 22,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 675.0, 885.0, 239.5, 22.0 ],
					"text" : "mc.pack~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 22,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 426.02838134765625, 885.0, 239.5, 22.0 ],
					"text" : "mc.pack~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 22,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 168.0, 885.0, 239.5, 22.0 ],
					"text" : "mc.pack~ 22"
				}

			}
, 			{
				"box" : 				{
					"comment" : "(mc.pack'd sigs, -1. - 1) current z values",
					"id" : "obj-119",
					"index" : 3,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 675.0, 933.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "(mc.pack'd sigs, -1. - 1) current y values",
					"id" : "obj-118",
					"index" : 2,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 426.02838134765625, 937.43975830078125, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "(mc.pack'd sigs, -1. - 1) current x values",
					"id" : "obj-117",
					"index" : 1,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 168.0, 937.43975830078125, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 585.0, 749.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 585.0, 630.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 585.0, 509.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 585.0, 390.0, 85.0, 22.0 ],
					"text" : "get_atom_pos"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1161.161376953125, 205.81500244140625, 77.0, 22.0 ],
					"text" : "restitution $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 0,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 1115.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "(message) modifies all 6 wall objects",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 34.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"linecount" : 7,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 842.24993896484375, 100.0, 127.0, 102.0 ],
									"text" : "phys_wall @name cube_back @scale 1. 1. 1. @shape cube @scale 5 0 5 @mass 0 @position 0. 0. 5. @restitution 1\\, @rotate 90. 90. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"linecount" : 7,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 677.24993896484375, 100.0, 127.0, 102.0 ],
									"text" : "phys_wall @name cube_front @scale 1. 1. 1. @shape cube @scale 5 0 5 @mass 0 @position 0. 0. -5. @restitution 1\\, @rotate 90. 90. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"linecount" : 7,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 519.49993896484375, 100.0, 127.0, 102.0 ],
									"text" : "phys_wall @name cube_right @scale 1. 1. 1. @shape cube @scale 5 0 5 @mass 0 @position -5. 0. 0. @restitution 1\\, @rotate 90. 0. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"linecount" : 7,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 360.999969482421875, 100.0, 131.0, 102.0 ],
									"text" : "phys_wall @name cube_left @scale 1. 1. 1. @shape cube @scale 5 0 5 @mass 0 @position 5. 0. 0. @restitution 1\\, @rotate 90. 0. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"linecount" : 6,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 205.75, 100.0, 127.0, 89.0 ],
									"text" : "phys_wall @name cube_top @scale 1. 1. 1. @shape cube @scale 5 0 5 @mass 0 @position 0. 5. 0. @restitution 1\\,"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"linecount" : 6,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 100.0, 127.0, 89.0 ],
									"text" : "phys_wall @name cube_bottom @scale 1. 1. 1. @shape cube @scale 5 0 5 @mass 0 @position 0. -5. 0. @restitution 1\\,"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"order" : 5,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"midpoints" : [ 59.5, 80.0, 215.25, 80.0 ],
									"order" : 4,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"midpoints" : [ 59.5, 81.0, 370.499969482421875, 81.0 ],
									"order" : 3,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"midpoints" : [ 59.5, 79.0, 528.99993896484375, 79.0 ],
									"order" : 2,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"midpoints" : [ 59.5, 79.0, 686.74993896484375, 79.0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"midpoints" : [ 59.5, 80.0, 851.74993896484375, 80.0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 629.5, 181.74639892578125, 45.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p walls"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 585.0, 690.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 4 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 585.0, 571.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 3 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 585.0, 450.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 2 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 585.0, 331.0, 126.0, 49.0 ],
					"text" : "phys_atom @name 1 @scale 1. 1. 1. @restitution 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 140.5, 199.0, 35.0, 22.0 ],
					"text" : "reset"
				}

			}
, 			{
				"box" : 				{
					"attr" : "camera",
					"id" : "obj-72",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 97.60736083984375, 277.0, 203.32098388671875, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 302.74993896484375, 243.5, 96.0, 23.0 ],
					"text" : "route collisions"
				}

			}
, 			{
				"box" : 				{
					"attr" : "collisions",
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-29",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 183.0, 199.0, 96.0, 23.0 ],
					"text_width" : 76.0
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.502, 0.502, 0.502, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 17.969135284423828, 539.0, 160.0, 23.0 ],
					"text" : "jit.gl.physdraw @enable 1"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.502, 0.502, 0.502, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 183.0, 243.5, 86.0, 23.0 ],
					"text" : "jit.phys.world"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 17.969135284423828, 337.579742431640625, 177.0, 23.0 ],
					"text" : "jit.gl.render @camera 0 0 -15"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "erase" ],
					"patching_rect" : [ 18.0, 277.0, 74.0, 23.0 ],
					"text" : "t b erase"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 18.0, 217.0, 25.0, 25.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 18.0, 247.0, 145.0, 23.0 ],
					"text" : "qmetro @interval 60 hz"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"midpoints" : [ 1396.9871826171875, 137.30158942937851, 1312.698433041572571, 137.30158942937851, 1312.698433041572571, -42.063492715358734, 1336.484130859375, -42.063492715358734 ],
					"order" : 1,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"midpoints" : [ 1396.9871826171875, 124.0, 1409.9871826171875, 124.0 ],
					"order" : 0,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"midpoints" : [ 1359.5, 125.0, 1409.9871826171875, 125.0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"midpoints" : [ 959.50006103515625, 910.913998782634735, 991.0, 910.913998782634735 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"midpoints" : [ 1336.484130859375, 0.0, 1359.5, 0.0 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"midpoints" : [ 1110.1583251953125, 3.888893127441406, 1133.1741943359375, 3.888893127441406 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"midpoints" : [ 594.5, 420.999984741210938, 488.0, 420.999984741210938, 488.0, 819.999984741210938, 177.5, 819.999984741210938 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"midpoints" : [ 627.5, 432.142810940742493, 510.714229702949524, 432.142810940742493, 510.714229702949524, 833.333243131637573, 435.52838134765625, 833.333243131637573 ],
					"source" : [ "obj-106", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 660.5, 443.406592488288879, 530.769256711006165, 443.406592488288879, 530.769256711006165, 812.637379765510559, 684.5, 812.637379765510559 ],
					"source" : [ "obj-106", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 1 ],
					"midpoints" : [ 594.5, 537.999984741210938, 488.0, 537.999984741210938, 488.0, 820.999984741210938, 188.0, 820.999984741210938 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 1 ],
					"midpoints" : [ 627.5, 553.571369051933289, 510.714229702949524, 553.571369051933289, 510.714229702949524, 834.523719191551208, 446.02838134765625, 834.523719191551208 ],
					"source" : [ "obj-107", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"midpoints" : [ 660.5, 563.18681812286377, 531.868157863616943, 563.18681812286377, 531.868157863616943, 812.637379765510559, 695.0, 812.637379765510559 ],
					"source" : [ "obj-107", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 2 ],
					"midpoints" : [ 594.5, 660.999984741210938, 487.0, 660.999984741210938, 487.0, 820.999984741210938, 198.5, 820.999984741210938 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 2 ],
					"midpoints" : [ 627.5, 671.428498983383179, 509.523753643035889, 671.428498983383179, 509.523753643035889, 832.142767071723938, 456.52838134765625, 832.142767071723938 ],
					"source" : [ "obj-108", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 2 ],
					"midpoints" : [ 660.5, 682.96704375743866, 531.868157863616943, 682.96704375743866, 531.868157863616943, 812.637379765510559, 705.5, 812.637379765510559 ],
					"source" : [ "obj-108", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 3 ],
					"midpoints" : [ 594.5, 780.999984741210938, 488.0, 780.999984741210938, 488.0, 820.999984741210938, 209.0, 820.999984741210938 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 3 ],
					"midpoints" : [ 627.5, 792.857057094573975, 510.714229702949524, 792.857057094573975, 510.714229702949524, 834.523719191551208, 467.02838134765625, 834.523719191551208 ],
					"source" : [ "obj-109", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 3 ],
					"midpoints" : [ 660.5, 801.648368239402771, 531.868157863616943, 801.648368239402771, 531.868157863616943, 811.53847861289978, 716.0, 811.53847861289978 ],
					"source" : [ "obj-109", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"midpoints" : [ 1133.1741943359375, 133.333335399627686, 1170.661376953125, 133.333335399627686 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"midpoints" : [ 1170.661376953125, 141.190482556819916, 1086.372627377510071, 141.190482556819916, 1086.372627377510071, -38.174599587917328, 1110.1583251953125, -38.174599587917328 ],
					"order" : 1,
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"order" : 0,
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"midpoints" : [ 854.60455322265625, 178.571431338787079, 893.5, 178.571431338787079 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 2 ],
					"source" : [ "obj-115", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 1 ],
					"source" : [ "obj-115", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"midpoints" : [ 1409.9871826171875, 238.832088589668274, 1170.661376953125, 238.832088589668274 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"midpoints" : [ 397.2044677734375, 102.755747318267822, 305.915718257427216, 102.755747318267822, 305.915718257427216, -76.609334826469421, 358.24993896484375, -76.609334826469421 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 1 ],
					"midpoints" : [ 397.2044677734375, 163.461543917655945, 358.24993896484375, 163.461543917655945 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"order" : 1,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 177.5, 930.555491089820862, 945.830527395009995, 930.555491089820862, 945.830527395009995, 854.018027424812317, 1001.5, 854.018027424812317 ],
					"order" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ -66.5, 326.166660130023956, 27.469135284423828, 326.166660130023956 ],
					"source" : [ "obj-16", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"order" : 1,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"midpoints" : [ 435.52838134765625, 921.666602730751038, 934.34537947177887, 921.666602730751038, 934.34537947177887, 846.188570499420166, 1232.148193359375, 846.188570499420166 ],
					"order" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"order" : 1,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"midpoints" : [ 684.5, 912.777714371681213, 924.213366657495499, 912.777714371681213, 924.213366657495499, 838.462472796440125, 1463.5, 838.462472796440125 ],
					"order" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"midpoints" : [ 439.804484049479186, 8.026806473731995, 397.2044677734375, 8.026806473731995 ],
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-22", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 41.5, 439.833322823047638, -111.666664004325867, 439.833322823047638, -111.666664004325867, 234.166661083698273, -95.166656494140625, 234.166661083698273 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"order" : 2,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 302.74993896484375, 155.0, 231.0, 155.0, 231.0, -33.0, 358.24993896484375, -33.0 ],
					"order" : 1,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"midpoints" : [ 302.74993896484375, 153.030289530754089, 413.704498291015625, 153.030289530754089 ],
					"order" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 1 ],
					"midpoints" : [ 358.24993896484375, 90.384618401527405, 398.076936364173889, 90.384618401527405, 398.076936364173889, 160.576928436756134, 358.24993896484375, 160.576928436756134 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 2 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 7 ],
					"midpoints" : [ 744.5, 778.999984741210938, 487.0, 778.999984741210938, 487.0, 818.999984741210938, 251.0, 818.999984741210938 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 7 ],
					"midpoints" : [ 777.5, 791.666581034660339, 510.714229702949524, 791.666581034660339, 510.714229702949524, 830.952291011810303, 509.02838134765625, 830.952291011810303 ],
					"source" : [ "obj-30", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 7 ],
					"midpoints" : [ 810.5, 802.74726939201355, 530.769256711006165, 802.74726939201355, 530.769256711006165, 812.637379765510559, 758.0, 812.637379765510559 ],
					"source" : [ "obj-30", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 6 ],
					"midpoints" : [ 744.5, 659.999984741210938, 487.0, 659.999984741210938, 487.0, 820.999984741210938, 240.5, 820.999984741210938 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 6 ],
					"midpoints" : [ 777.5, 670.238022923469543, 511.904705762863159, 670.238022923469543, 511.904705762863159, 833.333243131637573, 498.52838134765625, 833.333243131637573 ],
					"source" : [ "obj-31", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 6 ],
					"midpoints" : [ 810.5, 682.96704375743866, 530.769256711006165, 682.96704375743866, 530.769256711006165, 813.736280918121338, 747.5, 813.736280918121338 ],
					"source" : [ "obj-31", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 5 ],
					"midpoints" : [ 744.5, 539.190460801124573, 489.0, 539.190460801124573, 489.0, 818.999984741210938, 230.0, 818.999984741210938 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 5 ],
					"midpoints" : [ 777.5, 552.380892992019653, 510.714229702949524, 552.380892992019653, 510.714229702949524, 833.333243131637573, 488.02838134765625, 833.333243131637573 ],
					"source" : [ "obj-35", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 5 ],
					"midpoints" : [ 810.5, 563.18681812286377, 530.769256711006165, 563.18681812286377, 530.769256711006165, 811.53847861289978, 737.0, 811.53847861289978 ],
					"source" : [ "obj-35", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 4 ],
					"midpoints" : [ 744.5, 420.999984741210938, 490.0, 420.999984741210938, 490.0, 819.999984741210938, 219.5, 819.999984741210938 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 4 ],
					"midpoints" : [ 777.5, 431.551094949245453, 511.904705762863159, 431.551094949245453, 511.904705762863159, 832.142767071723938, 477.52838134765625, 832.142767071723938 ],
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 4 ],
					"midpoints" : [ 810.5, 442.2231605052948, 531.868157863616943, 442.2231605052948, 531.868157863616943, 812.637379765510559, 726.5, 812.637379765510559 ],
					"source" : [ "obj-36", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"midpoints" : [ 1213.261393229166742, 46.031746745109558, 1170.661376953125, 46.031746745109558 ],
					"source" : [ "obj-38", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 15 ],
					"midpoints" : [ 1044.5, 779.999984741210938, 490.0, 779.999984741210938, 490.0, 821.999984741210938, 335.0, 821.999984741210938 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 15 ],
					"midpoints" : [ 1077.5, 791.666581034660339, 510.714229702949524, 791.666581034660339, 510.714229702949524, 832.808627128601074, 593.02838134765625, 832.808627128601074 ],
					"source" : [ "obj-42", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 15 ],
					"midpoints" : [ 1110.5, 802.74726939201355, 530.769256711006165, 802.74726939201355, 530.769256711006165, 811.53847861289978, 842.0, 811.53847861289978 ],
					"source" : [ "obj-42", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 14 ],
					"midpoints" : [ 1044.5, 659.999984741210938, 490.0, 659.999984741210938, 490.0, 819.999984741210938, 324.5, 819.999984741210938 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 14 ],
					"midpoints" : [ 1077.5, 670.238022923469543, 510.714229702949524, 670.238022923469543, 510.714229702949524, 833.070935130119324, 582.52838134765625, 833.070935130119324 ],
					"source" : [ "obj-43", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 14 ],
					"midpoints" : [ 1110.5, 680.769241452217102, 530.769256711006165, 680.769241452217102, 530.769256711006165, 812.637379765510559, 831.5, 812.637379765510559 ],
					"source" : [ "obj-43", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 13 ],
					"midpoints" : [ 1044.5, 539.190460801124573, 489.0, 539.190460801124573, 489.0, 820.999984741210938, 314.0, 820.999984741210938 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 13 ],
					"midpoints" : [ 1077.5, 551.190416932106018, 510.714229702949524, 551.190416932106018, 510.714229702949524, 832.64720630645752, 572.02838134765625, 832.64720630645752 ],
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 13 ],
					"midpoints" : [ 1110.5, 562.087916970252991, 530.769256711006165, 562.087916970252991, 530.769256711006165, 811.53847861289978, 821.0, 811.53847861289978 ],
					"source" : [ "obj-44", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 12 ],
					"midpoints" : [ 1044.5, 419.999984741210938, 490.0, 419.999984741210938, 490.0, 821.999984741210938, 303.5, 821.999984741210938 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 12 ],
					"midpoints" : [ 1077.5, 430.952334880828857, 510.714229702949524, 430.952334880828857, 510.714229702949524, 832.156855225563049, 561.52838134765625, 832.156855225563049 ],
					"source" : [ "obj-45", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 12 ],
					"midpoints" : [ 1110.5, 441.208790183067322, 530.769256711006165, 441.208790183067322, 530.769256711006165, 811.53847861289978, 810.5, 811.53847861289978 ],
					"source" : [ "obj-45", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 11 ],
					"midpoints" : [ 894.5, 780.999984741210938, 487.0, 780.999984741210938, 487.0, 819.999984741210938, 293.0, 819.999984741210938 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 11 ],
					"midpoints" : [ 927.5, 791.666581034660339, 509.523753643035889, 791.666581034660339, 509.523753643035889, 833.333243131637573, 551.02838134765625, 833.333243131637573 ],
					"source" : [ "obj-50", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 11 ],
					"midpoints" : [ 960.5, 802.74726939201355, 530.769256711006165, 802.74726939201355, 530.769256711006165, 811.53847861289978, 800.0, 811.53847861289978 ],
					"source" : [ "obj-50", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 10 ],
					"midpoints" : [ 894.5, 660.408268749713898, 488.0, 660.408268749713898, 488.0, 821.408268749713898, 282.5, 821.408268749713898 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 10 ],
					"midpoints" : [ 927.5, 670.843827068805695, 511.904705762863159, 670.843827068805695, 511.904705762863159, 832.149811148643494, 540.52838134765625, 832.149811148643494 ],
					"source" : [ "obj-51", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 10 ],
					"midpoints" : [ 960.5, 681.868142604827881, 530.769256711006165, 681.868142604827881, 530.769256711006165, 812.637379765510559, 789.5, 812.637379765510559 ],
					"source" : [ "obj-51", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 9 ],
					"midpoints" : [ 894.5, 537.999984741210938, 488.0, 537.999984741210938, 488.0, 820.999984741210938, 272.0, 820.999984741210938 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 9 ],
					"midpoints" : [ 927.5, 552.380892992019653, 511.904705762863159, 552.380892992019653, 511.904705762863159, 831.376019835472107, 530.02838134765625, 831.376019835472107 ],
					"source" : [ "obj-52", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 9 ],
					"midpoints" : [ 960.5, 563.18681812286377, 530.769256711006165, 563.18681812286377, 530.769256711006165, 811.53847861289978, 779.0, 811.53847861289978 ],
					"source" : [ "obj-52", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 8 ],
					"midpoints" : [ 894.5, 419.999984741210938, 490.0, 419.999984741210938, 490.0, 820.999984741210938, 261.5, 820.999984741210938 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 8 ],
					"midpoints" : [ 927.5, 431.551094949245453, 511.904705762863159, 431.551094949245453, 511.904705762863159, 830.952291011810303, 519.52838134765625, 830.952291011810303 ],
					"source" : [ "obj-53", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 8 ],
					"midpoints" : [ 960.5, 441.124259352684021, 531.868157863616943, 441.124259352684021, 531.868157863616943, 812.637379765510559, 768.5, 812.637379765510559 ],
					"source" : [ "obj-53", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 19 ],
					"midpoints" : [ 1194.5, 779.761820435523987, 489.285660624504089, 779.761820435523987, 489.285660624504089, 821.428482532501221, 377.0, 821.428482532501221 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 19 ],
					"midpoints" : [ 1227.5, 791.758257865905762, 512.087937116622925, 791.758257865905762, 512.087937116622925, 831.318699359893799, 635.02838134765625, 831.318699359893799 ],
					"source" : [ "obj-58", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 19 ],
					"midpoints" : [ 1260.5, 802.74726939201355, 529.670355558395386, 802.74726939201355, 529.670355558395386, 812.637379765510559, 884.0, 812.637379765510559 ],
					"source" : [ "obj-58", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 18 ],
					"midpoints" : [ 1194.5, 659.523738384246826, 489.285660624504089, 659.523738384246826, 489.285660624504089, 820.238006472587585, 366.5, 820.238006472587585 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 18 ],
					"midpoints" : [ 1227.5, 670.879131078720093, 510.989035964012146, 670.879131078720093, 510.989035964012146, 831.910415351390839, 624.52838134765625, 831.910415351390839 ],
					"source" : [ "obj-59", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 18 ],
					"midpoints" : [ 1260.5, 681.868142604827881, 529.670355558395386, 681.868142604827881, 529.670355558395386, 812.637379765510559, 873.5, 812.637379765510559 ],
					"source" : [ "obj-59", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"midpoints" : [ 259.5, 270.432096719741821, 275.925947964191437, 270.432096719741821, 275.925947964191437, 238.333328723907471, 312.24993896484375, 238.333328723907471 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ 192.5, 273.518516719341278, 9.25925999879837, 273.518516719341278, 9.25925999879837, 307.469136714935303, 27.469135284423828, 307.469136714935303 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 17 ],
					"midpoints" : [ 1194.5, 540.476132392883301, 489.285660624504089, 540.476132392883301, 489.285660624504089, 820.238006472587585, 356.0, 820.238006472587585 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 17 ],
					"midpoints" : [ 1227.5, 552.70499175786972, 510.989035964012146, 552.70499175786972, 510.989035964012146, 833.178378164768219, 614.02838134765625, 833.178378164768219 ],
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 17 ],
					"midpoints" : [ 1260.5, 562.087916970252991, 530.769256711006165, 562.087916970252991, 530.769256711006165, 812.637379765510559, 863.0, 812.637379765510559 ],
					"source" : [ "obj-60", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 16 ],
					"midpoints" : [ 1194.5, 420.23805034160614, 489.285660624504089, 420.23805034160614, 489.285660624504089, 820.238006472587585, 345.5, 820.238006472587585 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 16 ],
					"midpoints" : [ 1227.5, 431.318679809570312, 510.989035964012146, 431.318679809570312, 510.989035964012146, 832.417600512504578, 603.52838134765625, 832.417600512504578 ],
					"source" : [ "obj-62", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 16 ],
					"midpoints" : [ 1260.5, 440.617074191570282, 529.670355558395386, 440.617074191570282, 529.670355558395386, 812.637379765510559, 852.5, 812.637379765510559 ],
					"source" : [ "obj-62", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 1 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 21 ],
					"midpoints" : [ 1344.5, 540.476132392883301, 490.476136684417725, 540.476132392883301, 490.476136684417725, 820.238006472587585, 398.0, 820.238006472587585 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 21 ],
					"midpoints" : [ 1377.5, 551.098905444145203, 509.890134811401367, 551.098905444145203, 509.890134811401367, 831.318699359893799, 656.02838134765625, 831.318699359893799 ],
					"source" : [ "obj-69", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 21 ],
					"midpoints" : [ 1410.5, 562.087916970252991, 529.670355558395386, 562.087916970252991, 529.670355558395386, 811.53847861289978, 905.0, 811.53847861289978 ],
					"source" : [ "obj-69", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 20 ],
					"midpoints" : [ 1344.5, 420.23805034160614, 489.285660624504089, 420.23805034160614, 489.285660624504089, 820.238006472587585, 387.5, 820.238006472587585 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 20 ],
					"midpoints" : [ 1377.5, 430.219778656959534, 509.890134811401367, 430.219778656959534, 509.890134811401367, 832.417600512504578, 645.52838134765625, 832.417600512504578 ],
					"source" : [ "obj-70", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 20 ],
					"midpoints" : [ 1410.5, 440.194419860839844, 530.769256711006165, 440.194419860839844, 530.769256711006165, 812.214725434780121, 894.5, 812.214725434780121 ],
					"source" : [ "obj-70", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ 107.10736083984375, 306.23456871509552, 27.469135284423828, 306.23456871509552 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"midpoints" : [ 150.0, 230.999984741210938, 192.5, 230.999984741210938 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 1 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ 82.5, 306.851852715015411, 27.469135284423828, 306.851852715015411 ],
					"source" : [ "obj-8", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 2 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"midpoints" : [ 302.74993896484375, 232.999984741210938, 192.5, 232.999984741210938 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"midpoints" : [ 1200.5, -1.0, 1359.5, -1.0 ],
					"order" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"midpoints" : [ 1200.5, 2.0, 1133.1741943359375, 2.0 ],
					"order" : 1,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"midpoints" : [ 1439.587198893229242, 50.0, 1396.9871826171875, 50.0 ],
					"source" : [ "obj-92", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 22 ],
					"source" : [ "obj-94", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 21 ],
					"source" : [ "obj-94", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 20 ],
					"source" : [ "obj-94", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 19 ],
					"source" : [ "obj-94", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 18 ],
					"source" : [ "obj-94", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 17 ],
					"source" : [ "obj-94", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 16 ],
					"source" : [ "obj-94", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 15 ],
					"source" : [ "obj-94", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 14 ],
					"source" : [ "obj-94", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 13 ],
					"source" : [ "obj-94", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 12 ],
					"source" : [ "obj-94", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 11 ],
					"source" : [ "obj-94", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 10 ],
					"source" : [ "obj-94", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 9 ],
					"source" : [ "obj-94", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 8 ],
					"source" : [ "obj-94", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 7 ],
					"source" : [ "obj-94", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 6 ],
					"source" : [ "obj-94", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 5 ],
					"source" : [ "obj-94", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 4 ],
					"source" : [ "obj-94", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 3 ],
					"source" : [ "obj-94", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 2 ],
					"source" : [ "obj-94", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 1 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 44 ],
					"source" : [ "obj-95", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 43 ],
					"source" : [ "obj-95", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 42 ],
					"source" : [ "obj-95", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 41 ],
					"source" : [ "obj-95", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 40 ],
					"source" : [ "obj-95", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 39 ],
					"source" : [ "obj-95", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 38 ],
					"source" : [ "obj-95", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 37 ],
					"source" : [ "obj-95", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 36 ],
					"source" : [ "obj-95", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 35 ],
					"source" : [ "obj-95", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 34 ],
					"source" : [ "obj-95", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 33 ],
					"source" : [ "obj-95", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 32 ],
					"source" : [ "obj-95", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 31 ],
					"source" : [ "obj-95", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 30 ],
					"source" : [ "obj-95", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 29 ],
					"source" : [ "obj-95", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 28 ],
					"source" : [ "obj-95", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 27 ],
					"source" : [ "obj-95", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 26 ],
					"source" : [ "obj-95", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 25 ],
					"source" : [ "obj-95", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 24 ],
					"source" : [ "obj-95", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 23 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 66 ],
					"source" : [ "obj-96", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 65 ],
					"source" : [ "obj-96", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 64 ],
					"source" : [ "obj-96", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 63 ],
					"source" : [ "obj-96", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 62 ],
					"source" : [ "obj-96", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 61 ],
					"source" : [ "obj-96", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 60 ],
					"source" : [ "obj-96", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 59 ],
					"source" : [ "obj-96", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 58 ],
					"source" : [ "obj-96", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 57 ],
					"source" : [ "obj-96", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 56 ],
					"source" : [ "obj-96", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 55 ],
					"source" : [ "obj-96", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 54 ],
					"source" : [ "obj-96", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 53 ],
					"source" : [ "obj-96", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 52 ],
					"source" : [ "obj-96", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 51 ],
					"source" : [ "obj-96", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 50 ],
					"source" : [ "obj-96", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 49 ],
					"source" : [ "obj-96", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 48 ],
					"source" : [ "obj-96", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 47 ],
					"source" : [ "obj-96", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 46 ],
					"source" : [ "obj-96", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 45 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 1 ],
					"source" : [ "obj-97", 0 ]
				}

			}
 ],
		"styles" : [ 			{
				"name" : "buttonGreen-1",
				"default" : 				{
					"bgcolor" : [ 0.043137, 0.364706, 0.094118, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "dUG Yello 01-1",
				"message" : 				{
					"fontface" : [ 1 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"fontsize" : [ 10.0 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.94902, 0.992157, 1.0, 1.0 ],
						"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"default" : 				{
					"fontface" : [ 1 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ],
					"fontsize" : [ 10.0 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.94902, 0.992157, 1.0, 1.0 ],
						"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"newobj" : 				{
					"fontface" : [ 1 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ],
					"fontsize" : [ 10.0 ]
				}
,
				"button" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ]
				}
,
				"toggle" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"fontsize" : [ 12.059008 ],
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rsliderGold",
				"default" : 				{
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
					"color" : [ 0.646639, 0.821777, 0.854593, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
