{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 0,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 522.1363525390625, 496.0, 30.0, 22.0 ],
					"text" : "*~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 478.59307861328125, 496.0, 30.0, 22.0 ],
					"text" : "*~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1199.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1147.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1095.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1043.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 991.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 939.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 887.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 835.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 783.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 731.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 679.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 627.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 575.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 523.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 471.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 419.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 367.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 315.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 263.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 211.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 159.272705078125, 373.795166015625, 44.640777587890625, 49.0 ],
					"presentation_linecount" : 3,
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 107.272697448730469, 373.795166015625, 44.640777587890625, 49.0 ],
					"text" : "phys_stereo_ch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1054.25, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1032.4945068359375, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1010.7392578125, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 988.98382568359375, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 967.2283935546875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 945.47296142578125, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 923.717529296875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 901.96209716796875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 880.20672607421875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 858.4512939453125, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 836.69586181640625, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 814.9404296875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 793.18505859375, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 771.42962646484375, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 749.67425537109375, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 727.9188232421875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 706.1634521484375, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 684.40802001953125, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 662.652587890625, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 640.89715576171875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 619.14178466796875, 140.909072875976562, 18.0, 102.0 ],
					"presentation_linecount" : 7,
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 534.1363525390625, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 512.3809814453125, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 490.625518798828125, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 468.8701171875, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 447.114715576171875, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 425.359283447265625, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 403.603912353515625, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 381.8485107421875, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 360.09307861328125, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 338.337677001953125, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 316.582275390625, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 294.82684326171875, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 273.071441650390625, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 251.315994262695312, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 229.560592651367188, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 207.80517578125, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 186.049758911132812, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 164.294342041015625, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 142.5389404296875, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 120.783523559570312, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 99.028106689453125, 140.909072875976562, 18.0, 116.0 ],
					"presentation_linecount" : 8,
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"linecount" : 8,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 77.272697448730469, 140.909072875976562, 18.0, 116.0 ],
					"text" : "phys_pan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"linecount" : 7,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 597.3863525390625, 140.909072875976562, 18.0, 102.0 ],
					"text" : "phys_am"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 22,
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 597.3863525390625, 99.999992370605469, 475.8636474609375, 22.0 ],
					"text" : "mc.unpack~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 22,
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 77.272697448730469, 99.999992370605469, 475.8636474609375, 22.0 ],
					"text" : "mc.unpack~ 22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 488.772705078125, 690.18182373046875, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 460.272705078125, 534.5909423828125, 102.0, 132.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_type" : 0,
							"parameter_unitstyle" : 4,
							"parameter_mmin" : -70.0,
							"parameter_longname" : "live.gain~",
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_shortname" : "live.gain~"
						}

					}
,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 3,
					"outlettype" : [ "multichannelsignal", "multichannelsignal", "multichannelsignal" ],
					"patching_rect" : [ 570.3863525390625, 46.409088134765625, 73.0, 22.0 ],
					"text" : "phys_model"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"midpoints" : [ 579.8863525390625, 82.524270713329315, 86.772697448730469, 82.524270713329315 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 1 ],
					"midpoints" : [ 998.48382568359375, 269.0, 1061.319630940755133, 269.0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 1 ],
					"midpoints" : [ 976.7283935546875, 271.0, 1009.319630940755246, 271.0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 1 ],
					"midpoints" : [ 954.97296142578125, 273.0, 957.319630940755246, 273.0 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 116.772697448730469, 440.80581921339035, 488.09307861328125, 440.80581921339035 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 142.413475036621094, 433.009702801704407, 531.6363525390625, 433.009702801704407 ],
					"source" : [ "obj-106", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 168.772705078125, 441.0, 488.09307861328125, 441.0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 194.413482666015625, 432.0, 531.6363525390625, 432.0 ],
					"source" : [ "obj-107", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 220.772705078125, 442.0, 488.09307861328125, 442.0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 246.413482666015625, 432.0, 531.6363525390625, 432.0 ],
					"source" : [ "obj-108", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 272.772705078125, 442.0, 488.09307861328125, 442.0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 298.413482666015625, 432.0, 531.6363525390625, 432.0 ],
					"source" : [ "obj-109", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 324.772705078125, 441.0, 488.09307861328125, 441.0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 350.413482666015625, 432.0, 531.6363525390625, 432.0 ],
					"source" : [ "obj-110", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 376.772705078125, 443.0, 488.09307861328125, 443.0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 402.413482666015625, 430.0, 531.6363525390625, 430.0 ],
					"source" : [ "obj-111", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 428.772705078125, 441.0, 488.09307861328125, 441.0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 454.413482666015625, 430.0, 531.6363525390625, 430.0 ],
					"source" : [ "obj-112", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 480.772705078125, 444.0, 488.09307861328125, 444.0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 506.413482666015625, 431.0, 531.6363525390625, 431.0 ],
					"source" : [ "obj-113", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 532.772705078125, 446.0, 488.09307861328125, 446.0 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 558.413482666015625, 432.0, 531.6363525390625, 432.0 ],
					"source" : [ "obj-114", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 584.772705078125, 448.0, 488.09307861328125, 448.0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 610.413482666015625, 434.0, 531.6363525390625, 434.0 ],
					"source" : [ "obj-115", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 636.772705078125, 448.0, 488.09307861328125, 448.0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 662.413482666015625, 432.0, 531.6363525390625, 432.0 ],
					"source" : [ "obj-116", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 688.772705078125, 447.0, 488.09307861328125, 447.0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 714.413482666015625, 434.0, 531.6363525390625, 434.0 ],
					"source" : [ "obj-117", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 740.772705078125, 447.0, 488.09307861328125, 447.0 ],
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 766.413482666015625, 432.0, 531.6363525390625, 432.0 ],
					"source" : [ "obj-118", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 792.772705078125, 447.0, 488.09307861328125, 447.0 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 818.413482666015625, 433.0, 531.6363525390625, 433.0 ],
					"source" : [ "obj-120", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 844.772705078125, 450.0, 488.09307861328125, 450.0 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 870.413482666015625, 431.0, 531.6363525390625, 431.0 ],
					"source" : [ "obj-121", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 896.772705078125, 451.0, 488.09307861328125, 451.0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 922.413482666015625, 433.0, 531.6363525390625, 433.0 ],
					"source" : [ "obj-122", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 948.772705078125, 450.0, 488.09307861328125, 450.0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 974.413482666015625, 433.0, 531.6363525390625, 433.0 ],
					"source" : [ "obj-123", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 1000.772705078125, 448.0, 488.09307861328125, 448.0 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 1026.413482666015625, 434.0, 531.6363525390625, 434.0 ],
					"source" : [ "obj-124", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 1052.772705078125, 451.0, 488.09307861328125, 451.0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 1078.413482666015625, 433.0, 531.6363525390625, 433.0 ],
					"source" : [ "obj-125", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 1104.772705078125, 450.0, 488.09307861328125, 450.0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 1130.413482666015625, 434.0, 531.6363525390625, 434.0 ],
					"source" : [ "obj-126", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 1208.772705078125, 450.0, 488.09307861328125, 450.0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 1234.413482666015625, 433.0, 531.6363525390625, 433.0 ],
					"source" : [ "obj-127", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 1156.772705078125, 450.0, 488.09307861328125, 450.0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 1182.413482666015625, 433.0, 531.6363525390625, 433.0 ],
					"source" : [ "obj-128", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"source" : [ "obj-13", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-40", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-40", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-40", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-40", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-40", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"source" : [ "obj-40", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-40", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"source" : [ "obj-40", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-40", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-40", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"source" : [ "obj-40", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"source" : [ "obj-40", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-40", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-40", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"source" : [ "obj-40", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-40", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"source" : [ "obj-40", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-40", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-40", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-40", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"source" : [ "obj-40", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-50", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-50", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"source" : [ "obj-50", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-50", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-50", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"source" : [ "obj-50", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-50", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-50", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"source" : [ "obj-50", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-50", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-50", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"source" : [ "obj-50", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"source" : [ "obj-50", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"source" : [ "obj-50", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-50", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-50", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-50", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-50", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-50", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-50", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"source" : [ "obj-50", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 1 ],
					"midpoints" : [ 606.8863525390625, 279.541527509689331, 125.319623311360687, 279.541527509689331 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 3 ],
					"midpoints" : [ 88.272697448730469, 319.667283475399017, 142.413475036621094, 319.667283475399017 ],
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 2 ],
					"midpoints" : [ 84.272697448730469, 302.550035059452057, 133.866549173990876, 302.550035059452057 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 3 ],
					"midpoints" : [ 110.028106689453125, 319.948857605457306, 194.413482666015625, 319.948857605457306 ],
					"source" : [ "obj-61", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 2 ],
					"midpoints" : [ 106.028106689453125, 302.161537051200867, 185.866556803385407, 302.161537051200867 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 3 ],
					"midpoints" : [ 153.5389404296875, 320.0, 298.413482666015625, 320.0 ],
					"source" : [ "obj-62", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 2 ],
					"midpoints" : [ 149.5389404296875, 303.424635469913483, 289.866556803385436, 303.424635469913483 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 3 ],
					"midpoints" : [ 131.783523559570312, 319.0, 246.413482666015625, 319.0 ],
					"source" : [ "obj-63", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 2 ],
					"midpoints" : [ 127.783523559570312, 302.054772555828094, 237.866556803385407, 302.054772555828094 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 3 ],
					"midpoints" : [ 240.560592651367188, 321.0, 506.413482666015625, 321.0 ],
					"source" : [ "obj-64", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 2 ],
					"midpoints" : [ 236.560592651367188, 304.109566926956177, 497.866556803385436, 304.109566926956177 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 3 ],
					"midpoints" : [ 218.80517578125, 322.0, 454.413482666015625, 322.0 ],
					"source" : [ "obj-65", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 2 ],
					"midpoints" : [ 214.80517578125, 303.424635469913483, 445.866556803385436, 303.424635469913483 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 3 ],
					"midpoints" : [ 197.049758911132812, 321.0, 402.413482666015625, 321.0 ],
					"source" : [ "obj-66", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 2 ],
					"midpoints" : [ 193.049758911132812, 302.739704012870789, 393.866556803385436, 302.739704012870789 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 3 ],
					"midpoints" : [ 175.294342041015625, 321.0, 350.413482666015625, 321.0 ],
					"source" : [ "obj-67", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 2 ],
					"midpoints" : [ 171.294342041015625, 303.424635469913483, 341.866556803385436, 303.424635469913483 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 3 ],
					"midpoints" : [ 414.603912353515625, 322.0, 922.413482666015625, 322.0 ],
					"source" : [ "obj-68", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 2 ],
					"midpoints" : [ 410.603912353515625, 303.0, 913.866556803385492, 303.0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 3 ],
					"midpoints" : [ 392.8485107421875, 323.0, 870.413482666015625, 323.0 ],
					"source" : [ "obj-69", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 2 ],
					"midpoints" : [ 388.8485107421875, 304.0, 861.866556803385492, 304.0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 3 ],
					"midpoints" : [ 371.09307861328125, 322.0, 818.413482666015625, 322.0 ],
					"source" : [ "obj-70", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 2 ],
					"midpoints" : [ 367.09307861328125, 303.0, 809.866556803385492, 303.0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 3 ],
					"midpoints" : [ 349.337677001953125, 322.0, 766.413482666015625, 322.0 ],
					"source" : [ "obj-71", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 2 ],
					"midpoints" : [ 345.337677001953125, 303.0, 757.866556803385492, 303.0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 3 ],
					"midpoints" : [ 327.582275390625, 321.0, 714.413482666015625, 321.0 ],
					"source" : [ "obj-72", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 2 ],
					"midpoints" : [ 323.582275390625, 304.0, 705.866556803385492, 304.0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 3 ],
					"midpoints" : [ 305.82684326171875, 323.0, 662.413482666015625, 323.0 ],
					"source" : [ "obj-73", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 2 ],
					"midpoints" : [ 301.82684326171875, 302.739704012870789, 653.866556803385492, 302.739704012870789 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 3 ],
					"midpoints" : [ 284.071441650390625, 321.0, 610.413482666015625, 321.0 ],
					"source" : [ "obj-74", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 2 ],
					"midpoints" : [ 280.071441650390625, 302.739704012870789, 601.866556803385492, 302.739704012870789 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 3 ],
					"midpoints" : [ 262.315994262695312, 323.0, 558.413482666015625, 323.0 ],
					"source" : [ "obj-75", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 2 ],
					"midpoints" : [ 258.315994262695312, 303.424635469913483, 549.866556803385492, 303.424635469913483 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 3 ],
					"midpoints" : [ 545.1363525390625, 323.0, 1234.413482666015625, 323.0 ],
					"source" : [ "obj-76", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 2 ],
					"midpoints" : [ 541.1363525390625, 304.0, 1225.866556803385265, 304.0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 3 ],
					"midpoints" : [ 523.3809814453125, 322.0, 1182.413482666015625, 322.0 ],
					"source" : [ "obj-77", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 2 ],
					"midpoints" : [ 519.3809814453125, 303.0, 1173.866556803385265, 303.0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 3 ],
					"midpoints" : [ 501.625518798828125, 324.0, 1130.413482666015625, 324.0 ],
					"source" : [ "obj-78", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 2 ],
					"midpoints" : [ 497.625518798828125, 305.0, 1121.866556803385265, 305.0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 3 ],
					"midpoints" : [ 479.8701171875, 322.0, 1078.413482666015625, 322.0 ],
					"source" : [ "obj-79", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 2 ],
					"midpoints" : [ 475.8701171875, 304.0, 1069.866556803385265, 304.0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 3 ],
					"midpoints" : [ 458.114715576171875, 322.0, 1026.413482666015625, 322.0 ],
					"source" : [ "obj-80", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 2 ],
					"midpoints" : [ 454.114715576171875, 302.0, 1017.866556803385492, 302.0 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 3 ],
					"midpoints" : [ 436.359283447265625, 322.0, 974.413482666015625, 322.0 ],
					"source" : [ "obj-81", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 2 ],
					"midpoints" : [ 432.359283447265625, 304.0, 965.866556803385492, 304.0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 1 ],
					"midpoints" : [ 628.64178466796875, 280.651289224624634, 177.319630940755189, 280.651289224624634 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 1 ],
					"midpoints" : [ 650.39715576171875, 279.0, 229.319630940755189, 279.0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 1 ],
					"midpoints" : [ 672.152587890625, 280.0, 281.319630940755189, 280.0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 1 ],
					"midpoints" : [ 759.17425537109375, 278.0, 489.319630940755189, 278.0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 1 ],
					"midpoints" : [ 737.4188232421875, 278.0, 437.319630940755189, 278.0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 1 ],
					"midpoints" : [ 715.6634521484375, 279.0, 385.319630940755189, 279.0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 1 ],
					"midpoints" : [ 693.90802001953125, 279.0, 333.319630940755189, 279.0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 1 ],
					"midpoints" : [ 933.217529296875, 275.0, 905.319630940755246, 275.0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 1 ],
					"midpoints" : [ 911.46209716796875, 274.0, 853.319630940755246, 274.0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 1 ],
					"midpoints" : [ 889.70672607421875, 276.0, 801.319630940755246, 276.0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 1 ],
					"midpoints" : [ 867.9512939453125, 276.0, 749.319630940755246, 276.0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 1 ],
					"midpoints" : [ 846.19586181640625, 278.0, 697.319630940755246, 278.0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 1 ],
					"midpoints" : [ 824.4404296875, 278.0, 645.319630940755246, 278.0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 1 ],
					"midpoints" : [ 802.68505859375, 278.0, 593.319630940755246, 278.0 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 1 ],
					"midpoints" : [ 780.92962646484375, 279.0, 541.319630940755246, 279.0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 1 ],
					"midpoints" : [ 1063.75, 270.0, 1217.319630940755133, 270.0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 1 ],
					"midpoints" : [ 1041.9945068359375, 270.0, 1165.319630940755133, 270.0 ],
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 1 ],
					"midpoints" : [ 1020.2392578125, 270.0, 1113.319630940755133, 270.0 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-13" : [ "live.gain~", "live.gain~", 0 ],
			"parameterbanks" : 			{

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "phys_model.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/phys_modeling",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "phys_atom.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/phys_modeling",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "random_impuls.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/phys_modeling",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "get_atom_pos.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/phys_modeling",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "phys_am.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/phys_modeling",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "phys_pan.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/phys_modeling",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "phys_stereo_ch.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/phys_modeling",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
