{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 79.0, 1212.0, 687.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 228.0, 369.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 91.0,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "/Users/cella/Documents/Music/Boards of Canada - Hi Scores EP/04 - june 9th.mp3",
								"filename" : "04 - june 9th.mp3",
								"filekind" : "audiofile",
								"selection" : [ 0.375, 0.509615384615385 ],
								"loop" : 1,
								"content_state" : 								{
									"slurtime" : [ 0.0 ],
									"originaltempo" : [ 120.0 ],
									"basictuning" : [ 440 ],
									"pitchcorrection" : [ 0 ],
									"followglobaltempo" : [ 0 ],
									"quality" : [ "basic" ],
									"pitchshift" : [ 1.0 ],
									"originallengthms" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"play" : [ 0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"formant" : [ 1.0 ],
									"mode" : [ "basic" ]
								}

							}
 ]
					}
,
					"id" : "obj-2",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 228.0, 83.0, 150.0, 92.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "clouds.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 228.0, 191.0, 210.0, 159.0 ],
					"varname" : "clouds",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 1 ],
					"midpoints" : [ 237.5, 361.0, 263.5, 361.0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-1::obj-43::obj-74" : [ "live.numbox[10]", "live.numbox", 0 ],
			"obj-1::obj-3" : [ "pitch[1]", "pitch", 0 ],
			"obj-1::obj-74::obj-12" : [ "ratecontrol[2]", "ratecontrol", 0 ],
			"obj-1::obj-25::obj-12" : [ "ratecontrol[4]", "ratecontrol", 0 ],
			"obj-1::obj-74::obj-74" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-1::obj-2::obj-142" : [ "Channel[1]", "Channel", 0 ],
			"obj-1::obj-25::obj-9" : [ "live.text[5]", "live.text", 0 ],
			"obj-1::obj-29::obj-9" : [ "live.text[6]", "live.text", 0 ],
			"obj-1::obj-2::obj-9" : [ "live.text[1]", "live.text", 0 ],
			"obj-1::obj-42::obj-74" : [ "live.numbox[6]", "live.numbox", 0 ],
			"obj-1::obj-36::obj-142" : [ "Channel[9]", "Channel", 0 ],
			"obj-1::obj-52::obj-9" : [ "live.text[12]", "live.text", 0 ],
			"obj-1::obj-34" : [ "freeze[1]", "freeze", 0 ],
			"obj-1::obj-112::obj-74" : [ "live.numbox[76]", "live.numbox", 0 ],
			"obj-1::obj-42::obj-12" : [ "ratecontrol[6]", "ratecontrol", 0 ],
			"obj-1::obj-43::obj-12" : [ "ratecontrol[7]", "ratecontrol", 0 ],
			"obj-1::obj-112::obj-142" : [ "Channel[76]", "Channel", 0 ],
			"obj-1::obj-93::obj-9" : [ "live.text[9]", "live.text", 0 ],
			"obj-1::obj-7::obj-74" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-1::obj-43::obj-142" : [ "Channel[10]", "Channel", 0 ],
			"obj-1::obj-74::obj-142" : [ "Channel[2]", "Channel", 0 ],
			"obj-1::obj-42::obj-142" : [ "Channel[6]", "Channel", 0 ],
			"obj-1::obj-7::obj-142" : [ "Channel[3]", "Channel", 0 ],
			"obj-1::obj-93::obj-12" : [ "ratecontrol[8]", "ratecontrol", 0 ],
			"obj-1::obj-52::obj-74" : [ "live.numbox[7]", "live.numbox", 0 ],
			"obj-1::obj-29::obj-142" : [ "Channel[5]", "Channel", 0 ],
			"obj-1::obj-33::obj-97" : [ "Quantized Rate[81]", "Quantized Rate", 0 ],
			"obj-1::obj-52::obj-12" : [ "ratecontrol[10]", "ratecontrol", 0 ],
			"obj-1::obj-112::obj-12" : [ "ratecontrol[167]", "ratecontrol", 0 ],
			"obj-1::obj-112::obj-9" : [ "live.text[50]", "live.text", 0 ],
			"obj-1::obj-2::obj-12" : [ "ratecontrol[1]", "ratecontrol", 0 ],
			"obj-1::obj-2::obj-74" : [ "live.numbox[1]", "live.numbox", 0 ],
			"obj-1::obj-25::obj-142" : [ "Channel[4]", "Channel", 0 ],
			"obj-1::obj-42::obj-9" : [ "live.text[7]", "live.text", 0 ],
			"obj-1::obj-93::obj-142" : [ "Channel[8]", "Channel", 0 ],
			"obj-1::obj-36::obj-9" : [ "live.text[10]", "live.text", 0 ],
			"obj-1::obj-29::obj-74" : [ "live.numbox[5]", "live.numbox", 0 ],
			"obj-1::obj-25::obj-74" : [ "live.numbox[4]", "live.numbox", 0 ],
			"obj-1::obj-74::obj-9" : [ "live.text[2]", "live.text", 0 ],
			"obj-1::obj-7::obj-9" : [ "live.text[4]", "live.text", 0 ],
			"obj-1::obj-36::obj-74" : [ "live.numbox[9]", "live.numbox", 0 ],
			"obj-1::obj-7::obj-12" : [ "ratecontrol[3]", "ratecontrol", 0 ],
			"obj-1::obj-29::obj-12" : [ "ratecontrol[5]", "ratecontrol", 0 ],
			"obj-1::obj-93::obj-74" : [ "live.numbox[8]", "live.numbox", 0 ],
			"obj-1::obj-52::obj-142" : [ "Channel[7]", "Channel", 0 ],
			"obj-1::obj-33::obj-9" : [ "Manual Rate[69]", "Manual Rate", 0 ],
			"obj-1::obj-36::obj-12" : [ "ratecontrol[9]", "ratecontrol", 0 ],
			"obj-1::obj-43::obj-9" : [ "live.text[11]", "live.text", 0 ],
			"parameterbanks" : 			{

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "clouds.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/clouds",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"patcherrelativepath" : "../../audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"patcherrelativepath" : "../../audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_receive_mains_out.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/EG/delta",
				"patcherrelativepath" : "../EG/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bpatcher_name.js",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"patcherrelativepath" : "../../audio",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "clouds_midi_logic.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019/clouds",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "clouds_MIDI_interp",
				"bootpath" : "~/12c/12c_sandbox/js",
				"patcherrelativepath" : "../../js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "metro_time_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/utils",
				"patcherrelativepath" : "../utils",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_AM.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/mixer",
				"patcherrelativepath" : "../mixer",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "04 - june 9th.mp3",
				"bootpath" : "~/Documents/Music/Boards of Canada - Hi Scores EP",
				"patcherrelativepath" : "../../../../Documents/Music/Boards of Canada - Hi Scores EP",
				"type" : "Mp3",
				"implicit" : 1
			}
, 			{
				"name" : "mutatedtexturizer~.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
