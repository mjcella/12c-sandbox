{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 79.0, 1212.0, 687.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-36",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 20.891326904296875, 16.0, 13.0 ],
					"text" : "B",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-33",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 31.100738525390625, 16.0, 13.0 ],
					"text" : "A#",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-32",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 43.100738525390625, 16.0, 13.0 ],
					"text" : "A",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-31",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 56.100738525390625, 18.25, 13.0 ],
					"text" : "G#",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-30",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 68.90203857421875, 16.0, 13.0 ],
					"text" : "G",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-29",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 81.90203857421875, 16.0, 13.0 ],
					"text" : "F#",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-28",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 94.224822998046875, 16.0, 13.0 ],
					"text" : "F",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-27",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 107.224822998046875, 16.0, 13.0 ],
					"text" : "E",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-25",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 119.224822998046875, 16.0, 13.0 ],
					"text" : "D#",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-24",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 132.224822998046875, 16.0, 13.0 ],
					"text" : "D",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-11",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 144.4605712890625, 16.0, 13.0 ],
					"text" : "C#",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-10",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 169.272308349609375, 368.6590576171875, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.25, 154.91510009765625, 15.5, 13.0 ],
					"text" : "C",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.643137254901961, 0.643137254901961, 0.643137254901961, 0.73 ],
					"contdata" : 1,
					"id" : "obj-214",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 145.772308349609375, 353.172332763671875, 15.333328247070312, 52.000164031982422 ],
					"presentation" : 1,
					"presentation_rect" : [ 1.5, 24.095897674560547, 12.331811904907227, 139.705780029296875 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "scale[2]",
							"parameter_initial_enable" : 1,
							"parameter_invisible" : 1,
							"parameter_mmax" : 11.0,
							"parameter_initial" : [ 0 ],
							"parameter_shortname" : "scale",
							"parameter_type" : 3
						}

					}
,
					"setminmax" : [ 0.0, 11.0 ],
					"setstyle" : 1,
					"settype" : 0,
					"slidercolor" : [ 0.0, 0.035294117647059, 0.152941176470588, 1.0 ],
					"varname" : "scale"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@scripting_name", "scale_visualization", "@display_name", "scale", "@int_mode", 1 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-226",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_multislider_markov_ui_2019.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 226.151382446289062, 448.523223876953125, 107.744186401367188, 25.837209701538086 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.75, 139.169281005859375, 106.53936767578125, 24.632390975952148 ],
					"varname" : "_multislider_markov_ui_2019",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 325.87353515625, 110.01641845703125, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 190.11407470703125, 201.01641845703125, 62.0, 22.0 ],
					"text" : "switch 2 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 276.54022216796875, 110.01641845703125, 42.0, 35.0 ],
					"text" : "snapshot~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 325.87353515625, 63.509918212890625, 29.5, 22.0 ],
					"text" : "> 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 276.54022216796875, 63.509918212890625, 42.0, 35.0 ],
					"text" : "round~ 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ -12.0, 12.0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-82",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "delta_select_ui.maxpat",
					"numinlets" : 3,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int", "", "" ],
					"patching_rect" : [ 276.54022216796875, 19.0, 167.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 103.32965087890625, 0.31329345703125, 104.5, 26.0 ],
					"varname" : "Pitchmod",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.16168212890625, 24.095897674560547, 13.0, 13.0 ],
					"text" : "0",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.16168212890625, 36.095897674560547, 13.0, 13.0 ],
					"text" : "1",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.16168212890625, 47.095897674560547, 13.0, 13.0 ],
					"text" : "2",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.16168212890625, 59.095897674560547, 13.0, 13.0 ],
					"text" : "3",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-19",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.145294189453125, 69.902053833007812, 13.0, 13.0 ],
					"text" : "4",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-18",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 180.447189331054688, 150.62847900390625, 18.504587173461914, 13.0 ],
					"text" : "11",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-17",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 180.447189331054688, 139.169281005859375, 18.504587173461914, 13.0 ],
					"text" : "10",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.145294189453125, 128.224822998046875, 13.0, 13.0 ],
					"text" : "9",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-15",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.145294189453125, 116.224822998046875, 13.0, 13.0 ],
					"text" : "8",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-14",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.145294189453125, 93.34649658203125, 13.0, 13.0 ],
					"text" : "6",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-13",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.145294189453125, 81.34649658203125, 13.0, 13.0 ],
					"text" : "5",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 6.0,
					"id" : "obj-12",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 187.45147705078125, 424.690673828125, 13.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.145294189453125, 105.12646484375, 13.0, 13.0 ],
					"text" : "7",
					"textcolor" : [ 0.996078431606293, 0.996078431606293, 0.996078431606293, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.0, 0.037238, 0.153355, 1.0 ],
					"contdata" : 1,
					"id" : "obj-246",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 142.10565185546875, 452.858856201171875, 69.599998474121094, 17.16595458984375 ],
					"presentation" : 1,
					"presentation_rect" : [ 172.35284423828125, 24.095897674560547, 32.693264007568359, 139.532577514648438 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 1,
					"settype" : 0,
					"size" : 12,
					"slidercolor" : [ 0.960784, 0.827451, 0.156863, 1.0 ],
					"spacing" : 1,
					"varname" : "scale_visualization"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 190.11407470703125, 237.454620361328125, 105.0, 22.0 ],
					"text" : "s global_pitchmod"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 217.154800415039062, 393.493499755859375, 153.0, 35.0 ],
					"text" : "loadmess 1 1 1 1 1 1 1 1 1 1 1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 17.0, 19.0, 56.0, 22.0 ],
					"restore" : 					{
						"scale" : [ 0 ],
						"scale_visualization" : [ 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0 ]
					}
,
					"text" : "autopattr",
					"varname" : "u886000819"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-26",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_scale_control_taller.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 120.930557250976562, 19.0, 109.683509826660156, 65.523330688476562 ],
					"presentation" : 1,
					"presentation_rect" : [ -0.226776123046875, 0.31329345703125, 172.493316650390625, 163.65972900390625 ],
					"varname" : "MIDI1_scale_control",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 45.556316375732422, 360.451141357421875, 73.157890319824219, 49.0 ],
					"text" : "loadmess 1 0 1 1 0 1 0 1 1 0 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 63.782989501953125, 484.9652099609375, 75.0, 22.0 ],
					"text" : "s global_key"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 142.10565185546875, 484.9652099609375, 85.0, 22.0 ],
					"text" : "s global_scale"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 142.10565185546875, 415.690673828125, 29.5, 22.0 ],
					"text" : "t b l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 187.45147705078125, 310.324951171875, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 162.10565185546875, 445.760670781135559, 73.282989501953125, 445.760670781135559 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"midpoints" : [ 235.651382446289062, 480.376560777425766, 222.59413754940033, 480.376560777425766, 222.59413754940033, 443.974887907505035, 151.60565185546875, 443.974887907505035 ],
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"order" : 0,
					"source" : [ "obj-246", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"midpoints" : [ 151.60565185546875, 476.396396219730377, 219.219219088554382, 476.396396219730377, 219.219219088554382, 442.224055588245392, 235.651382446289062, 442.224055588245392 ],
					"order" : 1,
					"source" : [ "obj-246", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 1 ],
					"source" : [ "obj-26", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 0 ],
					"midpoints" : [ 196.95147705078125, 341.227576822042465, 155.105636596679688, 341.227576822042465 ],
					"source" : [ "obj-265", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"midpoints" : [ 335.37353515625, 157.0, 199.61407470703125, 157.0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 1 ],
					"midpoints" : [ 226.654800415039062, 436.443507313728333, 324.39556884765625, 436.443507313728333 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"midpoints" : [ 55.056316375732422, 445.263167381286621, 151.60565185546875, 445.263167381286621 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-82", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 2 ],
					"midpoints" : [ 286.04022216796875, 174.0, 242.61407470703125, 174.0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-214" : [ "scale[2]", "scale", 0 ],
			"obj-26::obj-186::obj-226::obj-67" : [ "min_max_offset_ui[6]", "min_max_offset_ui", 0 ],
			"obj-26::obj-186::obj-82::obj-142" : [ "Channel[123]", "Channel", 0 ],
			"obj-226::obj-112::obj-9" : [ "Hard Sync Toggle[1]", "Hard Sync Toggle", 0 ],
			"obj-82::obj-12" : [ "ratecontrol[1]", "ratecontrol", 0 ],
			"obj-26::obj-186::obj-226::obj-112::obj-9" : [ "Hard Sync Toggle", "Hard Sync Toggle", 0 ],
			"obj-226::obj-32" : [ "off[1]", "off", 0 ],
			"obj-26::obj-186::obj-226::obj-112::obj-108" : [ "Hard Sync Threshold[39]", "Hard Sync Threshold", 0 ],
			"obj-82::obj-72" : [ "Jam Mode[1]", "Jam Mode", 0 ],
			"obj-226::obj-68" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-26::obj-186::obj-12" : [ "Slider Qty[10]", "Slider Qty", 0 ],
			"obj-26::obj-186::obj-28" : [ "octave_select[10]", "octave_select", 0 ],
			"obj-26::obj-186::obj-226::obj-55" : [ "live.text[6]", "live.text[1]", 0 ],
			"obj-26::obj-186::obj-82::obj-9" : [ "live.text[238]", "live.text", 0 ],
			"obj-26::obj-186::obj-82::obj-72" : [ "Jam Mode[72]", "Jam Mode", 0 ],
			"obj-26::obj-186::obj-97" : [ "Quantized Rate[73]", "Quantized Rate", 0 ],
			"obj-26::obj-186::obj-226::obj-68" : [ "live.numbox[10]", "live.numbox", 0 ],
			"obj-26::obj-186::obj-226::obj-29::obj-9" : [ "Manual Rate[2]", "Manual Rate", 0 ],
			"obj-26::obj-186::obj-226::obj-29::obj-97" : [ "Quantized Rate[2]", "Quantized Rate", 0 ],
			"obj-26::obj-186::obj-17" : [ "Slider Values[2]", "Slider Values", 0 ],
			"obj-226::obj-112::obj-107" : [ "Delta Channel #[1]", "Delta Channel #", 0 ],
			"obj-82::obj-142" : [ "Channel[1]", "Channel", 0 ],
			"obj-226::obj-29::obj-9" : [ "Manual Rate[3]", "Manual Rate", 0 ],
			"obj-226::obj-29::obj-97" : [ "Quantized Rate[3]", "Quantized Rate", 0 ],
			"obj-26::obj-186::obj-226::obj-112::obj-107" : [ "Delta Channel #[39]", "Delta Channel #", 0 ],
			"obj-26::obj-186::obj-226::obj-32" : [ "off", "off", 0 ],
			"obj-226::obj-112::obj-108" : [ "Hard Sync Threshold[1]", "Hard Sync Threshold", 0 ],
			"obj-82::obj-74" : [ "live.numbox[1]", "live.numbox", 0 ],
			"obj-226::obj-67" : [ "min_max_offset_ui[1]", "min_max_offset_ui", 0 ],
			"obj-26::obj-186::obj-82::obj-74" : [ "live.numbox[84]", "live.numbox", 0 ],
			"obj-226::obj-55" : [ "live.text[1]", "live.text[1]", 0 ],
			"obj-82::obj-9" : [ "live.text[7]", "live.text", 0 ],
			"obj-226::obj-47" : [ "modulation_indices[1]", "modulation_indices", 0 ],
			"obj-26::obj-186::obj-82::obj-12" : [ "ratecontrol[87]", "ratecontrol", 0 ],
			"obj-26::obj-186::obj-93" : [ "Trig mode[1]", "Trig mode", 0 ],
			"obj-26::obj-186::obj-226::obj-47" : [ "modulation_indices", "modulation_indices", 0 ],
			"parameterbanks" : 			{

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "_scale_control_taller.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI",
				"patcherrelativepath" : "../MIDI",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "scalable_multislider_taller.maxpat",
				"bootpath" : "~/12c/12c_sandbox/utility",
				"patcherrelativepath" : "../utility",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_multislider_markov_ui_2019.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2019",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "multislider_jam.js",
				"bootpath" : "~/12c/12c_sandbox/js",
				"patcherrelativepath" : "../js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "metro_time_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/utils",
				"patcherrelativepath" : "./utils",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "hard_sync_via_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/utils",
				"patcherrelativepath" : "./utils",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_receive_mains_out.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/EG/delta",
				"patcherrelativepath" : "./EG/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"patcherrelativepath" : "../audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bpatcher_name.js",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"patcherrelativepath" : "../audio",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "note_in_scale2.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2018",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "el.counter~.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "buttonGreen-1",
				"default" : 				{
					"bgcolor" : [ 0.043137, 0.364706, 0.094118, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "dUG Yello 01-1",
				"newobj" : 				{
					"fontsize" : [ 10.0 ],
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"fontface" : [ 1 ]
				}
,
				"message" : 				{
					"fontsize" : [ 10.0 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"fontface" : [ 1 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.94902, 0.992157, 1.0, 1.0 ],
						"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"default" : 				{
					"fontsize" : [ 10.0 ],
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"fontface" : [ 1 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.94902, 0.992157, 1.0, 1.0 ],
						"color2" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"button" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ]
				}
,
				"toggle" : 				{
					"bgcolor" : [ 1.0, 0.941176, 0.803922, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "ksliderWhite",
				"default" : 				{
					"color" : [ 1.0, 1.0, 1.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjCyan-1",
				"default" : 				{
					"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rsliderGold",
				"default" : 				{
					"color" : [ 0.646639, 0.821777, 0.854593, 1.0 ],
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "tap",
				"default" : 				{
					"fontname" : [ "Lato Light" ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
