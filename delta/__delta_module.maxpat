{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 2,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 34.0, 79.0, 1192.0, 683.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1064.0, -53.999977, 69.0, 22.0 ],
					"style" : "",
					"text" : "delay 2000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1064.0, -19.666695, 97.0, 22.0 ],
					"style" : "",
					"text" : "zoomfactor 0.89"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1064.0, -88.666656, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1169.138672, -99.460609, 150.0, 20.0 ],
					"style" : "",
					"text" : "fits my screen well :D"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 1135.0, 130.333328, 71.0, 22.0 ],
					"style" : "",
					"text" : "metronome"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 12.0,
					"id" : "obj-116",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 692.333374, 6.26667, 49.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 688.533264, 3.000023, 78.199997, 20.0 ],
					"style" : "",
					"text" : "Fullscreen",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 977.0, -19.666695, 79.0, 22.0 ],
					"style" : "",
					"text" : "fullscreen $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "live.toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 977.5, -53.999977, 15.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 673.5, 5.000023, 15.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.toggle[142]",
							"parameter_shortname" : "live.toggle[142]",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "off", "on" ]
						}

					}
,
					"varname" : "live.toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 977.0, 8.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 2,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-102",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 116.583374, 304.333344, 29.5, 22.0 ],
									"style" : "",
									"text" : "On"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 83.333374, 304.333344, 29.5, 22.0 ],
									"style" : "",
									"text" : "Off"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 239.000122, 277.666687, 77.0, 22.0 ],
									"style" : "",
									"text" : "255 255 255"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 187.333374, 321.666687, 177.0, 22.0 ],
									"style" : "",
									"text" : "sprintf bgfillcolor %s %s %s %s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 145.999878, 277.666687, 71.0, 22.0 ],
									"style" : "",
									"text" : "255 0 0 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 311.333374, 222.666672, 150.0, 33.0 ],
									"style" : "",
									"text" : "if 0, turn panel red\nif 1, turn panel grey"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 228.666748, 228.166672, 46.0, 22.0 ],
									"style" : "",
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 228.666748, 183.0, 69.0, 22.0 ],
									"style" : "",
									"text" : "unpack s 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 205.666672, 150.0, 33.0 ],
									"style" : "",
									"text" : "displays whether audio processing is on or not"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 68.666687, 100.0, 128.0, 22.0 ],
									"style" : "",
									"text" : "metro 1000 @active 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 68.666687, 131.0, 93.0, 22.0 ],
									"style" : "",
									"text" : "adstatus switch"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-108",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 93.958374, 403.666687, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-109",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 187.333374, 403.666687, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-61", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-61", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-80", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1052.0, 207.666656, 89.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p audio_status"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1036.833374, 164.666672, 37.0, 22.0 ],
					"style" : "",
					"text" : "dac~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1021.0, 246.166656, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 711.0, 62.333313, 30.0, 22.0 ],
					"style" : "",
					"text" : "On"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 12.0,
					"id" : "obj-56",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1064.0, 81.833336, 49.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 678.333374, 38.26667, 96.199997, 20.0 ],
					"style" : "",
					"text" : "Audio Status:",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 24.0,
					"id" : "obj-46",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 892.783264, 81.833336, 140.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 795.066711, 40.666668, 112.0, 33.0 ],
					"style" : "",
					"text" : "Activate",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 0.98 ],
					"blinkcolor" : [ 0.019608, 0.254902, 0.035294, 1.0 ],
					"id" : "obj-42",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 0.086275, 0.309804, 0.52549, 0.0 ],
					"patching_rect" : [ 1043.333374, 90.833336, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 785.833313, -0.8, 124.599998, 124.599998 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1039.0, 130.333328, 74.0, 22.0 ],
					"style" : "",
					"text" : "startwindow"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-33",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_delta_to_CC.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 912.783264, 445.666656, 468.0, 292.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 913.449951, 560.216797, 468.0, 294.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 1 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-13",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_delta_to_CC.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1385.0, 442.0, 476.0, 300.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 913.449951, 272.333313, 468.0, 294.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "float", "float", "float" ],
					"patching_rect" : [ 1385.0, -125.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "unpack 0. 0. 0. 0."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-43",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_aux_param_group.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1385.0, -425.166656, 574.0, 283.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 905.0, -8.666687, 476.449951, 283.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 905.0, 895.250122, 92.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 853.366638, 782.250122, 62.999977, 20.0 ],
					"style" : "",
					"text" : "slider rate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 638.0, 1004.250122, 92.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 617.333313, 782.250122, 70.0, 20.0 ],
					"style" : "",
					"text" : "DB out rate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 394.0, 1004.250122, 92.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 395.033295, 782.500244, 70.0, 20.0 ],
					"style" : "",
					"text" : "ramp 2 rate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 154.0, 1004.250122, 92.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 169.0, 782.250122, 71.0, 20.0 ],
					"style" : "",
					"text" : "ramp 1 rate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 880.699951, 895.333313, 80.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 837.033264, 720.500244, 77.0, 20.0 ],
					"style" : "",
					"text" : "LFO 2 offset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 626.0, 894.916809, 93.033264, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 596.333313, 720.250122, 91.0, 20.0 ],
					"style" : "",
					"text" : "LFO 2 intensity"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 402.0, 895.333313, 77.366592, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 389.033295, 720.250122, 77.0, 20.0 ],
					"style" : "",
					"text" : "LFO 1 offset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 149.0, 895.333313, 91.833344, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 153.0, 720.0, 91.0, 20.0 ],
					"style" : "",
					"text" : "LFO 1 intensity"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 1, 500 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-29",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 745.166687, 1004.250122, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 689.533264, 782.250122, 239.833374, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0, 50 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 500.166809, 1004.250122, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 463.033417, 782.250122, 228.0, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0, 5 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-31",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 255.166687, 1004.250122, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 240.033279, 782.250122, 228.0, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0, 5 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-32",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 12.833344, 1004.250122, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.366613, 782.250122, 228.0, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ -128, 128 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-21",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 732.699951, 895.333313, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 689.533264, 720.250122, 233.916656, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0, 1 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-20",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 491.033264, 894.916809, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 463.033417, 720.250122, 228.0, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ -128, 128 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-18",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 251.366592, 895.333313, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 240.033279, 720.250122, 228.0, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 0, 1 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-14",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_receive_delta.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 12.833344, 895.333313, 228.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.366613, 720.250122, 228.0, 65.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-17",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_note_mapping.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 20.0, 796.250122, 325.333344, 81.333328 ],
					"presentation" : 1,
					"presentation_rect" : [ 654.366638, 570.216797, 275.0, 76.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-12",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_note_mapping.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 353.633392, 796.250122, 324.0, 81.333328 ],
					"presentation" : 1,
					"presentation_rect" : [ 654.366638, 645.333435, 269.083282, 80.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 702.583374, -185.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 749.400024, -74.999977, 115.0, 22.0 ],
					"style" : "",
					"text" : "s delta_module_out"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1288.083374, -231.299988, 37.0, 22.0 ],
					"style" : "",
					"text" : "adc~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1236.833374, -261.799988, 89.0, 22.0 ],
					"style" : "",
					"text" : "loadmess start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1236.833374, -231.299988, 37.0, 22.0 ],
					"style" : "",
					"text" : "dac~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-367",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.799988, -272.799988, 437.200012, 33.0 ],
					"style" : "",
					"text" : "appends the combined output of any selected channels into the list of outputted messages"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-355",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_multiplex.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 679.400024, -343.666656, 643.0, 59.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.366613, 666.916809, 642.333313, 57.333336 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"comment" : "delta module list output",
					"id" : "obj-8",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 702.583374, -78.999977, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"linecount" : 11,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1217.030029, -54.23333, 214.0, 154.0 ],
					"style" : "",
					"text" : "1 - LFO 1\n2 - LFO 2\n3 - Ramp 1\n4 - Ramp 2\n5-12 - CC audio 1\n13-20 - CC audio 2\n21 - 28 - sliders\n29 - DB data out\n30 - Mux\n31-32 - Note map out\n33-36 - aux 1-4"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_db_data.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "" ],
					"patching_rect" : [ 361.0, 512.333313, 369.333344, 262.666687 ],
					"presentation" : 1,
					"presentation_rect" : [ 656.366638, 344.333344, 269.083282, 236.333374 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 703.083374, -150.666641, 736.0, 22.0 ],
					"style" : "",
					"text" : "70. 75. 127. 127. 0. 2.988235 2. 0. 0. 0. 0. 0. 0. 2.988235 2. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 36,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 702.583374, -214.666656, 510.0, 22.0 ],
					"style" : "",
					"text" : "pak 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0."
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-7",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_LFO.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 30.0, -214.666656, 634.666687, 120.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.56661, 62.333313, 649.800049, 68.000015 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ctrl_audio.maxpat",
					"numinlets" : 0,
					"numoutlets" : 8,
					"offset" : [ 1.0, 2.0 ],
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 361.0, 157.333344, 329.333313, 340.333313 ],
					"presentation" : 1,
					"presentation_rect" : [ 336.366608, 353.666779, 323.333344, 317.333313 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ctrl_audio.maxpat",
					"numinlets" : 0,
					"numoutlets" : 8,
					"offset" : [ 1.0, 2.0 ],
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 12.833344, 161.333344, 322.166656, 324.333313 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.366613, 353.916779, 323.333344, 317.333313 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-25",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ramp.maxpat",
					"numinlets" : 1,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 361.0, -88.666656, 326.133423, 232.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 336.366608, 127.000015, 323.333344, 231.466797 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-19",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_ramp.maxpat",
					"numinlets" : 1,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 20.0, -88.666656, 319.0, 232.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.366613, 127.000015, 323.333344, 231.666687 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-16",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_LFO.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 30.0, -343.666656, 634.666687, 120.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.56661, -2.666672, 649.800049, 68.666687 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_slider.maxpat",
					"numinlets" : 1,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 12.833344, 504.666656, 326.166656, 278.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 656.366638, 123.333328, 269.083282, 274.833313 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.439216, 0.74902, 0.254902, 0.8 ],
					"id" : "obj-28",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1496.0, -86.666695, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.666643, 720.250122, 895.700073, 125.666664 ],
					"proportion" : 0.39,
					"rounded" : 0,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 255, 255, 255, 0.5 ],
					"id" : "obj-57",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1122.0, 246.166656, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.333313, -0.666667, 133.333328, 124.0 ],
					"proportion" : 0.39,
					"rounded" : 0,
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 11 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 10 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 9 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 8 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-110", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 31 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 30 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 29 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-355", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 27 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 26 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 25 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 24 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 23 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 22 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 21 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 20 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 35 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 34 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 33 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 32 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 28 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 19 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 18 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 17 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 16 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 15 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 14 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 13 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 12 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-355", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-1::obj-33" : [ "live.dial[46]", "live.dial", 0 ],
			"obj-1::obj-122" : [ "live.dial[25]", "live.dial", 0 ],
			"obj-43::obj-3::obj-80" : [ "number[97]", "number", 0 ],
			"obj-43::obj-4::obj-80" : [ "number[99]", "number", 0 ],
			"obj-33::obj-18::obj-2" : [ "live.toggle[83]", "live.toggle", 0 ],
			"obj-16::obj-122" : [ "intensity[4]", "intensity", 0 ],
			"obj-1::obj-25" : [ "live.toggle[158]", "live.toggle", 0 ],
			"obj-6::obj-49" : [ "live.dial[7]", "live.dial", 0 ],
			"obj-355::obj-192" : [ "live.toggle[71]", "live.toggle[40]", 0 ],
			"obj-355::obj-176" : [ "live.toggle[55]", "live.toggle[40]", 0 ],
			"obj-20::obj-156" : [ "number[28]", "number[2]", 0 ],
			"obj-13::obj-36" : [ "number[133]", "number[20]", 0 ],
			"obj-1::obj-39" : [ "live.dial[44]", "live.dial", 0 ],
			"obj-43::obj-4::obj-3::obj-11" : [ "number[101]", "number[3]", 0 ],
			"obj-22::obj-6" : [ "toggle", "toggle", 0 ],
			"obj-16::obj-55" : [ "smooth low[1]", "low ramp", 0 ],
			"obj-1::obj-21" : [ "live.toggle[162]", "live.toggle", 0 ],
			"obj-6::obj-120" : [ "live.dial[21]", "live.dial", 0 ],
			"obj-355::obj-177" : [ "live.toggle[56]", "live.toggle[40]", 0 ],
			"obj-355::obj-167" : [ "live.toggle[46]", "live.toggle[40]", 0 ],
			"obj-21::obj-2" : [ "live.toggle[91]", "live.toggle", 0 ],
			"obj-33::obj-16::obj-2" : [ "live.toggle[100]", "live.toggle", 0 ],
			"obj-1::obj-30" : [ "live.dial[47]", "live.dial", 0 ],
			"obj-43::obj-4::obj-15" : [ "live.toggle[134]", "live.toggle", 0 ],
			"obj-13::obj-13::obj-2" : [ "live.toggle[148]", "live.toggle", 0 ],
			"obj-6::obj-52" : [ "gain~[1]", "gain~[1]", 0 ],
			"obj-355::obj-168" : [ "live.toggle[47]", "live.toggle[40]", 0 ],
			"obj-355::obj-178" : [ "live.toggle[57]", "live.toggle[40]", 0 ],
			"obj-31::obj-156" : [ "number[22]", "number[2]", 0 ],
			"obj-30::obj-2" : [ "live.toggle[88]", "live.toggle", 0 ],
			"obj-33::obj-15::obj-11" : [ "number[163]", "number[3]", 0 ],
			"obj-4::obj-4" : [ "number", "number", 0 ],
			"obj-6::obj-108" : [ "live.dial[8]", "live.dial", 0 ],
			"obj-6::obj-21" : [ "live.toggle[254]", "live.toggle", 0 ],
			"obj-7::obj-132" : [ "freq[5]", "freq", 0 ],
			"obj-13::obj-14::obj-156" : [ "number[146]", "number[2]", 0 ],
			"obj-355::obj-165" : [ "live.toggle[44]", "live.toggle[40]", 0 ],
			"obj-355::obj-179" : [ "live.toggle[58]", "live.toggle[40]", 0 ],
			"obj-43::obj-1::obj-15" : [ "live.toggle[141]", "live.toggle", 0 ],
			"obj-33::obj-13::obj-156" : [ "number[164]", "number[2]", 0 ],
			"obj-33::obj-38" : [ "number[35]", "number[22]", 0 ],
			"obj-6::obj-104" : [ "live.dial[11]", "live.dial", 0 ],
			"obj-5::obj-15" : [ "number[8]", "number", 0 ],
			"obj-13::obj-19::obj-2" : [ "live.toggle[145]", "live.toggle", 0 ],
			"obj-1::obj-27" : [ "live.dial[48]", "live.dial", 0 ],
			"obj-355::obj-180" : [ "live.toggle[59]", "live.toggle[40]", 0 ],
			"obj-355::obj-166" : [ "live.toggle[45]", "live.toggle[40]", 0 ],
			"obj-43::obj-2::obj-3::obj-156" : [ "number[105]", "number[2]", 0 ],
			"obj-33::obj-36" : [ "number[37]", "number[20]", 0 ],
			"obj-4::obj-12" : [ "number[1]", "number[1]", 0 ],
			"obj-6::obj-122" : [ "live.dial[22]", "live.dial", 0 ],
			"obj-13::obj-17::obj-156" : [ "number[140]", "number[2]", 0 ],
			"obj-33::obj-18::obj-156" : [ "number[156]", "number[2]", 0 ],
			"obj-25::obj-50" : [ "number[3]", "number[2]", 0 ],
			"obj-1::obj-117" : [ "live.dial[30]", "live.dial", 0 ],
			"obj-1::obj-105" : [ "live.dial[38]", "live.dial", 0 ],
			"obj-6::obj-110" : [ "live.dial[14]", "live.dial", 0 ],
			"obj-355::obj-181" : [ "live.toggle[60]", "live.toggle[40]", 0 ],
			"obj-43::obj-3::obj-3::obj-11" : [ "number[103]", "number[3]", 0 ],
			"obj-18::obj-11" : [ "number[31]", "number[3]", 0 ],
			"obj-13::obj-38" : [ "number[131]", "number[22]", 0 ],
			"obj-19::obj-62" : [ "number[232]", "number[1]", 0 ],
			"obj-6::obj-24" : [ "live.toggle[237]", "live.toggle", 0 ],
			"obj-43::obj-4::obj-3::obj-156" : [ "number[100]", "number[2]", 0 ],
			"obj-17::obj-15" : [ "number[169]", "number[1]", 0 ],
			"obj-21::obj-11" : [ "number[27]", "number[3]", 0 ],
			"obj-13::obj-34" : [ "number[135]", "number[18]", 0 ],
			"obj-1::obj-10" : [ "gain~[3]", "gain~", 0 ],
			"obj-6::obj-105" : [ "live.dial[12]", "live.dial", 0 ],
			"obj-43::obj-4::obj-63" : [ "live.toggle[132]", "live.toggle[2]", 0 ],
			"obj-13::obj-12::obj-2" : [ "live.toggle[149]", "live.toggle", 0 ],
			"obj-6::obj-44" : [ "live.dial[5]", "live.dial", 0 ],
			"obj-6::obj-10" : [ "gain~", "gain~", 0 ],
			"obj-12::obj-16" : [ "number[170]", "number", 0 ],
			"obj-32::obj-11" : [ "number[25]", "number[3]", 0 ],
			"obj-33::obj-17::obj-156" : [ "number[154]", "number[2]", 0 ],
			"obj-7::obj-22" : [ "live.toggle[24]", "live.toggle", 0 ],
			"obj-13::obj-15::obj-2" : [ "live.toggle[147]", "live.toggle", 0 ],
			"obj-1::obj-22" : [ "live.toggle[161]", "live.toggle", 0 ],
			"obj-6::obj-117" : [ "live.dial[19]", "live.dial", 0 ],
			"obj-7::obj-35" : [ "number[5]", "number", 0 ],
			"obj-43::obj-1::obj-3::obj-2" : [ "live.toggle[140]", "live.toggle", 0 ],
			"obj-6::obj-64" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-7::obj-55" : [ "smooth low[2]", "low ramp", 0 ],
			"obj-13::obj-19::obj-156" : [ "number[144]", "number[2]", 0 ],
			"obj-1::obj-45" : [ "live.dial[42]", "live.dial", 0 ],
			"obj-5::obj-22" : [ "live.toggle[25]", "live.toggle", 0 ],
			"obj-43::obj-2::obj-3::obj-11" : [ "number[106]", "number[3]", 0 ],
			"obj-33::obj-37" : [ "number[36]", "number[21]", 0 ],
			"obj-16::obj-133" : [ "y-offset[4]", "y-offset", 0 ],
			"obj-5::obj-49" : [ "metronome frame rate", "rate (ms)", 0 ],
			"obj-13::obj-18::obj-2" : [ "live.toggle[105]", "live.toggle", 0 ],
			"obj-33::obj-17::obj-11" : [ "number[155]", "number[3]", 0 ],
			"obj-1::obj-28" : [ "live.toggle[157]", "live.toggle[6]", 0 ],
			"obj-1::obj-113" : [ "live.dial[31]", "live.dial", 0 ],
			"obj-1::obj-103" : [ "live.dial[39]", "live.dial", 0 ],
			"obj-43::obj-2::obj-15" : [ "live.toggle[138]", "live.toggle", 0 ],
			"obj-6::obj-102" : [ "live.dial[9]", "live.dial", 0 ],
			"obj-355::obj-77" : [ "live.toggle[101]", "live.toggle[40]", 0 ],
			"obj-14::obj-2" : [ "live.toggle[94]", "live.toggle", 0 ],
			"obj-13::obj-32" : [ "number[137]", "number", 0 ],
			"obj-33::obj-12::obj-11" : [ "number[167]", "number[3]", 0 ],
			"obj-1::obj-123" : [ "live.dial[26]", "live.dial", 0 ],
			"obj-1::obj-114" : [ "live.dial[32]", "live.dial", 0 ],
			"obj-43::obj-3::obj-63" : [ "live.toggle[96]", "live.toggle[2]", 0 ],
			"obj-6::obj-119" : [ "live.dial[20]", "live.dial", 0 ],
			"obj-5::obj-39" : [ "number[7]", "number[3]", 0 ],
			"obj-355::obj-78" : [ "live.toggle[102]", "live.toggle[40]", 0 ],
			"obj-17::obj-16" : [ "number[168]", "number", 0 ],
			"obj-20::obj-2" : [ "live.toggle[92]", "live.toggle", 0 ],
			"obj-13::obj-35" : [ "number[134]", "number[19]", 0 ],
			"obj-1::obj-16" : [ "live.numbox[6]", "Amp ∆/T", 0 ],
			"obj-355::obj-162" : [ "live.toggle[41]", "live.toggle[40]", 0 ],
			"obj-13::obj-12::obj-11" : [ "number[153]", "number[3]", 0 ],
			"obj-355::obj-79" : [ "live.toggle[103]", "live.toggle[40]", 0 ],
			"obj-32::obj-156" : [ "number[24]", "number[2]", 0 ],
			"obj-33::obj-13::obj-2" : [ "live.toggle[152]", "live.toggle", 0 ],
			"obj-33::obj-14::obj-11" : [ "number[161]", "number[3]", 0 ],
			"obj-33::obj-16::obj-156" : [ "number[128]", "number[2]", 0 ],
			"obj-19::obj-7" : [ "number[252]", "number", 0 ],
			"obj-6::obj-107" : [ "live.dial[13]", "live.dial", 0 ],
			"obj-13::obj-13::obj-156" : [ "number[150]", "number[2]", 0 ],
			"obj-16::obj-53" : [ "live.dial[53]", "hi ramp", 0 ],
			"obj-1::obj-125" : [ "number[43]", "number", 0 ],
			"obj-355::obj-80" : [ "live.toggle[104]", "live.toggle[40]", 0 ],
			"obj-12::obj-8" : [ "live.toggle[154]", "live.toggle", 0 ],
			"obj-29::obj-2" : [ "live.toggle[87]", "live.toggle", 0 ],
			"obj-6::obj-13" : [ "live.numbox", "live.numbox", 0 ],
			"obj-7::obj-53" : [ "live.dial[56]", "hi ramp", 0 ],
			"obj-355::obj-48" : [ "live.toggle[235]", "live.toggle[40]", 0 ],
			"obj-13::obj-14::obj-2" : [ "live.toggle[146]", "live.toggle", 0 ],
			"obj-25::obj-43" : [ "function[2]", "function", 0 ],
			"obj-1::obj-102" : [ "live.dial[40]", "live.dial", 0 ],
			"obj-6::obj-37" : [ "live.dial[3]", "live.dial", 0 ],
			"obj-6::obj-125" : [ "number[173]", "number", 0 ],
			"obj-30::obj-11" : [ "number[21]", "number[3]", 0 ],
			"obj-43::obj-1::obj-80" : [ "number[107]", "number", 0 ],
			"obj-33::obj-39" : [ "number[34]", "number[23]", 0 ],
			"obj-13::obj-18::obj-156" : [ "number[142]", "number[2]", 0 ],
			"obj-4::obj-17" : [ "live.toggle[8]", "live.toggle[8]", 0 ],
			"obj-1::obj-44" : [ "live.dial[43]", "live.dial", 0 ],
			"obj-6::obj-114" : [ "live.dial[17]", "live.dial", 0 ],
			"obj-355::obj-161" : [ "live.toggle[40]", "live.toggle[40]", 0 ],
			"obj-43::obj-2::obj-80" : [ "number[104]", "number", 0 ],
			"obj-112" : [ "live.toggle[142]", "live.toggle[142]", 0 ],
			"obj-4::obj-20" : [ "live.toggle", "live.toggle", 0 ],
			"obj-14::obj-11" : [ "number[33]", "number[3]", 0 ],
			"obj-13::obj-16::obj-156" : [ "number[138]", "number[2]", 0 ],
			"obj-33::obj-12::obj-156" : [ "number[166]", "number[2]", 0 ],
			"obj-4::obj-18" : [ "live.toggle[3]", "live.toggle[3]", 0 ],
			"obj-25::obj-1" : [ "live.toggle[9]", "live.toggle", 0 ],
			"obj-1::obj-107" : [ "live.dial[35]", "live.dial", 0 ],
			"obj-6::obj-25" : [ "live.toggle[236]", "live.toggle", 0 ],
			"obj-43::obj-3::obj-3::obj-2" : [ "live.toggle[97]", "live.toggle", 0 ],
			"obj-1::obj-24" : [ "live.toggle[159]", "live.toggle", 0 ],
			"obj-18::obj-2" : [ "live.toggle[93]", "live.toggle", 0 ],
			"obj-13::obj-37" : [ "number[132]", "number[21]", 0 ],
			"obj-33::obj-14::obj-2" : [ "live.toggle[151]", "live.toggle", 0 ],
			"obj-33::obj-19::obj-11" : [ "number[159]", "number[3]", 0 ],
			"obj-4::obj-5" : [ "live.toggle[1]", "toggle", 0 ],
			"obj-1::obj-49" : [ "live.dial[41]", "live.dial", 0 ],
			"obj-6::obj-111" : [ "live.dial[15]", "live.dial", 0 ],
			"obj-6::obj-23" : [ "live.toggle[10]", "live.toggle", 0 ],
			"obj-5::obj-26" : [ "live.toggle[26]", "live.toggle", 0 ],
			"obj-43::obj-4::obj-3::obj-2" : [ "live.toggle[133]", "live.toggle", 0 ],
			"obj-6::obj-45" : [ "live.dial[6]", "live.dial", 0 ],
			"obj-21::obj-156" : [ "number[26]", "number[2]", 0 ],
			"obj-31::obj-11" : [ "number[23]", "number[3]", 0 ],
			"obj-33::obj-14::obj-156" : [ "number[160]", "number[2]", 0 ],
			"obj-33::obj-16::obj-11" : [ "number[129]", "number[3]", 0 ],
			"obj-1::obj-116" : [ "live.dial[29]", "live.dial", 0 ],
			"obj-6::obj-88" : [ "live.toggle[11]", "live.toggle", 0 ],
			"obj-13::obj-13::obj-11" : [ "number[151]", "number[3]", 0 ],
			"obj-33::obj-15::obj-156" : [ "number[162]", "number[2]", 0 ],
			"obj-6::obj-116" : [ "live.dial[18]", "live.dial", 0 ],
			"obj-355::obj-182" : [ "live.toggle[61]", "live.toggle[40]", 0 ],
			"obj-31::obj-2" : [ "live.toggle[89]", "live.toggle", 0 ],
			"obj-19::obj-1" : [ "live.toggle[238]", "live.toggle", 0 ],
			"obj-6::obj-22" : [ "live.toggle[12]", "live.toggle", 0 ],
			"obj-7::obj-122" : [ "intensity[5]", "intensity", 0 ],
			"obj-13::obj-15::obj-156" : [ "number[148]", "number[2]", 0 ],
			"obj-1::obj-88" : [ "live.toggle[155]", "live.toggle", 0 ],
			"obj-6::obj-142" : [ "number[172]", "number[1]", 0 ],
			"obj-355::obj-183" : [ "live.toggle[62]", "live.toggle[40]", 0 ],
			"obj-43::obj-1::obj-3::obj-156" : [ "number[108]", "number[2]", 0 ],
			"obj-33::obj-15::obj-2" : [ "live.toggle[144]", "live.toggle", 0 ],
			"obj-33::obj-34" : [ "number[39]", "number[18]", 0 ],
			"obj-19::obj-43" : [ "function[1]", "function", 0 ],
			"obj-25::obj-62" : [ "number[2]", "number[1]", 0 ],
			"obj-6::obj-70" : [ "live.toggle[13]", "live.toggle", 0 ],
			"obj-6::obj-20" : [ "live.toggle[255]", "live.toggle", 0 ],
			"obj-5::obj-14" : [ "number[9]", "number[1]", 0 ],
			"obj-13::obj-19::obj-11" : [ "number[145]", "number[3]", 0 ],
			"obj-25::obj-7" : [ "number[4]", "number", 0 ],
			"obj-1::obj-110" : [ "live.dial[33]", "live.dial", 0 ],
			"obj-1::obj-70" : [ "live.toggle[156]", "live.toggle", 0 ],
			"obj-1::obj-64" : [ "live.numbox[5]", "live.numbox", 0 ],
			"obj-6::obj-72" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-355::obj-184" : [ "live.toggle[63]", "live.toggle[40]", 0 ],
			"obj-43::obj-2::obj-3::obj-2" : [ "live.toggle[137]", "live.toggle", 0 ],
			"obj-33::obj-35" : [ "number[38]", "number[19]", 0 ],
			"obj-6::obj-28" : [ "live.toggle[14]", "live.toggle[6]", 0 ],
			"obj-13::obj-17::obj-11" : [ "number[141]", "number[3]", 0 ],
			"obj-1::obj-111" : [ "live.dial[34]", "live.dial", 0 ],
			"obj-6::obj-30" : [ "live.dial[1]", "live.dial", 0 ],
			"obj-355::obj-163" : [ "live.toggle[42]", "live.toggle[40]", 0 ],
			"obj-355::obj-169" : [ "live.toggle[48]", "live.toggle[40]", 0 ],
			"obj-43::obj-3::obj-3::obj-156" : [ "number[102]", "number[2]", 0 ],
			"obj-16::obj-35" : [ "number[45]", "number", 0 ],
			"obj-6::obj-27" : [ "live.dial", "live.dial", 0 ],
			"obj-6::obj-123" : [ "live.dial[23]", "live.dial", 0 ],
			"obj-18::obj-156" : [ "number[30]", "number[2]", 0 ],
			"obj-13::obj-16::obj-2" : [ "live.toggle[15]", "live.toggle", 0 ],
			"obj-13::obj-33" : [ "number[136]", "number[1]", 0 ],
			"obj-33::obj-19::obj-156" : [ "number[158]", "number[2]", 0 ],
			"obj-1::obj-108" : [ "live.dial[36]", "live.dial", 0 ],
			"obj-1::obj-104" : [ "live.dial[37]", "live.dial", 0 ],
			"obj-6::obj-16" : [ "live.numbox[1]", "Amp ∆/T", 0 ],
			"obj-355::obj-170" : [ "live.toggle[49]", "live.toggle[40]", 0 ],
			"obj-355::obj-164" : [ "live.toggle[43]", "live.toggle[40]", 0 ],
			"obj-355::obj-210" : [ "number[10]", "number[10]", 0 ],
			"obj-17::obj-8" : [ "live.toggle[233]", "live.toggle", 0 ],
			"obj-29::obj-11" : [ "number[19]", "number[3]", 0 ],
			"obj-43::obj-3::obj-15" : [ "live.toggle[135]", "live.toggle", 0 ],
			"obj-4::obj-23" : [ "live.toggle[6]", "live.toggle[6]", 0 ],
			"obj-20::obj-11" : [ "number[29]", "number[3]", 0 ],
			"obj-13::obj-17::obj-2" : [ "live.toggle[16]", "live.toggle", 0 ],
			"obj-13::obj-39" : [ "number[130]", "number[23]", 0 ],
			"obj-4::obj-15" : [ "live.toggle[2]", "live.toggle[2]", 0 ],
			"obj-19::obj-50" : [ "number[233]", "number[2]", 0 ],
			"obj-1::obj-13" : [ "live.numbox[7]", "live.numbox", 0 ],
			"obj-355::obj-187" : [ "live.toggle[66]", "live.toggle[40]", 0 ],
			"obj-355::obj-171" : [ "live.toggle[50]", "live.toggle[40]", 0 ],
			"obj-13::obj-12::obj-156" : [ "number[152]", "number[2]", 0 ],
			"obj-4::obj-24" : [ "live.toggle[7]", "live.toggle[7]", 0 ],
			"obj-5::obj-45" : [ "number[6]", "number[2]", 0 ],
			"obj-12::obj-15" : [ "number[171]", "number[1]", 0 ],
			"obj-32::obj-2" : [ "live.toggle[90]", "live.toggle", 0 ],
			"obj-33::obj-17::obj-2" : [ "live.toggle[17]", "live.toggle", 0 ],
			"obj-1::obj-72" : [ "live.numbox[4]", "live.numbox", 0 ],
			"obj-1::obj-37" : [ "live.dial[45]", "live.dial", 0 ],
			"obj-355::obj-188" : [ "live.toggle[67]", "live.toggle[40]", 0 ],
			"obj-355::obj-172" : [ "live.toggle[51]", "live.toggle[40]", 0 ],
			"obj-13::obj-15::obj-11" : [ "number[149]", "number[3]", 0 ],
			"obj-4::obj-21" : [ "live.toggle[4]", "live.toggle[4]", 0 ],
			"obj-6::obj-39" : [ "live.dial[4]", "live.dial", 0 ],
			"obj-43::obj-1::obj-3::obj-11" : [ "number[109]", "number[3]", 0 ],
			"obj-33::obj-19::obj-2" : [ "live.toggle[150]", "live.toggle", 0 ],
			"obj-16::obj-22" : [ "live.toggle[108]", "live.toggle", 0 ],
			"obj-6::obj-103" : [ "live.dial[10]", "live.dial", 0 ],
			"obj-7::obj-133" : [ "y-offset[5]", "y-offset", 0 ],
			"obj-355::obj-173" : [ "live.toggle[52]", "live.toggle[40]", 0 ],
			"obj-355::obj-189" : [ "live.toggle[68]", "live.toggle[40]", 0 ],
			"obj-13::obj-14::obj-11" : [ "number[147]", "number[3]", 0 ],
			"obj-33::obj-12::obj-2" : [ "live.toggle[153]", "live.toggle", 0 ],
			"obj-4::obj-22" : [ "live.toggle[5]", "live.toggle[5]", 0 ],
			"obj-1::obj-142" : [ "number[42]", "number[1]", 0 ],
			"obj-1::obj-52" : [ "gain~[2]", "gain~[1]", 0 ],
			"obj-1::obj-23" : [ "live.toggle[160]", "live.toggle", 0 ],
			"obj-6::obj-113" : [ "live.dial[16]", "live.dial", 0 ],
			"obj-43::obj-1::obj-63" : [ "live.toggle[139]", "live.toggle[2]", 0 ],
			"obj-33::obj-13::obj-11" : [ "number[165]", "number[3]", 0 ],
			"obj-33::obj-33" : [ "number[40]", "number[1]", 0 ],
			"obj-355::obj-190" : [ "live.toggle[69]", "live.toggle[40]", 0 ],
			"obj-355::obj-174" : [ "live.toggle[53]", "live.toggle[40]", 0 ],
			"obj-29::obj-156" : [ "number[18]", "number[2]", 0 ],
			"obj-13::obj-18::obj-11" : [ "number[143]", "number[3]", 0 ],
			"obj-1::obj-119" : [ "live.dial[27]", "live.dial", 0 ],
			"obj-1::obj-120" : [ "live.dial[28]", "live.dial", 0 ],
			"obj-6::obj-33" : [ "live.dial[24]", "live.dial", 0 ],
			"obj-355::obj-71" : [ "live.toggle[95]", "live.toggle[40]", 0 ],
			"obj-30::obj-156" : [ "number[20]", "number[2]", 0 ],
			"obj-43::obj-2::obj-63" : [ "live.toggle[136]", "live.toggle[2]", 0 ],
			"obj-33::obj-32" : [ "number[41]", "number", 0 ],
			"obj-16::obj-132" : [ "freq[4]", "freq", 0 ],
			"obj-1::obj-20" : [ "live.toggle[163]", "live.toggle", 0 ],
			"obj-355::obj-191" : [ "live.toggle[70]", "live.toggle[40]", 0 ],
			"obj-355::obj-175" : [ "live.toggle[54]", "live.toggle[40]", 0 ],
			"obj-14::obj-156" : [ "number[32]", "number[2]", 0 ],
			"obj-13::obj-16::obj-11" : [ "number[139]", "number[3]", 0 ],
			"obj-33::obj-18::obj-11" : [ "number[157]", "number[3]", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "_slider.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "randomness_feedback.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "hilo_limit.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_LFO.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ramp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_ctrl_audio.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "2ch_Amp_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "amp_delta_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "root_freq_over_time.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "in_phase.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "1ch_amp.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta/CC_audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_db_data.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_multiplex.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ok.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_note_mapping.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_receive_delta.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_aux_param_group.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "_aux_param.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "quantize_bang.maxpat",
				"bootpath" : "~/12c/12c_sandbox/MIDI",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "param_processing.js",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_delta_to_CC.maxpat",
				"bootpath" : "~/12c/12c_sandbox/delta",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "metronome.maxpat",
				"bootpath" : "~/12c/12c_sandbox/metronome",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "buttonGreen-1",
				"default" : 				{
					"bgcolor" : [ 0.043137, 0.364706, 0.094118, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"fontsize" : [ 12.059008 ],
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rsliderGold",
				"default" : 				{
					"color" : [ 0.646639, 0.821777, 0.854593, 1.0 ],
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
