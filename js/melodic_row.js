function anything() {
	var list = arrayfromargs(messagename, arguments);
	for (var i = 0; i < list.length; i++) {
		var out_data = [];
		var note_length = 1;
		note_length = findNoteLength(list, i, list[i]);
		out_data = [i, note_length, list[i]];
		i+= ( note_length - 1);
		outlet(0, out_data);
	}
}

function findNoteLength(list, offset, value) {
	var count = 1;
	for ( var a = offset + 1; a < list.length; a++ ) {
		if ( list[a] == value ) {
			count++;
		}
		else {
			return count;
		}
	}
	return count;
}