inlets = 2;
outlets = 1;

var note_modulo;
var scale;

function list() {
	// sets global data
	if (inlet === 0 ) {
		scale = arrayfromargs(arguments);
	}
	if (inlet === 1 ) {
		note_modulo = arrayfromargs(arguments);
	}
}
function bang() {
	var closest_number_lower = false;
	var smallest_diff_found = 12;
	// loop through scale, output closest modulo to the note's. if
	// the modulo is lower, it goes out outlet 1, if higher, it goes out outlet 0
	for ( var i = 0; i < scale.length; i++ ) {
		var current_diff =  Math.abs(scale[i] - note_modulo);
		if ( current_diff < smallest_diff_found ) {
			smallest_diff_found = current_diff;
			if ( scale[i] < note_modulo ) {
				closest_number_lower = true;
			}
			else {
				closest_number_lower = false;
			}
		}
	}
	if ( closest_number_lower ) {
		outlet(0, smallest_diff_found * -1);
	}
	else {
		outlet(0, smallest_diff_found);
	}
}
