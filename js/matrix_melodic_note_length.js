inlets = 4;
outlets = 1;

var current_matrix_obj = [];
var row_note_lengths = [];
var num_columns;
var row_number;
var lookup;

function list() {
	// sets global data
	if (inlet === 0 ) {
		var current_matrix = arrayfromargs(arguments);
		current_matrix_obj = createArrayFromMatrixData(current_matrix);
	}
	if (inlet === 1 ) {
		num_columns = arrayfromargs(arguments);
	}
	if (inlet === 2 ) {
		var lookup = arrayfromargs(arguments);
		getNoteLengthByCol(lookup);
	}
	if (inlet === 3 ) {
		row_number = arrayfromargs(arguments);
	}
}

function bang() {
	// actually runs process
	var row = current_matrix_obj[row_number];
	for (var col = 0; col < row.length; col++ ) {
		var cell_value = row[col];
		if ( cell_value == 0 ) {
			// note is off
			row_note_lengths[col] = 0;
		}
		else if ( col > 0 && cell_value === row[col - 1] ) {
			// note is longer than 1 cell. special looping backwards in row needed to determine length
			var note_end_found = false;
			var distance_moved_backwards = 1;
			var note_length = 1;
			while ( note_end_found === false ) {
				if ( col - distance_moved_backwards < 0 ) {
					note_end_found = true;
				}
				else if ( cell_value == row[col - distance_moved_backwards]) {
					// cells in the middle of a longer note are "off"
					distance_moved_backwards++;
					note_length++;
				}
				else {
					note_end_found = true;
					// cells starting a longer note are "on"
				}
			}
			row_note_lengths[(col - distance_moved_backwards + 1)] = note_length;
			row_note_lengths[col] = 0;
		}
		else {
			// note is 1 cell long, or this is the beginning of a longer note
			row_note_lengths[col] = 1;
		}
	}
}

function createArrayFromMatrixData(source_array) {
	// takes <col row val> 16x16 list and creates a corresponding hierarchical array with 16 rows
	// and only the corresponding rows values in it
	var destination_array = [];
	var column_count = 0;
	var current_row = 0;
	destination_array[0] = [];
	for ( var i = 0; i < source_array.length; i++ ) {
		// skip row/col: only store value. easier to do in JS than with max zl
		if ( (i + 1) % 3 === 0 ) {
			destination_array[current_row][column_count] = source_array[i];
			column_count++;
		}
		if ( column_count == num_columns ) {
			column_count = 0;
			current_row++;
			destination_array[current_row] = [];
		}
	}
	return destination_array;
}

function getNoteLengthByCol(col) {
	outlet(0, row_note_lengths[col]);
}
