inlets = 1;
outlets = 1;

// scales the convolved signal gain based on the average loudness of the convolving buffer
// since that can range wildly, this helps to compress the output into similar ranges
// so that a quiet impulse or a loud impulse both result in just a normal-sounding
// effect
function msg_float(f) {
	if ( f  < 0.001 ) {
		outlet(0, 1);
	}
	else if ( f < 0.008 ) {
		outlet(0, 0.2);
	}
	else if ( f < 0.015 ) {
		outlet(0, 0.1);
	}
	else {
		outlet(0, 0.05);
	}
}