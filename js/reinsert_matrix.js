inlets = 2;
outlets = 1;
// inlet 1: cell value for a paricular location in matrix, list e.g. 0 0 1
// inlet 2: #0columns, number of columns in matrix
var stored_matrix_str = '';
var cell_value;
var num_cols;

function list() {
	// sets global data
	if (inlet === 0 ) {
		var val = arrayfromargs(arguments);
		cell_value = val;
	}
	if (inlet === 1 ) {
		var cols = arrayfromargs(arguments);
		num_cols = cols;
	}
}

function bang() {
// actually runs process
	run(row_number, num_cols);
}

function run(row_number, num_cols) {
	var matrix_in_str = '';
		for ( var current_col_num = 0; current_col_num < num_cols; current_col_num++ ) {
			var current_cell_value = 0;
			matrix_in_str += current_col_num + ' ' + row_number + ' ' + 0 + ' ';
		}
	outlet(0, matrix_in_str);
}
