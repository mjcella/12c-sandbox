inlets = 1;
outlets = 1;

function anything() {
	var data_from_pattr = arrayfromargs(arguments);
	var low = data_from_pattr[0];
	var high = data_from_pattr[1];
	var out = '';

	for (var i = low; i <= high; i++ ) {
		out += i + ' ';
	}
	outlet(0, out);
}
