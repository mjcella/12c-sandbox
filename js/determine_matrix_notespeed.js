inlets = 1;
outlets = 1;

var tempo;
var all_possible_note_values = ['1n','2n','2nt','4n','4nt','8n','8nt','16n','16nt','32n','32nt','64n'];

function anything() {
	var list_of_values = arrayfromargs(messagename, arguments);
	tempo = list_of_values[0];
	var tempo_options = getPossibleNoteValuesGivenGlobalTempo();
	outlet(0, tempo_options);
}

function getPossibleNoteValuesGivenGlobalTempo() {
	// given the global tempo, returns that tempo and the next 4 possible notevalues. allows the
	// phasor to work at any speed
	var out_str = '0 ';
	var base_note_value = arraySearch(all_possible_note_values, tempo);
	var i = base_note_value;
	while ( i < base_note_value + 4 ) {
		out_str += all_possible_note_values[i] + ' ';
		i++;
	}
	out_str += all_possible_note_values[i];
	return out_str;
}

function arraySearch(arr,val) {
	// returns key of element in array whose value matches val
	for ( var i = 0; i < arr.length; i++) {
		if (arr[i] === val) {
			return i;
		}
	}
}
