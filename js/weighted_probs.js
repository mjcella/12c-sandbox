inlets = 1;
outlets = 1;

var probs = [];

function list() {
	// sets global data
	if ( inlet === 0 ) {
		probs = arrayfromargs(arguments);
	}
}

function bang() {
	var sum = 0;
	for ( var i = 0; i < probs.length; i++ ) {
		sum+= probs[i];
	}

	var decimals = [];
	for ( var i = 0; i < probs.length; i++ ) {
		decimals.push(probs[i] / sum);
	}

	var random_float = Math.random();
	var out_sum = 0;

	// output a weighted note int
	for (var note_int in decimals) {
    	if (decimals.hasOwnProperty(note_int)) {
			var note_int_parsed = parseInt(note_int) + 1;
			out_sum += decimals[note_int];
			if (out_sum > random_float ) {
				outlet(0, note_int_parsed);
				break;
			}
    	}
	}
}


function getNoteByProb() {
	var sum = 0;
	for ( var i = 0; i < probs.length; i++ ) {
		sum+= probs[i];
	}

	var decimals = [];
	for ( var i = 0; i < probs.length; i++ ) {
		decimals.push(probs[i] / sum);
	}

	var random_float = Math.random();
	var out_sum = 0;

	// output a weighted note int
	for (var note_int in decimals) {
    	if (decimals.hasOwnProperty(note_int)) {
			var note_int_parsed = parseInt(note_int) + 1;
			out_sum += decimals[note_int];
			if (out_sum > random_float ) {
				return note_int_parsed;
			}
    	}
	}
}