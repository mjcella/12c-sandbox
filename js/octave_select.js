function anything() {
	var themessage = arrayfromargs(messagename, arguments);
	var a = themessage[0];
	var b = themessage[1];
	outlet(0, a);
	outlet(0, b);
	var response;
	if (a == -2 && b == -2) {
		response = [-24];
	}
	else if (a == -2 && b == -1) {
		response = [-24, -12];
	}
	else if (a == -2 && b == 0) {
		response = [-24, -12, 0];
	}
	else if (a == -2 && b == 1) {
		response = [-24, -12, 0, 12];
	}
	else if (a == -2 && b == 2) {
		response = [-24, -12, 0, 12, 24];
	}
	else if (a == -1 && b == -1) {
		response = [-12];
	}
	else if (a == -1 && b == 0) {
		response = [-12, 0];
	}
	else if (a == -1 && b == 1) {
		response = [-12, 0, 12];
	}
	else if (a == -1 && b == 2) {
		response = [-12, 0, 12, 24];
	}
	else if (a == 0 && b == 0) {
		response = [0];
	}
	else if (a == 0 && b == 1) {
		response = [0, 12];
	}
	else if (a == 0 && b == 2) {
		response = [0, 12, 24];
	}
	else if (a == 1 && b == 1) {
		response = [12];
	}
	else if (a == 1 && b == 2) {
		response = [12, 24];
	}
	else if (a == 2 && b == 2) {
		response = [24];
	}
	outlet(0, response);
}