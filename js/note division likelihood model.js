function anything()
{
	var random_number = arrayfromargs(messagename, arguments);
	if ( random_number < 563 ) {
		outlet(0, 1);
	}
	else if ( random_number < 813 ) {
		outlet(0, 2);
	}
	else if ( random_number < 938 ) {
		outlet(0, 3);
	}
	else {
		outlet(0, 4);
	}
}

	/* takes number 0-999 and returns 1-4 according to the following probability:
		1- 56.25%
		2 -25%
		3- 12.5%
		4- 6.25%
	*/