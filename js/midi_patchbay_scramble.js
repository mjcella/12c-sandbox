function msg_int(a) {
	midipatchbay(a);
}

function bang() {
	midipatchbay(4);
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
    return a;
}

function midipatchbay(cells_on_per_column) {
	  var on_cells = [];
    var max = 16;
    var min = 0;
    var output_str = '';

    // initialize all needed array keys inside on_cells to false
    for ( var i = 0; i < 16; i++ ) {
        on_cells[i] = [];
        for ( var j = 0; j < 16; j++ ) {
            on_cells[i][j] = 0;
        }
    }

    for ( var y = 0; y < 16; y++ ) {
	// y is the current row
        for ( var x = 0; x < cells_on_per_column; x++ ) {
            // a is the location of the next ON cell in this column
            var a = Math.floor(Math.random() * (max - min)) + min;
            if ( on_cells[a][y] == 0 ) {
                on_cells[y][a] = 1;
            }
        }
    }

    /* subroutine for removing invalid cell combinations:
      find each "1" location in the current row, find each of those rows and loop through each of them
      if any of those rows match any of the "1" locations in the current row,
      unset that location in the current row.
    */
    for ( var y = 0; y < on_cells.length; y++ ) {
      var current_row = on_cells[y];
      for ( var x = 0; x < on_cells.length; x++ ) {
        var current_cell = current_row[x];
        if ( current_cell == 1 ) {
            // find each of those rows
            var search_row = on_cells[x];
            // loop through search_row
            for ( var i = 0; i < search_row.length; i++ ) {
              if ( search_row[i] == 1 && current_row[i] == 1 ) {
                current_row[i] = 0;
              }
			// also loop through any rows of any cells found on here.
			if ( search_row[i] == 1 ) {
				search_row = on_cells[i];
				for ( var r = 0; r < search_row.length; r++ ) {
              		if ( search_row[r] == 1 && current_row[r] == 1 ) {
                	current_row[r] = 0;
              		}
				}
            }
        }
      }
    }
    // scambles the array just to make any weirdness of the above algorithm occur
    // not in the same location every time.
    on_cells = shuffle(on_cells);

    for ( var i = 0; i < on_cells.length; i++ ) {
      for ( var a = 0; a < on_cells.length; a++ ) {
        output_str += on_cells[i][a] += ' ';
      }
    }
    output_str = output_str.substring(0, output_str.length - 1);
    // 	 return output_str;

	  outlet(0,output_str);
}
}