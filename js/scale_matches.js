function anything(input_str) {
	// process the arguments so that the list before | becomes old_scale,
	// the next list between both || becomes new_scale, and the last int
	// becomes delta_threshold
	var themessage = arrayfromargs(messagename, arguments);
	var input_str = themessage.join(' ');
	var split_str = input_str.split('|');
	var old_scale = split_str[0];
	old_scale = old_scale.split('scaleMatches ');
	old_scale = old_scale[1];
	var new_scale = split_str[1];
	var delta_threshold = parseInt(split_str[2]);
	new_scale = new_scale.split(' ');
	old_scale = old_scale.split(' ');
	
	var delta_threshold_counter = 0;

	for ( var i = 0; i < new_scale.length ;  i++ ) {
		var new_scale_note = new_scale[i];
		var new_scale_note_is_in_old_scale = false;
		for ( var j = 0 ; j < old_scale.length ; j++ ) {
			var old_scale_note = old_scale[j];
			if ( old_scale_note != new_scale_note ) {
				continue;
			}
			else {
				new_scale_note_is_in_old_scale = true;
				break;
			}
		}
		if ( new_scale_note_is_in_old_scale == false ) {
			delta_threshold_counter++;
		}
		if (delta_threshold_counter > delta_threshold) {
			outlet(0, 0);
			return;
		}
	}
	outlet(0, new_scale);
}