this works to convert .flv to .mp4:

ffmpeg -i {input}.flv -c:v libx264 -crf 23 -c:a aac -q:a 100 {output}.mp4

it also works for .ogv:

ffmpeg -i {input}.ogv -c:v libx264 -crf 23 -c:a aac -q:a 100 {output}.mp4


to remove black strips, i did a quicktime screen recording of of only the video content, then ran the output (which was too high-res for use with the video_to_audio patcher i built) through this, which reduced the resolution to 240px wide

https://www.onlinevideoconverter.com/cloud-converter

