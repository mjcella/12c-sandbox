function msg_int(v)
{
	// initializes all possibilities to 1 in the prob object
	// a flat, fully random distribution
    for (i = 0; i < v; i++ ) {
        for (a = 0; a < v; a++) {
			outlet(0, (i+1), (a+1), 1);
        }
    }
}