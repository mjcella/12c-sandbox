{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 34.0, 79.0, 893.0, 751.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 12.0, 64.0, 58.0, 22.0 ],
					"style" : "",
					"text" : "autopattr",
					"varname" : "u240019985"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 92.0, 64.0, 218.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 45, 749, 737 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"style" : "",
					"text" : "pattrstorage @savemode 0 @greedy 1",
					"varname" : "u680019113"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "synth" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "_preset_select.maxpat",
					"numinlets" : 1,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "bang" ],
					"patching_rect" : [ 92.0, 18.0, 660.0, 30.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, -1.0, 662.0, 32.0 ],
					"varname" : "_preset_select",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "DPO", 63, 64 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "vco_ctrl.maxpat",
					"numinlets" : 0,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 16.0, 97.0, 667.0, 232.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 29.0, 648.0, 226.0 ],
					"varname" : "vco_ctrl_dpo",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-3",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 16.0, 18.0, 35.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 15.0, 660.0, 240.0 ],
					"proportion" : 0.39,
					"rounded" : 0,
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 101.5, 97.0, 82.0, 97.0, 82.0, 7.0, 101.5, 7.0 ],
					"source" : [ "obj-59", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-2::obj-8::obj-187" : [ "rslider[7]", "rslider", 0 ],
			"obj-2::obj-241" : [ "Retrigger", "Retrigger", 0 ],
			"obj-4::obj-61" : [ "pattr8ExistsIndicator", "pattr8ExistsIndicator", 0 ],
			"obj-2::obj-28::obj-30" : [ "live.text[21]", "live.text[1]", 0 ],
			"obj-4::obj-95" : [ "transitionMatrix[1]", "transitionMatrix", 0 ],
			"obj-2::obj-65::obj-8::obj-10" : [ "Manual Metro Rate[6]", "Rate", 0 ],
			"obj-2::obj-8::obj-8::obj-12" : [ "Metro On/Off[17]", "Metro On/Off", 0 ],
			"obj-2::obj-65::obj-8::obj-12" : [ "Metro On/Off[6]", "Metro On/Off", 0 ],
			"obj-2::obj-65::obj-12" : [ "ratecontrol[11]", "ratecontrol", 0 ],
			"obj-2::obj-49::obj-8::obj-56" : [ "Dropdown Control[6]", "Select", 0 ],
			"obj-2::obj-65::obj-8::obj-18" : [ "umenu[7]", "umenu", 0 ],
			"obj-4::obj-50" : [ "pattr1ExistsIndicator", "pattr1ExistsIndicator", 0 ],
			"obj-2::obj-8::obj-8::obj-13" : [ "Quantize Metro Rate[18]", "Quantize Metro Rate", 0 ],
			"obj-2::obj-49::obj-8::obj-12" : [ "Metro On/Off[5]", "Metro On/Off", 0 ],
			"obj-4::obj-63" : [ "pattr6ExistsIndicator", "pattr6ExistsIndicator", 0 ],
			"obj-2::obj-8::obj-12" : [ "ratecontrol[8]", "ratecontrol", 0 ],
			"obj-4::obj-129" : [ "pattrInterpolation", "pattrInterpolation", 0 ],
			"obj-2::obj-49::obj-8::obj-10" : [ "Manual Metro Rate[12]", "Rate", 0 ],
			"obj-2::obj-88" : [ "musical_scale[2]", "musical_scale", 0 ],
			"obj-2::obj-65::obj-30" : [ "live.text[14]", "live.text[1]", 0 ],
			"obj-2::obj-8::obj-8::obj-18" : [ "umenu[4]", "umenu", 0 ],
			"obj-2::obj-65::obj-8::obj-34" : [ "live.text[39]", "live.text", 0 ],
			"obj-2::obj-8::obj-142" : [ "Channel[4]", "Channel", 0 ],
			"obj-2::obj-11" : [ "Note Matrix[1]", "Note Matrix", 0 ],
			"obj-2::obj-45" : [ "stick", "stick", 0 ],
			"obj-2::obj-133" : [ "automatic envelope length[2]", "automatic envelope length", 0 ],
			"obj-4::obj-52" : [ "pattr4ExistsIndicator", "pattr4ExistsIndicator", 0 ],
			"obj-2::obj-65::obj-8::obj-13" : [ "Quantize Metro Rate[5]", "Quantize Metro Rate", 0 ],
			"obj-2::obj-65::obj-142" : [ "Channel[7]", "Channel", 0 ],
			"obj-2::obj-4" : [ "Randomize melodic sequence[2]", "Randomize melodic sequence", 0 ],
			"obj-4::obj-70" : [ "pattr9ExistsIndicator", "pattr9ExistsIndicator", 0 ],
			"obj-2::obj-28::obj-8::obj-13" : [ "Quantize Metro Rate[3]", "Quantize Metro Rate", 0 ],
			"obj-2::obj-65::obj-8::obj-31" : [ "live.text[40]", "live.text", 0 ],
			"obj-2::obj-8::obj-9" : [ "live.text[36]", "live.text", 0 ],
			"obj-2::obj-28::obj-8::obj-18" : [ "umenu[5]", "umenu", 0 ],
			"obj-2::obj-49::obj-30" : [ "live.text[24]", "live.text[1]", 0 ],
			"obj-2::obj-161" : [ "Regen %[2]", "Regen %", 0 ],
			"obj-2::obj-8::obj-8::obj-34" : [ "live.text[17]", "live.text", 0 ],
			"obj-2::obj-49::obj-142" : [ "Channel[6]", "Channel", 0 ],
			"obj-2::obj-49::obj-8::obj-18" : [ "umenu[6]", "umenu", 0 ],
			"obj-2::obj-12" : [ "note off UI", "note off UI", 0 ],
			"obj-2::obj-24::obj-97" : [ "Quantized Rate[1]", "Quantized Rate", 0 ],
			"obj-2::obj-65::obj-8::obj-56" : [ "Dropdown Control[7]", "Select", 0 ],
			"obj-4::obj-51" : [ "pattr2ExistsIndicator", "pattr2ExistsIndicator", 0 ],
			"obj-2::obj-28::obj-9" : [ "live.text[20]", "live.text", 0 ],
			"obj-2::obj-49::obj-12" : [ "ratecontrol[10]", "ratecontrol", 0 ],
			"obj-4::obj-62" : [ "pattr7ExistsIndicator", "pattr7ExistsIndicator", 0 ],
			"obj-2::obj-49::obj-8::obj-34" : [ "live.text[22]", "live.text", 0 ],
			"obj-2::obj-49::obj-187" : [ "rslider[9]", "rslider", 0 ],
			"obj-2::obj-124" : [ "bar length[2]", "bar length", 0 ],
			"obj-4::obj-18" : [ "transitionMatrix", "transitionMatrix", 0 ],
			"obj-2::obj-8::obj-8::obj-10" : [ "Manual Metro Rate[4]", "Rate", 0 ],
			"obj-2::obj-65::obj-9" : [ "live.text[13]", "live.text", 0 ],
			"obj-2::obj-2" : [ "function[1]", "function", 0 ],
			"obj-2::obj-28::obj-8::obj-12" : [ "Metro On/Off[4]", "Metro On/Off", 0 ],
			"obj-2::obj-8::obj-8::obj-56" : [ "Dropdown Control[4]", "Select", 0 ],
			"obj-2::obj-134" : [ "manual envelope length[2]", "manual envelope length", 0 ],
			"obj-4::obj-69" : [ "pattr10ExistsIndicator", "pattr10ExistsIndicator", 0 ],
			"obj-2::obj-28::obj-8::obj-34" : [ "live.text[19]", "live.text", 0 ],
			"obj-2::obj-65::obj-187" : [ "rslider[10]", "rslider", 0 ],
			"obj-2::obj-21" : [ "Auto Regen[2]", "Auto Regen", 0 ],
			"obj-4::obj-64" : [ "pattr5ExistsIndicator", "pattr5ExistsIndicator", 0 ],
			"obj-2::obj-28::obj-8::obj-10" : [ "Manual Metro Rate[5]", "Rate", 0 ],
			"obj-2::obj-66" : [ "delta next note threshold[1]", "delta next note threshold", 0 ],
			"obj-2::obj-28::obj-142" : [ "Channel[5]", "Channel", 0 ],
			"obj-2::obj-86" : [ "MIDI On/Off[1]", "MIDI On/Off", 0 ],
			"obj-4::obj-14" : [ "pattrDuration", "pattrDuration", 0 ],
			"obj-2::obj-49::obj-8::obj-31" : [ "live.text[23]", "live.text", 0 ],
			"obj-2::obj-49::obj-8::obj-13" : [ "Quantize Metro Rate[4]", "Quantize Metro Rate", 0 ],
			"obj-2::obj-8::obj-8::obj-31" : [ "live.text[35]", "live.text", 0 ],
			"obj-2::obj-28::obj-8::obj-56" : [ "Dropdown Control[5]", "Select", 0 ],
			"obj-2::obj-28::obj-12" : [ "ratecontrol[9]", "ratecontrol", 0 ],
			"obj-2::obj-28::obj-8::obj-31" : [ "live.text[18]", "live.text", 0 ],
			"obj-2::obj-28::obj-187" : [ "rslider[8]", "rslider", 0 ],
			"obj-2::obj-49::obj-9" : [ "live.text[38]", "live.text", 0 ],
			"obj-2::obj-24::obj-9" : [ "Manual Rate[1]", "Manual Rate", 0 ],
			"obj-2::obj-8::obj-30" : [ "live.text[37]", "live.text[1]", 0 ],
			"obj-4::obj-54" : [ "pattr3ExistsIndicator", "pattr3ExistsIndicator", 0 ],
			"obj-2::obj-131" : [ "note offset[2]", "note offset", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "vco_ctrl.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/EG",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "metro_time_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/2017/utils",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select_ui.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "delta_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "12c_markov.maxpat",
				"bootpath" : "~/12c/12c_sandbox/probability",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "markov_init.js",
				"bootpath" : "~/12c/12c_sandbox/probability/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bpatcher_name.js",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "melodic_row.js",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "_preset_select.maxpat",
				"bootpath" : "~/12c/12c_sandbox/audio",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "filename concat2.js",
				"bootpath" : "~/12c/12c_sandbox/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "savemode.maxpat",
				"bootpath" : "~/12c/12c_sandbox/presets",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
